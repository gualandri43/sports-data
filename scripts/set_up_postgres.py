'''Set up a fresh postgres database with all default data.'''

SPORTS_DATA_POSTGRES_HOST = None
SPORTS_DATA_POSTGRES_PORT = None
SPORTS_DATA_POSTGRES_USERNAME = None
SPORTS_DATA_POSTGRES_PASSWORD = None
SPORTS_DATA_POSTGRES_DBNAME = None

###########################################

import sqlalchemy
from sports_data.data_model.schema import create_tables, add_available_sites

connectionString = 'postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(
    SPORTS_DATA_POSTGRES_USERNAME,
    SPORTS_DATA_POSTGRES_PASSWORD,
    SPORTS_DATA_POSTGRES_HOST,
    SPORTS_DATA_POSTGRES_PORT,
    SPORTS_DATA_POSTGRES_DBNAME
)

engine = sqlalchemy.create_engine(connectionString)
create_tables(engine)
add_available_sites(engine)
