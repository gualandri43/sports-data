'''
Module for downloading data from fantasypros.com
'''

import os
import re
import datetime as dt
from typing import Iterable, List, Tuple, Union
import requests
import bs4
from bs4 import BeautifulSoup

from sports_data.data_model.schema import NflProjection, NflOffense, NflDefense, NflKicking, MlbProjection, MlbBatting, MlbPitching
import sports_data.utils


class FantasyprosScraper:
    '''
        Scraper for data from fantasypros.com

        SPORTS_DATA_FANTASYPROS_USERNAME
        SPORTS_DATA_FANTASYPROS_PASSWORD
    '''

    loginUrl = 'https://secure.fantasypros.com/accounts/login/'

    projectionSource = 'fantasypros'

    projectionScopes = [
        'season',
        'restOfSeason',
        'weekly',
        'daily'
    ]

    websiteHeaderMap = {
        'nfl': {
            'passing.att': 'pass_attempt',
            'passing.cmp': 'pass_completion',
            'passing.yds': 'pass_yard',
            'passing.tds': 'pass_touchdown',
            'passing.ints': 'pass_interception',
            'rushing.att': 'rush_attempt',
            'rushing.yds': 'rush_yard',
            'rushing.tds': 'rush_touchdown',
            'receiving.rec': 'rec_reception',
            'receiving.yds': 'rec_yard',
            'receiving.tds': 'rec_touchdown',
            'misc.fl': 'fumble_lost',
            'dst.sack': 'sack',
            'dst.int': 'interception',
            'dst.fr': 'fumble_recovery',
            'dst.ff': 'fumble_forced',
            'dst.td': 'touchdown',
            'dst.safety': 'safety',
            'dst.pa': 'points_allowed',
            'dst.yds_agn': 'yards_against',
            'k.fg': 'field_goal',
            'k.fga': 'field_goal_attempt',
            'k.xpt': 'extra_point'
        },
        'mlb': {
            'ab': 'at_bat',
            'r': 'run',
            'hr': 'homerun',
            'so': 'strikeout',
            'rbi': 'rbi',
            'sb': 'stolen_base',
            'avg': 'average',
            'obp': 'obp',
            'h': 'hit',
            '2b': 'double',
            '3b': 'triple',
            'bb': 'walk',
            'slg': 'slg',
            'ops': 'ops',
            'ip': 'ip',
            'k': 'strikeout',
            'sv': 'sv',
            'era': 'era',
            'g': 'game',
            'gs': 'game_started',
            'w': 'win',
            'l': 'loss',
            'cg': 'complete_game'
        }
    }

    def __init__(self, scope, username=None, password=None):

        if scope not in self.projectionScopes:
            raise ValueError('Invalid fantasypros.com scope.')

        self.csrfToken = None

        self.scope = scope

        if username:
            self.username = username
        else:
            self.username = os.environ.get('SPORTS_DATA_FANTASYPROS_USERNAME')

        if password:
            self.password = password
        else:
            self.password = os.environ.get('SPORTS_DATA_FANTASYPROS_PASSWORD')

    def _parse_hdrs(self, tableElement: bs4.element.Tag) -> List[str]:
        '''Parses the header rows of the table and returns a formatted list with
            columns in order from left to right. Multiple rows are collapsed into
            one row with names separated by a dot.

            Parameters
            ----------
            tableElement
                The `<table>` element loaded in a BeautifulSoup element.

        '''

        hdrs = []
        tblHeader = tableElement.find('thead')

        # iterate through all possible header rows.
        for _hdrRow in tblHeader.find_all('tr'):

            currentRowHdrs = []

            _rowCols = _hdrRow.find_all('th')

            if not _rowCols:
                _rowCols = _hdrRow.find_all('td')

            for _hdrCol in _rowCols:

                if 'colspan' in _hdrCol.attrs:
                    currentRowHdrs.extend([_hdrCol.get_text().strip()] * int(_hdrCol['colspan']))
                else:
                    currentRowHdrs.append(_hdrCol.get_text().strip())

            hdrs.append(currentRowHdrs)

        # remove empty rows
        for _i in reversed(range(len(hdrs))):
            if len(hdrs[_i]) == 0:
                hdrs.pop(_i)

        # combine multiple header rows if present
        if len(hdrs) == 0:
            hdrs = []
        elif len(hdrs) == 1:
            hdrs = hdrs[0]
        else:
            for _additionalRow in hdrs[1:]:
                hdrs[0] = ['{0}.{1}'.format(_base,_add) for _base,_add in zip(hdrs[0],_additionalRow)]

            hdrs = hdrs[0]

            # clean up leading or trailing periods.
            for _i in range(len(hdrs)):
                if hdrs[_i].startswith('.'):
                    hdrs[_i] = hdrs[_i][1:]

                if hdrs[_i].endswith('.'):
                    hdrs[_i] = hdrs[_i][:-1]

        # clean up headers and return
        hdrs = [x.lower() for x in hdrs]
        hdrs = [x.replace(' ', '_') for x in hdrs]

        return hdrs

    def _clean_stat(self, stat: Union[str,float,None]) -> Union[float,None]:
        '''
        clean up stats in string format to numerical.
        '''

        if stat is None:
            return None

        if isinstance(stat, str):
            cleaned = stat.replace(',','').replace('%','').strip()

            if cleaned == '':
                return 0.0
            else:
                return float(cleaned)

        if isinstance(stat, int):
            return float(stat)

        return float(stat)

    def set_csrf_token(self, loginRespHtml: str) -> None:
        '''
        parse out the CSRF token from the login page response and set the internal
        attribute to the extracted value.
        '''

        pg = BeautifulSoup(loginRespHtml, 'lxml')
        self.csrfToken = pg.find('input', attrs={'name':'csrfmiddlewaretoken'}).get('value')

    def build_initial_login_request(self) -> requests.Request:
        '''Build request to GET the login page to retrieve the CSRF token.

            Returns
            -------
            requests.Request
                A requests.Request object with the required URLS and parameters.
        '''

        return requests.Request('GET', self.loginUrl)

    def build_secondary_login_request(self) -> requests.Request:
        '''Build a POST requests to login to website.

            Must perform initial login and set the internal csrf token before
            sending this request. Do so in the same session that the
            build_initial_login_request() Request was sent in.

            Returns
            -------
            requests.Request
                A requests.Request object with the required URLS and parameters.
        '''

        if self.csrfToken is None:
            raise ValueError('csrf token not set.')

        return requests.Request('POST',
            self.loginUrl,
            data= {
                'username': self.username,
                'password': self.password,
                'csrfmiddlewaretoken': self.csrfToken
            }
        )

    def build_mlb_projections_request(self, position: str, minMax: bool = False) -> requests.Request:
        '''Build a request for MLB projections.

            Parameters
            ----------
            position : {hitters,pitchers}
                The position to request projections for.

            minMax : optional
                Flag to include the minimum and maximum projections. False only will retrieve
                consensus projections. Defaults to False. Must use a logged in session if using
                minMax=True.

            Returns
            -------
            requests.Request
                A request object with the required URL and parameters defined.
        '''

        #example URL: https://www.fantasypros.com/mlb/projections/hitters.php?points=Y&max-yes=true&min-yes=true

        if position not in ['hitters','pitchers']:
            raise ValueError('position not valid.')

        params = {
            'points': 'Y'
        }

        if self.scope == 'season':
            baseUrl = 'https://www.fantasypros.com/mlb/projections/'

        if self.scope == 'weekly':
            baseUrl = 'https://www.fantasypros.com/mlb/projections/weekly-'

        if self.scope == 'restOfSeason':
            baseUrl = 'https://www.fantasypros.com/mlb/projections/ros-'

        if self.scope == 'daily':
            baseUrl = 'https://www.fantasypros.com/mlb/projections/daily-'

        if minMax:
            params.update(
                {
                    'max-yes': 'true',
                    'min-yes': 'true'
                }
            )

        url = '{0}{1}.php'.format(baseUrl, position)
        return requests.Request('GET', url, params=params)

    def build_nfl_projections_requests(self, position: str, week: int = None, minMax: bool = False) -> requests.Request:
        '''Build a request for NFL projections.

            Parameters
            ----------
            position : {qb,rb,wr,te,k,dst}
                The position to request projections for.

            week : optional
                The integer week of the season to pull projections for (only valid for scope='weekly'). If left as None,
                the latest weekly projections are downloaded.

            minMax : optional
                Flag to include the minimum and maximum projections. False only will retrieve
                consensus projections. Defaults to False. Must use a logged in session if using
                minMax=True.

            Returns
            -------
            requests.Request
                A request object with the required URL and parameters defined.
        '''

        # example url
        # https://www.fantasypros.com/nfl/projections/rb.php?max-yes=true&min-yes=true&week=draft&scoring=PPR&week=draft

        if position not in ['qb','rb','wr','te','k','dst']:
            raise ValueError('position is invalid.')

        params = {
            'scoring': 'PPR'
        }

        if self.scope == 'season':
            params['week'] = 'draft'

        if self.scope == 'weekly' and week is not None:
            params['week'] = week

        if self.scope == 'restOfSeason':
            raise ValueError('Scope not available for scraping.')

        if self.scope == 'daily':
            raise ValueError('Scope not available for scraping.')

        if minMax:
            params.update(
                {
                    'max-yes': 'true',
                    'min-yes': 'true'
                }
            )

        url = 'https://www.fantasypros.com/nfl/projections/{0}.php'.format(position)
        return requests.Request('GET', url, params=params)

    def login(self) -> requests.Session:
        '''Log in to fantasypros.com using the defined username and password.

            Fantasypros.com uses a csrf token during login authentication. The token is stored in
            an <input> element with name=csrfmiddlewaretoken and value=token_value. To login,
            follow these steps.
            1. Start a session to store cookies, headers, etc.
            2. GET request the login page within the session.
            3. parse and extract the CSRF token.
            4. POST request using username, password, and token
            5. session now has proper authentication cookies.

            Returns
            -------
            requests.Session
                A Session object that has been logged in that can be used to send
                further requests.
        '''

        session = requests.Session()

        loginReq = self.build_initial_login_request()
        initialLoginPage = session.send(
            session.prepare_request(loginReq)
        )

        self.set_csrf_token(initialLoginPage.text)

        secondaryReq = self.build_secondary_login_request()
        resp = session.send(
            session.prepare_request(secondaryReq)
        )

        if resp.status_code != 200:
            raise Exception("Non-200 status code resulted from login.")

        return session

    def _parse_table(self, tableElement: bs4.element.Tag, hdrMap: dict = None, hdrPrefix: str = None) -> Iterable[Tuple[dict,dict,dict,dict]]:
        '''Helper function to parse a table. Also handles min/max row entries for projection tables.

            Parameters
            ----------
            tableElement
                The `<table>` element loaded in a BeautifulSoup element.

            hdrMap : Optional
                A dictionary with mappings for renaming table headers. Keys are existing header names,
                values are the names to replace.

            hdrPrefix : Optional
                String prefix to put in front of all headers.

            Yields
            ------
                A tuple with dictionaries containing row data for each parsed row.
                (consensusRowStatData, minRowStatData, maxRowStatData, rowCustomData)
        '''

        #table headers
        hdrs = self._parse_hdrs(tableElement)

        # table body
        tableBody = tableElement.find('tbody')
        for _playerRow in tableBody.find_all('tr'):

            rowCustomData = dict()
            consensusRowStatData = dict()
            minRowStatData = dict()
            maxRowStatData = dict()

            colCount = 0
            for _playerCol in _playerRow.find_all('td'):

                if colCount < len(hdrs):

                    if _playerCol.find('a', class_='player-name'):
                        # column is a player name

                        rowCustomData['player.name'] = _playerCol.find('a', class_='player-name').text.strip()
                        rowCustomData['player.link'] = _playerCol.find('a', class_='player-name')['href']

                        playerIdStr = _playerCol.parent['class'][0]
                        rowCustomData['player.id'] = int(playerIdStr.split('-')[-1])

                    else:
                        #data column
                        # columns are in same order as hdr row names

                        minStatElem = _playerCol.find('div', class_="min-cell")
                        maxStatElem = _playerCol.find('div', class_="max-cell")

                        if hdrPrefix:
                            colHdr = hdrPrefix + hdrs[colCount]
                        else:
                            colHdr = hdrs[colCount]

                        if hdrMap:
                            statName = hdrMap.get(colHdr, None)
                        else:
                            statName = colHdr

                        if statName:

                            consensusRowStatData[statName] = self._clean_stat(_playerCol.contents[0])

                            if minStatElem:
                                minRowStatData[statName] = self._clean_stat(minStatElem.text)

                            if maxStatElem:
                                maxRowStatData[statName] = self._clean_stat(maxStatElem.text)

                colCount += 1

            yield (consensusRowStatData, minRowStatData, maxRowStatData, rowCustomData)

    def _parse_nfl_projection_page_metadata(self, projPage: bs4.BeautifulSoup) -> NflProjection:
        '''Parse metadata from a projection page.

            Parameters
            ----------
            projPage
                The projection page loaded into a BeautifulSoup object.

            Returns
            -------
            A NflProjection instance with fantasypros metadata populated.
        '''

        scrapeTime = sports_data.utils.TimeRecorder()

        projection = NflProjection(
            source_site=self.projectionSource,
            scope=self.scope,
            scrape_time_local=scrapeTime.timeLocal,
            scrape_time_utc=scrapeTime.timeUtc,
            custom_data={}
        )

        #  parse out position
        #
        projection.custom_data['position'] = str(
                projPage.find(attrs={"data-mobile-label": "Positions"})
                .find('li', class_='active')
                .find('a').text
            ).lower()

        # parse out week
        #
        pageHeader = projPage.find('div', class_='primary-heading-subheading').find('h1')

        if self.scope == 'weekly':
            capt = re.search(r'Fantasy Football Projections.*Week (.*)', pageHeader.text)
            if capt.group(1) is not None:
                projection.projection_week = int(capt.group(1))

        elif self.scope == 'season':
            capt = re.search(r'Fantasy Football Projections.*\((.*)\)', pageHeader.text)
            if capt.group(1) is not None:
                projection.season_year = int(capt.group(1))

        elif self.scope == 'restOfSeason':
            raise ValueError('Scope not available for scraping.')

        elif self.scope == 'daily':
            raise ValueError('Scope not available for scraping.')

        else:
            raise ValueError('Scope not available for scraping.')


        # parse out date
        #   in format Oct 22, 2020
        #
        elem = projPage.find('time')
        projection.projection_date = dt.datetime.strptime(elem.text.strip(), '%b %d, %Y')

        # fill season_year if not set
        #   base this on league year ending in April
        if projection.season_year is None:
            if dt.date.today().month <= 4:
                projection.season_year = dt.date.today().year - 1
            else:
                projection.season_year = dt.date.today().year

        return projection

    def parse_nfl_projections(self, resp: requests.Response) -> Iterable[NflProjection]:
        '''Parse a NFL projections page.

            Parameters
            ----------
            resp
                The HTTP response from a projections page request. The request is
                built by build_nfl_projections_requests().

            Yields
            ------
            This method is a generator. Will yield NflProjection objects for each projection table row.
        '''

        #load page tree
        respPage = BeautifulSoup(resp.text, 'lxml')

        # projections from table
        #
        projTable = respPage.find('div', class_="main-content").find('table', id='data')

        # get projection object with populated metadata
        projection = self._parse_nfl_projection_page_metadata(respPage)
        playerPosition = projection.custom_data['position']

        if playerPosition == 'dst':
            prefix = 'dst.'

        elif playerPosition == 'k':
            prefix = 'k.'

        else:
            prefix = None

        # create projection objects
        for _consensusRowStatData,_minRowStatData,_maxRowStatData,_rowCustomData in self._parse_table(projTable, hdrMap=self.websiteHeaderMap['nfl'], hdrPrefix=prefix):

            # build projection object for each range
            if len(_consensusRowStatData) > 0:

                consensusProj = self._parse_nfl_projection_page_metadata(respPage)
                consensusProj.custom_data.update(_rowCustomData)
                consensusProj.custom_data['range'] = 'consensus'

                if playerPosition == 'dst':

                    consensusProj.defense_stats = NflDefense(**_consensusRowStatData)

                elif playerPosition == 'k':

                    consensusProj.kicking_stats = NflKicking(**_consensusRowStatData)

                else:

                    consensusProj.offense_stats = NflOffense(**_consensusRowStatData)

                yield consensusProj

            if len(_minRowStatData) > 0:

                minProj = self._parse_nfl_projection_page_metadata(respPage)
                minProj.custom_data.update(_rowCustomData)
                minProj.custom_data['range'] = 'min'

                if playerPosition == 'dst':

                    minProj.defense_stats = NflDefense(**_minRowStatData)

                elif playerPosition == 'k':

                    minProj.kicking_stats = NflKicking(**_minRowStatData)

                else:

                    minProj.offense_stats = NflOffense(**_minRowStatData)

                yield minProj

            if len(_maxRowStatData) > 0:

                maxProj = self._parse_nfl_projection_page_metadata(respPage)
                maxProj.custom_data.update(_rowCustomData)
                maxProj.custom_data['range'] = 'max'

                if playerPosition == 'dst':

                    maxProj.defense_stats = NflDefense(**_maxRowStatData)

                elif playerPosition == 'k':

                    maxProj.kicking_stats = NflKicking(**_maxRowStatData)

                else:

                    maxProj.offense_stats = NflOffense(**_maxRowStatData)

                yield maxProj

    def _parse_mlb_projection_page_metadata(self, projPage: bs4.BeautifulSoup) -> MlbProjection:
        '''Parse metadata from a projection page.

            Parameters
            ----------
            projPage
                The projection page loaded into a BeautifulSoup object.

            Returns
            -------
            A MlbProjection instance with fantasypros metadata populated.
        '''

        scrapeTime = sports_data.utils.TimeRecorder()

        projection = MlbProjection(
            source_site=self.projectionSource,
            scope=self.scope,
            scrape_time_local=scrapeTime.timeLocal,
            scrape_time_utc=scrapeTime.timeUtc,
            custom_data={}
        )

        #  parse out position
        #
        projection.custom_data['position'] = str(
            projPage.find(id="main-container")
            .find('div', class_="feature-bg")
            .find('li', class_='active')
            .find('a')
            .text
        ).lower()

        # parse out week or year
        #

        pageHeader = projPage.find('div', class_='primary-heading-subheading').find('h1')
        pageSubHeader = projPage.find('div', class_='primary-heading-subheading').find('h2')

        if self.scope == 'season':
            capt = re.search(r'(\d+) Fantasy Baseball Projections', pageHeader.text)
            if capt.group(1) is not None:
                projection.season_year = int(capt.group(1))

        if self.scope == 'weekly':
            capt = re.search(r'Week (.*) \(.*\) Projections', pageHeader.text)
            if capt.group(1) is not None:
                projection.projection_week = int(capt.group(1))

        if self.scope == 'daily':
            capt = re.search(r'Daily Projections: (.*)', pageHeader.text)
            if capt.group(1) is not None:
                cleanedCapture = str(capt.group(1)).replace('st', '').replace('th','').replace('rd', '')
                projection.projection_date = dt.datetime.strptime(cleanedCapture, '%a, %b %d').date()

        if self.scope in ['weekly', 'daily', 'restOfSeason']:
            capt = re.search(r'(\d+) Fantasy Baseball', pageSubHeader.text)
            if capt.group(1) is not None:
                projection.season_year = int(capt.group(1))


        #
        # guess date if needed
        #
        if projection.projection_date is None:
            projection.projection_date = dt.date.today()

        # fill season_year if not set
        #   base this on league year ending in April
        if projection.season_year is None:
            if dt.date.today().month <= 11:
                projection.season_year = dt.date.today().year - 1
            else:
                projection.season_year = dt.date.today().year

        return projection

    def parse_mlb_projections(self, resp: requests.Response) -> Iterable[MlbProjection]:
        '''Parse a MLB projections page.

            Parameters
            ----------
            resp
                The HTTP response from a projections page request. The request is
                built by build_mlb_projections_requests().

            Yields
            ------
            This method is a generator. Will yield MlbProjection objects for each projection table row.
        '''

        #load page tree
        respPage = BeautifulSoup(resp.text, 'lxml')

        # projections from table
        #
        projTable = respPage.find('div', class_="main-content").find('table',id="data")

        # get projection object with populated metadata
        projection = self._parse_mlb_projection_page_metadata(respPage)
        playerPosition = projection.custom_data['position']

        # create projection objects
        for _consensusRowStatData,_minRowStatData,_maxRowStatData,_rowCustomData in self._parse_table(projTable, hdrMap=self.websiteHeaderMap['mlb']):

            # build projection object for each range
            if len(_consensusRowStatData) > 0:

                consensusProj = self._parse_mlb_projection_page_metadata(respPage)
                consensusProj.custom_data.update(_rowCustomData)
                consensusProj.custom_data['range'] = 'consensus'

                if playerPosition == 'hitters':

                    consensusProj.batting_stats = MlbBatting(**_consensusRowStatData)

                    if consensusProj.batting_stats.single is None and all([consensusProj.batting_stats.hit,consensusProj.batting_stats.double,consensusProj.batting_stats.triple,consensusProj.batting_stats.homerun]):
                        consensusProj.batting_stats.single = (
                            consensusProj.batting_stats.hit
                            - consensusProj.batting_stats.double
                            - consensusProj.batting_stats.triple
                            - consensusProj.batting_stats.homerun
                        )

                elif playerPosition == 'pitchers':

                    consensusProj.pitching_stats = MlbPitching(**_consensusRowStatData)

                yield consensusProj

            if len(_minRowStatData) > 0:

                minProj = self._parse_mlb_projection_page_metadata(respPage)
                minProj.custom_data.update(_rowCustomData)
                minProj.custom_data['range'] = 'min'

                if playerPosition == 'hitters':

                    minProj.batting_stats = MlbBatting(**_minRowStatData)

                    if minProj.batting_stats.single is None and all([minProj.batting_stats.hit,minProj.batting_stats.double,minProj.batting_stats.triple,minProj.batting_stats.homerun]):
                        minProj.batting_stats.single = (
                            minProj.batting_stats.hit
                            - minProj.batting_stats.double
                            - minProj.batting_stats.triple
                            - minProj.batting_stats.homerun
                        )

                elif playerPosition == 'pitchers':

                    minProj.pitching_stats = MlbPitching(**_minRowStatData)

                yield minProj

            if len(_maxRowStatData) > 0:

                maxProj = self._parse_mlb_projection_page_metadata(respPage)
                maxProj.custom_data.update(_rowCustomData)
                maxProj.custom_data['range'] = 'max'

                if playerPosition == 'hitters':

                    maxProj.batting_stats = MlbBatting(**_maxRowStatData)

                    if maxProj.batting_stats.single is None and all([maxProj.batting_stats.hit,maxProj.batting_stats.double,maxProj.batting_stats.triple,maxProj.batting_stats.homerun]):
                        maxProj.batting_stats.single = (
                            maxProj.batting_stats.hit
                            - maxProj.batting_stats.double
                            - maxProj.batting_stats.triple
                            - maxProj.batting_stats.homerun
                        )

                elif playerPosition == 'pitchers':

                    maxProj.pitching_stats = MlbPitching(**_maxRowStatData)

                yield maxProj

