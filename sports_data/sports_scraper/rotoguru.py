'''
Scraper for rotoguru.com
'''

from typing import Iterable
import re
import datetime
import requests
import bs4

from sports_data.data_model.schema import NflPlayerDfs, MlbPlayerDfs


class RotoGuruScraper:
    '''Scraper for RotoGuru.com data'''

    # maps rotoguru team codes to baseball reference team codes
    mlbTeamNameMap = {
        'Kan': 'KCR',
        'Oak': 'OAK',
        'Ari': 'ARI',
        'Cin': 'CIN',
        'NYY': 'NYY',
        'Sea': 'SEA',
        'StL': 'STL',
        'Los': 'LAD',
        'Atl': 'ATL',
        'Cle': 'CLE',
        'NYM': 'NYM',
        'Sdg': 'SDP',
        'Bal': 'BAL',
        'Fla': 'MIA',
        'Sfo': 'SFG',
        'Was': 'WSN',
        'Ana': 'LAA',
        'ChW': 'CHW',
        'Det': 'DET',
        'Tam': 'TBR',
        'ChC': 'CHC',
        'Tor': 'TOR',
        'Min': 'MIN',
        'Mil': 'MIL',
        'Hou': 'HOU',
        'Pit': 'PIT',
        'Phi': 'PHI',
        'Bos': 'BOS',
        'Tex': 'TEX',
        'Col': 'COL'
    }

    # maps rotoguru team codes to pro-football-reference team codes
    nflTeamNameMap = {
        'bal': 'RAV',
        'tam': 'TAM',
        'ind': 'CLT',
        'was': 'WAS',
        'nyj': 'NYJ',
        'sea': 'SEA',
        'lar': 'RAM',
        'stl': 'RAM',
        'cle': 'CLE',
        'den': 'DEN',
        'ten': 'OTI',
        'buf': 'BUF',
        'jac': 'JAX',
        'lvr': 'RAI',
        'oak': 'RAI',
        'sfo': 'SFO',
        'ari': 'CRD',
        'min': 'MIN',
        'dal': 'DAL',
        'nor': 'NOR',
        'gnb': 'GNB',
        'phi': 'PHI',
        'mia': 'MIA',
        'car': 'CAR',
        'atl': 'ATL',
        'det': 'DET',
        'lac': 'SDG',
        'sdg': 'SDG',
        'pit': 'PIT',
        'nwe': 'NWE',
        'hou': 'HTX',
        'cin': 'CIN',
        'chi': 'CHI',
        'kan': 'KAN',
        'nyg': 'NYG'
    }

    def build_dfs_salary_request_nfl(self, dfsSite: str, season: int, week: int) -> requests.Request:
        '''Build the request for DFS salary.

            This page will have the table formatted as a semi-colon delimited table.

            Parameters
            ----------
            dfsSite : {draftkings, fanduel, yahoo}
                The DFS site to get salaries for.

            season
                The NFL season year.

            week
                The week number.

            Returns
            -------
            requests.Request
                A HTTP request that will retrieve the desired webpage.
        '''

        if dfsSite == 'draftkings':
            siteCode = 'dk'

        elif dfsSite == 'fanduel':
            siteCode = 'fd'

        elif dfsSite == 'yahoo':
            siteCode = 'yh'

        else:
            raise ValueError('Invalid dfsSite.')


        return requests.Request(
            method='GET',
            url='http://rotoguru1.com/cgi-bin/fyday.pl',
            params={
                'week': week,
                'season': season,
                'game': siteCode,
                'scsv': 1
            }
        )

    def build_player_lookup_page_request_nfl(self, season: int) -> requests.Request:

        if season < datetime.datetime.now().year:
            season = str(season)[-2:]
        else:
            season = ''

        # http://rotoguru1.com/cgi-bin/playrf19.cgi?0
        # http://rotoguru1.com/cgi-bin/playrf.cgi?0

        return requests.Request(
            method='GET',
            url=f'http://rotoguru1.com/cgi-bin/playrf{season}.cgi?0'
        )

    def build_dfs_player_salary_history_nfl(self, season: int, playerId: str) -> requests.Request:
        '''Get the salary history page for all sites for a season for a given player.'''

        if season < datetime.datetime.now().year:
            season = str(season)[-2:]
        else:
            season = ''

        playerId = str(playerId)

        return requests.Request(
            method='GET',
            url=f'http://rotoguru1.com/cgi-bin/playrf{season}.cgi?{playerId}'
        )

    def build_dfs_salary_request_mlb(self, dfsSite: str, season: int, month: int, day: int) -> requests.Request:
        '''Build the request for DFS salary.

            The semi-colon delimited table is behind a subscription paywall, so get the html table.

            Parameters
            ----------
            dfsSite : {draftkings, fanduel, yahoo}
                The DFS site to get salaries for.

            season
                The year of the date to get data for.

            month
                The month number of the day to get data for.

            day
                The day of the month to get data for.

            Returns
            -------
            requests.Request
                A HTTP request that will retrieve the desired webpage.
        '''

        if dfsSite == 'draftkings':
            siteCode = 'dk'

        elif dfsSite == 'fanduel':
            siteCode = 'fd'

        elif dfsSite == 'yahoo':
            siteCode = 'yh'

        else:
            raise ValueError('Invalid dfsSite.')

        #pad day if needed
        if day < 10:
            day = f'0{day}'

        return requests.Request(
            method='GET',
            url='http://rotoguru1.com/cgi-bin/byday.pl',
            params={
                'date': f'{month}{day}',
                'year': season,
                'game': siteCode
            }
        )

    def build_dfs_player_salary_history_mlb(self, season: int, playerId: str) -> requests.Request:
        '''Get the salary history page for all sites for a season for a given player.'''

        if season < 2021:
            season = str(season)[-2:]
        else:
            season = ''

        playerId = str(playerId)

        # an `x` at the end of the playerId will get full season data
        if playerId[-1] != 'x':
            playerId += 'x'

        return requests.Request(
            method='GET',
            url=f'http://rotoguru1.com/cgi-bin/player{season}.cgi?{playerId}'
        )

    def build_player_lookup_page_request_mlb(self, season: int) -> requests.Request:

        if season < 2021:
            season = str(season)[-2:]
        else:
            season = ''

        return requests.Request(
            method='GET',
            url=f'http://rotoguru1.com/cgi-bin/player{season}.cgi?0'
        )

    def parse_dfs_salary_response_nfl(self, pg: bs4.BeautifulSoup) -> Iterable[NflPlayerDfs]:
        '''Parse the response of the build_dfs_salary_request_nfl() request.

            Parameters
            ----------
            pg
                The response page.

            Yields
            ------
            NflPlayerDfs
                An object for every player in the response.
        '''

        # the semi-colon delimited table is in the <pre> tag.
        tbl = pg.find('pre')
        tblText = tbl.get_text()

        lines = tblText.splitlines()
        headers = [x.strip() for x in lines[0].split(';')]

        if 'dk' in headers[-1].lower():
            dfsSite = 'draftkings'

        elif 'fd' in headers[-1].lower():
            dfsSite = 'fanduel'

        elif 'yh' in headers[-1].lower():
            dfsSite = 'yahoo'

        else:
            dfsSite = None


        for _line in lines[1:]:
            # header row: Week;Year;GID;Name;Pos;Team;h/a;Oppt;FD points;FD salary

            data = _line.split(';')

            if data[-1].strip().lower() == 'n/a':
                salary = None
            else:
                salary = data[-1].strip()

            yield NflPlayerDfs(
                custom_data = {
                    'rotoguru_id': data[2],
                    'player_name': data[3]
                },
                source_site = 'rotoguru',
                dfs_site = dfsSite,
                season_year = data[1],
                week = data[0],
                salary = salary
            )

    def parse_player_ids_from_lookup_page(self, pg: bs4.BeautifulSoup, sport: str) -> Iterable[dict]:
        '''Parse all available player IDs from the build_player_lookup_page_request_mlb() request.

            Returns set of dictionaries with keys: playerId,lastName,team
        '''

        if sport == 'mlb':
            teamNameMap = self.mlbTeamNameMap

        elif sport == 'nfl':
            teamNameMap = self.nflTeamNameMap

        else:
            raise ValueError('Must be a valid sport.')

        for _selectorBox in pg.find_all('select', attrs={'name': 'guru_id'}):
            for _player in _selectorBox.find_all('option'):

                playerId = _player.get('value', None)
                if playerId and playerId != '0000':

                    # the text is in format: <lastname>, <firstname> (<team)
                    nameParse = re.search(r'([a-zA-Z\.\-\s\']+), ([a-zA-Z\.\-\s\']+) \(([a-zA-Z]+)\)', _player.get_text())

                    if nameParse:
                        # player
                        lastName = nameParse.group(1).strip()
                        firstName = nameParse.group(2).strip()
                        teamName = teamNameMap.get(nameParse.group(3).strip(), None)

                    else:
                        # defense/special teams just has one name and abbreviation
                        nameParse = re.search(r'([a-zA-Z\.\-\s\']+)\s+\(([a-zA-Z]+)\)', _player.get_text())
                        lastName = nameParse.group(1).strip()
                        firstName = None
                        teamName = teamNameMap.get(nameParse.group(2).strip(), None)

                    yield {
                        'playerId': playerId,
                        'lastName': lastName,
                        'firstName': firstName,
                        'team': teamName
                    }

    def parse_dfs_player_salary_history_nfl(self, pg: bs4.BeautifulSoup) -> Iterable[NflPlayerDfs]:

        # can get year from the title text string like 2019 Individual Baseball Player Stats
        yearPart = re.search(r'(\d+) Individual Football Player Stats', pg.get_text())
        year = int(yearPart.group(1))

        # find table header row
        #   <table>
        #       <tbody>
        #           <tr> -- first header
        #               <th>
        #           <tr> -- spanning headers -- Schedule	FanDuel	Yahoo DFS	DraftDay	DraftKings
        #           <tr> -- individual column headers Date	Oppt.	Position	Opp.SP	FDP	FD Price	YHP	YH Price	DDP	DD Price	DKP	DK Price
        #
        scheduleHeading = pg.find('font', string='Schedule').parent.parent

        # if heading not found, no data available
        if not scheduleHeading:
            raise ValueError('No data available on player page.')

        firstRowHeaders = []
        for _hdr in scheduleHeading.find_all('th'):
            firstRowHeaders.extend([_hdr.get_text().strip().lower()] * int(_hdr.get('colspan')))

        headerRow = scheduleHeading.findNext('tr')
        secondRowHeaders = [x.get_text().strip().lower() for x in headerRow.find_all('th')]

        headers = [f'{x}.{y}' for x,y in zip(firstRowHeaders,secondRowHeaders)]

        # find player ID in a link on the page
        priorSeasonLinkElem = pg.find('a', string=re.compile(r'View \d+ player history page'))
        priorSeasonLink = priorSeasonLinkElem.get('href')
        rotoguruId = priorSeasonLink.split('?')[-1]

        # get column index for useful columns
        weekCol = headers.index('schedule.week')
        fanduelCol = headers.index('fanduel.price')
        yahooCol = headers.index('yahoo dfs.price')
        draftkingsCol = headers.index('draftkings.price')

        # find results (after the header row)
        for _game in headerRow.findNextSiblings('tr'):
            cols = [x.get_text() for x in _game.find_all('td')]

            weekNumber = int(cols[weekCol])
            dkSalary = cols[draftkingsCol].replace('$','').replace(',','').strip()
            fanduelSalary = cols[fanduelCol].replace('$','').replace(',','').strip()
            yahooSalary = cols[yahooCol].replace('$','').replace(',','').strip()

            customData = {
                'rotoguruId': rotoguruId
            }

            if dkSalary != '':
                yield NflPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'draftkings',
                    season_year = year,
                    week=weekNumber,
                    player_id = None,
                    salary = dkSalary
                )

            if fanduelSalary != '':
                yield NflPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'fanduel',
                    season_year = year,
                    week=weekNumber,
                    player_id = None,
                    salary = fanduelSalary
                )

            if yahooSalary != '':
                yield NflPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'yahoo',
                    season_year = year,
                    week=weekNumber,
                    player_id = None,
                    salary = yahooSalary
                )

    def parse_dfs_player_salary_history_mlb(self, pg: bs4.BeautifulSoup) -> Iterable[MlbPlayerDfs]:

        # can get year from the title text string like 2019 Individual Baseball Player Stats
        yearPart = re.search(r'(\d+) Individual Baseball Player Stats', pg.get_text())
        year = int(yearPart.group(1))

        # find results (after the row with Daily Results in it)
        resultsStart = pg.find('b', string='Daily results')

        if resultsStart:
            resultsStart = resultsStart.parent.parent
        else:
            raise ValueError('No data available on player page.')

        # find player ID in a link on the page
        priorSeasonLinkElem = pg.find('a', string=re.compile(r'Click here for \d+ season history'))
        priorSeasonLink = priorSeasonLinkElem.get('href')
        rotoguruId = priorSeasonLink.split('?')[-1]

        # get player IDs from cross reference links
        #
        espnId = None
        mlbId = None
        yahooId = None
        rotowireId = None
        razzballId = None
        rotoballerId = None
        fangraphsId = None

        #
        # ESPN
        #   <a href="http://sports.espn.go.com/mlb/players/profile?playerId=31815" target="_blank">ESPN</a>
        espnLinkElem = pg.find('a', string='ESPN')
        if espnLinkElem:
            espnLink = espnLinkElem.get('href')
            espnIdMatch = re.search(r'\?playerId=(\d+)', espnLink)
            espnId = espnIdMatch.group(1) if espnIdMatch else None

        #
        # MLB
        #   <a href="https://www.mlb.com/player/572971" target="_blank">MLB.com</a>
        mlbLinkElem = pg.find('a', string='MLB.com')
        if mlbLinkElem:
            mlbLink = mlbLinkElem.get('href')
            mlbIdMatch = re.search(r'https://www.mlb.com/player/(\d+)', mlbLink)
            mlbId = mlbIdMatch.group(1) if mlbIdMatch else None

        #
        # Yahoo
        #   <a href="http://sports.yahoo.com/mlb/players/9217/" target="_blank">Yahoo</a>
        yahooLinkElem = pg.find('a', string='Yahoo')
        if yahooLinkElem:
            yahooLink = yahooLinkElem.get('href')
            yahooIdMatch = re.search(r'http://sports.yahoo.com/mlb/players/(\d+)/', yahooLink)
            yahooId = yahooIdMatch.group(1) if yahooIdMatch else None

        #
        # RotoWire
        #   <a href="http://www.rotowire.com/baseball/player.php?id=11751" target="_blank">RotoWire</a>
        rotowireLinkElem = pg.find('a', string='RotoWire')
        if rotowireLinkElem:
            rotowireLink = rotowireLinkElem.get('href')
            rotowireIdMatch = re.search(r'http://www.rotowire.com/baseball/player.php\?id=(\d+)', rotowireLink)
            rotowireId = rotowireIdMatch.group(1) if rotowireIdMatch else None

        #
        # RazzBall
        #   <a href="https://razzball.com/player/9434/Dallas+Keuchel/" target="_blank">Razzball</a>
        razzballLinkElem = pg.find('a', string='Razzball')
        if razzballLinkElem:
            razzballLink = razzballLinkElem.get('href')
            razzballIdMatch = re.search(r'https://razzball.com/player/(\d+)/.*/', razzballLink)
            razzballId = razzballIdMatch.group(1) if razzballIdMatch else None

        #
        # RotoBaller
        #   <a href="http://www.rotoballer.com/mlb/player/572971/Dallas+Keuchel" target="_blank">RotoBaller</a>
        rotoballerLinkElem = pg.find('a', string='RotoBaller')
        if rotoballerLinkElem:
            rotoballerLink = rotoballerLinkElem.get('href')
            rotoballerIdMatch = re.search(r'http://www.rotoballer.com/mlb/player/(\d+)/.*', rotoballerLink)
            rotoballerId = rotoballerIdMatch.group(1) if rotoballerIdMatch else None

        #
        # Fangraphs
        #   <a href="http://www.fangraphs.com/statss.aspx?playerid=9434" target="_blank">Fangraphs</a>
        fangraphsLinkElem = pg.find('a', string='Fangraphs')
        if fangraphsLinkElem:
            fangraphsLink = fangraphsLinkElem.get('href')
            fangraphsIdMatch = re.search(r'http://www.fangraphs.com/statss.aspx\?playerid=(\d+)', fangraphsLink)
            fangraphsId = fangraphsIdMatch.group(1) if fangraphsIdMatch else None


        # find table header row
        #   <table>
        #       <tbody>
        #           <tr> -- first header
        #               <th>
        #           <tr> -- spanning headers -- Schedule	FanDuel	Yahoo DFS	DraftDay	DraftKings
        #           <tr> -- individual column headers Date	Oppt.	Position	Opp.SP	FDP	FD Price	YHP	YH Price	DDP	DD Price	DKP	DK Price
        #
        scheduleHeading = pg.find('font', string='Schedule')
        headerRow = scheduleHeading.parent.findNext('tr')
        headers = [x.get_text().strip() for x in headerRow.find_all('th')]

        dateCol = headers.index('Date')
        fanduelCol = headers.index('FD Price')
        yahooCol = headers.index('YH Price')
        draftkingsCol = headers.index('DK Price')

        for _game in resultsStart.next_siblings:
            cols = [x.get_text() for x in _game.find_all('td')]

            dateParts = [int(x) for x in cols[dateCol].split('-')]
            rowDate = datetime.date(year, dateParts[0], dateParts[1])

            dkSalary = cols[draftkingsCol].replace('$','').replace(',','').strip()
            fanduelSalary = cols[fanduelCol].replace('$','').replace(',','').strip()
            yahooSalary = cols[yahooCol].replace('$','').replace(',','').strip()

            customData = {
                'rotoguruId': rotoguruId,
                'espnId': espnId,
                'mlbId': mlbId,
                'yahooId': yahooId,
                'rotowireId': rotowireId,
                'razzballId': razzballId,
                'rotoballerId': rotoballerId,
                'fangraphsId': fangraphsId
            }

            if dkSalary != '':
                yield MlbPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'draftkings',
                    season_year = rowDate.year,
                    game_date = rowDate,
                    player_id = None,
                    salary = dkSalary
                )

            if fanduelSalary != '':
                yield MlbPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'fanduel',
                    season_year = rowDate.year,
                    game_date = rowDate,
                    player_id = None,
                    salary = fanduelSalary
                )

            if yahooSalary != '':
                yield MlbPlayerDfs(
                    custom_data = customData,
                    source_site = 'rotoguru',
                    dfs_site = 'yahoo',
                    season_year = rowDate.year,
                    game_date = rowDate,
                    player_id = None,
                    salary = yahooSalary
                )

