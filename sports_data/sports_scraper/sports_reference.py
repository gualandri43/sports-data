'''
Scrapers for sports-reference.com sites (baseball-reference.com, pro-football-reference.com, etc.)
'''

import datetime
import re
from typing import Dict, Iterable, List

import requests
import bs4
from bs4 import BeautifulSoup

from sports_data.data_model.schema import MlbBatting, MlbPitching, MlbPlayerGamelog, MlbPlayerSeason, NflOffense, NflPlayerGamelog, NflRoster



class SportsReferenceScraper:
    '''
        Base scraper for common functions across all sports-reference.com sites.
    '''

    def _parse_table(self, tableElement: bs4.element.Tag, includeRows=None, excludeRows=None) -> Iterable[dict]:
        '''Parse a data table into a list of dictionary records. The field names are
            the `data-stat` element attribute. If it is a link, an additional field is
            added with _link appended to the name.

            Parameters
            ----------
            tableElement
                A BeautifulSoup element that represents the HTML table to parse.

            includeRows, excludeRows
                Optional filters to include/exclude table rows. Filter is a dictionary with
                key -> attribute name, value -> string. If the attribute value contains
                the string, it is included/excluded.

            Yields
            ------
            A dictionary record of each row, with keys corresponding to data-stat elements.
        '''

        if includeRows and excludeRows:
            raise ValueError('includeRows and excludeRows cannot both be specified.')

        # data rows are tr tags with a data-row attribute
        for _row in tableElement.find('tbody').find_all('tr'):

            isRepeatedHeader = False
            if _row.get('class'):
                if _row.get('class') == 'thead':
                    isRepeatedHeader = True

            skipRow = False

            if includeRows:
                skipRow = True

                for _attrib,_val in includeRows.items():
                    if _row.get(_attrib):
                        if _val in _row.get(_attrib):
                            skipRow = False

            if excludeRows:
                skipRow = False

                for _attrib,_val in excludeRows.items():
                    if _row.get(_attrib):
                        if _val in _row.get(_attrib):
                            skipRow = True

            if not isRepeatedHeader and not skipRow:
                rec = dict()

                # normal <td> elements in row
                for _col in _row.find_all('td'):
                    statName = _col.get('data-stat')
                    if statName:
                        rec[statName] = _col.get_text()

                        # check for link and add a new field
                        for _link in _col.find_all('a'):
                            if _link.get('href'):
                                rec[statName + '_link'] = _link.get('href')

                        # csk attribute sometimes has better formatted data
                        if _col.get('csk'):
                            rec[statName + '_csk'] = _col.get('csk')

                # if there is data in the row that is a <th> tag, grab that
                #   don't include actual table header
                for _col in _row.find_all('th', scope='row'):
                    statName = _col.get('data-stat')
                    if statName and statName not in rec:
                        rec[statName] = _col.get_text()

                if len(rec) > 0:
                    yield rec

class BaseballReferenceScraper(SportsReferenceScraper):
    '''
        Pull data from baseball-reference.com.

        Attributes
        ----------
        franchiseCodes : List[str]
            The 3 letter code that refers to each MLB franchise.

        teamCodes : List[Tupe[str,int,int]]
            A list of team codes and the seasons that they are valid for. Each
            tuple is (team code, start year, end year).

    '''

    franchiseCodes = [
        'ARI',
        'ATL',
        'BAL',
        'BOS',
        'CHC',
        'CHW',
        'CIN',
        'CLE',
        'COL',
        'DET',
        'HOU',
        'KCR',
        'ANA',
        'LAD',
        'FLA',
        'MIL',
        'MIN',
        'NYM',
        'NYY',
        'OAK',
        'PHI',
        'PIT',
        'SDP',
        'SFG',
        'SEA',
        'STL',
        'TBD',
        'TEX',
        'TOR',
        'WSN'
    ]

    teamCodes = [
        #tuples of
        #   (code, startYear, endYear)
        ('ARI',1998,None),  # arizona diamondback, 1998-present
        ('ATL',1966,None),  # atlanta braves, 1966-present
        ('MLN',1953,1965),  # milwaukee braves, 1953-1965
        ('BSN',1876,1952),  # Boston red stockings, beaneaters, doves, rustlers, bees, braves 1876-1952
        ('BAL',1954,None),  # baltimore orioles, 1954-present
        ('SLB',1902,1953),  # st. louis browns, 1902-1953
        ('MLA',1901,1901),  # milwaukee brewers, 1901
        ('BOS',1901,None),  # boston red sox, americans, 1901-present
        ('CHC',1876,None),  # chicago cubs, white stockings, colts, orphans, 1876-present
        ('CHW',1901,None),  # chicago white sox, 1901-present
        ('CIN',1882,None),  # cincinnati reds, redlegs, red stockings, 1882-present
        ('CLE',1901,None),  # cleveland indians, naps, bronchos, blues, 1901-present
        ('COL',1993,None),  # colorado rockies 1993-present
        ('DET',1901,None),  # detroit tigers 1901-present
        ('HOU',1962,None),  # houston astros, colt .45s, 1962-present
        ('KCR',1969,None),  # kansas city royals, 1969-present
        ('ANA',1997,2004),  # Anaheim Angels, 1997-2004
        ('LAA',2005,None),  # Los Angeles Angels, Los Angeles Angels of Anaheim, 2005-present, 1961-1964
        ('LAA',1961,1964),
        ('CAL',1965,1996),  # California Angels, 1965-1996
        ('LAD',1969,None),  # Los Angeles Dodgers, 1969-present
        ('BRO',1884,1968),  # Brookly Dodgers, etc, 1884-1968
        ('FLA',1993,2011),  # Florida marlins, 1993-2011
        ('MIA',2012,None),  # miami marlins, 2012-present
        ('MIL',1970,None),  # milwaukee brewers, 1970-present
        ('SEP',1969,1969),  # seattle pilots, 1969
        ('MIN',1961,None),  # minnesota twins, 1961-present
        ('WSH',1901,1960),  # washington senators, 1901-1960
        ('NYM',1962,None),  # new york mets, 1962-present
        ('NYY',1903,None),  # new york yankees, highlanders, 1903-present
        ('OAK',1968,None),  # oakland athletics, 1968-present
        ('KCA',1955,1967),  # kansas city athletics, 1955-1967
        ('PHA',1901,1954),  # philedelphia athletics, 1901-1954
        ('PHI',1883,None),  # philedelphia phillies, quakers, 1883-present
        ('PIT',1882,None),  # pittsburgh pirates, alleghenys, 1882-present
        ('SDP',1969,None),  # san diego padres, 1969-present
        ('SFG',1958,None),  # san francisco giants, 1958-present
        ('NYG',1883,1957),  # new york giants, gothams, 1883-1957
        ('SEA',1977,None),  # seattle mariners, 1977-present
        ('STL',1882,None),  # st louis cardinals, perfectos, browns, 1882-present
        ('TBR',2008,None),  # tampa bay rays, 2008-present
        ('TBD',1998,2007),  # tampa bay devil rays, 1998-2007
        ('TEX',1972,None),  # texas rangers 1972-present
        ('WSA',1961,1968),  # washington senators, 1961-1968
        ('TOR',1977,None),  # Toronto blue jays, 1977-present
        ('WSN',2005,None),  # washington nationals, 2005-present
        ('MON',1969,2004)   # montreal expos 1969-2004
    ]

    @classmethod
    def get_team_codes(cls, season: int) -> List[str]:
        '''Get a list of team codes for a given season.

            Parameters
            ----------
            season
                The season to get the team codes for.

            Returns
            -------
            List[str]
                A list of the team codes valid for a given season.
        '''

        season = int(season)
        codes = set()

        for _team in cls.teamCodes:
            if season >= _team[1]:
                if _team[2] is None:
                    codes.add(_team[0])

                elif season <= _team[2]:
                    codes.add(_team[0])

        return list(codes)

    @property
    def currentTeamCodes(self) -> List[str]:
        '''All valid team codes for the current year.'''
        return BaseballReferenceScraper.get_team_codes(datetime.date.today().year)

    def _convert_string_stat(self, stat):
        ''''''

        if stat is None:
            return None

        try:
            return float(stat)
        except:
            return stat

    def _extract_player_id_from_link(self, playerPageLink):
        '''Extract the baseballreference player id from a player page link.'''

        return playerPageLink.split('/')[-1].replace('.shtml', '').strip()

    def _create_pitching_stat_from_dict(self, pitchingStat: dict) -> MlbPitching:
        '''
            Create and fill a MlbPitching object from a baseball-reference heading dictionary.
        '''

        statDict = dict(pitchingStat)

        stats = MlbPitching(
            custom_data = dict(),
            ip = statDict.pop('IP', None),
            hit = statDict.pop('H', None),
            double = statDict.pop('2B', None),
            triple = statDict.pop('3B', None),
            homerun = statDict.pop('HR', None),
            er = statDict.pop('ER', None),
            walk = statDict.pop('BB', None),
            strikeout = statDict.pop('SO', None),
            hit_by_pitch = statDict.pop('HBP', None),
            era = statDict.pop('earned_run_avg', None),
            intentional_walk = statDict.pop('IBB', None),
            shoutout = statDict.pop('SHO', None),
            complete_game = statDict.pop('CG', None),
            game = statDict.pop('G', None),
            game_started = statDict.pop('GS', None),
            sv = statDict.pop('SV', None),
            runs_allowed = statDict.pop('R', None),
            win = statDict.pop('W', None),
            loss = statDict.pop('L', None),
            wild_pitch = statDict.pop('WP', None),
            balk = statDict.pop('BK', None),
            whip = statDict.pop('whip', None),
            fip = statDict.pop('fip', None),
            walk_per_nine = statDict.pop('bases_on_balls_per_nine', None),
            strikeout_per_nine = statDict.pop('strikeouts_per_nine', None)
        )

        if stats.single is None:
            if stats.hit is not None and stats.double is not None and stats.triple is not None and stats.homerun is not None:
                stats.single = stats.hit - stats.double - stats.triple - stats.homerun

        stats.custom_data.update(statDict)

        if 'player_link' in statDict:
            stats.custom_data['player_id'] = self._extract_player_id_from_link(statDict['player_link'])

        return stats

    def _create_batting_stat_from_dict(self, battingStat: dict) -> MlbBatting:
        '''
            Create and fill a MlbBatting object from a baseball-reference heading dictionary.
        '''

        statDict = dict(battingStat)

        stats = MlbBatting(
            custom_data = dict(),
            plate_appearance = statDict.pop('PA', None),
            game = statDict.pop('G', None),
            at_bat = statDict.pop('AB', None),
            run = statDict.pop('R', None),
            hit = statDict.pop('H', None),
            double = statDict.pop('2B', None),
            triple = statDict.pop('3B', None),
            homerun = statDict.pop('HR', None),
            total_bases = statDict.pop('TB', None),
            rbi = statDict.pop('RBI', None),
            walk = statDict.pop('BB', None),
            sacrifice_fly = statDict.pop('SF', None),
            sacrifice_bunt = statDict.pop('SH', None),
            intentional_walk = statDict.pop('IBB', None),
            strikeout = statDict.pop('SO', None),
            hit_by_pitch = statDict.pop('HBP', None),
            stolen_base = statDict.pop('SB', None),
            caught_stealing = statDict.pop('CS', None),
            gidp = statDict.pop('GIDP', None),
            average = statDict.pop('batting_avg', None),
            obp = statDict.pop('onbase_perc', None),
            slg = statDict.pop('slugging_perc', None),
            ops = statDict.pop('onbase_plus_slugging', None)
        )

        if stats.single is None:
            if stats.hit is not None and stats.double is not None and stats.triple is not None and stats.homerun is not None:
                stats.single = stats.hit - stats.double - stats.triple - stats.homerun

        stats.custom_data.update(statDict)

        if 'player_link' in statDict:
            stats.custom_data['player_id'] = self._extract_player_id_from_link(statDict['player_link'])

        return stats

    def build_team_page_request(self, teamCode: str, season: int) -> requests.Request:
        '''Build a requests.Request object to retrieve a team page for a given season.

            This page must be rendered with a JS engine to include data tables.

            Parameters
            ----------
            teamCode
                The three letter team code of the team to retrieve. The available team codes for the season
                are in the teamCodes attribute or by using the currentTeamCodes property.

            season
                The season to retrieve for the team.
        '''

        if teamCode not in self.get_team_codes(season):
            raise ValueError('Not a valid team code for the given year.')

        return requests.Request(
            method='GET',
            url='https://www.baseball-reference.com/teams/{0}/{1}.shtml'.format(teamCode, season)
        )

    def build_player_page_request(self, playerId: str) -> requests.Request:
        '''Build a request for a player page on baseball-reference.com

            Parameters
            ----------
            playerId
                The baseball-refernce alphanumeric player id.
        '''

        firstLetter = playerId[0].lower()
        playerId = playerId.lower()

        return requests.Request(
            method='GET',
            url='https://www.baseball-reference.com/players/{0}/{1}.shtml'.format(firstLetter, playerId)
        )

    def build_player_gamelog_request(self, playerId: str, season: int, statType: str) -> requests.Request:
        '''Build a request for a player gamelog page on baseball-reference.com

            Parameters
            ----------
            playerId
                The baseball-refernce alphanumeric player id.

            season
                Season to retrieve game logs for.

            statType : {batting,pitching,fielding}
                The type of stat to get.
        '''

        if statType not in ['batting','pitching','fielding']:
            raise ValueError('statType not a valid option.')

        return requests.Request(
            method='GET',
            url='https://www.baseball-reference.com/players/gl.fcgi',
            params = {
                'id': playerId.lower(),
                't': statType[0].lower(),   # stat type is either b,p,f
                'year': int(season)
            }
        )

    def build_season_stat_request(self, season: int, statType: str) -> requests.Request:
        '''Build a request for a season stat page on baseball-reference.com. This is a request
            for the whole season (all teams and players).

            This page must be rendered with a JS engine to include all data tables.

            Parameters
            ----------
            season
                Season to retrieve game logs for.

            statType : {batting,pitching,fielding}
                The type of stat to download.
        '''

        if statType not in ['batting','pitching']:
            raise ValueError('statType not a valid option.')

        if statType == 'batting':

            return requests.Request(
                method='GET',
                url='https://www.baseball-reference.com/leagues/MLB/{0}-standard-batting.shtml'.format(season)
            )

        elif statType == 'pitching':

            return requests.Request(
                method='GET',
                url='https://www.baseball-reference.com/leagues/MLB/{0}-standard-pitching.shtml'.format(season)
            )

    def parse_available_player_game_logs(self, playerPageResp: requests.Response, statType: str) -> List[int]:
        '''Retrieve the avilable seasons of game logs for a player.

            Parameters
            ----------
            The HTTP response from a player page request. That request is built by build_player_page_request().

            statType : {batting,pitching,fielding}
                The type of stat to get.

            Returns
            -------
            List[int]
                A list of available season gamelogs.
        '''

        playerPage = BeautifulSoup(playerPageResp.text, 'lxml')
        gamelogList = playerPage.find('span', string='Game Logs').parent

        logType = None
        available = list()
        for _child in gamelogList.div.children:

            if _child.name == 'p':
                logType = _child.get_text()

                if logType.lower() == statType:

                    if _child.name == 'ul':
                        for _season in _child.find_all('a'):
                            available.append(int(_season.get_text()))

        return available

    def parse_player_game_logs(self, gamelogResp: requests.Response) -> Iterable[MlbPlayerGamelog]:
        '''Parse the response of a player game log table request.

            Parameters
            ----------
            gamelogResp
                The HTTP response from a player gamelog request.
                The request is built by build_player_gamelog_request()

            Yields
            ------
            MlbPlayerGamelog
                This function is a generator, yielding a MlbPlayerGamelog for each player record.
        '''

        pg = BeautifulSoup(gamelogResp.text, 'lxml')

        # extract player id
        for _link in pg.find_all('link'):
            if 'rel' in _link.attrs and 'canonical' in _link['rel']:
                playerId = re.search(
                    r'.*id=(.*\d\d)&',
                    _link['href']
                ).group(1)

                break

        logTable = pg.find('table', id='pitching_gamelogs')
        statType = 'pitching'

        if logTable is None:
            logTable = pg.find('table', id='batting_gamelogs')
            statType = 'batting'

        if logTable is None:
            logTable = pg.find('table', id='fielding_gamelogs')
            statType = 'fielding'

        # gamelog data rows have an id like id="batting_gamelogs.###"
        includeFilter = {
            'id': 'gamelogs'
        }

        for _game in self._parse_table(logTable, includeRows=includeFilter):

            _gameData = dict(_game)
            gameKey = _gameData.pop('date_game_csk',None).split('.')[-1].strip()
            gameLog = MlbPlayerGamelog(
                baseball_reference_game_key = gameKey,
                game_date = datetime.date(
                    int(gameKey[3:7]),
                    int(gameKey[7:9]),
                    int(gameKey[9:11])
                ),
                season_year = int(gameKey[3:7]),
                source_site = 'baseball_reference',
                custom_data = {
                    'player_id': playerId
                }
            )

            if statType == 'batting':
                gameLog.batting_stats = self._create_batting_stat_from_dict(_gameData)

            if statType == 'pitching':
                gameLog.pitching_stats = self._create_pitching_stat_from_dict(_gameData)

            yield gameLog

    def parse_team_stats_page(self, teamPageRespText: str) -> Dict[str,List[dict]]:
        '''Parse all tables on a team's season page.

            Parameters
            ----------
            teamPageResp
                The HTML response text from a team page request.
                This page must have been rendered with a JS engine.
                The request is built by build_team_page_request()

            Returns
            -------
            Dict[str,List[dict]]
                A dictionary holding a list of parsed records from all data tables on the team stats page.
        '''

        pg = BeautifulSoup(teamPageRespText, 'lxml')

        return {
            'full_season_roster': list(self._parse_table(pg.find('table', id='appearances'))),
            'team_pitching': list(self._parse_table(pg.find('table', id='team_pitching'))),
            'team_batting': list(self._parse_table(pg.find('table', id='team_batting'))),
            'coaches': list(self._parse_table(pg.find('table', id='coaches'))),
            'standard_fielding': list(self._parse_table(pg.find('table', id='standard_fielding'))),
            'players_value_batting': list(self._parse_table(pg.find('table', id='players_value_batting'))),
            'players_value_pitching': list(self._parse_table(pg.find('table', id='players_value_pitching')))
        }

    def parse_season_stats_page(self, seasonPageRespText: str) -> Iterable[MlbPlayerSeason]:
        '''Parse the full season player stats on a MLB season page.

            Parameters
            ----------
            seasonPageResp
                The HTML response text from a season stat request.
                This page must have been rendered with a JS engine.
                The request is built by build_season_stat_request().

            Yields
            ------
            MlbPlayerSeason
                This function is a generator, yielding one MlbPlayerSeason object for each season's stats.
        '''

        pg = BeautifulSoup(seasonPageRespText, 'lxml')

        seasonYear = int(pg.find('title').get_text()[:4])

        if pg.find('table', id='players_standard_batting'):
            statType = 'batting'
            tbl = self._parse_table(pg.find('table', id='players_standard_batting'))

        elif pg.find('table', id='players_standard_pitching'):
            statType = 'pitching'
            tbl = self._parse_table(pg.find('table', id='players_standard_pitching'))

        for _season in tbl:

            if 'player_link' in _season:
                seasonLog = MlbPlayerSeason(
                    custom_data = dict(),
                    source_site = 'baseball_reference',
                    season_year = seasonYear
                )

                seasonLog.custom_data['player_id'] = self._extract_player_id_from_link(_season['player_link'])

                if statType == 'batting':
                    seasonLog.batting_stats = self._create_batting_stat_from_dict(_season)

                if statType == 'pitching':
                    seasonLog.pitching_stats = self._create_pitching_stat_from_dict(_season)

                yield seasonLog

class ProFootballReferenceScraper(SportsReferenceScraper):
    '''
        Pull data from pro-football-reference.com.

        Attributes
        ----------
        franchiseCodes : List[str]
            The 3 letter code that refers to each NFL franchise. From https://www.pro-football-reference.com/teams/.
    '''

    franchiseCodes = [
        'CRD',
        'ATL',
        'RAV',
        'BUF',
        'CAR',
        'CHI',
        'CIN',
        'CLE',
        'DAL',
        'DEN',
        'DET',
        'GNB',
        'HTX',
        'CLT',
        'JAX',
        'KAN',
        'RAI',
        'SDG',
        'RAM',
        'MIA',
        'MIN',
        'NWE',
        'NOR',
        'NYG',
        'NYJ',
        'PHI',
        'PIT',
        'SFO',
        'SEA',
        'TAM',
        'OTI',
        'WAS'
    ]

    def _extract_player_id_from_link(self, playerPageLink):
        '''Extract the player id from a player page link.'''

        if playerPageLink is None:
            return None

        #https://www.pro-football-reference.com/players/H/HickAk00.htm
        endingPart = playerPageLink.split('/')[-1]
        return endingPart.split('.')[0].strip()

    def build_team_roster_request(self, team: str, season: int) -> requests.Request:
        '''Build a request for a team's roster of players for a season.
            This request requires a JS rendering engine to get the full team roster.

            Parameters
            ----------
            team
                The team code to query.

            season
                The season to get the roster for.

        '''

        return requests.Request(
            method='GET',
            url='https://www.pro-football-reference.com/teams/{0}/{1}_roster.htm'.format(team.lower(), season)
        )

    def parse_team_roster_response(self, pg: bs4.BeautifulSoup) -> Iterable[NflRoster]:
        '''Parse the response from the request built by build_team_roster_request().

            Parameters
            ----------
            response
                The response from the web request.
        '''

        tbl = pg.find('table', class_='stats_table', id='roster')

        # get team and year from page url
        pgUrl = pg.find('link', rel='canonical')['href']
        extracted = re.search(r'.*teams/(.*)/(.*)_roster.*', pgUrl)

        for _player in self._parse_table(tbl):
            yield NflRoster(
                source_site='pro_football_reference',
                season_year=extracted.group(2),
                pro_football_reference_team_key=extracted.group(1).upper(),
                jersey_number=_player.get('uniform_number', None),
                position=_player.get('pos', None),
                custom_data={
                    'player_id': self._extract_player_id_from_link(_player.get('player_link', None)),
                    'player_name': _player.get('player', None),
                    'player_link': _player.get('player_link', None)
                }
            )

    def build_player_gamelog_request(self, playerId: str, season: int) -> requests.Request:
        '''Build a request for a player gamelog page on www.pro-football-reference.com

            Parameters
            ----------
            playerId
                The www.pro-football-reference.com alphanumeric player id.

            season
                Season to retrieve game logs for.

        '''

        return requests.Request(
            method='GET',
            url='https://www.pro-football-reference.com/players/{0}/{1}/gamelog/{2}'.format(
                playerId[0],
                playerId,
                season
            )
        )

    def parse_player_game_logs(self, response: requests.Response) -> Iterable[NflPlayerGamelog]:
        '''Parse the response of a player game log table request.

            Parameters
            ----------
            response
                The HTTP response from a player gamelog request.
                The request is built by build_player_gamelog_request()

            Yields
            ------
            MlbPlayerGamelog
                This function is a generator, yielding a NflPlayerGamelog for each player record.
        '''

        pg = BeautifulSoup(response.text)

        tbl = pg.find('table', id='stats')



        for _game in self._parse_table(tbl):

            data = dict(_game)

            gameDate = datetime.datetime.strptime(data['game_date'], '%Y-%m-%d').date()
            season = data['opp_link'].split('/')[-1].split('.')[0]
            gameKey = data['game_result_link'].split('/')[-1].split('.')[0]

            offenseStats = NflOffense(
                pass_attempt=data.pop('pass_att', None),
                pass_completion=data.pop('pass_cmp', None),
                pass_yard=data.pop('pass_yds', None),
                pass_touchdown=data.pop('pass_td', None),
                pass_interception=data.pop('pass_int', None),
                rush_attempt=data.pop('rush_att', None),
                rush_yard=data.pop('rush_yds', None),
                rush_touchdown=data.pop('rush_td', None),
                rec_target=data.pop('targets', None),
                rec_reception=data.pop('rec', None),
                rec_yard=data.pop('rec_yds', None),
                rec_touchdown=data.pop('rec_td', None),
                fumble=data.pop('fumbles', None),
                fumble_lost=data.pop('fumbles_lost', None),
                snaps_offense=data.pop('offense', None),
                snaps_offense_pct=data.pop('off_pct', None),
                sacked=data.pop('pass_sacked', None),
                sacked_yards_lost=data.pop('pass_sacked_yds', None)
            )

            yield NflPlayerGamelog(
                source_site='pro_football_reference',
                season_year=season,
                game_date=gameDate,
                pro_football_reference_game_key=gameKey,
                player_id=None,
                offense_stats=offenseStats,
                defense_stats=None,
                kicking_stats=None,
                custom_data=dict(data)
            )
