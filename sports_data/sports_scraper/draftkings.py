'''
Module for scraping draftkings.com data.
'''


def build_contest_link(contestId):
    '''Build contest link URL.'''

    # example
    #   https://www.draftkings.com/contest/gamecenter/118427388

    return 'https://www.draftkings.com/contest/gamecenter/{0}'.format(contestId)

