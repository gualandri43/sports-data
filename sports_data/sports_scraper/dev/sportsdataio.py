'''
Module for pulling data from the sportsdataio API.
Free API keys have access to some unscrambled descriptive data.
'''

import os
import io
import csv
import requests

class SportsDataIoData:
    '''Data object for sportsdata.io API response data.'''

    @classmethod
    def build_from_api_docs(cls, tableName):
        '''
            Build a data object of a table using the API's data dictionary.
        '''

        resp = requests.get('https://sportsdata.io/developers/datadictionary_download?league=MLB')
        data = io.StringIO(resp.text)

        reader = csv.DictReader(data)

        obj = cls()

        for _row in reader:
            if _row['TableName'] == tableName:
                setattr(obj, _row['Name'], None)

        return obj

    @classmethod
    def build_from_api_response(cls, respObj):
        '''
            Build a filled data object of a table using the json response.

            respObj         The dictionary representing the parsed json response.
        '''

        obj = cls()

        for _field, _val in respObj.items():
            setattr(obj, _field, _val)

        return obj

class SportsDataIo:
    '''
        Pull data from sportsdata.io API.

        apiKey      The sportsdata.io API key. Can be specified directly, or read
                    from environment variable SPORTSDATAIO_API_KEY.
    '''

    def __init__(self, apiKey=None):

        self.apiKey = apiKey

        if self.apiKey is None:
            self.apiKey = os.environ.get('SPORTSDATAIO_API_KEY')

        self.authHeader = {
            'Ocp-Apim-Subscription-Key': self.apiKey
        }

    def build_games_request(self, season):
        '''Build requests.Request object to query the season games endpoint. '''

        return requests.Request(
            method='GET',
            url='https://fly.sportsdata.io/v3/mlb/scores/json/Games/{0}'.format(season),
            headers=self.authHeader
        )

