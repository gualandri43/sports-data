'''
Download stats from FootballOutsiders.com
'''

import re
import os
import lxml.etree
import lxml.html
import requests

import gualandri_common.time
import sports_data.data_model.schema_historical as schema_historical

class FootballOutsidersScraper:
    '''
    Class containing methods for downloading stats from footballoutsiders.com
    '''

    source = 'footballoutsiders'
    scope = 'seasonToDate'

    def _format_stat(self, statText):
        '''
        format and clean stat table values
        '''

        if statText is None:
            return None
        elif statText.strip() == '':
            return None
        else:

            statText = statText.strip()

            try:
                num = float(statText.replace('%',''))

                if num.is_integer():
                    return int(num)
                else:
                    return num
            except:
                return statText

    def build_dvoa_request(self, season, week):
        '''
        requests.Request object to get the weekly analysis posting of season-to-date DVOA rankings.
        '''

        if not isinstance(season, int):
            raise ValueError('Invalid season')

        if not isinstance(week, int):
            raise ValueError('Invalid week')

        if week > 17:
            raise ValueError('Invalid week')

        if week == 17:
            return requests.Request(
                method='GET',
                url='https://www.footballoutsiders.com/dvoa-ratings/{0}/final-{0}-dvoa-ratings'.format(season)
            )
        else:
            return requests.Request(
                method='GET',
                url='https://www.footballoutsiders.com/dvoa-ratings/{0}/week-{1}-dvoa-ratings'.format(season, week)
            )

    def parse_dvoa_ratings(self, responseText, season):
        '''
        parse the dvoa ratings page

        specifically meant to parse the analysis post
        (i.e. https://www.footballoutsiders.com/dvoa-ratings/2019/week-16-dvoa-ratings)

        //table[@border="2"]
        '''

        pg = lxml.html.fromstring(responseText)

        dvoaTable = pg.xpath('//table[@border="2"]')

        if dvoaTable is None:
            return list()
        else:

            timeRecorder = gualandri_common.time.TimeRecorder()

            templateStat = schema_historical.NflHistoricalTeamStat.create_empty()
            templateStat.source_site = self.source
            templateStat.scope = 'seasonToDate'
            templateStat.scrape_time_local = timeRecorder.timeLocal
            templateStat.scrape_time_utc = timeRecorder.timeUtc
            templateStat.season_year = season

            # title is Week 2 DVOA rankings, except for last week, which is Final 2019 DVOA Rankings
            elem = pg.xpath('//h1[@class="page-title"]/span')[0]

            capt = re.search(r'Final (\d+) DVOA Ratings', elem.text)
            if capt is not None and capt.group(1) is not None:
                templateStat.week = 17

            capt = re.search(r'Week (\d+) DVOA Ratings', elem.text)
            if capt is not None and capt.group(1) is not None:
                templateStat.week = int(capt.group(1))

            tbl = dvoaTable[0]

            #headers
            hdrs = list()
            for hdr in tbl.xpath('./thead//th'):
                hdrs.append(str(hdr.text_content()).strip().lower().replace(' ','_').replace(os.linesep,'_').replace('\t','').replace('.',''))

            print(hdrs)
            #data
            statsData = list()
            for row in tbl.xpath('./tbody/tr'):

                teamStat = schema_historical.NflHistoricalTeamStat.create_from_existing(templateStat)

                colCount = 0
                for col in row.xpath('./td'):

                    if hdrs[colCount] == 'team':
                        teamStat.dvoa.custom_data['team'] = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'total_dvoa':
                        teamStat.dvoa.total_dvoa = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'total_dave':
                        teamStat.dvoa.total_dave = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'wei_dvoa':
                        teamStat.dvoa.weighted_dvoa = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'off_dvoa':
                        teamStat.dvoa.offense_dvoa = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'def_dvoa':
                        teamStat.dvoa.defense_dvoa = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'st_dvoa':
                        teamStat.dvoa.special_teams_dvoa = self._format_stat(col.text_content())

                    elif hdrs[colCount] == 'w-l':
                        winLoss = str(col.text_content()).split('-')

                        if len(winLoss) == 3:
                            teamStat.custom_data['win'] = int(winLoss[0])
                            teamStat.custom_data['loss'] = int(winLoss[1])
                            teamStat.custom_data['tie'] = int(winLoss[2])

                        elif len(winLoss) == 2:
                            teamStat.custom_data['win'] = int(winLoss[0])
                            teamStat.custom_data['loss'] = int(winLoss[1])
                            teamStat.custom_data['tie'] = 0

                    colCount += 1

                statsData.append(teamStat)

            return statsData


if __name__ == "__main__":
    scraper = FootballOutsidersScraper()
    _session = requests.Session()
    resp = _session.send(scraper.build_dvoa_request(2019,17).prepare())
    projs = scraper.parse_dvoa_ratings(resp.text,2019)

    print(projs[0])
    print(projs[-1])
    print(len(projs))

