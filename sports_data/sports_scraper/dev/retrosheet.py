'''
scrapers for retrosheet data

The underlying data is obtained free of charge from and is copyrighted by Retrosheet. Interested parties may contact Retrosheet at www.retrosheet.org.
'''

import io
import requests
import pandas as pd
import numpy as np

import sports_data.data_model.schema_stats as schema_stats

class RetrosheetScraper:
    '''
    Class containing methods for downloading retrosheet data
    '''

    #websiteHeaderMap = {
    #    'player_id': 'player_id',
    #    'playerid': 'player_id',
    #    'Name': 'player_name',
    #    'Team': 'player_team',
    #    'Pos': 'player_position',
    #    'G': 'game',
    #    'PA': 'plate_appearance',
    #    'AB': 'at_bat',
    #    'H': 'hit',
    #    '1B': 'single',
    #    '2B': 'double',
    #    '3B': 'triple',
    #    'HR': 'homerun',
    #    'R': 'run',
    #    'RBI': 'rbi',
    #    'BB': 'walk',
    #    'SO': 'strikeout',
    #    'SB': 'stolen_base',
    #    'CS': 'caught_stealing',
    #    'AVG': 'average',
    #    'OBP': 'obp',
    #    'SLG': 'slg',
    #    'OPS': 'ops',
    #    'wOBA': 'woba',
    #    'W': 'win',
    #    'L': 'loss',
    #    'SV': 'sv',
    #    'HLD': 'hold',
    #    'ERA': 'era',
    #    'ER': 'er',
    #    'GS': 'game_started',
    #    'IP': 'ip',
    #    'WHIP': 'whip',
    #    'K/9': 'strikeout_per_nine',
    #    'BB/9': 'walk_per_nine',
    #    'FIP': 'fip',
    #    'wRC+': 'wrc_plus',
    #    'BsR': 'bsr',
    #    'Fld': 'fld',
    #    'HBP': 'hit_by_pitch',
    #    'TBF': 'total_batters_faced',
    #    'WAR': 'war'
    #}

    projectionSources = [
        'retrosheet'
    ]

    projectionScopes = [
        'daily'
    ]

    def __init__(self, source='retrosheet', scope='daily'):

        if scope not in self.projectionScopes:
            raise ValueError('Invalid projection scope.')

        self.source = source
        self.scope = scope

    def _parse_csv(self, csvText, colsToDelete=None):
        '''
        parse the csv text into a list of dictionary records.

        colsToDelete    list of column names to remove
        '''

        #parse CSV response text into a dataframe
        dataDF = pd.read_csv(io.StringIO(csvText))

        for col in colsToDelete:
            if col in dataDF.columns:
                dataDF.drop(columns=col, inplace=True)

        data = dataDF.to_dict(orient='records')

        # pandas is not designed to be able to replace nan with None without
        #   messing with dtypes and conversions etc. The below combination
        #   of list and dictionary comprehension recreates the list of dicts
        #   output by to_dict(), but replaces any np.nan with None.
        #
        # this will be relatively slow but it works.
        #
        return [
            {k:v if v is not np.nan else None for k,v in x.items()} for x in data
        ]

    def build_mlb_player_gamelog_download_request(self, season):
        '''
        Build requests.Request objects for downloading the player gamelogs
        for a season.

        returns a Request object.
        '''
        # The Chadwick Bureau project maintains github repositories of retrosheet data. The retrosplits project (https://github.com/chadwickbureau/retrosplits)
        # has combined daily csv gamelog data for each player per season (daybyday/playing-2020.csv). This is in a more easily usable format than
        # raw Retrosheet event files.

        url = 'https://github.com/chadwickbureau/retrosplits/raw/master/daybyday/playing-{0}.csv'.format(season)

        return requests.Request('GET', url)

    def parse_mlb_player_gamelog(self, gamelogCsvText):
        '''
            parse gamelog csv text.
        '''
        #schema_stats.MlbPlayerGamelog
        pass

