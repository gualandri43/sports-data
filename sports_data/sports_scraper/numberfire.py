'''
    Scraping tools for data hosted on www.numberfire.com.
'''

from typing import Dict, Iterable, Union
import datetime as dt
import re
import bs4
from bs4 import BeautifulSoup
import requests

from sports_data.utils import TimeRecorder
from sports_data.data_model.schema import MlbBatting, MlbPitching, MlbProjection, NflOffense, NflProjection


class NumberfireScraper:
    '''Scrape data from numberfire.com.

        Parameters
        ----------
        projectionScope : {season,restOfSeason,weekly,daily}
            The scope of projections to scrape.

        Attributes
        ----------
        projectionSource : str
            The reference name of the site source. It is always `numberfire`.

        validProjectionScopes : list
            A list of the valid projection scopes this scraper can download.

        projectionScope : str
            The scope of the projections that will be scraped.

    '''

    projectionSource = 'numberfire'

    validProjectionScopes = [
        'season',
        'restOfSeason',
        'weekly',
        'daily'
    ]

    def __init__(self, projectionScope: str):

        if projectionScope not in self.validProjectionScopes:
            raise ValueError('Invalid projection scope.')

        self.projectionScope = projectionScope

    def _get_row_id_mapping(self, responseText: str) -> Dict[int,Dict[str,str]]:
        '''Common helper function to map numberfire player IDs from a table to a data row. This
            mapping can then be used during scraping of each table row.

            Parameters
            ----------
            reponseText
                The page response text for a numberfire projection table

            Returns
            -------
            dict
                A dictionary with key=idx, value={idx, link, name}
        '''

        pids = {}

        respPage = BeautifulSoup(responseText, 'lxml')

        projTable = respPage.find('div', class_='projection-wrap')

        for _player in projTable.find('table', class_ = "projection-table--fixed").find_all('tr'):

            if 'data-row-index' in _player.attrs:
                playerIdx = _player['data-row-index']
                playerLink = _player.find('a')['href']
                playerName = _player.find('span', class_='full').text

                pids[playerIdx] = {
                    'idx': playerIdx,
                    'link': playerLink,
                    'name': playerName
                }

        return pids

    def _format_stat(self, statText: Union[str,bs4.element.PageElement]) -> Union[float,None]:
        '''Format and clean stat table values.'''

        if isinstance(statText, bs4.element.PageElement):
            statText = statText.text

        if statText is None:
            return None
        elif statText.strip() == 'N/A':
            return None
        else:
            return float(statText.replace('$','').strip())

    def _format_id(self, playerLink: str) -> Union[str,None]:
        '''Get playerid from the player link.'''

        if playerLink is None:
            return None
        else:
            return playerLink.strip().split('/')[-1].strip()

    def build_mlb_projections_requests(self, position: str) -> requests.Request:
        '''Build the requests.Request objects to retrieve the mlb data.

            Parameters
            ----------
            position : {batters,pitchers}
                The position type to generate a projection page request for.

            Returns
            -------
            requests.Request
                Request objects that will get the projections web page.
        '''

        baseUrl = 'https://www.numberfire.com/mlb'

        if self.projectionScope == 'season':

            return requests.Request(
                method='GET',
                url='{0}/fantasy/yearly-projections/{1}'.format(baseUrl,position)
            )

        raise NotImplementedError('No request available for the selected scope.')

    def build_nfl_projections_requests(self, position: str) -> requests.Request:
        '''Build the requests.Request objects to retrieve the mlb data.

            Parameters
            ----------
            position : {qb,rb,wr,te,k,d}
                The position type to generate a projection page request for.

            Returns
            -------
            requests.Request
                Request objects that will get the projections web page.
        '''

        baseUrl = 'https://www.numberfire.com/nfl'

        if self.projectionScope == 'weekly':
            return requests.Request(
                method='GET',
                url='{0}/fantasy/fantasy-football-projections/{1}'.format(baseUrl, position)
            )

        if self.projectionScope == 'restOfSeason':

            return requests.Request(
                method='GET',
                url='{0}/fantasy/remaining-projections/{1}'.format(baseUrl, position)
            )

        raise NotImplementedError('No request available for the selected scope.')

    def parse_mlb_projections(self, response: requests.Response, position: str) -> Iterable[MlbProjection]:
        '''Parse a MLB projection HTML page.

            Parameters
            ----------
            response
                The response of the HTTP request to download the player projection page. That request
                is built by build_mlb_projections_requests().

            position : {batters,pitchers}
                The position type the projection page is for.

            Yields
            ------
            MlbProjection
                A filled projection object for each row in the projection table.
        '''

        scrapeTime = TimeRecorder()

        pids = self._get_row_id_mapping(response.text)
        respPage = BeautifulSoup(response.text, 'lxml')

        projTable = respPage.find('div', class_='projection-wrap')

        for _playerData in projTable.find('table', class_ = "no-fix").find_all('tr'):

            if 'data-row-index' in _playerData.attrs:
                playerIdx = _playerData['data-row-index']

                playerProjection = MlbProjection(
                    source_site = self.projectionSource,
                    scope = self.projectionScope,
                    scrape_time_local = scrapeTime.timeLocal,
                    scrape_time_utc = scrapeTime.timeUtc,
                    season_year = scrapeTime.timeLocal.year,
                    custom_data = {
                        'player.idx': playerIdx,
                        'player.link': pids[playerIdx]['link'],
                        'player.id': self._format_id(pids[playerIdx]['link']),
                        'player.name': pids[playerIdx]['name'].strip()
                    }
                )

                if position == 'batters':

                    playerProjection.batting_stats = MlbBatting(
                        plate_appearance = self._format_stat(_playerData.find('td', class_='pa')),
                        run = self._format_stat(_playerData.find('td', class_='r')),
                        homerun = self._format_stat(_playerData.find('td', class_='hr')),
                        rbi = self._format_stat(_playerData.find('td', class_='rbi')),
                        stolen_base = self._format_stat(_playerData.find('td', class_='sb')),
                        average = self._format_stat(_playerData.find('td', class_='avg')),
                        ops = self._format_stat(_playerData.find('td', class_='ops'))
                    )


                if position == 'pitchers':

                    playerProjection.pitching_stats = MlbPitching(
                        ip = self._format_stat(_playerData.find('td', class_='ip')),
                        win = self._format_stat(_playerData.find('td', class_='w')),
                        strikeout = self._format_stat(_playerData.find('td', class_='k')),
                        walk = self._format_stat(_playerData.find('td', class_='bb')),
                        sv = self._format_stat(_playerData.find('td', class_='sv')),
                        era = self._format_stat(_playerData.find('td', class_='era')),
                        whip = self._format_stat(_playerData.find('td', class_='whip'))
                    )

                yield playerProjection

    def _parse_nfl_projection_metadata(self, response: BeautifulSoup) -> NflProjection:
        '''Return a metadata-populated NflProjection object'''

        respPage = BeautifulSoup(response.text, 'lxml')

        scrapeTime = TimeRecorder()

        projection = NflProjection(
            source_site = self.projectionSource,
            scope = self.projectionScope,
            scrape_time_local = scrapeTime.timeLocal,
            scrape_time_utc = scrapeTime.timeUtc,
            projection_date = dt.date.today()
        )

        if dt.date.today().month >= 4:
            projection.season_year = dt.date.today().year
        else:
            projection.season_year = dt.date.today().year - 1

        if self.projectionScope == 'weekly':
            elem = respPage.find('div', class_='projection-rankings').find('h2')
            capt = re.search(r'Week (\d+) Fantasy Football', elem.text)
            if capt.group(1) is not None:
                projection.projection_week = int(capt.group(1))

        return projection

    def parse_nfl_projections(self, response: requests.Response, position: str) -> Iterable[NflProjection]:
        '''Parse a NFL projection HTML page.

            Parameters
            ----------
            response
                The response of the HTTP request to download the player projection page. That request
                is built by build_nfl_projections_requests().

            position : {qb,rb,wr,te,k,d}
                The position type the projection page is for.

            Yields
            ------
            NflProjection
                A filled projection object for each row in the projection table.
        '''

        pids = self._get_row_id_mapping(response.text)

        respPage = BeautifulSoup(response.text, 'lxml')

        projTable = respPage.find('div', class_='projection-wrap')

        for _playerData in projTable.find('table', class_ = "no-fix").find_all('tr'):

            if 'data-row-index' in _playerData.attrs:
                playerIdx = _playerData['data-row-index']

                playerProjection = self._parse_nfl_projection_metadata(respPage)

                playerProjection.custom_data = {
                    'player.idx': playerIdx,
                    'player.link': pids[playerIdx]['link'],
                    'player.id': self._format_id(pids[playerIdx]['link']),
                    'player.name': pids[playerIdx]['name'].strip()
                }

                if position in ['qb','rb','wr','te']:
                    playerProjection.offense_stats = NflOffense(
                        pass_yard = self._format_stat(_playerData.find('td', class_='pass_yd')),
                        pass_touchdown = self._format_stat(_playerData.find('td', class_='pass_td')),
                        pass_interception = self._format_stat(_playerData.find('td', class_='pass_int')),
                        rush_attempt = self._format_stat(_playerData.find('td', class_='rush_att')),
                        rush_yard = self._format_stat(_playerData.find('td', class_='rush_yd')),
                        rush_touchdown = self._format_stat(_playerData.find('td', class_='rush_td')),
                        rec_reception = self._format_stat(_playerData.find('td', class_='rec')),
                        rec_yard = self._format_stat(_playerData.find('td', class_='rec_yd')),
                        rec_touchdown = self._format_stat(_playerData.find('td', class_='rec_td')),
                        custom_data = {
                            'fanduel_cost': self._format_stat(_playerData.find('td', class_='fanduel_cost')),
                            'draftkings_cost': self._format_stat(_playerData.find('td', class_='draftkings_cost')),
                            'yahoo_cost': self._format_stat(_playerData.find('td', class_='yahoo_cost'))
                        }
                    )

                elif position in ['d','k']:
                    raise NotImplementedError()

                yield playerProjection

