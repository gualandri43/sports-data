'''
Module for scraping draftkings.com data.
'''


def build_contest_link(slateId, contestId):
    '''Build contest link URL.'''

    # example
    #   https://www.fanduel.com/games/67080/contests/67080-251503223/scoring

    return 'https://www.fanduel.com/games/{0}/contests/{0}-{1}/scoring'.format(slateId, contestId)

