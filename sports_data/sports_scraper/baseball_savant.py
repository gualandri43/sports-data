'''
    Scraper for https://baseballsavant.mlb.com/.
'''

import datetime
import io
import csv
from typing import Iterator
import requests

from sports_data.data_model.schema import MlbStatcast


class StatcastSearchRequest:
    '''
        Build a statcast search request URL based on allowed parameters.

        hfGT                Season Type. Default to R. List of [R,PO,F,D,L,W,S] -- Regular Season, Playoffs, Wildcard, Division Series, League Championship Seiers, World Series, Spring Training
        hfSea               The seasons to query. list of integers. defaults to 2020
        player_type         Player type. list of [batter, pitcher] (others available). Default to pitcher. TODO: implement other positions.
        game_date_gt        Beginning game date. a datetime.date object. defaults to None.
        game_date_lt        Ending game date. a datetime.date object. defaults to None.
        pitchers_lookup     Pitchers involved in the play. An integer MLB player ID number. Only can accept a single value currently.
        batters_lookup      Batters involved in the play. An integer MLB player ID number. Only can accept a single value currently.
        team                The team to include players for (none is all). Must be a valid MLBAM team abbreviation.
        min_pitches         Minimum number of pitches to qualify. Defaults to 0.
        min_results         Minimum number of results to qualify. Defaults to 0.
        min_pas             Minimum number of plate appearances to qualify. Defaults to 0.
        group_by            The field to group on. Defaults to name
        sort_col            The field to sort on. Defaults to pitches
        player_event_sort   The field to sort events on. Defaults to api_p_release_speed
        sort_order          The sort order. Defaults to desc

        Others (TODO: document and implement other options)
        -------
        hfPT
        hfAB
        hfPR
        hfZ
        stadium
        hfBBL
        hfNewZones
        hfPull
        hfC
        hfSit
        hfOuts
        opponent
        pitcher_throws
        batter_stands
        hfSA
        hfInfield
        position
        hfOutfield
        hfRO
        home_road
        hfFlag
        hfBBT
        metric_1
        hfInn
        type
    '''

    # # Notes on URL
        #
        # date format YYYY-MM-DD
        # %5B  [
        # %5D  ]
        # %7C  |

        #https://baseballsavant.mlb.com/statcast_search/csv?
        # all=true
        # &hfPT=
        # &hfAB=
        # &hfGT=R%7C
        # &hfPR=
        # &hfZ=
        # &stadium=
        # &hfBBL=
        # &hfNewZones=
        # &hfPull=
        # &hfC=
        # &hfSea=2020%7C2019%7C
        # &hfSit=
        # &player_type=pitcher
        # &hfOuts=
        # &opponent=
        # &pitcher_throws=
        # &batter_stands=
        # &hfSA=
        # &game_date_gt=
        # &game_date_lt=
        # &hfInfield=
        # &team=
        # &position=
        # &hfOutfield=
        # &hfRO=
        # &home_road=
        # &hfFlag=
        # &hfBBT=
        # &pitchers_lookup%5B%5D=608337
        # &batters_lookup%5B%5D=596146
        # &metric_1=
        # &hfInn=
        # &min_pitches=0
        # &min_results=0
        # &group_by=name
        # &sort_col=pitches
        # &player_event_sort=api_p_release_speed
        # &sort_order=desc
        # &min_pas=0
        # &type=details&

    queryParams = [
        'hfPT',
        'hfAB',
        'hfGT',
        'hfPR',
        'hfZ',
        'stadium',
        'hfBBL',
        'hfNewZones',
        'hfPull',
        'hfC',
        'hfSea',
        'hfSit',
        'player_type',
        'hfOuts',
        'opponent',
        'pitcher_throws',
        'batter_stands',
        'hfSA',
        'game_date_gt',
        'game_date_lt',
        'hfInfield',
        'team',
        'position',
        'hfOutfield',
        'hfRO',
        'home_road',
        'hfFlag',
        'hfBBT',
        'pitchers_lookup',
        'batters_lookup',
        'metric_1',
        'hfInn',
        'min_pitches',
        'min_results',
        'group_by',
        'sort_col',
        'player_event_sort',
        'sort_order',
        'min_pas',
        'type'
    ]

    teamCodes = [
        "LAA",
        "HOU",
        "OAK",
        "TOR",
        "ATL",
        "MIL",
        "STL",
        "CHC",
        "ARI",
        "LAD",
        "SF",
        "CLE",
        "SEA",
        "MIA",
        "NYM",
        "WSH",
        "BAL",
        "SD",
        "PHI",
        "PIT",
        "TEX",
        "TB",
        "BOS",
        "CIN",
        "COL",
        "KC",
        "DET",
        "MIN",
        "CWS",
        "NYY"
    ]


    def __init__(self, **kwargs):

        self.baseUrl = 'https://baseballsavant.mlb.com/statcast_search/csv'
        self.params = {k:None for k in self.queryParams}

        # validate any given parameters and assign defaults
        self._parameter_validation('hfGT', kwargs.get('hfGT', None), list, ['R'], allowedChoices=['R','PO','F','D','L','W','S'])
        self._parameter_validation('hfSea', kwargs.get('hfSea', None), list, [2020])
        self._parameter_validation('player_type', kwargs.get('player_type', None), str, 'pitcher', allowedChoices=['batter', 'pitcher'])
        self._parameter_validation('game_date_gt', kwargs.get('game_date_gt', None), datetime.date, None)
        self._parameter_validation('game_date_lt', kwargs.get('game_date_lt', None), datetime.date, None)
        self._parameter_validation('pitchers_lookup', kwargs.get('pitchers_lookup', None), int, None)
        self._parameter_validation('batters_lookup', kwargs.get('batters_lookup', None), int, None)
        self._parameter_validation('team', kwargs.get('team', None), str, None, allowedChoices=self.teamCodes)
        self._parameter_validation('min_pitches', kwargs.get('min_pitches', None), int, 0)
        self._parameter_validation('min_results', kwargs.get('min_results', None), int, 0)
        self._parameter_validation('min_pas', kwargs.get('min_pas', None), int, 0)
        self._parameter_validation('group_by', kwargs.get('group_by', None), str, 'name')
        self._parameter_validation('sort_col', kwargs.get('sort_col', None), str, 'pitches')
        self._parameter_validation('player_event_sort', kwargs.get('player_event_sort', None), str, 'api_p_release_speed')
        self._parameter_validation('sort_order', kwargs.get('sort_order', None), str, 'desc')


        # format parameters
        self.params['hfGT'] = '|'.join(self.params['hfGT']) + '|'
        self.params['hfSea'] = '|'.join([str(x) for x in self.params['hfSea']]) + '|'

        if self.params['game_date_gt']:
            self.params['game_date_gt'] = self.params['game_date_gt'].isoformat()

        if self.params['game_date_lt']:
            self.params['game_date_lt'] = self.params['game_date_lt'].isoformat()

        # pitchers_lookup and batters_lookup need [] added
        self.params['pitchers_lookup[]'] = self.params.pop('pitchers_lookup')
        self.params['batters_lookup[]'] = self.params.pop('batters_lookup')

        # add the always needed params
        self.params['all'] = True
        self.params['type'] = 'details'

    def _parameter_validation(self, name, newValue, expectedType, defaultValue, allowedChoices=None):
        '''
            name                name of parameter
            newValue            candidate value of new parameter
            expectedType        type check for parameter
            defaultValue        value to assign if None
            allowedChoices      list of allowed values (if given)
        '''

        if name not in self.params:
            raise ValueError('Parameter not defined in queryParams.')

        if newValue:
            # if defined, check type and allowed values

            if not isinstance(newValue, expectedType):
                raise ValueError('{0} must be of type {1}.'.format(name, expectedType))

            if allowedChoices:
                if newValue not in allowedChoices:
                    raise ValueError('{0} must be one of {1}.'.format(name, ','.join(allowedChoices)))

            self.params[name] = newValue

        else:
            # parameter is none, assign default
            self.params[name] = defaultValue

    def build_request(self) -> requests.Request:
        '''Get a requests.Request object with all parameters defined.'''

        return requests.Request(
            method='GET',
            url=self.baseUrl,
            params=self.params
        )

    def build_url(self) -> str:
        '''Build the full url based on the defined parameters.'''

        req = self.build_request()
        prepped = req.prepare()
        return prepped.url

class BaseballSavantScraper:
    '''
        Scrape data from baseballsavant.
    '''

    def parse_statcast_response(self, statcastSearchResponse: requests.Response) -> Iterator[MlbStatcast]:
        '''
            Parse the response from a completed StatcastSearchRequest.

            This function is a generator, yielding one MlbStatcast per parsed record.
        '''

        statcastSearchResponse.encoding = 'utf-8-sig'  # fixes some odd BOM utf encoding issues
        csvResponse = io.StringIO(statcastSearchResponse.text)
        reader = csv.DictReader(csvResponse)

        for _rec in reader:

            _data = dict(_rec)

            obj = MlbStatcast(
                custom_data = dict(),
                game_mlb_key = _data.pop('game_pk',None),
                game_date = _data.pop('game_date',None),
                game_season = _data.pop('game_year',None),
                game_type = _data.pop('game_type',None),
                game_event_id = _data.pop('sv_id',None),
                game_plate_appearance = _data.pop('at_bat_number',None),
                game_home_team =_data.pop('home_team',None),
                game_away_team =_data.pop('away_team',None),
                plate_appearance_pitch_number =_data.pop('pitch_number',None),
                plate_appearance_batter_id =_data.pop('batter',None),
                plate_appearance_pitcher_id =_data.pop('pitcher',None),
                plate_appearance_inning =_data.pop('inning',None),
                plate_appearance_inning_topbot =_data.pop('inning_topbot',None),
                event_fielder_2 =_data.pop('fielder_2',None),
                event_fielder_3 =_data.pop('fielder_3',None),
                event_fielder_4 =_data.pop('fielder_4',None),
                event_fielder_5 =_data.pop('fielder_5',None),
                event_fielder_6 =_data.pop('fielder_6',None),
                event_fielder_7 =_data.pop('fielder_7',None),
                event_fielder_8 =_data.pop('fielder_8',None),
                event_fielder_9 =_data.pop('fielder_9',None),
                event_if_fielding_alignment =_data.pop('if_fielding_alignment',None),
                event_of_fielding_alignment =_data.pop('of_fielding_alignment',None),
                event_runner_on_3b =_data.pop('on_3b',None),
                event_runner_on_2b =_data.pop('on_2b',None),
                event_runner_on_1b =_data.pop('on_1b',None),
                event_balls_pre =_data.pop('balls',None),
                event_strikes_pre =_data.pop('strikes',None),
                event_outs_pre =_data.pop('outs_when_up',None),
                event_home_score_pre =_data.pop('home_score',None),
                event_away_score_pre =_data.pop('away_score',None),
                event_bat_score_pre =_data.pop('bat_score',None),
                event_fld_score_pre =_data.pop('fld_score',None),
                event_home_score_post =_data.pop('post_home_score',None),
                event_away_score_post =_data.pop('post_away_score',None),
                event_bat_score_post =_data.pop('post_bat_score',None),
                event_fld_score_post =_data.pop('post_fld_score',None),
                event_pitcher_throws =_data.pop('p_throws',None),
                pitch_type =_data.pop('pitch_type',None),
                pitch_name =_data.pop('pitch_name',None),
                pitch_release_speed =_data.pop('release_speed',None),
                pitch_release_pos_x =_data.pop('release_pos_x',None),
                pitch_release_pos_y =_data.pop('release_pos_y',None),
                pitch_release_pos_z =_data.pop('release_pos_z',None),
                pitch_release_spin_rate =_data.pop('release_spin_rate',None),
                pitch_release_extension =_data.pop('release_extension',None),
                pitch_spin_axis =_data.pop('spin_axis',None),
                pitch_pfx_x =_data.pop('pfx_x',None),
                pitch_pfx_z =_data.pop('pfx_z',None),
                pitch_plate_x =_data.pop('plate_x',None),
                pitch_plate_z =_data.pop('plate_z',None),
                pitch_sz_top =_data.pop('sz_top',None),
                pitch_sz_bot =_data.pop('sz_bot',None),
                pitch_zone_location =_data.pop('zone',None),
                pitch_vx0 =_data.pop('vx0',None),
                pitch_vy0 =_data.pop('vy0',None),
                pitch_vz0 =_data.pop('vz0',None),
                pitch_ax =_data.pop('ax',None),
                pitch_ay =_data.pop('ay',None),
                pitch_az =_data.pop('az',None),
                event_batter_side =_data.pop('stand',None),
                result_pitch_description =_data.pop('description',None),
                result_plate_appearance_description =_data.pop('des',None),
                result_event =_data.pop('events',None),
                result_pitch_type =_data.pop('type',None),
                result_hit_location =_data.pop('hit_location',None),
                result_batted_ball_type =_data.pop('bb_type',None),
                result_batted_ball_hit_coord_x =_data.pop('hc_x',None),
                result_batted_ball_hit_coord_y =_data.pop('hc_y',None),
                result_projected_hit_distance =_data.pop('hit_distance_sc',None),
                result_launch_speed =_data.pop('launch_speed',None),
                result_launch_angle =_data.pop('launch_angle',None),
                result_effective_speed =_data.pop('effective_speed',None),
                result_estimated_batting_avg =_data.pop('estimated_ba_using_speedangle',None),
                result_estimated_woba =_data.pop('estimated_woba_using_speedangle',None),
                result_woba =_data.pop('woba_value',None),
                result_woba_denom =_data.pop('woba_denom',None),
                result_babip =_data.pop('babip_value',None),
                result_iso =_data.pop('iso_value',None),
                result_batted_ball_category =_data.pop('launch_speed_angle',None),
                result_delta_home_win_exp =_data.pop('delta_home_win_exp',None),
                result_delta_run_exp =_data.pop('delta_run_exp',None)
            )

            # put any leftover in custom_data dict
            obj.custom_data.update(_data)

            yield obj

