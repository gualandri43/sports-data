#'http://www.dfsgold.com/nba/draftkings-daily-fantasy-recap-jan-31-2017'

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import json
import pandas as pd
import numpy as np
import datetime as dt
import time
import pytz
import random
from selenium import webdriver
import sportsdbInterface as db
from sports_data.common.common_funcs import isNumber
from sports_data.sportsScraper.common import GetURLtext, CleanUnicodeText, parseStandardHTMLTable


class MySportsFeeds_scraper:
    '''Class to handle requests and downloads from the MySportsFeeds API.

    Attributes:
        sport: The sport the scraper is targeting.
        _username: Internal variable holding the username credential for API access.
        _password: Internal variable holding the password credential for API access.
    '''

    def __init__(self,sport):
        '''Constructor.

        Args:
            sport (str): Sport to request data for. Options are 'nfl','mlb','nba','nhl'.
        '''

        self.sport = sport

        self._username = None
        self._password = None

    def setup(self):
        '''Set up class to initialize, load credentials.'''
        return

    def _SendRequest(self,url,params):
        '''Sends request to API and returns the dictionary result

        Args:
            url (str): API endpoint to send request to.
            params (dict): Dictionary of request parameters to send.

        Returns:
            If successful, returns the dictionary with the results of the request.
        '''

        return

    def UpdatePlayers(self):
        '''Updates database with the latest player information.'''
        return

class ProReferenceBase:
    #class serves as a common base for the sport-reference sites such as baseball-reference and pro-football-reference

    def TableParse(self,body_html,includeTH=True):
        #parses a typical data table
        #the body_html input is a beautiful soup object of the tbody element
        #uses the 'data-stat' attribute as the returned header label
        #the data stat is included inside the th or td tag in the table body
        #each entry is a dictionary with val and link keys
        #returns a dataframe of the results

        rowList = []

        rows = body_html.findAll('tr')
        for r in rows:
            rowDict = {}

            #check for in-row th tags
            if includeTH:
                for h in r.findAll('th'):

                    if h.has_attr('data-stat'):
                        rowDict[h['data-stat']] = {}
                        rowDict[h['data-stat']]['val'] = h.text

                    for a in h.findAll('a'):
                        if a.has_attr('href'):
                            rowDict[h['data-stat']]['link'] = a['href']
                            rowDict[h['data-stat']]['val'] = a.text

            #check for in row td tags
            for d in r.findAll('td'):

                if d.has_attr('data-stat'):
                    rowDict[d['data-stat']] = {}
                    rowDict[d['data-stat']]['val'] = d.text


                    for a in d.findAll('a'):
                        if a.has_attr('href'):
                            rowDict[d['data-stat']]['link'] = a['href']
                            rowDict[d['data-stat']]['val'] = a.text

            if len(rowDict) > 0:
                rowList.append(rowDict)

        #create data frame
        df = pd.DataFrame(rowList)

        return df

class proFootballReference_scraper(ProReferenceBase):
    #scraper to get nfl data from http://www.pro-football-reference.com
    #inherits from ProReferenceBase
    #Pro Football Reference is source ID: 1

    def __init__(self):

        #get a database connection
        self.db = db.dbInterface()

    def RefreshTeamList(self):
        #routine fetches all the NFL teams from source and adds or updates them in the database
        #tables affected: nfl.teams, nfl.id_conversion_team

        #get table of
        url = 'http://www.pro-football-reference.com/teams'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        teamTable = pg.find("table",{"id":"teams_active"})
        body = teamTable.find("tbody")

        #get data frame of table data
        tms = self.TableParse(body)

        #get team names and PFR team ids
        #ids are held in the link
        #data variable team_name is the PFR id variable
        #only teams with link in the entry are active teams
        teamList = []
        for i,t in tms['team_name'].iteritems():
            if 'link' in t:
                tmID = t['link'].split('/')[2]
                teamList.append((tmID,t['val']))  #(team id, team name) pair


        #check if there is a team id conversion present and update team name
        #if not, add new entry to teams list and record conversion
        #for t in teamList:
        #	if db.session.query(db.nfl.id_conversion_team).filter(db.nfl.id_conversion_team.val==t[0]) is None:

        return

class baseballReference_scraper(ProReferenceBase):
    """Scraper to get mlb data from http://www.baseball-reference.com/. Inherits from ProReferenceBase.

    Baseball Reference is source ID: 2

    Attributes:
        db (dbInterface): dbInterface class object representing the connection to the sportsdb.
        source_id (int): ID number for the source in the database.
        players: Holds scraped player information. Typically a dictionary.
        games: Holds scraped game information.
    """

    def __init__(self):

        #get a database connection
        self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')
        #self.db = db.dbInterface()
        self.source_id = 2

        self.players = None
        self.games = None

    def _ConvertProblematicTeams(self,tm):
        """Some teams have multiple IDs. This function converts the baseball reference team ids to the chosen ID.

        Args:
            tm (str): Team abbreviation to convert.

        Returns:
            (str): Abbreviation used for that team in the local database.

        """

        convTeams = {}
        convTeams['MLN'] = 'ATL'
        convTeams['BSN'] = 'ATL'
        convTeams['SLB'] = 'BAL'
        convTeams['MLA'] = 'BAL'
        convTeams['ANA'] = 'LAA'
        convTeams['CAL'] = 'LAA'
        convTeams['BRO'] = 'LAD'
        convTeams['FLA'] = 'MIA'
        convTeams['SEP'] = 'MIL'
        convTeams['WSH'] = 'MIN'
        convTeams['KCA'] = 'OAK'
        convTeams['PHA'] = 'OAK'
        convTeams['NYG'] = 'SFG'
        convTeams['TBD'] = 'TBR'
        convTeams['WSA'] = 'TEX'
        convTeams['MON'] = 'WSN'

        if tm in convTeams:
            return convTeams[tm]
        else:
            return tm

    def _ConvertPositions(self,pos):
        """Converts the positions to the database format. Acceptable conversions:

            - Catcher: C
            - First Baseman: 1B
            - Second Baseman: 2B
            - Third Baseman: 3B
            - Shortstop: SS
            - Rightfielder: RF, OF
            - Leftfielder: LF, OF
            - Centerfielder: CF, OF
            - Pitcher: P

        Args:
            pos (str): Baseball Reference position string.

        Returns:
            Database positions abbreviation. Returns None if the position string is not defined here.

        """

        posConv = {}
        posConv['Catcher'] = 'C'
        posConv['First Baseman'] = '1B'
        posConv['Second Baseman'] = '2B'
        posConv['Third Baseman'] = '3B'
        posConv['Shortstop'] = 'SS'
        posConv['Rightfielder'] = 'RF'
        posConv['Leftfielder'] = 'LF'
        posConv['Centerfielder'] = 'CF'
        posConv['Outfielder'] = 'OF'
        posConv['Pitcher'] = 'P'
        posConv['Designated Hitter'] = 'DH'

        if pos in posConv:
            return posConv[pos]
        else:
            return None

    def _ParseTeamID(self,lnk):
        """Parses the team ID out of the link url

        Args:
            lnk (str): Link that contains the team ID.

        Example link: /teams/ARI/2014.shtml
        """

        tmpStr = str(lnk)
        return tmpStr.split('/')[2]

    def _ParseGameID(self,lnk):
        """Parses the game ID out of the link url (for the boxscore)

        Args:
            lnk (str): Link that contains the game ID.

        Example link: /boxes/ARI/ARI201403230.shtml
        """

        tmpStr = str(lnk)
        return tmpStr.split('/')[3].replace('.shtml','')

    def RefreshTeamList(self):
        #routine fetches all the MLB teams from source and adds or updates them in the database
        #tables affected: mlb.teams, mlb.id_conversion_team

        #get table of
        url = 'http://www.baseball-reference.com/teams/'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        teamTable = pg.find("table",{"id":"teams_active"})
        body = teamTable.find("tbody")

        #get data frame of table data
        tms = self.TableParse(body)

        #get team names and BR team ids
        #ids are held in the link
        #data variable franchise_name is the BR id variable
        #only teams with link in the entry are active teams, other rows are extraneous
        teamList = []
        for i,t in tms['franchise_name'].iteritems():
            if 'link' in t:
                tmID = self._ParseTeamID(t['link'])
                teamList.append((tmID,t['val']))  #(team id, team name) pair

        #call database routine to add or update the team entry.
        for t in teamList:
            self.db.AddOrUpdateTeam('mlb',t[0],self.source_id,self.db.mlb.Teams(teamname=t[1],abbrev=t[0].lower()))

    def _GetOnlinePlayerList(self):
        """Populates players dictionary with the baseball reference player IDs from the web. Retrieves all players on the pages. Key is the BR source ID."""

        #start with fresh player list
        self.players = {}

        #alphabet letters
        alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

        #iterate through all lists of players
        for ltr in alpha:

            #get table of players (players starting with b have a link like http://www.baseball-reference.com/players/b)
            url = 'http://www.baseball-reference.com/players/' + ltr
            pg = BeautifulSoup(GetURLtext(url))

            #get list of players
            playerTable = pg.find("div",{"id":"div_players_"})

            #loop through entries
            #a bolded player is an active player
            for p in playerTable.findAll('p'):
                isActive = False

                boldSearch = p.findAll('b')
                if len(boldSearch) > 0:
                    #active player. example: <p><b><a href="/players/b/baezja01.shtml">Javier Baez</a>  (2014-2016)</b></p>
                    for ap in boldSearch:
                        for info in ap.findAll('a'):
                            #player id
                            srcID = info['href'].split('/')[3].replace('.shtml','').strip()

                            #player name
                            pName = info.text

                            #player is active
                            isActive = True

                else:
                    #inactive player. example <p><a href="/players/b/backmle01.shtml">Les Backman</a>  (1909-1910)</p>
                    for info in p.findAll('a'):
                        #player id
                        srcID = info['href'].split('/')[3].replace('.shtml','').strip()

                        #player name
                        pName = info.text

                        #player is active
                        isActive = False


                #store in dictionary
                if srcID not in self.players:
                    self.players[srcID] = {}

                self.players[srcID]['fullname'] = pName
                self.players[srcID]['firstname'] = pName.split(' ')[0].strip()
                self.players[srcID]['lastname'] = ''.join(pName.split(' ')[1:]).strip()
                self.players[srcID]['is_active'] = isActive

    def _GetPlayerDetails(self,pID):
        """Gets a single player's information details from the web. Adds the info to that players dictionary entry in 'players'

        Args:
            pID (str): Baseball reference player ID to download data from.
        """

        print('Downloading id: ' + str(pID))

        #retrieve player web page
        url = 'http://www.baseball-reference.com/players/' + pID[0] + '/' + pID + '.shtml'
        pg = BeautifulSoup(GetURLtext(url))

        playerDetail = pg.find("div",{"class":"players"})

        if 'positions' not in self.players[pID]:
            self.players[pID]['positions'] = []

        if 'birthdate' not in self.players[pID]:
            self.players[pID]['birthdate'] = None

        if 'team' not in self.players[pID]:
            self.players[pID]['team'] = None

        if 'bats' not in self.players[pID]:
            self.players[pID]['bats'] = None

        if 'throws' not in self.players[pID]:
            self.players[pID]['throws'] = None

        for p in playerDetail.findAll('p'):

            for t in p.findAll('strong'):

                #positions	<p><strong>Positions:</strong>First Baseman, Third Baseman and Leftfielder</p>
                if t.text.strip() == 'Position:' or t.text.strip() == 'Positions:':
                    tmpTxt = p.text.replace('and',',')
                    tmpTxt = tmpTxt.replace('Positions:','').strip()
                    tmpTxt = tmpTxt.replace('Position:','').strip()

                    for iterPos in tmpTxt.split(','):
                        self.players[pID]['positions'].append(self._ConvertPositions(iterPos.strip()))


                #team   <p><strong>Team:</strong><a href="/teams/DET/2016.shtml">Detroit Tigers</a>  (40-man)</p>
                if t.text.strip() == 'Team:':
                    for a in p.findAll('a'):
                        tmID = a['href'].split('/')[2].strip()

                    self.players[pID]['team'] = self._ConvertProblematicTeams(CleanUnicodeText(tmID))  #source teamID

                #bats   <p><strong>Bats: </strong>Right&nbsp;&bull;&nbsp;<strong>Throws: </strong>Right</p>
                if t.text.strip() == 'Bats:':

                    self.players[pID]['bats'] = CleanUnicodeText(p.contents[2])  #had to encode and decode to get rid of extra characters.
                    self.players[pID]['throws'] = CleanUnicodeText(p.contents[4])


            #birthdate <p><strong><a href="/bio/">Born:</a></strong><span itemprop="birthDate" id="necro-birth" data-birth="1983-04-18"><a href="/friv/birthdays.cgi?month=4&day=18">April 18</a>, <a href="/leagues/MLB/1983-births.shtml">1983</a></span><span itemprop="birthPlace">in&nbsp;Maracay,&nbsp;Venezuela</span><span class="f-i f-ve">ve</span></p>
            for bd in p.findAll('span'):
                if bd.has_attr('data-birth'):
                    self.players[pID]['birthdate'] = dt.datetime.strptime(CleanUnicodeText(bd['data-birth']),'%Y-%m-%d')

    def _AddPlayerInfoToDatabase(self,pID):
            """Adds player info to the sports database from the 'players' dictionary entry for the given pID.

            Args:
                pID (str): ID in the 'players' dictionary to add data for. Usually the baseball reference player ID.
            """

            #get player info sub dictionary for the given pID
            pInfo = self.players[pID]

            #create player
            self.db.AddOrUpdatePlayer('mlb',pID,self.source_id,self.db.mlb.Players(fullname=pInfo['fullname'],
                                                                                        firstname=pInfo['firstname'],
                                                                                        lastname=pInfo['lastname'],
                                                                                        is_active=pInfo['is_active'],
                                                                                        bats=pInfo['bats'],
                                                                                        throws=pInfo['throws'],
                                                                                        birthdate=pInfo['birthdate']))

            #player position updates
            if not self.db.AddOrUpdatePlayerPos('mlb', pID, self.source_id, pInfo['positions']):
                print('**PlayerPos Error**: ' + str(sID) + ' positions did not update.')

            #player team updates
            if not self.db.AddOrUpdatePlayerTeam('mlb', pID, self.source_id, pInfo['team']):
                print('**PlayerTeam Error**: ' + str(sID) + ' team did not update.')

    def RefreshPlayers(self,onlyActive=False):
        """Routine fetches the MLB players from source and adds or updates them in the database.

        If the onlyActive flag is true, the basic info (name, active status) is still updated in the database. This keeps the active players current but prevents
        unnecessary web requests.

        Tables affected: mlb.players, mlb.id_conversion

        Args:
            onlyActive (bool): Flag. Indicates whether to download information about all players, or just the ones marked active on baseball reference's page.
            Defaults to False (update all players).

        """

        #get list of online players. exit function if exception is raised.
        try:
            self._GetOnlinePlayerList()
        except Exception as e:
            print('Error ocurred while getting baseball reference players: ' + str(e))
            return None

        #get individual detailed information about the players
        isProcPlayer = False
        for sID in self.players:

            #determine whether or not the player detail should be updated
            if onlyActive:
                if self.players[sID]['is_active']:
                    isProcPlayer = True
                else:
                    isProcPlayer = False
            else:
                isProcPlayer = True

            #process player detail if needed
            if isProcPlayer:

                #add delay so website doesn't get hammered with requests
                time.sleep(0.8)

                try:
                    #get player details
                    self._GetPlayerDetails(sID)

                except Exception as e:
                    print('Error ocurred while downloading details for baseball reference player ID ' + sID + ': ' + str(e))


            #put database adding outside the conditional so that players that are no longer active get updated as such in the database.
            try:

                #add player information to the database
                self._AddPlayerInfoToDatabase(sID)

            except Exception as e:
                print('Error ocurred while adding baseball reference player ID ' + sID + ' to database: ' + str(e))

    def _GetOnlineGameList(self,year):
        """Gets list of games for the given year from online and adds them to the games dictionary.

        Args:
            year (int): Year to download the games for.
        """

        #get list of game IDs
        url = 'http://www.baseball-reference.com/leagues/MLB/' + int(year) + '-schedule.shtml'
        pg = BeautifulSoup(GetURLtext(url))

        #loop through tags of a that are game class to get the list of game ids
        for d in pg.findAll('div'):
            game_date = None
            date_text = None

            #get date text from group heading (<h3>Sunday, May 7, 2017</h3>)
            for h in d.findAll('h3'):
                date_text = h.text.strip()

            #dig into each game info
            for p in pg.findAll("p",attrs={"class":"game"}):
                teamList = []
                game_id = None
                home_team = None
                away_team = None
                hasBoxscore = False
                time_zone = None
                game_time = None

                #unplayed games have a time and timezone in a span tag. (<span tz="E"><strong>1:10 pm</strong></span>)
                for s in p.findAll('span'):
                     time_zone = s['tz'].strip()
                     game_time = s.text

                #other info in <a> tags
                for a in p.findAll('a'):
                    if a.has_attr('href'):

                        #link is for a team (href="/teams/LAD/2014.shtml")
                        if str(a['href']).find('teams') != -1:
                            teamList.append(self._ParseTeamID(a['href']))

                        #link is for a the game id (href="/boxes/ARI/ARI201403230.shtml")
                        if str(a['href']).find('boxes') != -1 and str(a['href']).find('.shtml') != -1:
                            game_id = self._ParseGameID(a['href'])
                            hasBoxscore = True

                        #link is for a the date (href="/boxes/?date=2014-03-23")
                        if str(a['href']).find('boxes') != -1 and str(a['href']).find('date') != -1:
                            game_date = a['href'].split('?')[1].replace('date=','')
                            hasBoxscore = True


                #assign home and away teams (home team is the first 3 letters of the game id if box score is available)
                #otherwise default to order being listed as away team @ home team
                for t in teamList:

                    if hasBoxscore:
                        #use game ID
                        if t.upper() == game_id[:3].upper():
                            home_team = t
                        else:
                            away_team = t
                    else:
                        #assign best guess at the game ID and home/away
                        #format: <home team id><date in yyyymmdd><0 for single game, 1 for first game of dh, 2 for second game of dh>

                        away_Team = teamList[0]
                        home_team = teamList[1]
                        tmpDate = dt.datetime.strptime(date_text, '%A, %b %d, %Y')
                        game_id = home_team.upper() + tmpDate.strftime('%Y%m%d')

                        #check for a double header situation. if none, add a 0 on the end
                        chkID = game_id + '0'
                        if chkID in self.games:
                            #make this game game 2
                            game_id = game_id + '2'

                            #change the existing dict entry to a 1
                            self.games[game_id + '1'] = self.games.pop(chkID)
                            self.games[game_id + '1'][chkID] = game_id + '1'

                        else:
                            #add the zero to game_id
                            game_id = game_id + '0'

                #add to games storage
                if game_id not in self.games:
                    self.games[game_id] = {}

                self.games[game_id]['game_id'] = game_id
                self.games[game_id]['home_team'] = home_team
                self.games[game_id]['away_team'] = away_team
                self.games[game_id]['hasBoxScore'] = hasBoxscore

                #set date based on what is available
                if date_text is None:
                    #a game date was found (already played games). date is stored and time info is in the boxscore.
                    self.games[game_id]['game_date'] = game_date

                else:
                    #only the heading text and separate time are available. create the game date from these.

                    #format a temporary string with the time included to parse all in one shot
                    tmpDate = date_text + ', ' + game_time
                    game_date = dt.datetime.strptime(tmpDate, '%A, %b %d, %Y, %H:%M %p')

                    if time_zone == 'E':
                        #options: 'US/Eastern', 'US/Central', 'US/Mountain', 'US/Pacific'
                        game_date = pytz.timezone('US/Eastern').localize(game_date)

                    self.games[game_id]['game_date'] = game_date

    def _GetGameDetails(self,gmID):
        """Gets the details of a given game from online by parsing the box score page. Updates info in the games dictionary.

        Games in the future do not have a box score page. This routine just adds None to the dictionary entries if this is the case.

        Args:
            gmID (str): Baseball reference ID of the game to get details for.
        """

        box_date = None
        box_time = None
        box_attendance = None
        box_venue = None
        box_duration = None
        box_surface = None

        if games[gID]['hasBoxScore']:
            #parse box info
            urlBox = 'http://www.baseball-reference.com/boxes/'+ home_team + '/'+ gmID + '.shtml'
            pgBox = BeautifulSoup(GetURLtext(urlBox))



            #parse meta data
            for scm in pgBox.findAll('div',attrs={"class":"scorebox_meta"}):
                for dv in scm.findAll('div'):

                    if dv.text.find('Start Time') != -1:
                        box_time = dv.text.replace('Start Time:','').replace('Local','').strip()

                    elif dv.text.find('Attendance:') != -1:
                        box_attendance = dv.text.replace('Attendance:','').strip()

                    elif dv.text.find('Venue:') != -1:
                        box_venue = dv.text.replace('Venue:','').strip()

                    elif dv.text.find('Game Duration:') != -1:
                        box_duration = dv.text.replace('Game Duration:','').strip()

                    elif dv.text.find('Game, on') != -1:
                        box_surface = dv.text.split(',')[1].replace('on','').strip()

                    else:
                        #date in Saturday, March 22, 2014 form
                        box_date = dv.text.strip()

            #sort out info
            tmpDate = games[gmID]['game_date'].strftime('%Y-%m-%d') + ', ' + box_time
            games[gmID]['game_date'] = dt.datetime.strptime(tmpDate,'%Y-%m-%d, %H:%M %p')  #leave timezone blank if unknown, will clean up later

        #add info to dictionary
        games[gmID]['attendance'] = box_attendance
        games[gmID]['venue'] = box_venue
        games[gmID]['duration'] = box_duration
        games[gmID]['surface'] = box_surface

    def _AddGameInfoToDatabase(self,gmID):
        """Adds the game info for the given game to the sportsdb.

        Args:
            gmID (str): Baseball reference ID of the game to get details for.
        """

        #Note: there should be a field to mark whether time is local, a specific time zone, or utc. functions will clean up local times later

        #convert home team to internal ID
        hmConv = self.db.GetTeamConversion('mlb',games[gmID]['home_team'],self.source_id)
        if hmConv is None:
            newTeam = self.db.mlb.Teams(teamname=games[gmID]['home_team'])
            hmTeam = self.db.AddOrUpdateTeam('mlb',games[gmID]['home_team'],self.source_id,newTeam)
            homeID = hmTeam.team_id
        else:
            homeID = hmConv.team_id

        #convert away team to internal ID
        awayConv = self.db.GetTeamConversion('mlb',games[gmID]['away_team'],self.source_id)
        if awayConv is None:
            newTeam = self.db.mlb.Teams(teamname=games[gmID]['away_team'])
            awayTeam = self.db.AddOrUpdateTeam('mlb',games[gmID]['away_team'],self.source_id,newTeam)
            awayID = awayTeam.team_id
        else:
            awayID = awayConv.team_id

        #deal with game time
        if games[gmID]['game_date'].tzname() is None:
            #timezone not defined, means that it is unknown and labeled as local time
            strTimeZone = 'local'
            storeDate = games[gmID]['game_date']

        if games[gmID]['game_date'].tzname() == 'UTC':
            #timezone is UTC, label as such and add time unaltered
            strTimeZone = 'utc'
            storeDate = games[gmID]['game_date']

        if games[gmID]['game_date'].tzname() != 'UTC' and games[gmID]['game_date'].tzname() is not None:
            #timezone is something other than utc, convert to utc
            strTimeZone = 'utc'
            storeDate = games[gmID]['game_date'].astimezone(pytz.utc)


        #add stadium if not found
        if games[gmID]['venue'] is None:
            stadID = None
        else:
            stadConv = self.db.GetStadiumConversion('mlb',games[gmID]['venue'],self.source_id)
            if stadConv is None:
                newStad = self.db.mlb.Stadiums(stadium=games[gmID]['venue'])
                newStad = self.db.AddOrUpdateStadium('mlb',games[gmID]['venue'],self.source_id,newStad)
                stadID = newStad.stadium_id
            else:
                stadID = stadConv.stadium_id

        #add game, remove tz info from date
        newGame = self.db.mlb.Schedule(home_team= homeID,
                                        away_team= awayID,
                                        stadium_id= stadID,
                                        time= storeDate.replace(tzinfo=None),
                                        is_season= False,
                                        tz=strTimeZone)

        newGame = self.db.AddOrUpdateGame('mlb',games[gmID]['game_id'],self.source_id,newGame)

    def GetSchedule(self,year):
        """Downloads and updates game schedule for the given year.

        Args:
            year (int): Year to download the schedule for.


            - List of games at http://www.baseball-reference.com/leagues/MLB/2014-schedule.shtml
            - Specific game box score http://www.baseball-reference.com/boxes/ARI/ARI201403220.shtml
                - Game IDs are typically the home team and date and an additional number for double headers
            - Games that are not yet played do not have a box score and have a slightly different schedule page structure
        """

        #get a fresh games dictionary.
        self.games = {}

        #get games for the year
        self._GetOnlineGameList(year)

        #get more details about each game (box score)
        for gID in games:
            self._GetGameDetails(gID)

        #add info to database
        for gID in games:
            self._AddGameInfoToDatabase(gID)

    def _GetHitterGameLog(self,pID):
        """Download a batter's game log stats

        Args:
            pID (str): Baseball reference player ID to download the game log from.
        """

        #get table
        url = 'http://www.baseball-reference.com/players/gl.fcgi?id=heywaja01&t=b&year=2016'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        logTable = pg.find("table",{"id":"batting_gamelogs"})
        body = logTable.find("tbody")

        #get data frame of table data
        log = self.TableParse(body)


        return

    def _GetPitcherGameLog(self,pID):
        """Download a pitchers's game log stats

        Args:
            pID (str): Baseball reference player ID to download the game log from.
        """

        return

class fantasyProsBase:
    '''Base class for the fantasypros.com scrapers.

    Attributes:
        source_ids (dict): Dictionary defining the source IDs that are for each source scraped from this site.
        sport (str): Sport the scraper is for.
        players: Holds scraped projection data.
        db: Instance of dbInterface class defining the connection to the database.

    '''

    def __init__(self,sport,week='draft',isTesting=False):
        '''Constructor.

        Args:
            sport (str): Sport the scraper is for. Options are ['mlb','nfl'].
            week: Optional. Define the week to download stats for. Defaults to 'draft' which means pre-draft full season.
            isTesting (bool): Optional flag. Set to true if it is desired to use the testing database.
        '''

        self.source_ids = {}
        self.sport = sport
        self.week = week
        self.players = None
        self.browser = None

        #if isTesting:
        #	self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')
        #else:
        #	self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb')

    def _CleanStat(self,stat):
        """Cleans the stats if not a number (-).

        Args:
            stat (value): Stat to clean.

        Returns:
            num: Returns a number (float or int) or None.
        """

        if isNumber(stat):
            return stat
        else:
            return None

    def _LogIn(self):
        '''Log in to the fantasy pros website using selenium. Returns True if login is successful.'''

        self.browser = webdriver.Firefox()

        #log in
        url = 'https://secure.fantasypros.com/accounts/login/'
        self.browser.get(url)

        userbox = self.browser.find_element_by_id('id_username')
        passbox = self.browser.find_element_by_id('id_password')

        if userbox is None or passbox is None:
            return False

        userbox.send_keys('gualandri43@gmail.com')
        passbox.send_keys('dtg1989')

        time.sleep(0.5)

        loginBtn = self.browser.find_element_by_xpath("//*[contains(text(),'LOG IN')]")
        loginBtn.click()

    def _Close(self):
        '''Cleans up browser session.'''

        if self.browser is not None:
            self.browser.quit()

    def _DownloadProjections(self,pos,cols,week,sources=None):
        '''Method to download consensus projections. Must use selenium to download

        Download url is formulated as https://www.fantasypros.com/<sport>/projections/<pos>.php?week=draft

        Args:
            pos (str): Position to download.
            cols: List of strings defining what the scraped column names on the web page should be. First column must be 'Player'
            week: Defines the week to download projections for. "draft" is for the pre-season projections.
            sources: Optional. List of integers representing the FantasyPros source ID. Defaults to downloading the full consensus projections.

        Return:
            Pandas dataframe with projections.
        '''

        #build url
        if sources is None:
            url = 'https://www.fantasypros.com/{0}/projections/{1}.php?week=draft'.format(self.sport,pos)
        else:
            url = 'https://www.fantasypros.com/{0}/projections/{1}.php?filters={2}&week=draft'.format(self.sport,pos,':'.join([str(x) for x in sources]))

        #get page html
        attempts = 0
        #while attempts < 3:
            #try:
        if self.browser is None:
            resp = urlopen(url)
            pg_html = resp.read()
        else:
            self.browser.get(url)
            pg_html = self.browser.page_source
            #except:
                #attempts +=1


        #parse html
        pg = BeautifulSoup(pg_html,"lxml")
        tbl = pg.find('table',id='data')	#projection data is in 'data' table
        tbl_string = str(tbl)

        #extract player ID and name info from Player column. Extracts to format <player id>:<player name>
        tbl_string = re.sub('<a.*?class="fp-player-link fp-id-(.*?)" fp-player-name="(.*?)".*?<\/a>', '\\1:\\2', tbl_string)

        #table will be the only one. skip the first header row, which is the spanned categories. Use the 2nd row as header (now the 1st after skip)
        #table headers don't seem to be consistent, so implemented a way to have it test until it finds correct header row.
        test_col = ''
        skipStart = 3
        while test_col.lower() != 'player':

            df = pd.read_html(tbl_string,skiprows=skipStart,header=0)[0]
            test_col = df.columns[0]
            skipStart -= 1

        df.columns = cols

        #expand player column
        expanded = df['Player'].str.split(":",expand=True)
        expanded.columns = ['pID','pName']

        #combine back into projection table
        proj = pd.merge(df,expanded,left_index=True,right_index=True)

        #add the source combination as a column
        if sources is None:
            proj = proj.set_index('pID')
        else:
            #proj['src'] = ':'.join([str(x) for x in sources])
            proj['src'] = ':'.join([str(x) for x in list(set(sources))])
            proj = proj.set_index(['pID','src'])

        proj['pos'] = str(pos)

        return proj

    def _RecoverSourceProjections(self,projData):
        '''Recovers individual source projections by solving a system of equations. Assumes that the consensus value is a result
        of the straight average of individual source components. Must have more that 2 different sources to provide a meaningful answer.

        This function evaluates one statistic at a time.

        Solves the system of equations. The first source is used in every line to prevent a singular coefficient matrix.

            0.5*src1 + 0.5*src2 = b1
            0.5*src1 + 0.5*src3 = b2
            0.5*src1 + 0.5*src4 = b3
            0.25*src1 + 0.25*src2 + 0.25*src3 + 0.25*src4 = b4

        The final version of this function uses all available source pairings, resulting in num equations > num unknowns. This is an overdetermined system,
        so np.linalg.lstsq() is used. This uses a least squares algorithmn to approximate the solution. The A matrix in this instance will result in a deterministic
        solution, since the extra equations are linear combinations of each other.

        Args:
            projData: input projection data for each the combination of sources for a given stat. Is a pandas series indexed by the
            source combination of form <src1>:<src2>.

        Returns:
            A pandas series with the recovered stats, indexed by source number.

        '''

        #get list of sources from the entries in the input data
        #only use pairs of sources for this routine
        src_list = []
        for comb in projData.index.tolist():
            splitSrc = comb.split(':')

            #if len(splitSrc) == 2:
            for src in splitSrc:
                src_list.append(int(src))

        srcs = np.array(src_list)
        srcs = np.unique(srcs)			#want unique values
        srcs.sort()						#sort in ascending order

        #return zeros if all inputs are zero
        if (projData < 0.0001).all():
            return pd.Series(np.zeros(srcs.size),index=srcs)

        #build A and B matrix
        A = np.zeros((projData.index.size,srcs.size))
        B = np.zeros((projData.index.size,1))

        rowCount = 0

        for idx in projData.index:

            idxSources = idx.split(':')
            for s in idxSources:

                A[rowCount,np.where(srcs == int(s))] = 1/len(idxSources)

            B[rowCount,0] = projData[idx]

            rowCount += 1

        #solve system and organize answers
        #will be an array of len(B)
        #use lstsq instead of solve, since the system will be over-determined
        sol = np.linalg.lstsq(A,B)

        retSeries = pd.Series(sol[0].flatten(),index=srcs)

        return retSeries

class fantasyProsNFL_scraper(fantasyProsBase):
    '''Class for nfl scraping from Fantasypros.com.

    Site source numbers: 11,71,73,120,152
                       : CBS,ESPN,numberFire,STATS,FFToday
    '''

    def __init__(self,week='draft',isTesting=False):
        '''Constructor.'''

        self.srcs = [11,71,73,120,152]
        self.positions = ['qb','rb','wr','te','k','dst']
        self.columns = {
                        'qb':['Player','pAtt','pCmp','pYd','pTd','pInt','rshAtt','rshYd','rshTd','fum','fPts'],
                        'rb':['Player','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','fPts'],
                        'wr':['Player','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','fPts'],
                        'te':['Player','recAtt','recYd','recTd','fum','fPts'],
                        'k':['Player','kFg','kFga','kXpt','fPts'],
                        'dst':['Player','defSack','defInt','defFr','defFf','defTd','defAssist','defSaf','defPa','defYdAgst','fPts']
                        }
        self.statColumns = ['pAtt','pCmp','pYd','pTd','pInt','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','kFg','kFga','kXpt',
                            'defSack','defInt','defFr','defFf','defTd','defAssist','defSaf','defPa','defYdAgst']
        self.scraped = None
        self.projs = None

        super(fantasyProsNFL_scraper, self).__init__('nfl',week=week,isTesting=isTesting)

    def _GetIndividualProjections(self,srcs):
        '''Download and parse out individual projections for the given sources.

        Args:
            srcs: List of integer site ids of the sources to download.
        '''

        #ensure there are more than two listed sources
        if len(srcs) < 2:
            return 'Unable to download projections for a single source.'

        self._LogIn()
        time.sleep(1)


        #create list of sources to iterate through
        #fills out all combinations of the source values that are greater than 1 item long
        #definitely not the most efficient way to do this
        #srcLists = []
        #for k in range(len(srcs)):
        #    for i in range(k+1,len(srcs)):
        #        for j in range(i,len(srcs)):
         #           l = srcs[k:i],[srcs[j]]
         #           srcLists.append([item for sublist in l for item in sublist])

        #download for all positions
        for pos in self.positions:

            #put in a time delay to prevent hammering the website
            time.sleep(random.randrange(1,10))

            #loop through sources so that all pairs are downloaded only once
            #for comb in srcLists:
            for src in srcs:

                    #print('Downloading projections for {0}, sources={1}'.format(pos,':'.join([str(x) for x in comb])))
                    print('Downloading projections for {0}, source={1}'.format(pos,src))

                    time.sleep(random.randrange(1,3))
                    projs = self._DownloadProjections(pos,self.columns[pos],self.week,sources=[src,src])
                    #projs['position'] = pos

                    if self.scraped is None:
                        self.scraped = projs.copy()
                    else:
                        self.scraped = pd.concat([self.scraped,projs])

        self._Close()



        #self.scraped = pd.DataFrame.from_csv('C:/Users/Dan/Desktop/scraped.csv',header=0,index_col=['pID','src'])

        #deduplicate (~ notation is for the logical not)
        #players may appear on multiple positions.
        self.scraped = self.scraped[~self.scraped.index.duplicated(keep='first')]

        ##recover individual stats
        #combinedSeries = pd.Series()

        ##idx will be a tuple of (pID,src) of the index
        #processCount = 0
        #totalToProcess = len(self.scraped)
        #for idx in self.scraped.index:
        #    print('Processing stats for {0}. {1}% complete.'.format(idx[0],processCount*100/totalToProcess))

        #    for stat in self.scraped.columns:
        #        if stat in self.statColumns:
        #            pID = idx[0]
        #            statSeries = self.scraped[stat][pID]

        #            numMask = np.logical_not(np.isnan(statSeries))

        #            #must have at least three sources to allow for deterministic recovery of a stat
        #            if np.sum(numMask) >= 3 and np.any(statSeries != 0):
        #                statSeries = self._RecoverSourceProjections(statSeries[numMask])

        #                newIdx = pd.MultiIndex.from_product([[pID],[stat],statSeries.index.tolist()],names=['pID','stat','src'])

        #                combinedSeries = combinedSeries.append(pd.Series(statSeries.values,index=newIdx))

        #    processCount += 1

        #self.projs = pd.DataFrame(combinedSeries,index=pd.MultiIndex.from_tuples(combinedSeries.index))
        #self.projs = self.projs.drop_duplicates(keep='first')
        #self.projs = self.projs.unstack()

        #temporary output
        #self.projs.to_csv('C:/Users/Dan/Desktop/projs.csv')

    def _GetConsensusProjections(self):
        '''Only download the consensus projections.'''

        for pos in self.positions:

            print('Downloading projections for {0}'.format(pos))

            #put in a time delay to prevent hammering the website
            time.sleep(random.randrange(1,5))

            projs = self._DownloadProjections(pos,self.columns[pos],self.week)

            if self.scraped is None:
                self.scraped = projs.copy()
            else:
                self.scraped = pd.concat([self.scraped,projs])

    def DownloadLatestProjections(self,breakout=False,saveScraped=None):
        '''Method to download the latest sets of projections from the site.

        Args:
            breakout (bool): Optional. If true, the projections will be broken out into their components. False causes only the consensus projections to be retrieved.
        '''

        if breakout:
            self._GetIndividualProjections(self.srcs)
        else:
            self._GetConsensusProjections()

        if saveScraped is not None:
            self.scraped.to_csv(saveScraped)

class fantasyPros_scraper:
    """Scrapes info from Fantasypros.com.

    Using pandas from_html works pretty well, but loses the links which have the player ID numbers in them.
    z = pd.read_html('https://www.fantasypros.com/mlb/projections/pitchers.php',attrs = {'id': 'data'})

    could use regex to extract links into text part? (http://stackoverflow.com/questions/31771619/html-table-to-pandas-table-info-inside-html-tags)
    tbl = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', tbl)

    THIS NO LONGER APPLIES:: clicking on name brings to a page with all the source projections, so all the different ones can be scraped for each individual player

    """

    def __init__(self,sport):
        self.source_ids = {}
        self.sport = sport
        self.players = None
        self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')

        self.source_ids['nfl'] = {}

        self.source_ids['mlb'] = {}
        self.source_ids['mlb']['FantasyPros'] = 3
        self.source_ids['mlb']['Steamer Blog'] = 4
        self.source_ids['mlb']['Clay Davenport'] = 5
        self.source_ids['mlb']['RotoChamp'] = 6
        self.source_ids['mlb']['CBS Sports'] = 7
        self.source_ids['mlb']['ESPN'] = 8
        self.source_ids['mlb']['Razzball'] = 9
        self.source_ids['mlb']['Baseball Think Factory'] = 11

    def _CleanStat(self,stat):
        """Cleans the stats if not a number (-).

        Args:
            stat (value): Stat to clean.

        Returns:
            num: Returns a number (float or int) or None.
        """

        if isNumber(stat):
            return stat
        else:
            return None

    def GetSeasonProjections(self,season):
        """Downloads season projections for the appropriate sport.

        Args:
            season (int): Season projections are being downloaded for.
        """

        if self.sport == 'mlb':
            self._mlbSeasonProjections(season)

    def _mlbSeasonProjections(self,season):
        """Downloads the season projections for MLB players from all sources on each players projection page."""

        #create a game for the season in questions
        newGame = self.db.CreateSeasonGame('mlb',season)

        #get overall projections and cleaned player info dataframe
        self._mlbGetPlayers()

        #get projections and add to database
        for posType in self.players:
            for pIter in self.players[posType].iterrows():
                tmpID = None
                p = pIter[1]
                pProj = self._mlbGetSinglePlayerProjections('https://www.fantasypros.com' + p['PlayerLink'])

                print('Processing projections for ' + p['PlayerName'])

                for src in pProj:
                    try:
                        #ensure player is in database and get their ID number
                        plyr = self.db.mlb.Players(fullname=p['PlayerName'],is_active=True)
                        teamConv = self.db.GetTeamConversion('mlb',p['PlayerTeam'],src)

                        if tmpID is None:
                            if teamConv is None:
                                plyr = self.db.AddOrUpdatePlayer('mlb',int(p['PlayerID']),int(src),plyr,updateExisting=False)
                            else:
                                plyr = self.db.AddOrUpdatePlayer('mlb',int(p['PlayerID']),int(src),plyr,updateExisting=False,team_id=teamConv.team_id)

                            tmpID = plyr.player_id

                        #ensure a game is in the table for the season stats (will create a conversion for it as well)
                        statGame = self.db.AddOrUpdateGame('mlb','season-2017',src,newGame)

                        #add projections
                        if posType == 'hitters':

                            #make projection object with common stats, then add dependent on source
                            tempProj = self.db.mlb.PlayerStatsProj(player_id= tmpID,
                                                                    source_id= int(src),
                                                                    game_id= statGame.game_id,
                                                                    h_ab= self._CleanStat(pProj[src]['AB']),
                                                                    h_r= self._CleanStat(pProj[src]['R']),
                                                                    h_hr= self._CleanStat(pProj[src]['HR']),
                                                                    h_rbi= self._CleanStat(pProj[src]['RBI']),
                                                                    h_sb= self._CleanStat(pProj[src]['SB']),
                                                                    h_avg= self._CleanStat(pProj[src]['AVG']),
                                                                    h_obp= self._CleanStat(pProj[src]['OBP']),
                                                                    h_h= self._CleanStat(pProj[src]['H']),
                                                                    h_bb= self._CleanStat(pProj[src]['BB']),
                                                                    h_k= self._CleanStat(pProj[src]['SO']),
                                                                    h_ops= self._CleanStat(pProj[src]['OPS']))

                            #add in site specific projections to avoid zeros
                            if src not in [6,8]:
                                #no double or triple projections for espn or rotochamp

                                if '2B' in pProj[src]:
                                    tempProj.h_2b= self._CleanStat(pProj[src]['2B'])

                                if '3B' in pProj[src]:
                                    tempProj.h_3b= self._CleanStat(pProj[src]['3B'])


                        if posType == 'pitchers':
                            #no QS data for any source
                            #no CG data for steamer
                            #no CG data for daveport
                            #no HR, G, GS, CG for rotochamp
                            #no HR, G, GS for cbs
                            #no L, HR, CG for espn
                            #no HR, CG for razzball
                            #no CG for think factory

                            #make projection object with common stats, then add dependent on source
                            tempProj = self.db.mlb.PlayerStatsProj(player_id= tmpID,
                                                                    source_id= int(src),
                                                                    game_id= statGame.game_id,
                                                                    p_ip= self._CleanStat(pProj[src]['IP']),
                                                                    p_k= self._CleanStat(pProj[src]['K']),
                                                                    p_w= self._CleanStat(pProj[src]['W']),
                                                                    p_era= self._CleanStat(pProj[src]['ERA']),
                                                                    p_whip= self._CleanStat(pProj[src]['WHIP']),
                                                                    p_er= self._CleanStat(pProj[src]['ER']),
                                                                    p_h= self._CleanStat(pProj[src]['H']),
                                                                    p_bb= self._CleanStat(pProj[src]['BB']),
                                                                    p_sv= self._CleanStat(pProj[src]['SV']),
                                                                    p_bs= self._CleanStat(pProj[src]['BS']),
                                                                    p_hld= self._CleanStat(pProj[src]['HD']))

                            #add site specific projections so that zeros don't get added to the database
                            if src != 8:
                                if 'L' in pProj[src]:
                                    tempProj.p_l = self._CleanStat(pProj[src]['L'])

                            if src not in [6,7,8,9]:
                                if 'HR' in pProj[src]:
                                    tempProj.p_hr = self._CleanStat(pProj[src]['HR'])

                            if src not in [6,7]:
                                if 'G' in pProj[src]:
                                    tempProj.p_g = self._CleanStat(pProj[src]['G'])

                            if src not in [6,7]:
                                if 'GS' in pProj[src]:
                                    tempProj.p_gs = self._CleanStat(pProj[src]['GS'])

                            if src not in [4,5,6,8,9,11]:
                                if 'CG' in pProj[src]:
                                    tempProj.p_cg = self._CleanStat(pProj[src]['CG'])


                        #merge into database, overwriting any pre-existing projection
                        if posType in ['hitters','pitchers']:
                            self.db.session.merge(tempProj)

                    except Exception as e:
                        print(str(e))

                self.db.session.commit()

        #commit changes
        self.db.session.commit()

    def _mlbGetSinglePlayerProjections(self,url):
        """Gets player projections from all sources on their player page that are defined in the sourc_ids dict.

        Args:
            url (str): URL to download player stats from

        Returns:
            dict: Dictionary with the projections from each source. Structured as key=srcID, val=dict of key=stat, val=projection value.

        """

        #result dict
        res = {}

        #get web page data
        pg_resp = urlopen(url)
        html = pg_resp.read()
        pg = BeautifulSoup(html,"lxml")

        #projection table is in here
        tbl = pg.find('div',attrs={"class":"mobile-table"})

        #parse projection table
        #headings
        headings = []
        for hdTag in tbl.find_all('thead'):
            for hd in hdTag.find_all('tr'):
                for h in hd.find_all('th'):
                    headings.append(h.text)

        #parse body
        body = []
        for bd in tbl.find_all('tbody'):
            for tr in bd.find_all('tr'):
                tempList = []
                for td in tr.find_all('td'):

                    #parse out expert name that's buried in a span element.
                    if len(td.find_all('span')) > 0:
                        for s in td.find_all('span'):
                            if s['class'] != ['expert-name']:
                                #ignore the expert name
                                #record all others using expert-affiliation as source identifier

                                tempList.append(s.text)
                    else:
                        tempList.append(td.text)

                body.append(tempList)

        #put body into dictionary entries for the player
        for r in body:
            for i in range(len(r)):

                #first entry is the source
                if headings[i] == 'Source':
                    srcID = self.source_ids['mlb'][r[i]]
                    res[srcID] = {}
                else:
                    res[srcID][headings[i]] = r[i]


        #return dictionary of source projections
        return res

    def _mlbCleanPlayerInfo(self,df):
        """Cleans the player info column from the consensus page and returns a dataframe with added columns with clean info.

        Args:
            df (dataframe): The dataframe of consensus player data to be cleaned.

        Returns:
            dataframe: Original dataframe with addes columns of cleaned information.

        """

        #"/mlb/projections/mookie-betts.php             : player link regex --> \/mlb\/projections.*?.php
        #class="fp-player-link fp-id-3215"              : id regex --> fp-id-.*?\"
        #fp-player-name="Clayton Kershaw                : name regex --> fp-player-name=.*
        #(/mlb/teams/los-angeles-dodgers.php LAD - SP)  : team/position regex --> \(\/mlb\/teams.*?\)
        #t = re.search('fp-id-.*?\"',df_p['Player'][0]).group(0)
        df['PlayerID'] = df['Player'].str.extract('(fp-id-.*?\")')  #parentheses define a capture group
        df['PlayerID'] = df['PlayerID'].str.replace('fp-id-','')
        df['PlayerID'] = df['PlayerID'].str.replace('"','')

        df['PlayerName'] = df['Player'].str.extract('(fp-player-name=.*)')  #parentheses define a capture group
        df['PlayerName'] = df['PlayerName'].str.replace('fp-player-name="','')

        df['PlayerTeam'] = df['Player'].str.extract('(\(\/mlb\/teams.*?\))')  #parentheses define a capture group
        df['PlayerTeam'] = df['PlayerTeam'].str.replace('\/mlb\/teams.*?.php','')
        df['PlayerTeam'] = df['PlayerTeam'].str.split('-')
        df['PlayerPos'] = df['PlayerTeam'].apply(lambda x: x[1].replace(')','').strip() if not isNumber(x) else None)
        df['PlayerTeam'] = df['PlayerTeam'].apply(lambda x: x[0].replace('(','').strip() if not isNumber(x) else None)

        df['PlayerLink'] = df['Player'].str.extract('(\/mlb\/projections.*?.php)')  #parentheses define a capture group

        return df

    def _mlbGetPlayers(self):
        """Gets list of players and their IDs from the concensus page"""

        #get data from web
        pg_resp_pitchers = urlopen('https://www.fantasypros.com/mlb/projections/pitchers.php')
        html_pitchers = pg_resp_pitchers.read()
        pg_resp_hitters = urlopen('https://www.fantasypros.com/mlb/projections/hitters.php')
        html_hitters = pg_resp_hitters.read()

        #pull links out from href tags
        cleaned_pitchers = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', str(html_pitchers))
        cleaned_hitters = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', str(html_hitters))

        #parse into pandas dataframes
        #Players column will be like '/mlb/projections/clayton-kershaw.php Clayton Kershaw (/mlb/teams/los-angeles-dodgers.php LAD - SP) #" class="fp-player-link fp-id-3215" fp-player-name="Clayton Kershaw'
        df_pitchers_parse = pd.read_html(cleaned_pitchers)
        df_pitchers = df_pitchers_parse[0]
        df_hitters_parse = pd.read_html(cleaned_hitters)
        df_hitters = df_hitters_parse[0]

        #clean dataframe player information
        df_pitchers = self._mlbCleanPlayerInfo(df_pitchers)
        df_hitters = self._mlbCleanPlayerInfo(df_hitters)

        #store
        self.players = {}
        self.players['hitters'] = df_hitters
        self.players['pitchers'] = df_pitchers

class dfsgold_scraper:

    def __init__(self,type,recap_date,dbInstance):
        #recap_date shoule be a datetime object
        #dbInstance should be ready to accept new stuff

        self.type = type
        self.recap_date = recap_date
        self.dbInstance = dbInstance
        self.idLookup = None
        self.url = None
        self.recapPg = None
        self.tbls = None		#table 0 is best lineup, table 1 is constent recap, table 2 is salary/player results
        self.sal_list = []
        self.contest_list = None
        sal_list = None

        if self.type == 'nfl':
            self.url = 'http://www.dfsgold.com/nfl/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        if self.type == 'nba':
            self.url = 'http://www.dfsgold.com/nba/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        if self.type == 'mlb':
            self.url = 'http://www.dfsgold.com/mlb/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        #build lookup
        self.GenerateIDLookup()

    def GenerateIDLookup(self):

        if self.type == 'nfl':
            self.idLookup = dbInstance.GetLookup('idConversion_nfl','dfsgold_id','master_id')

        if self.type == 'nba':
            self.idLookup = dbInstance.GetLookup('idConversion_nba','dfsgold_id','master_id')

        if self.type == 'mlb':
            self.idLookup = dbInstance.GetLookup('idConversion_mlb','dfsgold_id','master_id')

    def GetRecapPageTables(self):

        if not self.url is None:
            pg = urlopen(self.url)
            self.recapPg = BeautifulSoup(pg)

            #check if recap date is correct
            chkStr = self.recap_date.strftime('%b %d, %Y')

            title = self.recapPg.find('title').get_text()

            if title.find(chkStr) != -1:
                return False

            self.tbls = self.recapPg.find_all('table')

            return True

    def ParsePageRecapPage(self):

        #contest info
        self.contest_list = parseStandardHTMLTable(self.tbls[1])

        #salary info
        bd = tbls[2].find('tbody')
        for row in bd.find_all('tr'):
            col = row.find_all('td')
            pos = col[0].strip()
            tm = col[2].strip()
            pid = int(col[1].a['href'].split('/')[-1])  #player id is the last part of the href link
            name = col[1].get_text()
            sal = int(col[4].get_text().replace('$','').replace(',','').strip())
            d = self.recap_date.strftime('%Y-%m-%d')

            self.sal_list.append([pid,name,sal,d,pos,tm])

    def AddSalaryToDatabase(self):

        #add to database
        for p in self.sal_list:

            mID = self.idLookup.lookup(p[0])

            if self.type == 'nfl':
                if mID is None:
                    #need to add player to database master list
                    np = db.playersNFL( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionNFL.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionNFL.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryNFL(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionNFL.merge(psal)

            if self.type == 'nba':
                if mID is None:
                    np = db.playersNBA( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionNBA.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionNBA.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryNBA(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionNBA.merge(psal)

            if self.type == 'mlb':
                if mID is None:
                    np = db.playersMLB( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionMLB.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionMLB.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryMLB(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionMLB.merge(psal)


class rotoGrinders_scraper:
    def __init__(self,sport):
        self.sport = sport
        self.players = None
        self.salaries = None
        self.skipped_plyrs = []

    def GetPlayerIDs(self):
        #gets all player id numbers from the stats page

        #dict of downloaded pages
        pgs = {}

        if self.sport == 'nfl':
            #offensive stats over last two years
            urlOff = 'https://rotogrinders.com/game-stats/nfl-offense?site=draftkings&range=last-two'

            #defensive stats over last two years
            urlDef = 'https://rotogrinders.com/game-stats/nfl-defense?site=draftkings&range=last-two'

            #kicker stats over last two years
            urlK = 'https://rotogrinders.com/game-stats/nfl-kicker?site=draftkings&range=last-two'

            pgs['Off'] = GetURLtext(urlOff)
            pgs['Def'] = GetURLtext(urlDef)
            pgs['K'] = GetURLtext(urlK)

        if self.sport == 'nba':
            url = 'https://rotogrinders.com/game-stats?site=draftkings&sport=nba&range=last-two'

            pgs['Off'] = GetURLtext(url)

        if self.sport == 'mlb':
            urlHitter = 'https://rotogrinders.com/game-stats/mlb-hitter?site=draftkings&range=last-two'

            urlPitcher = 'https://rotogrinders.com/game-stats/mlb-pitcher?site=draftkings&range=last-two'

            pgs['Hitter'] = GetURLtext(urlHitter)
            pgs['Pitcher'] = GetURLtext(urlPitcher)

        #regex to get list of player stats
        #(var data = \[{"id":.*\];)
        #view-source:https://rotogrinders.com/game-stats?site=draftkings&sport=nfl&range=last-two
        self.players = []
        for pos,pg in pgs.items():
            s = re.search('(var data = \[{"id":.*?\];)',str(pg))
            #print(pg)
            if s:
                l = s.group(0).split("=")[1].replace(";","").replace('\\','').strip()
                j = json.loads(l)
                #print(j)
                for p in j:
                    self.players.append(p)

    def GetSalaries(self):
        #gets past available salaries for all players in self.players
        self.salaries = {}

        for p in self.players[:5]:
            self.salaries[p['id']] = []
            print(p['id'])
            self.GetPlayerSalary(p['id'])

    def GetPlayerSalary(self,pID):
        #page has salaries for all sites in a dict

        #regex to get data
        #(data = {"this-season":.*};)
        #view-source:https://rotogrinders.com/players/miguel-cabrera-10323
        #can just use the player id after players/
        try:
            url = 'https://rotogrinders.com/players/' + str(pID)
            pg = GetURLtext(url)

            s = re.search('(data = {"this-season":.*?};)',str(pg))
            if s:
                l = s.group(0).split("=")[1].replace(";","").strip()
                j = json.loads(l)

                for g in j['this-season']:
                    self.salaries[pID].append(g)

                for g in j['last-season']:
                    self.salaries[pID].append(g)

        except:
            self.skipped_plyrs.append(pID)


# r = rotoGrinders_scraper('nfl')
# r.GetPlayerIDs()
# print('ids gotten')
# r.GetSalaries()
# print(r.salaries)
# print(r.skipped_plyrs)

# pg = BeautifulSoup(GetURLtext('http://www.pro-football-reference.com/teams'))

# stat = pg.find("table",{"id":"teams_active"})

# body = stat.find("tbody")

# ref = ProReferenceBase()
# d = ref.TableParse(body)

# df = pd.DataFrame(d)
# print(df['team_name'])
if __name__ == "__main__":
    #pfr = proFootballReference_scraper()
    #pfr.RefreshTeamList()
    fp = fantasyProsNFL_scraper('draft',isTesting=False)
    fp.DownloadLatestProjections(breakout=True,saveScraped='C:/Users/Dan/Desktop/fp-projs-20170824.csv')

#class baseballSavant_scraper:

#class baseballReference_scraper:

#class basketballReference_scraper:
