'''
common scrapy functionality
'''

import abc
import logging
import argparse
import ftplib
import gzip
import scrapy
import scrapy.crawler
import psycopg2
import psycopg2.extras

import sports_data.support.secrets as secret_support
import sports_data.sports_scraper.support.auth as scraper_auth
import sports_data.sports_scraper.yahoo.oauth as yahoo_oauth


def load_parser_args_into_settings(parsedArgs):
    '''
    Load parsed arguments into a settings dictionary. Also looks for settings in
    environment variables. Command line arguments take precedence.

    returns a dictionary with the loaded settings

    parsedArgs      The parsed args from ArgumentParser.parse_args().
    '''

    #loop through all defined arguments and load into settings.
    #   any settings may also be defined by environment variables.
    returnDict = dict()
    for _arg in vars(parsedArgs):

        # load parsed cli argument value
        loadedArg = getattr(parsedArgs, _arg)

        # if no command line defined, search for defintions in specified files or env variables
        if loadedArg is None:
            loadedArg = secret_support.load_environment_variable_or_file(_arg)

        # if defined, add to output dict
        if loadedArg is not None:
            returnDict[_arg] = loadedArg

    return returnDict

class OAuthSpider(scrapy.Spider):
    '''
    base class to build spiders from that use OAuth authentication

    use in conjunction with OAuthDownloaderMiddleware

    all of the following settings must be defined:
        OAUTH_CREDENTIALS_JSON  :  A JSON string defining an OAuthCredentials object
    '''

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--LOG_LEVEL', dest='LOG_LEVEL', action='store',
                            choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'],
                            default='DEBUG',
                            help='The level for scrapy to log at. [CRITICAL, ERROR, WARNING, INFO, DEBUG].')

        parser.add_argument('--OAUTH_CREDENTIALS_JSON', dest='OAUTH_CREDENTIALS_JSON',
                            action='store', required=True,
                            help='A JSON string defining the OAUTH credentials used to connect to the data source.')

    def _check_for_required_settings(self):

        if self.settings.get('OAUTH_CREDENTIALS_JSON') is None:
                raise scrapy.exceptions.CloseSpider("Required setting OAUTH_CREDENTIALS_JSON is not defined.")

    def _load_oauth(self):
        '''
        Load OAuth creds
        '''

        _credsJson = self.settings.get('OAUTH_CREDENTIALS_JSON', default=None)
        _creds = scraper_auth.OAuthCredentials.from_json(_credsJson)
        self._OAuth = yahoo_oauth.YahooOAuth(_creds)

class OAuthDownloaderMiddleware(object):
    '''
    automatically add the oauth authorization header to all requests.
    '''

    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=scrapy.signals.spider_opened)
        return s

    def process_request(self, request, spider):
        '''
        modify request to include the OAuth authorization header


        Called for each request that goes through the downloader
        middleware.

        Must either:
        - return None: continue processing this request
        - or return a Response object
        - or return a Request object
        - or raise IgnoreRequest: process_exception() methods of
          installed downloader middleware will be called

        add the OAuth authorization header to current request
        '''

        if spider.crawler.stats.get_value('access_denied_retries', default=0) > 5:
            raise scrapy.exceptions.CloseSpider("Access token invalid or unable to be refreshed.")

        newHeader = spider.OAuth.get_auth_header()
        request.headers.update(newHeader)

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest

        # catch a 401 response code (access token is incorrect)
        #     force a token refresh
        #     reschedule the request if token is refreshed
        if response.status in [401, 403]:
            spider._OAuth.refresh_access_token()
            spider.crawler.stats.inc_value('access_denied_retries', start=0)
            return request
        else:
            #reset counter if response is something else
            spider.crawler.stats.set_value('access_denied_retries', 0)

        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)

class PostgresPipeline(object):
    '''
    Pipeline base class to save items to a Postgres database. Subclass this and
    overwrite all abstract methods.

    the self._meta attribute is a dictionary that allows for general
    metadata to be stored from within the various methods

    The following settings are required to allow saving:
        POSTGRES_DB_HOST
        POSTGRES_DB_NAME
        POSTGRES_DB_USER
        POSTGRES_DB_PASSWORD
        POSTGRES_DB_PORT
    '''

    def __init__(self):
        self._meta = dict()

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the necessary spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--POSTGRES_DB_HOST', dest='POSTGRES_DB_HOST', action='store',
                            help='The IP or hostname of the postgres database server to save data to.')

        parser.add_argument('--POSTGRES_DB_NAME', dest='POSTGRES_DB_NAME', action='store',
                            help='The name of the postgres database to save data to.')

        parser.add_argument('--POSTGRES_DB_USER', dest='POSTGRES_DB_USER', action='store',
                            help='The username for the postgres database to save data to.')

        parser.add_argument('--POSTGRES_DB_PORT', dest='POSTGRES_DB_PORT', action='store',
                            help='The port of the postgres database server to save data to.')

        parser.add_argument('--POSTGRES_DB_PASSWORD', dest='POSTGRES_DB_PASSWORD', action='store',
                            help='The password for POSTGRES_DB_USER.')

    @classmethod
    def from_crawler(cls, crawler):
        # return cls(
        #     mongo_uri=crawler.settings.get('MONGO_URI'),
        #     mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        # )
        return cls()

    def open_spider(self, spider):
        '''Runs when spider is opened. Initiates connection.'''

        self._setup(spider=spider)

        dbHost = spider.settings.get('POSTGRES_DB_HOST')
        dbUser = spider.settings.get('POSTGRES_DB_USER')
        dbPassword = spider.settings.get('POSTGRES_DB_PASSWORD')
        dbPort = spider.settings.get('POSTGRES_DB_PORT')
        dbName = spider.settings.get('POSTGRES_DB_NAME')

        if dbHost is None or dbPassword is None or dbUser is None or dbPort is None or dbName is None:
            spider.logger.warning("Database connection settings not fully defined. Pipeline will not save items to database.")
            self._connection = None

        else:
            try:
                self._connection = psycopg2.connect(
                        user= dbUser,
                        password= dbPassword,
                        host= dbHost,
                        database= dbName,
                        port= dbPort
                    )
                self._connection.autocommit = False

            except:
                spider.logger.error("Error connecting to database. Pipeline will not save items to database.")
                self._connection = None

    def close_spider(self, spider):
        '''Runs when spider closes. Cleans up database connection.'''

        self._cleanup(spider=spider)

        if self._connection:
            self._connection.close()

    @abc.abstractmethod
    def _setup(self, spider=None):
        '''This method runs when spider is opened, and allows
        for any customized setup. Can return None if no steps needed.
        '''
        pass

    @abc.abstractmethod
    def _cleanup(self, spider=None):
        '''This method runs when spider is closed, and allows
        for any customized clean up or teardown. Can return None if no steps needed.
        '''
        pass

    @abc.abstractmethod
    def _get_table_name(self, item=None, spider=None):
        '''Return the name of the database table to connect to.'''
        pass

    @abc.abstractmethod
    def _get_schema_name(self, item=None, spider=None):
        '''Return the name of the schema the table is in. return None if no schema is required.'''
        pass

    def _build_table_name(self, schemaName, tblName, item=None, spider=None):
        '''
        build the table name based on the schema and table, and optionally
        info from either the item passed to the pipeline or the spider.

        overload this method if further customization is needed using the
        item or spider.
        '''

        if schemaName:
            return '{0}.{1}'.format(schemaName, tblName)
        else:
            return tblName

    def process_item(self, item, spider):
        '''
        Save item to database if connection is present.

        The pipeline will save the item using the keys as field names. Each item is a record.
        The databsae table must be created before running the pipeline. The field names must match the
        item keys, and there can be no extra item keys that do not correspond to a database field.
        '''

        if self._connection:

            cursor = self._connection.cursor()

            try:
                fields = item.keys()
                values = []
                for k in fields:
                    if isinstance(item[k], dict) or isinstance(item[k], list):
                        # save dictionaries as JSON type
                        values.append(psycopg2.extras.Json(item[k]))
                    else:
                        values.append(item[k])

                tblName = self._build_table_name(
                                    self._get_schema_name(item=item, spider=spider),
                                    self._get_table_name(item=item, spider=spider),
                                    item=item,
                                    spider=spider
                                )

                insertQuery = 'INSERT INTO {0} ({1}) VALUES ({2})'.format(
                    tblName,
                    ','.join(fields),
                    ','.join(['%s'] * len(fields))
                )

                cursor.execute(insertQuery, values)
                self._connection.commit()

            except Exception as exceptHndle:
                self._connection.rollback()
                spider.logger.error('Error saving item to database. Error: {0}'.format(str(exceptHndle)))
            finally:
                cursor.close()

        return item

class FtpPipeline(object):
    '''
    Pipeline base class to save items to a file on a FTP server.
    Subclass this class and overwrite all abstract methods.

    the self._meta attribute is a dictionary that allows for general
    metadata to be stored from within the various methods

    The following settings are required to allow saving:
        FTP_HOST
        FTP_USER
        FTP_PASSWORD
        FTP_PORT
        FTP_ROOTDIR
        FTP_GZIP
    '''

    def __init__(self):
        self._meta = dict()

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the necessary spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--FTP_HOST', dest='FTP_HOST', action='store',
                            help='The IP or hostname of the FTP server to save data to.')

        parser.add_argument('--FTP_USER', dest='FTP_USER', action='store',
                            help='The username for the FTP server to save data to.')

        parser.add_argument('--FTP_PORT', dest='FTP_PORT', action='store',
                            help='The port of the FTP server to save data to.')

        parser.add_argument('--FTP_PASSWORD', dest='FTP_PASSWORD', action='store',
                            help='The password for FTP_USER.')

        parser.add_argument('--FTP_ROOTDIR', dest='FTP_ROOTDIR', action='store',
                            help='The root directory to save data to on the FTP server.')

        parser.add_argument('--FTP_GZIP', dest='FTP_GZIP', action='store_true',
                            help='If specified, the file will be compressed with gzip before uploading.')

    @classmethod
    def from_crawler(cls, crawler):
        return cls()

    def open_spider(self, spider):
        '''Runs when spider is opened. Initiates connection.'''

        self._setup(spider=spider)

        ftpHost = spider.settings.get('FTP_HOST', default='127.0.0.1')
        ftpUser = spider.settings.get('FTP_USER', default=None)
        ftpPassword = spider.settings.get('FTP_PASSWORD', default=None)
        ftpPort = spider.settings.get('FTP_PORT', default=21)
        ftpDir = spider.settings.get('FTP_ROOTDIR', default='/')

        if ftpHost is None or ftpUser is None or ftpPassword is None or ftpPort is None or ftpDir is None:
            spider.logger.warning("FTP connection settings not fully defined. Pipeline will not send items to FTP.")
            self._connection = None

        else:
            try:
                self._connection = ftplib.FTP()
                self._connection.connect(host=ftpHost, port=ftpPort)
                self._connection.login(user=ftpUser, passwd=ftpPassword)

                #navigate to directory
                self._connection.cwd(ftpDir)

            except:
                spider.logger.warning("Error connecting to database. Pipeline will not save items to database.")
                self._connection = None

    def close_spider(self, spider):
        '''Runs when spider closes. Cleans up database connection.'''

        self._cleanup(spider=spider)

        if self._connection:
            self._connection.quit()

    @abc.abstractmethod
    def _setup(self, spider=None):
        '''This method runs when spider is opened, and allows
        for any customized setup. Can return None if no steps needed.
        '''
        pass

    @abc.abstractmethod
    def _cleanup(self, spider=None):
        '''This method runs when spider is closed, and allows
        for any customized clean up or teardown. Can return None if no steps needed.
        '''
        pass

    @abc.abstractmethod
    def _get_filename(self, item=None, spider=None):
        '''
            Build or give a filename, may use info from the
            item or the spider
        '''
        pass

    @abc.abstractmethod
    def _item_to_file(self, item=None, spider=None):
        '''
            Provide a function to convert an item to a file. Called for each
            item.

            Must return a file-like object or stream (such as io.StringIO)
        '''
        pass

    def process_item(self, item, spider):
        '''
        Save item to file on the FTP server if connection is present.
        '''

        if self._connection:

            try:
                fileObj = self._item_to_file(item=item, spider=spider)

                if spider.settings.get('FTP_GZIP', default=False):
                    # compress fileObj before uploading if FTP_GZIP setting is true
                    fileObj = gzip.GzipFile(fileobj=fileObj)

                # create a STOR command to send to FTP server
                cmd = 'STOR {0}'.format(self._get_filename(item=item, spider=spider))
                self._connection.storbinary(cmd, fileObj)

            except Exception as exceptHndle:
                spider.logger.error('Error saving item to FTP server. Error: {0}'.format(str(exceptHndle)))

        return item

class ExtendedCrawlerProcess(scrapy.crawler.CrawlerProcess):
    '''
    Extend the normal CrawlerProcess class to add functionality.

    '''

    def __init__(self, settings=None, install_root_handler=True, additionalLoggerHandlers=None):
        '''
        Adds additionalLoggerHandlers argument.

        additionalLoggerHandlers        Additional logging handler (or [handlers]) to add to the
                                        scrapy root logger.
        '''

        # call parent constructor first
        #   this does various things, including initializing the normal logger
        super(ExtendedCrawlerProcess, self).__init__(settings, install_root_handler=install_root_handler)

        # add additional loggers
        if additionalLoggerHandlers:
            if not isinstance(additionalLoggerHandlers, list):
                additionalLoggerHandlers = [additionalLoggerHandlers]

            for handler in additionalLoggerHandlers:
                if isinstance(handler, logging.Handler):
                    logging.root.addHandler(handler)
