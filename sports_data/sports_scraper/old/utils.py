'''Utilities and helper functionality for scrapers.'''

import requests


def splash_server_render_html(splashServerUrl, renderUrl, renderHeaders=None, wait=0):
    '''
        Render a webpage using a Splash server.

        Returns the rendered HTML.

        splashServerUrl     URL to the Splash server (127.0.0.1:8050)
        renderUrl           URL to fetch from the web.
        renderHeaders       (optional) dictionary of headers to send with the renderURL request.
        wait                (optional) explicit wait time for JS to render.
    '''

    payload = {
        'url': renderUrl,
        'wait': wait,
        'http_method': 'GET'
    }

    if renderHeaders:
        payload['headers'] = renderHeaders


    resp = requests.post(
        splashServerUrl,
        data=payload,
        headers={
            'Content-Type': 'application/json'
        }
    )

    if resp.status_code == 200:
        return resp.text
    else:
        raise Exception('Error. HTTP return code: ' + str(resp.status_code))
