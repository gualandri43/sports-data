'''Classes and functions to scrape data from the rotogriders.com website.'''

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import json
import pandas as pd
import numpy as np
import datetime as dt
import time
import pytz
import random
from selenium import webdriver
import sportsdbInterface as db
from sports_data.common.common_funcs import isNumber
from sports_data.sportsScraper.common import GetURLtext, CleanUnicodeText, parseStandardHTMLTable


class rotoGrinders_scraper:
    def __init__(self,sport):
        self.sport = sport
        self.players = None
        self.salaries = None
        self.skipped_plyrs = []

    def GetPlayerIDs(self):
        #gets all player id numbers from the stats page

        #dict of downloaded pages
        pgs = {}

        if self.sport == 'nfl':
            #offensive stats over last two years
            urlOff = 'https://rotogrinders.com/game-stats/nfl-offense?site=draftkings&range=last-two'

            #defensive stats over last two years
            urlDef = 'https://rotogrinders.com/game-stats/nfl-defense?site=draftkings&range=last-two'

            #kicker stats over last two years
            urlK = 'https://rotogrinders.com/game-stats/nfl-kicker?site=draftkings&range=last-two'

            pgs['Off'] = GetURLtext(urlOff)
            pgs['Def'] = GetURLtext(urlDef)
            pgs['K'] = GetURLtext(urlK)

        if self.sport == 'nba':
            url = 'https://rotogrinders.com/game-stats?site=draftkings&sport=nba&range=last-two'

            pgs['Off'] = GetURLtext(url)

        if self.sport == 'mlb':
            urlHitter = 'https://rotogrinders.com/game-stats/mlb-hitter?site=draftkings&range=last-two'

            urlPitcher = 'https://rotogrinders.com/game-stats/mlb-pitcher?site=draftkings&range=last-two'

            pgs['Hitter'] = GetURLtext(urlHitter)
            pgs['Pitcher'] = GetURLtext(urlPitcher)

        #regex to get list of player stats
        #(var data = \[{"id":.*\];)
        #view-source:https://rotogrinders.com/game-stats?site=draftkings&sport=nfl&range=last-two
        self.players = []
        for pos,pg in pgs.items():
            s = re.search('(var data = \[{"id":.*?\];)',str(pg))
            #print(pg)
            if s:
                l = s.group(0).split("=")[1].replace(";","").replace('\\','').strip()
                j = json.loads(l)
                #print(j)
                for p in j:
                    self.players.append(p)

    def GetSalaries(self):
        #gets past available salaries for all players in self.players
        self.salaries = {}

        for p in self.players[:5]:
            self.salaries[p['id']] = []
            print(p['id'])
            self.GetPlayerSalary(p['id'])

    def GetPlayerSalary(self,pID):
        #page has salaries for all sites in a dict

        #regex to get data
        #(data = {"this-season":.*};)
        #view-source:https://rotogrinders.com/players/miguel-cabrera-10323
        #can just use the player id after players/
        try:
            url = 'https://rotogrinders.com/players/' + str(pID)
            pg = GetURLtext(url)

            s = re.search('(data = {"this-season":.*?};)',str(pg))
            if s:
                l = s.group(0).split("=")[1].replace(";","").strip()
                j = json.loads(l)

                for g in j['this-season']:
                    self.salaries[pID].append(g)

                for g in j['last-season']:
                    self.salaries[pID].append(g)

        except:
            self.skipped_plyrs.append(pID)

if __name__ == "__main__":
    print('import module to access classes for the rotogrinders.com scraper.')