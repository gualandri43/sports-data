'''
command line entrypoint for fangraphs scrapers

Settings are defined in create_settings_definitions(), and then they are added to
the applicable command line parser automatically.
'''

# note: add methods to ArgumentHandler and ArgumentDefinition when adding
#       a new scraper. Also add new options as appropriate to create_settings_definitions(), as well
#       as add the new scraper to applicableSpiders lists.

import argparse
import scrapy.crawler as scrapy_crawler

import sports_data.sports_scraper.support.scrapy as scrapy_support
import sports_data.sports_scraper.fangraphs.fangraphs_scrapy.spiders as fangraphs_spiders


class FangraphsArgumentHandler(scrapy_support.ArgumentHandler):
    '''
    Class contains methods to apply parsed arguments and
    launch required functionality.
    '''

    @classmethod
    def steamer_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.SteamerProjections)
        process.start()

    @classmethod
    def steamer600_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.Steamer600Projections)
        process.start()

    @classmethod
    def zips_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.ZipsProjections)
        process.start()

    @classmethod
    def atc_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.AtcProjections)
        process.start()

    @classmethod
    def thebat_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.TheBatProjections)
        process.start()

    @classmethod
    def depthcharts_parser(cls, args):
        '''
        args        The parsed args object from define_arguments()
        '''

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        settings = FangraphsArgumentHandler.load_args_into_settings(args, settingsDefs)

        process = scrapy_crawler.CrawlerProcess(settings=settings)
        process.crawl(fangraphs_spiders.DepthChartProjections)
        process.start()

class FangraphsArgumentDefinition:
    '''
    Methods to define command line arguments for each parser.

    main_parser() creates the parent parser and all sub parsers

    Each method takes a argparse.ArgumentParser class and adds all necessary
    descriptions and arguments.
    '''

    @classmethod
    def create_settings_definitions(cls):
        '''
        build out settings definitions for the fangraphs scraper.
        '''

        settingDefs = {}
        settingDefs.update(scrapy_support.SETTINGDEF)

        settingDefs['LOG_LEVEL']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])
        settingDefs['POSTGRES_DB_HOST']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])
        settingDefs['POSTGRES_DB_USER']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])
        settingDefs['POSTGRES_DB_PORT']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])
        settingDefs['POSTGRES_DB_PASSWORD']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])
        settingDefs['POSTGRES_DB_NAME']['applicableSpiders'].extend([
                                                        'SteamerProjections',
                                                        'Steamer600Projections',
                                                        'ZipsProjections',
                                                        'AtcProjections',
                                                        'TheBatProjections',
                                                        'DepthChartProjections'
                                                    ])

        settingDefs['FANGRAPHS_SCOPE'] = {
            'choices' : ['season', 'ros', 'update', 'daily'],
            'help': 'The scope of projections to scrape. One of [season, ros, update, daily].',
            'applicableSpiders': [
                'SteamerProjections',
                'Steamer600Projections',
                'ZipsProjections',
                'AtcProjections',
                'TheBatProjections',
                'DepthChartProjections'
            ]
        }

        return settingDefs

    @classmethod
    def main_parser(cls, parser):

        if not isinstance(parser, argparse.ArgumentParser):
            raise TypeError("parser should be an argparse.ArgumentParser")

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()

        parser.description = ('Scrape the Fangraphs website for various league datasets. '
                             'Each command line arg can also be defined by an environment variable '
                             'with the same name. _FILE may also be appended to each environment variable '
                             'to specify a path to read the variable value from. This file must only have '
                             'the variable contents in it. Command line defined arguments take precedence '
                             'over environment variables.')

        subparsers = parser.add_subparsers()

        FangraphsArgumentDefinition._steamer_projections(
            subparsers.add_parser('steamer_projections', help= 'Scrape steamer projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'SteamerProjections')
                                                                    )
            )
        )

        FangraphsArgumentDefinition._steamer600_projections(
            subparsers.add_parser('steamer600_projections', help= 'Scrape steamer600 projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'Steamer600Projections')
                                                                    )
            )
        )

        FangraphsArgumentDefinition._zips_projections(
            subparsers.add_parser('zips_projections', help= 'Scrape zips projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'ZipsProjections')
                                                                    )
            )
        )

        FangraphsArgumentDefinition._atc_projections(
            subparsers.add_parser('atc_projections', help= 'Scrape ATC projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'AtcProjections')
                                                                    )
            )
        )

        FangraphsArgumentDefinition._thebat_projections(
            subparsers.add_parser('thebat_projections', help= 'Scrape TheBat projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'TheBatProjections')
                                                                    )
            )
        )

        FangraphsArgumentDefinition._depthcharts_projections(
            subparsers.add_parser('depthcharts_projections', help= 'Scrape Depth Charts projections from Fangraphs. '
                                                                'settings applicable to this scraper are: '
                                                                + ", ".join(
                                                                        FangraphsArgumentHandler.get_applicable_settings(
                                                                            settingsDefs,
                                                                            'DepthChartProjections')
                                                                    )
            )
        )

    @classmethod
    def _steamer_projections(cls, subParser):
        '''
        add parsing details for the steamer parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.steamer_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'SteamerProjections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

    @classmethod
    def _steamer600_projections(cls, subParser):
        '''
        add parsing details for the steamer600 parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.steamer600_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'Steamer600Projections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

    @classmethod
    def _zips_projections(cls, subParser):
        '''
        add parsing details for the zips parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.zips_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'ZipsProjections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

    @classmethod
    def _atc_projections(cls, subParser):
        '''
        add parsing details for the atc parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.atc_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'AtcProjections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

    @classmethod
    def _thebat_projections(cls, subParser):
        '''
        add parsing details for the thebat parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.thebat_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'TheBatProjections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

    @classmethod
    def _depthcharts_projections(cls, subParser):
        '''
        add parsing details for the depthcharts parser
        '''

        if not isinstance(subParser, argparse.ArgumentParser):
            raise TypeError("subParser should be an argparse.ArgumentParser created by the add_parser() command.")


        subParser.set_defaults(func=FangraphsArgumentHandler.depthcharts_parser)

        settingsDefs = FangraphsArgumentDefinition.create_settings_definitions()
        for arg in FangraphsArgumentHandler.get_applicable_settings(settingsDefs, 'DepthChartProjections'):
            subParser.add_argument('--{0}'.format(arg), dest=arg, action='store', help=settingsDefs[arg]['help'])

def main():
    parser = argparse.ArgumentParser()
    FangraphsArgumentDefinition.main_parser(parser)
    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
