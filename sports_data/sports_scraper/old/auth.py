'''
support modules: common web authorization functionality
'''

import argparse
import sys
import logging
import json
import abc
import datetime as dt
import threading

import requests_oauthlib
import webbrowser
import base64

import sports_data.support.secrets as secret_support


class OAuthCredentials:
    '''
    Storage for OAuth credentials
    '''

    def __init__(self):
        '''
        consumerKey     API app key or ID given by provider
        consumerSecret  API app secrete given by provider

        tokenTime       datetime object for time token was obtained.
                        defaults to time of class creation
        expiresIn       time to access token expiration in seconds.
                        defaults to a negative number, which will always result in
                        token evaluating as expired
        '''

        self.consumerKey = None
        self.consumerSecret = None
        self.accessToken = None
        self.refreshToken = None
        self.tokenTime = dt.datetime.now()
        self.expiresIn = -1000
        self.tokenType = None

    def __str__(self):
        return self.to_json()

    @staticmethod
    def add_args_to_parser(parser):
        '''
        Add arguments needed to build this class from command line.

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--credentialsFile', dest='credsFile', action='store',
                            help='The path to the OAuth credentials file to read/write to. ')

        parser.add_argument('--consumerKey', dest='consumerKey', action='store',
                            help='The consumer key. Must also specify --consumerSecret.')

        parser.add_argument('--consumerSecret', dest='consumerSecret', action='store',
                            help='The consumer secret. Must also specify --consumerKey.')

        parser.add_argument('--consumerKeyFile', dest='consumerKeyFile', action='store',
                            help='the path to a text file holding a Yahoo API consumer key.')

        parser.add_argument('--consumerSecretFile', dest='consumerSecretFile', action='store',
                            help='the path to a text file holding a  Yahoo API consumer secret.')

    @staticmethod
    def create_from_parser_args(parsedArgs):
        '''
        Create class from from command line arguments.

        parsedArgs      The parsed args from ArgumentParser.parse_args().
        '''

        if parsedArgs.credsFile:
            return OAuthCredentials.from_file(parsedArgs.credsFile)

        _consumerKey = None
        _consumerSecret = None

        if parsedArgs.consumerKeyFile is not None:
            _consumerKey = secret_support.get_secret_from_file(parsedArgs.consumerKeyFile)

        if parsedArgs.consumerKey is not None:
            _consumerKey = parsedArgs.consumerKey

        if parsedArgs.consumerSecretFile is not None:
            _consumerSecret = secret_support.get_secret_from_file(parsedArgs.consumerSecretFile)

        if parsedArgs.consumerSecret is not None:
            _consumerSecret = parsedArgs.consumerSecret

        if _consumerKey is not None and _consumerSecret is not None:
            return OAuthCredentials.from_key_secret(_consumerKey, _consumerSecret)

        raise ValueError("Invalid arguments encountered.")

    @classmethod
    def from_dict(cls, storedDict):
        '''
        factory method to create an instance of OAuthCredentials from a dictionary.
        '''

        creds = cls()
        creds.consumerKey = storedDict.get('consumerKey')
        creds.consumerSecret = storedDict.get('consumerSecret')
        creds.accessToken = storedDict.get('accessToken')
        creds.refreshToken = storedDict.get('refreshToken')
        creds.tokenTime = storedDict.get('tokenTime')
        creds.expiresIn = storedDict.get('expiresIn')
        creds.tokenType = storedDict.get('tokenType')

        if creds.tokenTime is None:
            creds.tokenTime = dt.datetime.now()

        if creds.expiresIn is None:
            creds.expiresIn = -1000

        return creds

    @classmethod
    def from_json(cls, jsonString):
        '''
        factory method to create an instance of OAuthCredentials from a json string.

        jsonString is a json string of the data (i.e. from to_json() method)
        '''

        loadedData = json.loads(jsonString)
        loadedData['tokenTime'] = dt.datetime.strptime(loadedData['tokenTime'], '%Y-%m-%d %H:%M:%S')

        return cls.from_dict(loadedData)

    @classmethod
    def from_file(cls, credsFile):
        '''
        factory method to create an instance of OAuthCredentials from a text file.

        credsFile is the path to the text file
        '''

        with open(credsFile, 'r') as f:
            return cls.from_json(f.read())

    @classmethod
    def from_key_secret(cls, consumerKey, consumerSecret):
        '''
        factory method to create an instance of OAuthCredentials from a consumer key and secret.
        '''

        creds = cls()
        creds.consumerKey = consumerKey
        creds.consumerSecret = consumerSecret
        return creds

    def to_dict(self):
        '''
        create a dictionary for storing data
        '''

        return {
            'consumerKey': self.consumerKey,
            'consumerSecret': self.consumerSecret,
            'accessToken': self.accessToken,
            'refreshToken': self.refreshToken,
            'tokenTime': self.tokenTime,
            'expiresIn': self.expiresIn,
            'tokenType': self.tokenType
        }

    def to_json(self):
        '''
        create a json string of internal data
        '''

        data = self.to_dict()
        data['tokenTime'] = data['tokenTime'].strftime('%Y-%m-%d %H:%M:%S')
        return json.dumps(data)

    def to_file(self, credsFile):
        '''
        write out data to text file
        '''

        with open(credsFile, 'w') as f:
            f.write(self.to_json())
            #json.dump(self.to_dict(), f, indent=4, sort_keys=True)

    def is_expired(self):
        '''
        returns true if token is expired.
        '''

        expireTime = self.tokenTime + dt.timedelta(seconds=self.expiresIn)

        if dt.datetime.now() > expireTime:
            return True
        else:
            return False

    def needs_initial_auth(self):
        '''
        check if token needs initial authorization (no refresh token)
        '''

        if self.refreshToken is None:
            return True

        return False

class OAuthBase(abc.ABC):
    '''
    class to manage OAuth2 connections and credentials

    doesn't handle scopes (i.e for google apis) at the moment
    '''

    def __init__(self, credentials):
        '''
        credentials are an instance of OAuthCredentials
        '''

        self._logger = logging.getLogger(self._get_site())

        self._validate_credentials(credentials)
        self._creds = credentials

        self._urls = self._get_oauth_urls()
        self._validate_url_inputs(self._urls)

        self._session = requests_oauthlib.OAuth2Session(
            self._creds.consumerKey,
            redirect_uri=self._urls['redirectUrl']
        )

    def _validate_credentials(self, credentials):
        '''
        validate that the credentials input is correct
        '''

        if credentials is None:
            if not isinstance(credentials, OAuthCredentials):
                ValueError('credentials must be and instance of OAuthCredentials.')

            raise ValueError('credentials must be given.')

    def _validate_url_inputs(self, oauthUrls):
        '''
        validate the input dictionary that defines the OAuth endpoint urls
        '''

        if oauthUrls is None:
            raise ValueError('oauthUrls must be defined.')

        if 'authorizeTokenUrl' not in oauthUrls:
            raise ValueError('authorizeTokenUrl must be a key in oauthUrls.')

        if 'accessTokenUrl' not in oauthUrls:
            raise ValueError('accessTokenUrl must be a key in oauthUrls.')

        if 'redirectUrl' not in oauthUrls:
            raise ValueError('redirectUrl must be a key in oauthUrls.')

        for k,v in oauthUrls.items():
            if v == '':
                raise ValueError(k + ' must be a url.')

    @abc.abstractmethod
    def _get_oauth_urls(self):
        '''
        oauthUrls must be a dict with keys of
            authorizeTokenUrl,
            accessTokenUrl,
            redirectUrl

        return the dict from this method
        '''
        pass

    @abc.abstractmethod
    def _get_site(self):
        '''
        name of website or domain that the OAuth implementation is for.
        return a string
        '''
        pass

    @abc.abstractmethod
    def _get_extra_refresh_params(self):
        '''
        define dictionary of extra parameters that must be sent along with
        a refresh token

        return the dictionary of parameters
        '''
        pass

    def refresh_access_token(self, credentialsFile=None):
        '''
        Refresh access token

            oauth refresh example with extras:
                https://requests-oauthlib.readthedocs.io/en/latest/examples/real_world_example_with_refresh.html

        token dict from requests_oauthlib will have keys for
            access_token
            refresh_token
            token_type
            expires_in

        credentialsFile     If given, the updated credentials will be written to this path
        '''

        self._logger.info('Refreshing Access Token')

        extras = self._get_extra_refresh_params()
        refreshTime = dt.datetime.now()

        newToken = self._session.refresh_token(
            self._urls['accessTokenUrl'],
            refresh_token=self._creds.refreshToken,
            **extras
        )

        self._creds.accessToken = newToken['access_token']
        self._creds.refreshToken = newToken['refresh_token']
        self._creds.tokenTime = refreshTime
        self._creds.expiresIn = newToken['expires_in']
        self._creds.tokenType = newToken['token_type']

        self._logger.info('Token refresh successful.')

        if credentialsFile:
            self._creds.to_file(credentialsFile)

    def authorize_access(self, credentialsFile=None):
        '''
        initial access authorization

        only needed once, then the refresh token can be used from then on

        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        self._logger.info('Running Access Authorization.')

        authTime = dt.datetime.now()

        #state is for passing any csrf_token or similar fraud prevention hashes
        authorizationUrl, state = self._session.authorization_url(self._urls['authorizeTokenUrl'])
        webbrowser.open(authorizationUrl)
        verifierCode = input("Enter verifier code: ")

        self._logger.info('Obtaining Access Token.')

        newToken = self._session.fetch_token(
            self._urls['accessTokenUrl'],
            code=verifierCode,
            client_secret=self._creds.consumerSecret
        )

        self._creds.accessToken = newToken['access_token']
        self._creds.refreshToken = newToken['refresh_token']
        self._creds.tokenTime = authTime
        self._creds.expiresIn = newToken['expires_in']
        self._creds.tokenType = newToken['token_type']

        if credentialsFile is not None:
            self._creds.to_file(credentialsFile)

    def get_auth_header(self, autoRefresh=True, credentialsFile=None):
        '''
        Generate a dictionary with the header to include with an http request
        that grants access to the resource.

        autoRefresh         True will check for expiration and automatically
                            refresh the access token if needed.
        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        #refresh the access token if required
        if self._creds.is_expired() and autoRefresh:
            self.refresh_access_token(credentialsFile=credentialsFile)

        return create_oauth_header(self._creds.accessToken)

    def get_access_token(self, autoRefresh=True, credentialsFile=None):
        '''
        return the access token as a string

        autoRefresh     True will check for expiration and automatically
                        refresh the access token if needed.
        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        #refresh the access token if required
        if self._creds.is_expired() and autoRefresh:
            self.refresh_access_token(credentialsFile=credentialsFile)

        return self._creds.accessToken

    def get_credentials(self):
        '''
        return an instance of OAuthCredentials
        '''
        return self._creds

    def needs_refresh(self):
        '''
        checks if credentials need refreshing
        '''

        if self._creds.accessToken is None:
            return True

        return self._creds.is_expired()

class OAuthGlobalManager:
    '''
    use an instance of this class at the module level to create a global
    location to control OAuth functionality (get token, request refreshes, etc)
    '''

    def __init__(self):
        '''
        '''

        self._connector = None
        self._isInitialized = False

    @classmethod
    def from_connector(cls, oauthConnector):
        '''
        factory method to create an instance of OAuthGlobalManager from a oauthConnector.

        oauthConnector      instance of class that implements OAuthBase
        '''

        if not isinstance(oauthConnector, OAuthBase):
            raise ValueError('oauthConnector must implement OAuthBase.')

        mgr = cls()
        mgr._connector = oauthConnector
        mgr._isInitialized = True
        return mgr

    def refresh_access_token(self, credentialsFile=None):
        '''
        Refresh access token

        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        with threading.Lock():
            self._connector.refresh_access_token(credentialsFile=credentialsFile)

    def get_auth_header(self, credentialsFile=None):
        '''
        Generate a dictionary with the header to include with an http request
        that grants access to the resource.

        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        with threading.Lock():
            return self._connector.get_auth_header(credentialsFile=credentialsFile)

    def get_access_token(self, credentialsFile=None):
        '''
        return the access token as a string

        credentialsFile     If given, the updated credentials will be written to this path
                            if refresh was necessary.
        '''

        with threading.Lock():
            return self._connector.get_access_token(credentialsFile=credentialsFile)

    def get_credentials(self):
        '''
        return an instance of OAuthCredentials
        '''
        with threading.Lock():
            return self._connector.get_credentials()

    def is_initialized(self):
        '''
        True  -- manager has been initialized
        False -- manager has not been initialized
        '''
        return self._isInitialized

def refresh_oauth_access_token(oauthConnector, printCredentials=False):
    '''
    refreshes the oauth access token

    oauthConnector          a site-specific specialization of OAuthBase
    '''

    if not isinstance(oauthConnector, OAuthBase):
        raise ValueError('oauthConnector must implement the OAuthBase interface.')

    oauthConnector.refresh_access_token()

    if printCredentials:
        creds = oauthConnector.get_credentials()
        print(str(creds))

def create_http_basic_auth_header(username, password):
    '''
    create a header to use for HTTP basic authentication. can use for applications where
    the http library does not have an easy way to define it

    returns a dictionary with the header name and value pair
    '''

    #http basic auth credentials are the string <username>:<password>
    # encoded to base64
    credentials = ('{0}:{1}'.format(username, password))
    encodedCredentials = base64.b64encode(credentials.encode('ascii'))

    return {
        'Authorization': 'Basic {0}'.format(encodedCredentials.decode('ascii'))
    }

def create_oauth_header(accessToken):
        '''
        Generate a dictionary with the header to include with an http request
        that grants access to the resource.

        accessToken     The access token to use.
        '''

        authHeader = {
            'Authorization': 'Bearer {0}'.format(accessToken)
        }

        return authHeader

def handle_create_oauth_credentials_args(args):
    '''handles the create_oauth_credentials subcommand args'''

    _creds = OAuthCredentials.create_from_parser_args(args)
    print(_creds.to_json)

def main():
    parser = argparse.ArgumentParser(description='Authorization support functionality')

    subParsers = parser.add_subparsers()

    #
    # create_oauth_credentials

    parserCreateOauth = subParsers.add_parser('create_oauth_credentials',
                                              help='Command to create oauth credentials. A json string '
                                              'containing the credentials is printed to standard output. '
                                              'That output can be saved in a file for later loading.')

    OAuthCredentials.add_args_to_parser(parserCreateOauth)
    parserCreateOauth.set_defaults(func=handle_create_oauth_credentials_args)

    if len(sys.argv) > 1:
        args = parser.parse_args()
        args.func(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
