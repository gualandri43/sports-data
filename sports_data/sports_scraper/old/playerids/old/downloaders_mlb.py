'''
routines to download mlb player mappings.
'''

import io
import logging
import uuid
import requests
import pandas as pd
import numpy as np
from ohmysportsfeedspy import MySportsFeeds

from sports_data.support.logging import set_up_console_logger

LOGGER_NAME = "downloaders_mlb"
set_up_console_logger(LOGGER_NAME)
LOGGER = logging.getLogger(LOGGER_NAME)

def convert_to_uuid(inp):
    '''wrapper function to create uuid object'''

    return uuid.UUID(inp)

def int_or_str(inp):
    '''
    converter function to choose between integer or string
    '''

    try:
        return int(inp)
    except ValueError:
        return inp
    except TypeError:
        return inp

def get_chadwick_mapping():
    '''
    return a dataframe of the Chadwick Bureau Register ID mapping from Github.

    key_uuid:           The primary key, providing the most stable reference to a person.
                        Guaranteed not to be re-issued to a different person in the future.
    key_person:         The first eight (hex) digits of the key_uuid.
                        It is guaranteed that this is unique at any given time.
                        However, should a person's record be withdrawn from the register,
                        the same key_person may reappear referencing a different person in the future.
    key_retro:          The person's Retrosheet identifier.
    key_mlbam:          The person's identifier as used by MLBAM (for example, in Gameday).
    key_bbref:          The person's identifier on Major League pages on baseball-reference.com.
    key_bbref_minors:   The person's identifier on minor league and Negro League pages on baseball-reference.com.
    key_fangraphs:      The person's identifier on fangraphs.com (Major Leaguers). As fangraphs uses BIS identifiers,
                        this can also be used to match up people to BIS data.
    key_npb:            The person’s identifier on the NPB official site, npb.or.jp.
    key_sr_nfl:         The person's identifier on pro-football-reference.com
    key_sr_nba:         The person's identifier on basketball-reference.com
    key_sr_nhl:         The person's identifier on hockey-reference.com
    key_findagrave:     The identifier of the person's memorial on findagrave.com

    player info:
        name_last
        name_first
        name_given
        name_suffix
        name_matrilineal
        name_nick
        birth_year
        birth_month
        birth_day
        death_year
        death_month
        death_day
    '''

    #request headers
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br"
    }

    #download csv text file as string file object
    chadwickMappingURL = 'https://github.com/chadwickbureau/register/raw/master/data/people.csv'
    req = requests.get(chadwickMappingURL, headers=httpHeaders)
    chdwckFile = io.StringIO(req.text)

    #import all as object with a converter function
    #   object array can hold entries of other data types within it
    #   Int64Dtype is a nullable integer type in pandas, only available in later versions and still experimental
    #first line of file has the headers
    #set up function as converter for all headers
    headers = chdwckFile.readlines()[0].strip().split(',')

        # columns to include from this source, and the function to use to convert
    convertCols = {
        'key_uuid': int_or_str,
        'key_person': int_or_str,
        'key_retro': int_or_str,
        'key_mlbam': int_or_str,
        'key_bbref': int_or_str,
        'key_bbref_minors': int_or_str,
        'key_fangraphs': int_or_str,
        'key_npb': int_or_str,
        'key_sr_nfl': int_or_str,
        'key_sr_nba': int_or_str,
        'key_sr_nhl': int_or_str,
        'key_findagrave': int_or_str,
        'name_last': int_or_str,
        'name_first': int_or_str,
        'name_given': int_or_str,
        'name_suffix': int_or_str,
        'name_matrilineal': int_or_str,
        'name_nick': int_or_str,
        'birth_year': int_or_str,
        'birth_month': int_or_str,
        'birth_day': int_or_str,
        'death_year': int_or_str,
        'death_month': int_or_str,
        'death_day': int_or_str
    }

    # reset to beginning of file for import
    chdwckFile.seek(0)
    df = pd.read_csv(chdwckFile, dtype='object', usecols=convertCols.keys())

    for col, func in convertCols.items():
        if func is not None:
            mask = df[col].notna()
            df.loc[mask, col] = df.loc[mask, col].apply(func)

    LOGGER.info('downloaded chadwick.')
    return df

def get_crunchtime_baseball_mapping():
    '''
    return a dataframe of the CrunchTimeBaseball.com ID mapping.
    '''

    #request headers
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br"
    }

    #download csv text file as string file object
    crunchTimeURL = 'http://crunchtimebaseball.com/master.csv'
    req = requests.get(crunchTimeURL, headers=httpHeaders)
    crunchyFile = io.StringIO(req.text)

    # columns to include from this source
    convertCols = {
        'mlb_id': int_or_str,           # MLB.com
        'mlb_name': int_or_str,
        'mlb_pos': int_or_str,
        'mlb_team': int_or_str,
        'bp_id': int_or_str,            # baseball prospectus
        'bref_id': int_or_str,          # baseball reference
        'cbs_id': int_or_str,           # CBS
        'cbs_pos': int_or_str,
        'espn_id': int_or_str,          # ESPN
        'espn_pos': int_or_str,
        'fg_id': int_or_str,            # Fangraphs,
        'fg_pos': int_or_str,
        'lahman_id': int_or_str,        # Lahman database
        'nfbc_id': int_or_str,          # NFBC?
        'nfbc_pos': int_or_str,
        'retro_id': int_or_str,         # retrosheet
        'yahoo_id': int_or_str,         # yahoo
        'ottoneu_id': int_or_str,       # ottoneu,
        'ottoneu_pos': int_or_str,
        'rotowire_id': int_or_str,      # rotowire,
        'rotowire_pos': int_or_str
    }

    df = pd.read_csv(crunchyFile, dtype='object', usecols=convertCols.keys())

    for col, func in convertCols.items():
        if func is not None:
            mask = df[col].notna()
            df.loc[mask, col] = df.loc[mask, col].apply(func)

    LOGGER.info('downloaded crunchtime.')
    return df

def get_mysportsfeeds_mappings(api_key, season='2019-regular'):
    '''
    download id mappings from mysportsfeeds api

    player object has an externalMappings attribute
    "externalMappings":[{"source":"MLB.com","id":592094}]
    '''

    #disable local file caching?
    msf = MySportsFeeds(version="2.0")
    msf.authenticate(api_key, "MYSPORTSFEEDS")
    output = msf.msf_get_data(league='mlb', season=season, feed='players', format='json', force=True)

    foundMappings = []

    for player in output['players']:

        playerID = player['player']['id']
        externalMappings = player['player']['externalMappings']

        for externalMapping in externalMappings:
            if externalMapping['source'] == 'MLB.com':
                if externalMapping['id'] is not None:
                    foundMappings.append(
                                        {'mlb_id': str(externalMapping['id']),
                                        'mysportsfeeds_id': str(playerID)}
                                        )

    df = pd.DataFrame(foundMappings, dtype='object')

    #convert columns to dataframe
    convertCols = {
        'mlb_id': int_or_str,
        'mysportsfeeds_id': int_or_str
    }

    for col, func in convertCols.items():
        if func is not None:
            mask = df[col].notna()
            df.loc[mask, col] = df.loc[mask, col].apply(func)

    LOGGER.info('downloaded mysportsfeeds.')
    return df

def get_smart_fantasy_baseball_mapping():
    '''
    return a dataframe of the www.smartfantasybaseball.com ID mapping.
    '''

    #request headers
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br"
    }

    #download csv text file as string file object
    smartFantasyURL = 'https://www.smartfantasybaseball.com/PLAYERIDMAPCSV'
    req = requests.get(smartFantasyURL, headers=httpHeaders)
    smartFile = io.StringIO(req.text)

    # columns to include from this source, and the function to use to convert
    convertCols = {
        #'IDPLAYER',
        #'PLAYERNAME',
        #'TEAM',
        #'ALLPOS',
        'IDFANGRAPHS': int_or_str,      # fangraphs id
        'MLBID': int_or_str,            # MLB.com
        'CBSID': int_or_str,            # CBS
        'RETROID': int_or_str,          # retrosheet
        'BREFID': int_or_str,           # baseball reference
        'NFBCID': int_or_str,           # nfbc
        'ESPNID': int_or_str,           # ESPN,
        'KFFLNAME': int_or_str,         # KFFL (name, not id?)
        'DAVENPORTID': int_or_str,      #
        'BPID': int_or_str,             # baseball prospectus
        'YAHOOID': int_or_str,          # yahoo id
        'MSTRBLLNAME': int_or_str,      #
        'FANTPROSNAME': int_or_str,     # fantasy pros name (can get id by lowercase)
        'ROTOWIREID': int_or_str,       # rotowire
        'FANDUELID': int_or_str,        # fanduel,
        'OTTONEUID': int_or_str,        # ottoneu
        'HQID': int_or_str,             # hq
        'FANTRAXID': int_or_str,        # fantrax
        'RAZZBALLNAME': int_or_str,     # razzball name only
    }

    df = pd.read_csv(smartFile, dtype='object', usecols=convertCols.keys())

    for col, func in convertCols.items():
        if func is not None:
            mask = df[col].notna()
            df.loc[mask, col] = df.loc[mask, col].apply(func)

    LOGGER.info('downloaded smartfantasybaseball.com.')
    return df
