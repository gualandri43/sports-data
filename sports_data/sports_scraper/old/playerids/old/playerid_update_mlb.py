'''
Manages storage and combining of playerid mappings across various sources.

When the update is run, new mappings are downloaded and reconstructed or updated in
the database based on the newly downloaded mappings.
'''

import json
import uuid
import logging
import argparse
import pkg_resources
import pandas as pd
import numpy as np
import pymongo

from sports_data.support.database import DatabaseDefinition
from sports_data.support.logging import set_up_console_logger
from sports_modules import SETTINGS
import sports_data.sports_scraper.playerids.downloaders_mlb as downloaders_mlb

LOGGER_NAME = 'playerid_update_mlb'

class PlayerIDScrapeMLB:
    def __init__(self):
        self._combinedIDs = None
        self._updateSuccess = {
            'chadwick': False,
            'crunchtime': False,
            'smartfantasy': False,
            'mysportsfeeds': False
        }

        self.dbName = "id_mappings"
        self.collectionName = "mlb"

        dbDef = DatabaseDefinition()
        self._db = dbDef.get_mongodb_database(self.dbName)
        self._collection = self._db[self.collectionName]

        self._logger = logging.getLogger(LOGGER_NAME)

    def did_all_sources_update(self):
        '''
        return true if all sources updated, false if not.
        '''

        updateSeries = pd.Series(self._updateSuccess)
        return updateSeries.all()

    def build_mappings(self):
        '''
        build combined mappings
        '''

        self._logger.info('Building mappings')

        self._get_combined_mappings()
        self._update_from_known_mappings()
        self._convert_uuid()
        self._create_integer_playerid()

    def save_to_database(self, replaceAllFields=False):
        '''
        replaceAllFields:   boolean indicating whether to replace the whole mapping record, or
                            just update included fields.
        '''

        self._logger.info('Saving mappings to database')

        #need to replace NaN values with None, so that
        #   to_dict() correctly gives a None value that pymongo will
        #   recognize as null
        self._combinedIDs = self._combinedIDs.where(pd.notna, None)
        recs = self._combinedIDs.to_dict(orient='records')

        # loop through records and clean up
        #   transform key_uuid from string to uuid python type
        # then update or insert record in database
        updateWrites = []
        for rec in recs:

            #clean
            if 'key_uuid' in rec:
                rec['key_uuid'] = rec['key_uuid']

            #stupid fix to force integer mysports ids because pandas is stubborn
            if 'mysportsfeeds_id' in rec:
                if rec['mysportsfeeds_id'] is not None:
                    rec['mysportsfeeds_id'] = int(rec['mysportsfeeds_id'])

            # send to mongo using an Update upsert call, keying on UUID
            #   using $set will preserve fields not included in rec, just setting the
            #   new document will replace all
            #
            # _id field will not be replaced per MongoDB docs
            filterDict = {'key_uuid': rec['key_uuid']}

            if replaceAllFields:
                self._logger.debug('Replace all mappings for ' + str(rec['key_uuid']))
                updateDict = rec
            else:
                self._logger.debug('Update mappings for ' + str(rec['key_uuid']))
                updateDict = {'$set': rec}

            updateWrites.append(pymongo.UpdateOne(filterDict, updateDict, upsert=True))

        #send updates to database collection
        self._logger.debug('Sending bulk write update')
        self._collection.bulk_write(updateWrites)

    def _combine_shared_site_ids(self, baseColumn, updateColumn):
        '''
        use when combinedIDs has multiple columns representing mappings to the
        same site. this happen after merging.

        looks for empty entries in the baseColumn that have data in updateColumn, and adds
        those in. if an entry exists in baseColumn, it is not updated by updateColumn.
        After updating, the updateColumn is dropped from the dataframe

        baseColumn:         Series with mappings to use as base.
        updateColumn:       Series to update base with
        '''

        self._logger.debug('Updating {0} with {1}'.format(baseColumn, updateColumn))

        #find entries to update
        updateMask = self._combinedIDs[baseColumn].isna() & self._combinedIDs[updateColumn].notna()

        #check to see if all others match
        #notMatched = (self._combinedIDs.loc[~updateMask, baseColumn] != self._combinedIDs.loc[~updateMask, updateColumn])
        #if notMatched.any():
        #    print('there are ' + str(notMatched.sum()) + 'existing entries that are not a match')

        #update and drop updateColumn
        self._combinedIDs.loc[updateMask, baseColumn] = self._combinedIDs.loc[updateMask, updateColumn]
        self._combinedIDs.drop(updateColumn, axis=1, inplace=True)

    def _get_combined_mappings(self):
        '''
        chadwick list is the base, all other mappings are added to that.

        join on
            chadwick: key_mlbam
            crunchtime: mlb_id
            mysportsfeeds: mlb_id
            smartFantasy: MLBID
        '''

        self._logger.debug('Downloading and combining mappings')

        chadwick = downloaders_mlb.get_chadwick_mapping()
        self._updateSuccess['chadwick'] = True

        # add in crunchtime mappings
        # update any mappings that aren't there for shared sites
        #   fangraphs (key_fangraphs, fg_id)
        #   baseball reference (key_bbref, bref_id)
        #   retrosheet (key_retro, retro_id)
        crunch = downloaders_mlb.get_crunchtime_baseball_mapping()
        crunchMask = crunch['mlb_id'].notna()
        self._combinedIDs = chadwick.merge(crunch.loc[crunchMask], how='left', left_on='key_mlbam', right_on='mlb_id')
        self._combinedIDs.drop('mlb_id', axis=1, inplace=True)
        self._combine_shared_site_ids('key_fangraphs', 'fg_id')
        self._combine_shared_site_ids('key_bbref', 'bref_id')
        self._combine_shared_site_ids('key_retro', 'retro_id')
        self._updateSuccess['crunchtime'] = True

        # add in smartfantasy mappings
        #   fangraphs (key_fangraphs, IDFANGRAPHS)
        #   CBS (cbs_id, CBSID)
        #   Retrosheet (key_retro, RETROID)
        #   baseball reference (key_bbref, BREFID)
        #   espn (espn_id, ESPNID)
        #   nfbc (nfbc_id, NFBCID)
        #   baseball prospectus (bp_id, BPID)
        #   yahoo (yahoo_id, YAHOOID)
        #   rotowire (rotowire_id, ROTOWIREID)
        #   ottoneu (ottoneu_id, ottoneu_pos)
        smartFantasy = downloaders_mlb.get_smart_fantasy_baseball_mapping()
        smartMask = smartFantasy['MLBID'].notna()
        self._combinedIDs = self._combinedIDs.merge(smartFantasy.loc[smartMask], how='left', left_on='key_mlbam', right_on='MLBID')
        self._combinedIDs.drop('MLBID', axis=1, inplace=True)
        self._combine_shared_site_ids('key_fangraphs', 'IDFANGRAPHS')
        self._combine_shared_site_ids('cbs_id', 'CBSID')
        self._combine_shared_site_ids('key_retro', 'RETROID')
        self._combine_shared_site_ids('key_bbref', 'BREFID')
        self._combine_shared_site_ids('espn_id', 'ESPNID')
        self._combine_shared_site_ids('nfbc_id', 'NFBCID')
        self._combine_shared_site_ids('bp_id', 'BPID')
        self._combine_shared_site_ids('yahoo_id', 'YAHOOID')
        self._combine_shared_site_ids('rotowire_id', 'ROTOWIREID')
        self._combine_shared_site_ids('ottoneu_id', 'OTTONEUID')
        self._updateSuccess['smartfantasy'] = True

        # get mysportsfeeds IDs
        if SETTINGS.MYSPORTSFEEDS_API_KEY is None:
            self._logger.warning('Unable to download from mysportsfeeds. No API key found.')
        else:
            msf = downloaders_mlb.get_mysportsfeeds_mappings(SETTINGS.MYSPORTSFEEDS_API_KEY)
            msfMask = msf['mlb_id'].notna()
            self._combinedIDs = self._combinedIDs.merge(msf.loc[msfMask], how='left', left_on='key_mlbam', right_on='mlb_id')
            self._combinedIDs.drop('mlb_id', axis=1, inplace=True)
            self._updateSuccess['mysportsfeeds'] = False

    def _update_from_known_mappings(self):
        '''
        update records with any known mappings from saved file

        knownMappings structure
            updateMappings or replaceMappings:
                src: [ {
                    uuid: <uuid string>
                    updateColumn: <update column>
                    updateValue: <update value>
                 }
                ]
        '''
        self._logger.debug('Applying additional mapping fixes')

        with pkg_resources.resource_stream(__name__, 'knownMappings.json') as f:
            knownMappings = json.load(f)

        #updateMappings only update if no value is present
        self._logger.debug('Applying update mapping fixes')
        for src, mappingList in knownMappings['updateMappings'].items():
            for mapping in mappingList:

                if mapping['updateColumn'] not in self._combinedIDs:
                        self._combinedIDs.loc[:, mapping['updateColumn']] = np.nan

                mask = (self._combinedIDs['key_uuid'] == mapping['uuid']) & (self._combinedIDs[mapping['updateColumn']]).isna()

                if mask.any():

                    self._combinedIDs.loc[mask, mapping['updateColumn']] = mapping['updateValue']

        #replaceMappings always overwrite existing value with updateValue
        self._logger.debug('Applying replace mapping fixes')
        for src in knownMappings['replaceMappings']:
            for mapping in src:
                mask = (self._combinedIDs['key_uuid'] == mapping['uuid'])
                self._combinedIDs.loc[mask, mapping['updateColumn']] = mapping['updateValue']

    def _convert_uuid(self):
        '''
        convert key_uuid column to UUID class
        '''

        self._logger.debug('converting uuid column')
        self._combinedIDs.loc[:, 'key_uuid'] = self._combinedIDs['key_uuid'].apply(uuid.UUID)

    def _create_integer_playerid(self):
        '''
        create a new column from the first 8 hex digits of UUID that converts this to
        a 64 bit integer. chadwick ensures that key_person is unique for the given set
        (which is by definition the first 8 hex digits of UUID)
        but may not be in the future, as people could be added or removed.

        outside cross references should be stored using UUIDs if possible to preserve consistency.

        calculates based off the key_uuid instead of key_person to ensure consistency of calculation

        adds a new column "player_id" to the _combinedIDs dataframe
        '''

        self._logger.debug('Calculating interger player_id')

        if self._combinedIDs is None:
            raise ValueError("Combined IDs not downloaded yet.")

        # int(x, 16) converts a string assuming base-16 number (hexadecimal)
        #   full uuid hex representation is a 32 character string (uuid can be represented as 128 bit integer)
        #   first 8 digits will make a more manageable key that can be reproduced from UUID
        #   could also get from uuid.hex[:8]

        if 'key_uuid' not in self._combinedIDs.columns:
            raise ValueError('key_uuid field not available for conversion.')

        # 8 digit length is hard coded so calculation is consistent
        self._combinedIDs.loc[:, 'player_id'] = self._combinedIDs['key_uuid'].apply(lambda x: int(x.hex[:8], 16))

def get_script_args():
    '''parse script command line arguments'''

    parser = argparse.ArgumentParser(description='Update the MLB player ID mappings.')

    #arguments from global settings class
    SETTINGS.add_args_to_argparse(parser)

    return parser.parse_args()

def main():
    '''main script entrypoint'''

    #parse arguments
    args = get_script_args()
    SETTINGS.reload(cmdArgs=args)
    set_up_console_logger(LOGGER_NAME)

    idScraper = PlayerIDScrapeMLB()
    idScraper.build_mappings()

    if idScraper.did_all_sources_update():
        idScraper.save_to_database(replaceAllFields=True)
    else:
        idScraper.save_to_database(replaceAllFields=False)

if __name__ == "__main__":
    main()
