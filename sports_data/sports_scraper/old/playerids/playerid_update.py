'''
Manages storage and combining of playerid mappings across various sources.

When the update is run, new mappings are downloaded and reconstructed or updated in
the database based on the newly downloaded mappings.
'''

import json
import uuid
import enum
import logging
import argparse
import pkg_resources
import pandas as pd
import numpy as np

import sports_data.support.database as database_support
import sports_data.sports_scraper.playerids.mlb.downloaders as mlb_downloaders
from sports_data.support.logging import set_up_console_logger
from sports_data.db import SITE_ID

LOGGER_NAME = __name__

class PlayerIdMappingsMLB:
    '''
    class downloads, updates, and saves updates of player_id mappings
    across various MLB projection/fantasy sports/news sites.
    '''

    def __init__(self):
        '''
        databaseDef:    a DatabaseDefinition object defining
                        a database connection to save and load
                        mappings to.
        '''
        self._updateSuccess = {
            'chadwick': False,
            'crunchtime': False,
            'smartfantasy': False,
            'mysportsfeeds': False
        }

        # self._idmap is a pandas Series, indexed
        #  by (uuid,int) where the
        #  uuid is the key_uuid from chadwick and acts as
        #  the overall player id, and int is one of SITE_ID
        #  enum value. The value is a string containing the site
        #  player Id for SITE_ID
        self._idmap = None

        set_up_console_logger(LOGGER_NAME)
        self._logger = logging.getLogger(LOGGER_NAME)

    @classmethod
    def add_argparse_arguments(cls, parser):
        '''
        add arguments to argparse parser
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an argparse.ArgumentParser.")

        #add arguments for database
        database_support.DatabaseDefinition.add_argparse_arguments(parser)

        parser.add_argument(
            '--LOG_LEVEL',
            dest='LOG_LEVEL',
            action='store',
            default='INFO',
            help='The level to log at.'
        )

        parser.add_argument(
            '--OUTPUT_FILE_PATH',
            dest='OUTPUT_FILE_PATH',
            action='store',
            help='The path to save the completed mappings to a CSV.'
        )

        parser.add_argument(
            '--INPUT_FILE_PATH',
            dest='INPUT_FILE_PATH',
            action='store',
            help='The path to load the existing mappings from a CSV.'
        )

        parser.add_argument(
            '--DATA_SOURCE',
            dest='DATA_SOURCE',
            action='store',
            choices=['postgres', 'file', 'online'],
            help='The source of mappings to start from.'
        )

        parser.add_argument(
            '--DATA_DESTINATION',
            dest='DATA_DESTINATION',
            action='store',
            choices=['postgres', 'file'],
            help='The destination to save mappings to.'
        )

    def did_all_sources_update(self):
        '''
        return true if all sources updated, false if not.
        '''

        updateSeries = pd.Series(self._updateSuccess)
        return updateSeries.all()

    def build_updated_mappings(self, overwrite=False):
        '''
        build combined mappings
        '''

        self._logger.info('Building mappings')

        self._get_combined_mappings(overwrite=overwrite)
        self._update_from_known_mappings()
        self._create_integer_playerid()

    def get_from_postgres_database(self, databaseDef):
        '''
        load mappings from postgres database
        '''

        if databaseDef:
            if not isinstance(databaseDef, database_support.DatabaseDefinition):
                raise ValueError("databaseDef must be class DatabaseDefinition.")

        with databaseDef.create_postgres_connection() as pgConnection:

            sql = ('SELECT overall_player_id,source_id,source_player_id '
                  'FROM ids.mlb_id_map')

            df = pd.read_sql_query(sql, pgConnection)

            # convert overall_player_id to uuid and set index
            df.rename(columns={'overall_player_id': 'key_uuid'}, inplace=True)
            df = df.astype({'source_player_id': 'string'})
            #df.loc[:,'key_uuid'] = df['key_uuid'].apply(lambda x: uuid.UUID(x))
            df.set_index(['key_uuid','source_id'], inplace=True)

            # set to internal _idmap
            self._idmap = df['source_player_id']

    def save_to_postgres_database(self, databaseDef):
        '''
        save mappings to postgres database
        '''

        if databaseDef:
            if not isinstance(databaseDef, database_support.DatabaseDefinition):
                raise ValueError("databaseDef must be class DatabaseDefinition.")

        with databaseDef.create_postgres_connection() as pgConnection:
            pgConnection.autocommit = False
            cursor = pgConnection.cursor()

            try:
                for (key_uuid,source_id), player_id in self._idmap.items():

                    values = [key_uuid, int(source_id), str(player_id)] * 2
                    insertQuery = ('INSERT INTO ids.mlb_id_map (overall_player_id,source_id,source_player_id) '
                                'VALUES (%s,%s,%s) '
                                'ON CONFLICT (overall_player_id,source_id) DO '
                                'UPDATE SET overall_player_id=%s, source_id=%s, source_player_id=%s;')

                    cursor.execute(insertQuery, values)

                pgConnection.commit()

            except Exception as exceptHndle:
                pgConnection.rollback()
                raise exceptHndle

    def save_to_table_csv(self, filePath):
        '''
        Save mappings to a CSV file in tabular format.
        '''

        tabular = self._get_idmap_tabular()

        if tabular is not None:
            tabular.to_csv(filePath)
        else:
            raise ValueError("_idmap not initialized.")

    def load_from_table_csv(self, filePath):
        '''
        Load mappings from a CSV file in tabular format.
        '''

        with open(filePath) as f:
            mappings = pd.read_csv(f, header=0)
            mappings.loc[:,'key_uuid'] = mappings['key_uuid'].apply(lambda x: uuid.UUID(x))
            self._set_idmap_from_tabular(mappings.set_index('key_uuid'))

    def get_overall_player_id(self, matchSource, playerId):
        '''
        get the overall UUID of the player with id playerId in source matchSourceId.

        matchSource:    The source_id enum SITE_ID that describes the source
                        that the ids in other's index should be matched to in self._idmap.
                        This can also be None. if None, then other is assumed to be
                        indexed by (key_uuid, source_id) and ready for adding to _idmap.
        player_id:      The player's ID in matchSourceId.
        '''

        matchedMappings = self._idmap.loc[:, matchSource.value]
        mask = (matchedMappings == str(playerId))

        if not mask.any():
            # no match found
            return None
        else:
            matchedUUID = matchedMappings.index.get_level_values(level='key_uuid')[mask]
            return matchedUUID.values[0]

    def get_overall_player_id_multi(self, matchSource, playerId):
        '''
        get the overall UUIDs of the players with id playerId in source matchSourceId.

        matchSource:    The source_id enum SITE_ID that describes the source
                        that the ids in other's index should be matched to in self._idmap.
                        This can also be None. if None, then other is assumed to be
                        indexed by (key_uuid, source_id) and ready for adding to _idmap.
        player_id:      A list or set of player IDs in matchSourceId.

        returns a Series indexed by playerId, values of UUID
        '''

        matchedMappings = self._idmap.loc[:, matchSource.value]
        mask = matchedMappings.isin([str(x) for x in playerId])

        if not mask.any():
            # no match found, empty series return
            return pd.Series([], dtype='object')
        else:
            matchedUUID = matchedMappings.index.get_level_values(level='key_uuid')[mask]
            result = pd.Series(matchedUUID, index=playerId)
            result.index.name = matchSource.name
            return result

    def get_player_id(self, matchSource, matchPlayerId, newSource):
        '''
        convert the id of the player with id playerId in source matchSourceId to
        their ID in newSource.

        matchSource:    The source_id enum SITE_ID that describes the source
                        that the ids in other's index should be matched to in self._idmap.
                        This can also be None. if None, then other is assumed to be
                        indexed by (key_uuid, source_id) and ready for adding to _idmap.
        newSource:      The source_id enum SITE_ID that describes the desired source
                        that the matchPlayerId will be converted to.
        player_id:      The player's ID in matchSourceId.
        '''

        matchedUUID = self.get_overall_player_id(matchSource, matchPlayerId)

        if matchedUUID is None:
            return None

        try:
            #the below results in a string
            convertedId = self._idmap.loc[matchedUUID, newSource.value]
            return convertedId

        except KeyError:
            # mapping combo does not exist
            return None

    def get_player_id_multi(self, matchSource, matchPlayerId, newSource):
        '''
        convert the id of the player with id playerId in source matchSourceId to
        their ID in newSource.

        matchSource:    The source_id enum SITE_ID that describes the source
                        that the ids in other's index should be matched to in self._idmap.
                        This can also be None. if None, then other is assumed to be
                        indexed by (key_uuid, source_id) and ready for adding to _idmap.
        newSource:      The source_id enum SITE_ID that describes the desired source
                        that the matchPlayerId will be converted to.
        matchPlayerId:  A list or set of player IDs in matchSourceId.

        returns a Series indexed by matchPlayerId, values of id in newSource
        '''

        matchedUUID = self.get_overall_player_id_multi(matchSource, matchPlayerId)

        if matchedUUID is None:
            return None

        try:
            #the below results in a string
            convertedIds = self._idmap.loc[matchedUUID.values, newSource.value]
            mask = matchedUUID.isin(convertedIds.index.get_level_values(level='key_uuid'))
            newIds = pd.Series(convertedIds.values, index=matchedUUID.loc[mask])
            newIds.index.name = matchSource.name
            newIds.name = newSource.name
            return newIds

        except KeyError:
            # mapping combo does not exist
            return None

    def _get_idmap_tabular(self):
        '''
        turn self._idmap from a series to a table, with SITE_ID names in place of
        id values.

        returned table is indexed by key_uuid
        '''

        if self._idmap is None:
            return None
        else:
            # move source_id index level to columns (these are integer values)
            tabularIds = self._idmap.unstack(level='source_id')

            # rename columns to name corresponding to their SITE_ID enum value
            tabularIds.rename(columns=lambda x: SITE_ID(x).name, inplace=True)

            # name the column level and return
            tabularIds.columns.name = 'source_id'
            return tabularIds

    def _set_idmap_from_tabular(self, tabularIds):
        '''
        turn tabular id map into self._idmap. Inverse of _get_idmap_tabular().
        '''

        if tabularIds is None:
            raise ValueError("tabularIds not defined.")
        else:
            if tabularIds.index.name != 'key_uuid':
                raise ValueError("tabularIds must be indexed by key_uuid.")

            # rename columns to SITE_ID enum value based on their name
            tabularIds.rename(columns=lambda x: SITE_ID[x].value, inplace=True)

            # move columns to another index level, removing missing entries
            self._idmap = tabularIds.stack('source_id', drapna=True)

    def _add_in_mapping_set(self, other, matchSourceId, newSourceId, overwrite=False):
        '''
        Add in a new set of mappings to self._idmap.

        other:          Series of id mappings to add to self._idmap.
                        indexed by the player id that is to be used to match
                        with self._idmap (corresponding to site matchSourceId).
                        values should be dtype=string
        matchSourceId:  The source_id enum SITE_ID that describes the source
                        that the ids in other's index should be matched to in self._idmap.
                        This can also be None. if None, then other is assumed to be
                        indexed by (key_uuid, source_id) and ready for adding to _idmap.
        newSourceId:    The source_id enum SITE_ID that describes the source
                        that the ids in other will be added for.
        overwrite:      optional parameter. True means that mappings in other overwrite
                        the ones present in _idmap, false means they are ignored and
                        only new mappings are added.
        '''

        if not isinstance(matchSourceId, enum.Enum):
            if matchSourceId:
                raise ValueError("matchSourceId must be SITE_ID enum or None.")

        if not isinstance(newSourceId, enum.Enum):
            raise ValueError("newSourceId must be SITE_ID enum.")


        if self._idmap is None:
            raise ValueError('_idmap not initialized.')

        else:

            if matchSourceId is None:
                # matchSourceId is None, other is meant to be added directly to id_map.

                if not other.index.names[0] == 'key_uuid' or not other.index.names[1] == 'source_id':
                    raise ValueError("other must be indexed by (key_uuid,source_id) if no matching id is given.")

                self._logger.info('Adding mappings for {0} into id map.'.format(newSourceId.name))
                updateMatches = other

            else:
                # id to match on is given

                if not matchSourceId.value in self._idmap.index.levels[1]:
                    # matchSourceId is not an available source to match
                    #  abandon combine.
                    raise ValueError("matchSourceId is not available to match.")

                else:
                    # some entries in self._idmap have the correct source to attempt match

                    self._logger.info('Adding matched mappings for {0} into id map.'.format(newSourceId.name))

                    # make a dataframe with only the mappings available for matchSourceId.
                    #   the index is the key_uuid and the column name is the matchSourceId.value
                    idx = pd.IndexSlice
                    existingMatchSource = pd.DataFrame({matchSourceId.name: self._idmap.loc[idx[:, matchSourceId.value]]})

                    # other is indexed by the player IDs from matchSourceId. merge into the matched dataframe, joining
                    #   by those overlapping ids. non-matched rows are given <NA> values.
                    other.name = newSourceId.name
                    mergedDF = existingMatchSource.merge(other, how='left', left_on=matchSourceId.name, right_index=True)

                    # extract a series of the non-null entries from the merged frame
                    updateMatches = mergedDF.loc[~mergedDF[newSourceId.name].isna(), newSourceId.name]
                    updateMatches.drop_duplicates(inplace=True)

                    # the extracted updates will be indexed by key_uuid. add new source id as secondary index level
                    newIdx = pd.MultiIndex.from_arrays([updateMatches.index.values,[newSourceId.value]*len(updateMatches)])
                    updateMatches.index = newIdx

            # check if any of the updated matchings already exist
            #   difference returns the set that differs between the two
            # in this case, toUpdate is an index that shows the subset of
            #   new player id values that don't yet exist in _idmap
            toUpdate = updateMatches.index.difference(self._idmap.index)

            # add new mappings to self._idmap. updateMatches has the same index structure as _idmap
            #   will only add mappings that do not already exist in
            #   ensure no duplicate (key_uuid, source_id) index pairs
            if toUpdate.size > 0:
                self._idmap = self._idmap.append(updateMatches.loc[toUpdate], verify_integrity=True)

            if overwrite:
                # if the option is set, replace exisiting mappings in idmap that
                #   are redefined in other.
                toOverwrite = updateMatches.index.intersection(self._idmap.index)
                self._idmap.loc[toOverwrite] = updateMatches.loc[toOverwrite]

    def _add_chadwick(self, overwrite=False):
        '''
        add in mappings from chadwick bureau register.
        these are the foundation of the overall mappings,
        providing the player UUID.
        '''

        # note: the first level of the index is key_uuid
        # this also maps to source_id enums
        sourcesToKeep = {
            'key_retro': SITE_ID.RETROSHEET,
            'key_mlbam': SITE_ID.MLB,
            'key_bbref': SITE_ID.BASEBALL_REFERENCE,
            'key_bbref_minors': SITE_ID.BASEBALL_REFERENCE_MINORS,
            'key_fangraphs': SITE_ID.FANGRAPHS
        }

        selector = pd.IndexSlice[:, sourcesToKeep.keys()]
        chadwick = mlb_downloaders.get_chadwick_bureau_register()
        chadwick = chadwick.loc[selector]

        # move source_id index level to columns (these are integer values)
        chadwickTabular = chadwick.unstack(level='source_name')

        # rename columns to SITE_ID enum value corresponding to mapping in sourcesToKeep
        chadwickTabular.rename(columns=lambda x: sourcesToKeep[x].value, inplace=True)

        # restack to series and set to _idmap
        chadwickTabular.columns.name = 'source_id'
        stackedChad = chadwickTabular.stack(level='source_id')

        if overwrite:
            self._idmap = stackedChad
        else:
            toOverwrite = stackedChad.index.intersection(self._idmap.index)
            self._idmap.loc[toOverwrite] = stackedChad.loc[toOverwrite]

            toUpdate = stackedChad.index.difference(self._idmap.index)
            if toUpdate.size > 0:
                self._idmap = self._idmap.append(stackedChad.loc[toUpdate], verify_integrity=True)

        self._logger.info('Added chadwick bureau mappings into id map.')

    def _add_crunchtime(self, overwrite=False):
        '''
        add in mappings from CrunchTimeBaseball.com.
        these will be matched to _idmap using overlapping keys
        '''

        # note: the first level of the index is key_uuid
        # this also maps to source_id enums
        sourcesToKeep = {
            'retro_id': SITE_ID.RETROSHEET,
            'mlb_id': SITE_ID.MLB,
            'bref_id': SITE_ID.BASEBALL_REFERENCE,
            'fg_id': SITE_ID.FANGRAPHS,
            'bp_id': SITE_ID.BASEBALL_PROSPECTUS,
            'cbs_id': SITE_ID.CBS,
            'espn_id': SITE_ID.ESPN,
            'lahman_id': SITE_ID.LAHMAN,
            'nfbc_id': SITE_ID.NFBC,
            'yahoo_id': SITE_ID.YAHOO,
            'ottoneu_id': SITE_ID.OTTONUEU,
            'rotowire_id': SITE_ID.ROTOWIRE
        }

        crunchtime = mlb_downloaders.get_crunchtime_baseball_mapping()
        crunchtime = crunchtime.loc[:, sourcesToKeep.keys()]

        # rename columns to SITE_ID enum value corresponding to mapping in sourcesToKeep
        crunchtime.rename(columns=lambda x: sourcesToKeep[x].value, inplace=True)
        #crunchtime.columns.name = 'source_id'

        # mlb_id column is typically filled and can be used for joining
        crunchtime.set_index(SITE_ID.MLB.value, inplace=True)

        # add each set to _idmap
        for col in crunchtime.columns:
            self._add_in_mapping_set(crunchtime[col], SITE_ID.MLB, SITE_ID(col), overwrite=overwrite)

        self._logger.info('Added CrunchTimeBaseball.com mappings into id map.')

    def _add_smart(self, overwrite=False):
        '''
        add in mappings from www.smartfantasybaseball.com.
        these will be matched to _idmap using overlapping keys
        '''

        # note: the first level of the index is key_uuid
        # this also maps to source_id enums
        sourcesToKeep = {
            'RETROID': SITE_ID.RETROSHEET,
            'MLBID': SITE_ID.MLB,
            'BREFID': SITE_ID.BASEBALL_REFERENCE,
            'IDFANGRAPHS': SITE_ID.FANGRAPHS,
            'BPID': SITE_ID.BASEBALL_PROSPECTUS,
            'CBSID': SITE_ID.CBS,
            'ESPNID': SITE_ID.ESPN,
            'NFBCID': SITE_ID.NFBC,
            'YAHOOID': SITE_ID.YAHOO,
            'OTTONEUID': SITE_ID.OTTONUEU,
            'ROTOWIREID': SITE_ID.ROTOWIRE,
            'DAVENPORTID': SITE_ID.DAVENPORT,
            'FANTRAXID': SITE_ID.FANTRAX,
            'HQID': SITE_ID.BASEBALL_HQ,
            'FANDUELID': SITE_ID.FANDUEL,
            'RAZZBALLNAME': SITE_ID.RAZZBALL,
            'MSTRBLLNAME': SITE_ID.MASTERBALL
        }


        smrt = mlb_downloaders.get_smart_fantasy_baseball_mapping()
        smrt = smrt.loc[:, sourcesToKeep.keys()]

        # rename columns to SITE_ID enum value corresponding to mapping in sourcesToKeep
        smrt.rename(columns=lambda x: sourcesToKeep[x].value, inplace=True)

        # BREFID column is typically filled and can be used for joining
        smrt.set_index(SITE_ID.BASEBALL_REFERENCE.value, inplace=True)

        # add each set to _idmap
        for col in smrt.columns:
            self._add_in_mapping_set(smrt[col], SITE_ID.BASEBALL_REFERENCE, SITE_ID(col), overwrite=overwrite)

        self._logger.info('Added www.smartfantasybaseball.com mappings into id map.')

    def _add_mysportsfeeds(self, overwrite=False):
        '''
        add in mappings from mysportsfeeds api.
        these will be matched to _idmap using overlapping keys
        '''

        # note: the first level of the index is key_uuid
        # this also maps to source_id enums
        sourcesToKeep = {
            'mysportsfeeds_id': SITE_ID.MY_SPORTS_FEEDS,
            'mlb_id': SITE_ID.MLB
        }

        msf = mlb_downloaders.get_mysportsfeeds_mappings()
        msf = msf.loc[:, sourcesToKeep.keys()]

        # rename columns to SITE_ID enum value corresponding to mapping in sourcesToKeep
        msf.rename(columns=lambda x: sourcesToKeep[x].value, inplace=True)

        # mlb_id column can be used for joining
        msf.set_index(SITE_ID.MLB.value, inplace=True)

        # add ids to _idmap
        self._add_in_mapping_set(msf[SITE_ID.MY_SPORTS_FEEDS.value], SITE_ID.MLB, SITE_ID.MY_SPORTS_FEEDS, overwrite=overwrite)

        self._logger.info('Added mysportsfeeds API mappings into id map.')

    def _get_combined_mappings(self, overwrite=False):
        '''
        chadwick list is the base, all other mappings are added to that.
        '''

        self._logger.debug('Downloading and combining mappings')

        self._add_chadwick(overwrite=overwrite)
        self._updateSuccess['chadwick'] = True

        self._add_crunchtime(overwrite=overwrite)
        self._updateSuccess['crunchtime'] = True

        self._add_smart(overwrite=overwrite)
        self._updateSuccess['smartfantasy'] = True

        # get mysportsfeeds IDs
        #if SETTINGS.MYSPORTSFEEDS_API_KEY is None:
        #    self._logger.warning('Unable to download from mysportsfeeds. No API key found.')
        #else:
        #    self._add_mysportsfeeds(overwrite=overwrite)
        #    self._updateSuccess['mysportsfeeds'] = True

    def _update_from_known_mappings(self):
        '''
        update records with any known mappings from saved file

        knownMappings structure
            updateMappings: only add if not included
            replaceMappings: overwrite if present, otherwise add
            updateMappings or replaceMappings:
                <SITE_ID indicating source to update>: [ {
                    key_uuid: <key_uuid for player to update>
                    updateId: <player id>
                 }
                ]
        '''
        self._logger.debug('Applying additional mapping fixes')

        with pkg_resources.resource_stream(__name__, 'knownMappings.json') as f:
            knownMappings = json.load(f)

        #updateMappings only update if no value is present
        #  make a dataframe with compatible index, and send
        #  none-overwriting call to _add_in_mapping_set()
        self._logger.debug('Applying update mapping fixes')
        for src, mappingList in knownMappings['updateMappings'].items():
            updatesDF = pd.DataFrame.from_records(mappingList)
            updatesDF.loc[:, 'source_id'] = SITE_ID[src].value
            updatesDF.loc[:, 'key_uuid'] = updatesDF['key_uuid'].apply(lambda x: uuid.UUID(x))
            updatesDF.set_index(['key_uuid', 'source_id'], inplace=True)
            self._add_in_mapping_set(updatesDF['updateId'], None, SITE_ID[src], overwrite=False)

        #replaceMappings always overwrite existing value with updateValue
        self._logger.debug('Applying replace mapping fixes')
        for src, mappingList in knownMappings['replaceMappings'].items():
            replaceDF = pd.DataFrame.from_records(mappingList)
            replaceDF.loc[:, 'source_id'] = SITE_ID[src].value
            replaceDF.loc[:, 'key_uuid'] = replaceDF['key_uuid'].apply(lambda x: uuid.UUID(x))
            replaceDF.set_index(['key_uuid', 'source_id'], inplace=True)
            self._add_in_mapping_set(replaceDF['updateId'], None, SITE_ID[src], overwrite=True)

    def _create_integer_playerid(self):
        '''
        create a new column from the first 8 hex digits of UUID that converts this to
        a 64 bit integer. chadwick ensures that key_person is unique for the given set
        (which is by definition the first 8 hex digits of UUID)
        but may not be in the future, as people could be added or removed.

        outside cross references should be stored using UUIDs if possible to preserve consistency.

        calculates based off the key_uuid instead of key_person to ensure consistency of calculation

        adds a new set of mappings LOCAL_PLAYER_ID to the _idmap dataframe
        '''

        self._logger.debug('Calculating interger player_id')

        if self._idmap is None:
            raise ValueError("Combined IDs not downloaded yet.")

        # int(x, 16) converts a string assuming base-16 number (hexadecimal)
        #   full uuid hex representation is a 32 character string (uuid can be represented as 128 bit integer)
        #   first 8 digits will make a more manageable key that can be reproduced from UUID
        #   could also get from uuid.hex[:8]

        if self._idmap.index.names[0] != 'key_uuid':
            raise ValueError('key_uuid index level not available for conversion.')

        # create a series with value and index both
        #   deduplicate
        uuidSeries = self._idmap.index.get_level_values('key_uuid').to_series()
        uuidSeries.drop_duplicates(inplace=True)

        # calculate local 64 bit integer ID
        #   8 digit length is hard coded so calculation is consistent
        uuidSeries = uuidSeries.apply(lambda x: int(x.hex[:8], 16))

        # cast to string for compatibility
        uuidSeries = uuidSeries.apply(lambda x: str(x))
        uuidSeries = uuidSeries.astype('string')

        # make index compatible
        newIdx = pd.MultiIndex.from_arrays(
                    [uuidSeries.index.values,[SITE_ID.LOCAL_PLAYER_ID.value]*len(uuidSeries)],
                    names=['key_uuid', 'source_id']
                )
        uuidSeries.index = newIdx

        # add to _idmap if unique
        if uuidSeries.is_unique:
            self._add_in_mapping_set(
                uuidSeries,
                None,
                SITE_ID.LOCAL_PLAYER_ID,
                overwrite=True
            )
        else:
            self._logger.warning('Integer player IDs calculated from UUID are not unique. Not added to mapping set.')

def _get_mappings_cli(args):
    '''
    function for get_mappings command.
    '''

    if args.DATA_SOURCE is None:
        raise ValueError('DATA_SOURCE must be given.')

    if args.DATA_DESTINATION is None:
        raise ValueError('DATA_DESTINATION must be given.')

    if args.DATA_SOURCE == 'file':
        if args.INPUT_FILE_PATH is None:
            raise ValueError('INPUT_FILE_PATH must be given with file DATA_SOURCE.')

    if args.DATA_DESTINATION == 'file':
        if args.OUTPUT_FILE_PATH is None:
            raise ValueError('OUTPUT_FILE_PATH must be given with file DATA_DESTINATION.')

    idMappings = PlayerIdMappingsMLB()
    if args.DATA_SOURCE == 'postgres':
        sourceDbDef = database_support.DatabaseDefinition.from_argparse_arguments(args)
        idMappings.get_from_postgres_database(sourceDbDef)

    elif args.DATA_SOURCE == 'file':
        idMappings.load_from_table_csv(args.INPUT_FILE_PATH)

    elif args.DATA_SOURCE == 'online':
        idMappings.build_updated_mappings(overwrite=True)

    if args.DATA_DESTINATION == 'file':
        idMappings.save_to_table_csv(args.OUTPUT_FILE_PATH)

    if args.DATA_DESTINATION == 'postgres':
        destDbDef = database_support.DatabaseDefinition.from_argparse_arguments(args)
        idMappings.save_to_postgres_database(destDbDef)

def build_script_parser():
    '''parse script command line arguments'''

    parser = argparse.ArgumentParser(description='Update the MLB player ID mappings.')

    subparsers = parser.add_subparsers()

    get_mappings = subparsers.add_parser('get_mappings')
    get_mappings.set_defaults(func=_get_mappings_cli)
    PlayerIdMappingsMLB.add_argparse_arguments(get_mappings)

    return parser

def main():
    '''main script entrypoint'''

    #parse arguments
    parser = build_script_parser()
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()

    #--------------------------------------
    # for testing
    #parser = build_script_parser()
    #args = parser.parse_args()
    #sourceDbDef = database_support.DatabaseDefinition.from_argparse_arguments(args)
    #idMappings = PlayerIdMappingsMLB()
    #idMappings.get_from_postgres_database(sourceDbDef)
    #print(idMappings.get_player_id_multi(SITE_ID.LOCAL_PLAYER_ID, [2009,6328,10497], SITE_ID.MLB))
