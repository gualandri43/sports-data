'''
match numberfire players with current player mappings
'''

import logging
import json
import re
import pkg_resources
import argparse

from sports_data.support.database import DatabaseDefinition
from sports_data.support.logging import set_up_console_logger
from sports_modules import SETTINGS

LOGGER_NAME = "numberfire_playerid_match"

TEAM_CONVERSIONS = {
    'CHW': 'CWS'
}

def clean_first_name(name):
    '''
    helper function clean up numberfire first names.
    '''

    cleanName = name.capitalize()

    if re.match(r'.\..\.', cleanName):
        cleanName = '. '.join([x.capitalize() for x in cleanName.split('.')]).strip()

    if '-' in cleanName:
        cleanName = '-'.join([x.capitalize() for x in cleanName.split('-')]).strip()

    return cleanName

def clean_last_name(name):
    '''
    helper function clean up numberfire last names.
    '''

    cleanName = name.capitalize()

    if "'" in cleanName:
        cleanName = "'".join([x.capitalize() for x in cleanName.split("'")]).strip()

    if "Mcc" in cleanName[:3]:
        cleanName = 'McC' + cleanName[3:]

    if "Mcg" in cleanName[:3]:
        cleanName = 'McG' + cleanName[3:]

    return cleanName

def get_latest_player_list(dbDef):
    '''
    pulls a dataset of each player_id latest scraped record.
    '''

    logger = logging.getLogger(LOGGER_NAME)
    logger.info('pulling latest set of numbefire player ids')

    db = dbDef.get_mongodb_database('numberfire')
    playerCollection = db['mlb_players']

    matchDict = {'$match': {}}

    latestPlayersQuery = [
        matchDict,
        {'$sort': {'player_id': 1}},
        {'$group':
            {
                '_id': {'player_id': '$player_id'},
                'latestScrape': {'$last': '$scrape_time_utc'},
                'latestID': {'$last': '$_id'}
            }
        }
    ]

    latestPlayerIDs = []
    for rec in playerCollection.aggregate(latestPlayersQuery):
        latestPlayerIDs.append(rec['latestID'])

    # only pull latest player records
    playerCursor = playerCollection.find({'_id': {'$in': latestPlayerIDs}})

    return list(playerCursor)

def match_player(dbDef, firstname, lastname, team):
    '''
    returns UUID field of record found to be a match.
    None if a one-to-one match couldn't be found.
    '''

    logger = logging.getLogger(LOGGER_NAME)

    firstname = clean_first_name(firstname)
    lastname = clean_last_name(lastname)

    # get the overall player mapping
    db = dbDef.get_mongodb_database("id_mappings")
    collection = db["mlb"]

    qry = {"name_first": firstname, "name_last": lastname}
    numFound = collection.count_documents(qry)

    if numFound == 0:
        logger.warning('Player not found: {0} {1} {2}'.format(firstname, lastname, team))
        return None

    if numFound == 1:
        logger.debug('Player found: {0} {1} {2}'.format(firstname, lastname, team))
        player = collection.find_one(qry)
        return player['key_uuid']

    if numFound > 1:
        for player in collection.find(qry):
            if player['mlb_team'] == team:
                return player['key_uuid']

        logger.warning('Multiple matches found, could not determine specific match: {0} {1} {2}'.format(firstname, lastname, team))
        return None

def find_matches(dbDef):
    '''
    looks for matches between numberfire players and player id mappings

    run this script, then add to and commit changes to knownMappings.json

    returns a list of any new matched nf player ids and one of those
    that are not found or already known
    ([newMatches], [notFound])

        knownMappings structure
            updateMappings or replaceMappings:
                src: [ {
                    uuid: <uuid string>
                    updateColumn: <update column>
                    updateValue: <update value>
                 }
                ]
    '''

    logger = logging.getLogger(LOGGER_NAME)
    logger.info('finding matches to numbefire player ids')

    logger.debug('loading in knownMappings file')
    with pkg_resources.resource_stream(__name__, 'knownMappings.json') as f:
        knownMappings = json.load(f)

    #load in known mapped id sets
    alreadyKnownUUID = set()
    alreadyKnownIDs = set()
    for mapping in knownMappings['updateMappings']['numberfire']:
        alreadyKnownUUID.add(mapping['uuid'])
        alreadyKnownIDs.add(mapping['updateValue'])

    #try to match any unknowns in numberfire players collection
    matchNotFound = []
    newMatchFound = []
    for player in get_latest_player_list(dbDef):

        if player['team'] in TEAM_CONVERSIONS:
            team = TEAM_CONVERSIONS[player['team']]
        else:
            team = player['team']

        if player['player_id'] not in alreadyKnownIDs:

            foundUUID = match_player(dbDef, player['firstname'], player['lastname'], team)

            if foundUUID is not None:
                logger.debug('match found: ' + str(foundUUID))

                if str(foundUUID) not in alreadyKnownUUID:

                    alreadyKnownUUID.add(str(foundUUID))
                    newMatchFound.append(
                        {
                            'uuid': str(foundUUID),
                            'updateColumn': 'numberfire_id',
                            'updateValue': player['player_id']
                        }
                    )

            else:
                logger.debug('match not found found: ' + player['player_id'])
                matchNotFound.append(player['player_id'])

        else:
            logger.debug('player_id mapping already known: ' + player['player_id'])

    return (newMatchFound, matchNotFound)

def get_script_args():
    '''
    parse commandline args
    '''

    parser = argparse.ArgumentParser(description='Map numberfire player ids.')
    parser = SETTINGS.add_args_to_argparse(parser)

    #additional args
    parser.add_argument('--runAutoMapper', dest='runAutoMapper', action='store_true',
                        help='Attempt to match any unmapped numberfire ids')
    parser.add_argument('--outputDir', dest='outputDir', action='store',
                        help='directory to output results to. INCLUDE trailing slash')

    return parser.parse_args()

def main():
    '''
    entrypoint
    '''

    #parse arguments
    args = get_script_args()
    SETTINGS.reload(cmdArgs=args)

    set_up_console_logger(LOGGER_NAME)
    logger = logging.getLogger(LOGGER_NAME)

    dbDef = DatabaseDefinition()

    if args.runAutoMapper:
        logger.info('running automapper')
        newMatches, notFound = find_matches(dbDef)

        if args.outputDir is None:
            logger.info('new matches: {0}'.format(newMatches))
            logger.info('not found: {0}'.format(notFound))
        else:
            logger.info('outputting matches to {0}'.format(args.outputDir))

            with open(args.outputDir + 'newMatches.json', 'w') as f:
                json.dump(newMatches, f)

            with open(args.outputDir + 'notFound.json', 'w') as f:
                json.dump(notFound, f)

if __name__ == "__main__":
    main()
