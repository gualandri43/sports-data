'''
routines to download mlb player mappings.
'''

import io
import logging
import uuid
import requests
import pandas as pd
import numpy as np
#from ohmysportsfeedspy import MySportsFeeds

from sports_data.support.logging import set_up_console_logger

LOGGER_NAME = __name__
set_up_console_logger(LOGGER_NAME)
LOGGER = logging.getLogger(LOGGER_NAME)


def get_chadwick_bureau_register():
    '''
    return a series of the Chadwick Bureau Register ID mapping from Github.
    The return series has a multi-index of (key_uuid, field) where the fields
    are listed below. The value is a string.

    key_uuid                                field                   value
    --------                                -----                   --------
    663ecca1-6b4e-4a11-9494-1caa6d6b2d13    key_person              663ecca1
                                            key_mlbam                 439524
                                            key_bbref_minors    thoen-001eve


    key_uuid:           The primary key, providing the most stable reference to a person.
                        Guaranteed not to be re-issued to a different person in the future.
    key_person:         The first eight (hex) digits of the key_uuid.
                        It is guaranteed that this is unique at any given time.
                        However, should a person's record be withdrawn from the register,
                        the same key_person may reappear referencing a different person in the future.
    key_retro:          The person's Retrosheet identifier.
    key_mlbam:          The person's identifier as used by MLBAM (for example, in Gameday).
    key_bbref:          The person's identifier on Major League pages on baseball-reference.com.
    key_bbref_minors:   The person's identifier on minor league and Negro League pages on baseball-reference.com.
    key_fangraphs:      The person's identifier on fangraphs.com (Major Leaguers). As fangraphs uses BIS identifiers,
                        this can also be used to match up people to BIS data.
    key_npb:            The person’s identifier on the NPB official site, npb.or.jp.
    key_sr_nfl:         The person's identifier on pro-football-reference.com
    key_sr_nba:         The person's identifier on basketball-reference.com
    key_sr_nhl:         The person's identifier on hockey-reference.com
    key_findagrave:     The identifier of the person's memorial on findagrave.com

    player info:
        name_last
        name_first
        name_given
        name_suffix
        name_matrilineal
        name_nick
        birth_year
        birth_month
        birth_day
        death_year
        death_month
        death_day
    '''

    #request headers
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br"
    }

    #download csv text file as string file object
    chadwickMappingURL = 'https://github.com/chadwickbureau/register/blob/master/data/people.csv?raw=true'
    req = requests.get(chadwickMappingURL, headers=httpHeaders)
    chdwckFile = io.StringIO(req.text)
    LOGGER.info('downloaded chadwick.')

    #import all as string (StringDtype)
    #first line of file has the headers
    # all columns to use from the downloaded CSV
    colsToKeep = [
        'key_uuid',
        'key_person',
        'key_retro',
        'key_mlbam',
        'key_bbref',
        'key_bbref_minors',
        'key_fangraphs',
        'key_npb',
        'key_sr_nfl',
        'key_sr_nba',
        'key_sr_nhl',
        'key_findagrave',
        'name_last',
        'name_first',
        'name_given',
        'name_suffix',
        'name_matrilineal',
        'name_nick',
        'birth_year',
        'birth_month',
        'birth_day',
        'death_year',
        'death_month',
        'death_day'
    ]

    # reset to beginning of file for import
    #chdwckFile.seek(0)
    df = pd.read_csv(chdwckFile, dtype='string', header=0, usecols=colsToKeep)

    #convert necessary columns
    LOGGER.info('formatting chadwick data.')

    df = df.astype({'key_uuid': 'object'})
    df.loc[:, 'key_uuid'] = df.loc[:, 'key_uuid'].apply(lambda x: uuid.UUID(x))

    # reorganize index to match multi-index output
    df.set_index('key_uuid', inplace=True)
    df.columns.name = 'source_name'
    return df.stack()

def get_crunchtime_baseball_mapping():
    '''
    return a dataframe of the CrunchTimeBaseball.com ID mapping. All values are
    downloaded as strings.
    No indexing by default. mlb_id is typically filled, could use that.
    '''

    #request headers
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br"
    }

    #download csv text file as string file object
    crunchTimeURL = 'http://crunchtimebaseball.com/master.csv'
    req = requests.get(crunchTimeURL, headers=httpHeaders)
    crunchyFile = io.StringIO(req.text)

    # columns to include from this source
    colsToKeep = [
        'mlb_id',           # MLB.com
        'mlb_name',
        'mlb_pos',
        'mlb_team',
        'bp_id',            # baseball prospectus
        'bref_id',          # baseball reference
        'cbs_id',           # CBS
        'cbs_pos',
        'espn_id',          # ESPN
        'espn_pos',
        'fg_id',            # Fangraphs,
        'fg_pos',
        'lahman_id',        # Lahman database
        'nfbc_id',          # NFBC
        'nfbc_pos',
        'retro_id',         # retrosheet
        'yahoo_id',         # yahoo
        'ottoneu_id',       # ottoneu,
        'ottoneu_pos',
        'rotowire_id',      # rotowire,
        'rotowire_pos'
    ]

    # no overall id, so just return the dataframe
    #   mlb_id is typically filled
    df = pd.read_csv(crunchyFile, dtype='string', usecols=colsToKeep)

    LOGGER.info('downloaded crunchtime.')
    return df

def get_smart_fantasy_baseball_mapping():
    '''
    return a dataframe of the www.smartfantasybaseball.com ID mapping. All values are
    downloaded as strings.
    No overall index
    '''

    #request headers
    #   had to add a user agent, since default python requests agent is blocked
    httpHeaders = {
        "Accept-Encoding": "gzip, deflate, br",
        "User-Agent": "Chrome/80.0.3987.132"
    }

    #download csv text file as string file object
    smartFantasyURL = 'https://www.smartfantasybaseball.com/PLAYERIDMAPCSV'
    req = requests.get(smartFantasyURL, headers=httpHeaders)
    smartFile = io.StringIO(req.text)

    # columns to include from this source, and the function to use to convert
    colsToKeep = [
        #'IDPLAYER',
        #'PLAYERNAME',
        #'TEAM',
        #'ALLPOS',
        'IDFANGRAPHS',      # fangraphs id
        'MLBID',            # MLB.com
        'CBSID',            # CBS
        'RETROID',          # retrosheet
        'BREFID',           # baseball reference
        'NFBCID',           # nfbc
        'ESPNID',           # ESPN,
        'KFFLNAME',         # KFFL (name, not id?)
        'DAVENPORTID',      #
        'BPID',             # baseball prospectus
        'YAHOOID',          # yahoo id
        'MSTRBLLNAME',      #
        'FANTPROSNAME',     # fantasy pros name (can get id by lowercase)
        'ROTOWIREID',       # rotowire
        'FANDUELID',        # fanduel,
        'DRAFTKINGSNAME',   # draftkings name
        'OTTONEUID',        # ottoneu
        'HQID',             # hq
        'FANTRAXID',        # fantrax
        'RAZZBALLNAME'      # razzball name only
    ]

    #print(smartFile.readline())
    df = pd.read_csv(smartFile, dtype='string', header=0, usecols=colsToKeep)

    LOGGER.info('downloaded smartfantasybaseball.com.')
    return df


def get_mysportsfeeds_mappings(api_key, season='2019-regular'):
    '''
    download id mappings from mysportsfeeds api

    player object has an externalMappings attribute
    "externalMappings":[{"source":"MLB.com","id":592094}]
    '''
    pass

#     #disable local file caching?
#     msf = MySportsFeeds(version="2.0")
#     msf.authenticate(api_key, "MYSPORTSFEEDS")
#     output = msf.msf_get_data(league='mlb', season=season, feed='players', format='json', force=True)

#     foundMappings = []

#     for player in output['players']:

#         playerID = player['player']['id']
#         externalMappings = player['player']['externalMappings']

#         for externalMapping in externalMappings:
#             if externalMapping['source'] == 'MLB.com':
#                 if externalMapping['id'] is not None:
#                     foundMappings.append(
#                                         {'mlb_id': str(externalMapping['id']),
#                                         'mysportsfeeds_id': str(playerID)}
#                                         )

#     df = pd.DataFrame(foundMappings, dtype='object')

#     #convert columns to dataframe
#     convertCols = {
#         'mlb_id': int_or_str,
#         'mysportsfeeds_id': int_or_str
#     }

#     for col, func in convertCols.items():
#         if func is not None:
#             mask = df[col].notna()
#             df.loc[mask, col] = df.loc[mask, col].apply(func)

#     LOGGER.info('downloaded mysportsfeeds.')
#     return df

