'''
command line entrypoint for all scrapers

delegates to appropriate sub commands
'''

import argparse
import sys

import sports_data.sports_scraper.yahoo.scraper as yahoo_scraper
import sports_data.sports_scraper.fangraphs.scraper as fangraphs_scraper

def define_args():
    ''' build the command line argument parser.'''

    parser = argparse.ArgumentParser()

    parser.description = ('Unified script providing access to all scrapers provided by '
                          'the sports_scraper submodules.')

    subparsers = parser.add_subparsers()

    yahoo = subparsers.add_parser('yahoo', help= 'Scrapers for yahoo.com and its APIs.')
    yahoo_scraper.YahooArgumentDefinition.main_parser(yahoo)

    fangraphs = subparsers.add_parser('fangraphs', help= 'Scrapers for fangraphs.com.')
    fangraphs_scraper.FangraphsArgumentDefinition.main_parser(fangraphs)

    return parser

def main():
    parser = define_args()

    if (len(sys.argv) < 2):
        parser.print_help()
        exit

    else:

        args = parser.parse_args()
        args.func(args)

if __name__ == "__main__":
    main()
