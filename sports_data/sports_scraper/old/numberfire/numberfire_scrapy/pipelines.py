'''
scrapy pipeline definitions

-*- coding: utf-8 -*-

Define your item pipelines here

Don't forget to add your pipeline to the ITEM_PIPELINES setting
See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
'''


import datetime

from sports_data.sports_scraper.support import ScrapyPipelineSQLAlchemyBase
from sports_data.sports_scraper.support import ScrapyPipelineMongoDBBase
import sports_data.sports_scraper.numberfire.numberfire_scrapy.models as dbModel

#eventually update to new item def?
class NumberfirePipelinePostgres(ScrapyPipelineSQLAlchemyBase):
    '''
    Postgres pipeline
    '''

    def record_from_item(self, item):

        record = dbModel.NumberfireProjectionsNFL()

        record.player_id = item['id']
        record.scrape_time = datetime.datetime.strptime(item['scrape_time'], '%Y-%m-%d %H:%M:%S')
        record.week = item['week']
        record.name = item['name']
        record.link = item['link']
        record.p_yd = item['p_yd']
        record.p_td = item['p_td']
        record.p_int = item['p_int']
        record.rsh_att = item['rsh_att']
        record.rsh_yd = item['rsh_yd']
        record.rsh_td = item['rsh_td']
        record.rec_rec = item['rec_rec']
        record.rec_yd = item['rec_yd']
        record.rec_td = item['rec_td']
        record.fanduel_cost = item['fanduel_cost']
        record.draftkings_cost = item['draftkings_cost']
        record.yahoo_cost = item['yahoo_cost']

        return record

    def get_db_metadata(self):

        return dbModel.BASE.metadata

class NumberfirePipelineMongo(ScrapyPipelineMongoDBBase):
    '''
    unified pipeline for numberfire scraping
    '''

    def document_from_item(self, item):

        record = dict(item)
        return record

    def get_collection_name(self, spider):

        if spider.name == "nflproj_weekly":
            return 'weekly_nfl_projections'

        if spider.name == "mlbprojs_yearly":
            return 'yearly_mlb_projections'

        if spider.name == "mlbprojs_ros":
            return 'ros_mlb_projections'

        if spider.name == "mlbprojs_weekly":
            return 'weekly_mlb_projections'

        if spider.name == "mlbplayers":
            return 'mlb_players'

        #provide a default fallback
        return 'misc'
