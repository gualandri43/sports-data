'''
mlb player spiders
'''

import scrapy
from sports_data.sports_scraper.numberfire.numberfire_scrapy.spiders.spider_base import NumberfireProjectionBase
import sports_data.sports_scraper.support as scraper_support

class MLBplayers(NumberfireProjectionBase):
    '''
    spider for MLB player downloads from numberfire.com
    '''

    name = "mlbplayers"

    def start_requests(self):
        urls = [
                'http://www.numberfire.com/mlb/players'
               ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        '''
        all player elements are in this form:

        <div class="all-players__indiv" data-position="C" data-team="ARI" data-first-name="alex" data-last-name="avila">

			<a href="/mlb/players/alex-avila">
				Alex Avila			</a>
			(C, ARI)
		</div>
        '''

        scrapeTimes = scraper_support.time.record_scrape_times()

        for player in response.xpath('//div[@class = "all-players__indiv"]'):
            item = {}
            item.update(scrapeTimes)

            item['position'] = player.css('::attr(data-position)').extract_first()
            item['team'] = player.css('::attr(data-team)').extract_first()
            item['firstname'] = player.css('::attr(data-first-name)').extract_first()
            item['lastname'] = player.css('::attr(data-last-name)').extract_first()
            item['link'] = player.css('a::attr(href)').extract_first()
            item['player_id'] = item['link'].split('/')[-1]

            yield item
