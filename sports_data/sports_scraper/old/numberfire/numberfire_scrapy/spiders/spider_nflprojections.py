import scrapy
import datetime as dt
from sports_data.sports_scraper.numberfire.numberfire_scrapy.items import NumberfireWeeklyProjectionItem
from sports_data.sports_scraper.numberfire.numberfire_scrapy.spiders.spider_base import NumberfireProjectionBase
from sports_data.sports_scraper.support.time import record_scrape_times

class NflProjections(NumberfireProjectionBase):
    name = "nflproj_weekly"

    def start_requests(self):
        urls = [
                'http://www.numberfire.com/nfl/fantasy/fantasy-football-projections'
               ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        scrapeTimes = record_scrape_times()

        week_number = response.css('div.projection-rankings__hed h2::text').re_first(r'Week (.*) Fantasy')

        pids = self._get_row_id_mapping(response)

        for playerData in response.css('div.projection-wrap table.no-fix tbody tr'):
            playerIdx = self._get_player_idx(playerData)

            item = NumberfireWeeklyProjectionItem()

            item.update(scrapeTimes)

            item['idx'] = playerIdx
            item['link'] = pids[playerIdx]['link']
            item['id'] = self._format_id(pids[playerIdx]['link'])
            item['week'] = int(week_number)
            item['name'] = pids[playerIdx]['name'].strip()
            item['p_yd'] = self._format_stat(playerData.css('td.pass_yd::text').extract_first())
            item['p_td'] = self._format_stat(playerData.css('td.pass_td::text').extract_first())
            item['p_int'] = self._format_stat(playerData.css('td.pass_int::text').extract_first())
            item['rsh_att'] = self._format_stat(playerData.css('td.rush_att::text').extract_first())
            item['rsh_yd'] = self._format_stat(playerData.css('td.rush_yd::text').extract_first())
            item['rsh_td'] = self._format_stat(playerData.css('td.rush_td::text').extract_first())
            item['rec_rec'] = self._format_stat(playerData.css('td.rec::text').extract_first())
            item['rec_yd'] = self._format_stat(playerData.css('td.rec_yd::text').extract_first())
            item['rec_td'] = self._format_stat(playerData.css('td.rec_td::text').extract_first())
            item['fanduel_cost'] = self._format_stat(playerData.css('td.fanduel_cost::text').extract_first())
            item['draftkings_cost'] = self._format_stat(playerData.css('td.draft_kings_cost::text').extract_first())
            item['yahoo_cost'] = self._format_stat(playerData.css('td.yahoo_cost::text').extract_first())

            yield item

