# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

from .spider_nflprojections import NflProjections
from .spider_mlbplayers import MLBplayers
from .spider_mlbprojections import MLBYearlyProjections, MLBWeeklyProjections, MLBrosProjections
