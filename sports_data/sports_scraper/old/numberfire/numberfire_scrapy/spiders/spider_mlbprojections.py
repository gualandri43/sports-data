'''
numberfire mlb projection spiders
'''

import scrapy
import datetime as dt
import re
from sports_data.sports_scraper.numberfire.numberfire_scrapy.spiders.spider_base import NumberfireProjectionBase
from sports_data.sports_scraper.support.time import record_scrape_times

class MLBRequest(scrapy.Request):
    '''
    define a subclass of a scrapy request to use for numberfire mlb

    include the keyword arguments position to hold info about position
    '''

    def __init__(self, *args, **kwargs):

        self.position = kwargs.pop('position')
        super().__init__(*args, **kwargs)

class MLBYearlyProjections(NumberfireProjectionBase):
    '''
    yearly projections spider
    '''

    name = "mlbprojs_yearly"

    def start_requests(self):

        reqs = [
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/yearly-projections/batters',
                       position='batters',
                       callback=self.parse),
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/yearly-projections/pitchers',
                       position='pitchers',
                       callback=self.parse),
        ]

        for req in reqs:
            yield req

    def parse(self, response):

        scrapeTimes = record_scrape_times()

        pids = self._get_row_id_mapping(response)

        for playerData in response.css('div.projection-wrap table.no-fix tbody tr'):
            playerIdx = self._get_player_idx(playerData)

            item = {}
            item.update(scrapeTimes)

            item['idx'] = playerIdx
            item['link'] = pids[playerIdx]['link']
            item['id'] = self._format_id(pids[playerIdx]['link'])
            item['name'] = pids[playerIdx]['name'].strip()

            if response.request.position == 'batters':
                item['b_pa'] = self._format_stat(playerData.css('td.pa::text').get())
                item['b_r'] = self._format_stat(playerData.css('td.r::text').get())
                item['b_hr'] = self._format_stat(playerData.css('td.hr::text').get())
                item['b_rbi'] = self._format_stat(playerData.css('td.rbi::text').get())
                item['b_sb'] = self._format_stat(playerData.css('td.sb::text').get())
                item['b_avg'] = self._format_stat(playerData.css('td.avg::text').get())
                item['b_ops'] = self._format_stat(playerData.css('td.ops::text').get())

            if response.request.position == 'pitchers':
                item['p_ip'] = self._format_stat(playerData.css('td.ip::text').get())
                item['p_w'] = self._format_stat(playerData.css('td.w::text').get())
                item['p_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['p_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['p_sv'] = self._format_stat(playerData.css('td.sv::text').get())
                item['p_era'] = self._format_stat(playerData.css('td.era::text').get())
                item['p_whip'] = self._format_stat(playerData.css('td.whip::text').get())

            yield item

class MLBrosProjections(NumberfireProjectionBase):
    '''
    rest of season projections spider
    '''

    name = "mlbprojs_ros"

    def start_requests(self):

        reqs = [
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/remaining-projections/batters',
                       position='batters',
                       callback=self.parse),
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/yearly-projections/pitchers',
                       position='pitchers',
                       callback=self.parse),
        ]

        for req in reqs:
            yield req

    def parse(self, response):

        scrapeTimes = record_scrape_times()

        pids = self._get_row_id_mapping(response)

        for playerData in response.css('div.projection-wrap table.no-fix tbody tr'):
            playerIdx = self._get_player_idx(playerData)

            item = {}
            item.update(scrapeTimes)

            item['idx'] = playerIdx
            item['link'] = pids[playerIdx]['link']
            item['id'] = self._format_id(pids[playerIdx]['link'])
            item['name'] = pids[playerIdx]['name'].strip()

            if response.request.position == 'batters':
                item['b_pa'] = self._format_stat(playerData.css('td.pa::text').get())
                item['b_r'] = self._format_stat(playerData.css('td.r::text').get())
                item['b_hr'] = self._format_stat(playerData.css('td.hr::text').get())
                item['b_rbi'] = self._format_stat(playerData.css('td.rbi::text').get())
                item['b_sb'] = self._format_stat(playerData.css('td.sb::text').get())
                item['b_avg'] = self._format_stat(playerData.css('td.avg::text').get())
                item['b_ops'] = self._format_stat(playerData.css('td.ops::text').get())

            if response.request.position == 'pitchers':
                item['p_ip'] = self._format_stat(playerData.css('td.ip::text').get())
                item['p_w'] = self._format_stat(playerData.css('td.w::text').get())
                item['p_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['p_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['p_sv'] = self._format_stat(playerData.css('td.sv::text').get())
                item['p_era'] = self._format_stat(playerData.css('td.era::text').get())
                item['p_whip'] = self._format_stat(playerData.css('td.whip::text').get())

            yield item

class MLBWeeklyProjections(NumberfireProjectionBase):
    '''
    weekly projections spider
    '''

    name = "mlbprojs_weekly"

    def start_requests(self):

        reqs = [
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/weekly-projections/batters',
                       position='batters',
                       callback=self.parse_weekly_dropdown_options),
            MLBRequest(url='http://www.numberfire.com/mlb/fantasy/weekly-projections/pitchers',
                       position='pitchers',
                       callback=self.parse_weekly_dropdown_options),
        ]

        for req in reqs:
            yield req

    def _parse_week_dates(self, weekStr):
        '''
        parses something like
        "From Monday, February 18th to Sunday, February 24th"

        to (startDate, endDate) tuple of datetime objects
        '''

        match = re.search(r'From (.*)[^0-9][^0-9] to (.*)[^0-9.][^0-9]', weekStr.strip())

        startDate = dt.datetime.strptime(match.group(1),'%A, %B %d')
        startDate = startDate.replace(year=dt.datetime.now().year)

        endDate = dt.datetime.strptime(match.group(2),'%A, %B %d')
        endDate = endDate.replace(year=dt.datetime.now().year)

        return (startDate, endDate)

    def parse_weekly_dropdown_options(self, response):
        '''
        parse the dropdown box options to get date link options
        '''

        for val in response.css('select.dropdown-custom option::attr(value)').getall():

            if '?d=' in val:
                url = response.urljoin(val)
                yield MLBRequest(url=url,
                                 position=response.request.position,
                                 callback=self.parse)

    def parse(self, response):

        scrapeTimes = record_scrape_times()

        pids = self._get_row_id_mapping(response)

        startDate, endDate = self._parse_week_dates(
            response.css('div.projection-rankings__hed span.projection-rankings--date::text').get()
        )

        for playerData in response.css('div.projection-wrap table.no-fix tbody tr'):
            playerIdx = self._get_player_idx(playerData)

            item = {}
            item.update(scrapeTimes)

            item['idx'] = playerIdx
            item['link'] = pids[playerIdx]['link']
            item['id'] = self._format_id(pids[playerIdx]['link'])
            item['name'] = pids[playerIdx]['name'].strip()

            item['week_start'] = startDate
            item['week_end'] = endDate

            if response.request.position == 'batters':
                item['b_pa'] = self._format_stat(playerData.css('td.pa::text').get())
                item['b_r'] = self._format_stat(playerData.css('td.r::text').get())
                item['b_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['b_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['b_h'] = self._format_stat(playerData.css('td.h::text').get())
                item['b_h2b'] = self._format_stat(playerData.css('td.h2b::text').get())
                item['b_h3b'] = self._format_stat(playerData.css('td.h3b::text').get())
                item['b_hr'] = self._format_stat(playerData.css('td.hr::text').get())
                item['b_rbi'] = self._format_stat(playerData.css('td.rbi::text').get())
                item['b_sb'] = self._format_stat(playerData.css('td.sb::text').get())
                item['b_cs'] = self._format_stat(playerData.css('td.cs::text').get())
                item['b_avg'] = self._format_stat(playerData.css('td.avg::text').get())
                item['b_ops'] = self._format_stat(playerData.css('td.ops::text').get())
                item['b_obp'] = self._format_stat(playerData.css('td.obp::text').get())
                item['b_slg'] = self._format_stat(playerData.css('td.slg::text').get())

            if response.request.position == 'pitchers':
                item['p_g'] = self._format_stat(playerData.css('td.g::text').get())
                item['p_gs'] = self._format_stat(playerData.css('td.gs::text').get())
                item['p_ip'] = self._format_stat(playerData.css('td.ip::text').get())
                item['p_w'] = self._format_stat(playerData.css('td.w::text').get())
                item['p_l'] = self._format_stat(playerData.css('td.l::text').get())
                item['p_sv'] = self._format_stat(playerData.css('td.sv::text').get())
                item['p_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['p_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['p_er'] = self._format_stat(playerData.css('td.er::text').get())
                item['p_era'] = self._format_stat(playerData.css('td.era::text').get())
                item['p_whip'] = self._format_stat(playerData.css('td.whip::text').get())

            yield item

class MLBDailyProjections(NumberfireProjectionBase):
    '''
    daily projections spider
    '''

    name = "mlbprojs_daily"

    def start_requests(self):

        reqs = [
            MLBRequest(url='http://www.numberfire.com/mlb/daily-fantasy/daily-baseball-projections/batters',
                       position='batters',
                       callback=self.parse),
            MLBRequest(url='http://www.numberfire.com/mlb/daily-fantasy/daily-baseball-projections/pitchers',
                       position='pitchers',
                       callback=self.parse),
        ]

        for req in reqs:
            yield req

    def parse(self, response):

        scrapeTimes = record_scrape_times()

        for playerData in response.css('div.dfs-main__table div.stat-table--wrap table.stat-table tbody tr'):

            playerIdx = self._get_player_idx(playerData)

            item = {}
            item.update(scrapeTimes)

            item['idx'] = playerIdx
            item['link'] = playerData.css('td.team-player span.player-info a.full::attr(href)').get()
            item['id'] = self._format_id(item['link'])
            item['name'] = playerData.css('td.team-player span.player-info a.full::text').get()

            item['cost'] = self._format_stat(playerData.css('td.cost::text').get())
            item['value'] = self._format_stat(playerData.css('td.value::text').get())

            if response.request.position == 'batters':
                item['b_pa'] = self._format_stat(playerData.css('td.pa::text').get())
                item['b_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['b_1b'] = self._format_stat(playerData.css('td.1b::text').get())
                item['b_2b'] = self._format_stat(playerData.css('td.2b::text').get())
                item['b_3b'] = self._format_stat(playerData.css('td.3b::text').get())
                item['b_hr'] = self._format_stat(playerData.css('td.hr::text').get())
                item['b_r'] = self._format_stat(playerData.css('td.r::text').get())
                item['b_rbi'] = self._format_stat(playerData.css('td.rbi::text').get())
                item['b_sb'] = self._format_stat(playerData.css('td.sb::text').get())
                item['b_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['b_avg'] = self._format_stat(playerData.css('td.avg::text').get())

            if response.request.position == 'pitchers':
                winLoss = playerData.css('td.wl::text').get()
                item['p_w'] = winLoss.split('-')[0]
                item['p_l'] = winLoss.split('-')[-1]
                item['p_sv'] = self._format_stat(playerData.css('td.sv::text').get())
                item['p_ip'] = self._format_stat(playerData.css('td.ip::text').get())
                item['p_h'] = self._format_stat(playerData.css('td.h::text').get())
                item['p_er'] = self._format_stat(playerData.css('td.er::text').get())
                item['p_hr'] = self._format_stat(playerData.css('td.hr::text').get())
                item['p_k'] = self._format_stat(playerData.css('td.k::text').get())
                item['p_bb'] = self._format_stat(playerData.css('td.bb::text').get())
                item['p_era'] = self._format_stat(playerData.css('td.era::text').get())
                item['p_whip'] = self._format_stat(playerData.css('td.whip::text').get())

            yield item
