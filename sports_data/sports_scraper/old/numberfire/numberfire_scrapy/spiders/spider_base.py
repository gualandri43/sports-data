'''
A base spider class for numberfire spiders.
'''

import scrapy

class NumberfireProjectionBase(scrapy.Spider):
    '''
    base class to hold commong numberfire scraping code.
    '''

    download_delay = 1.0
    custom_settings = {
        'ITEM_PIPELINES': {
            'sports_data.sports_scraper.numberfire.numberfire_scrapy.pipelines.NumberfirePipelineMongo': 300
        }
    }

    def _get_player_idx(self, tableRow):
        '''
        extract the data row index from a table row

        returns the row index as int
        '''

        rowIdx = int(tableRow.css('::attr(data-row-index)').extract_first())
        return rowIdx

    def _get_row_id_mapping(self, response):
        '''
        common helper function to map numberfire player IDs from a table to a data row. this
        mapping can then be used during scraping of each table row.

        reponse:    The scrapy response object for a numberfire projection table

        returns a dictionary with key=idx, value={idx, link, name}
        '''

        pids = {}

        for player in response.css('div.projection-wrap table.projection-table--fixed tbody tr'):
            playerIdx = self._get_player_idx(player)
            playerLink = player.css('td.player a::attr(href)').extract_first()
            playerName = player.css('td.player a span.full::text').extract_first()

            pids[playerIdx] = {
                'idx': playerIdx,
                'link': playerLink,
                'name': playerName
            }

        return pids

    def _format_stat(self, statText):
        '''
        format and clean stat table values
        '''

        if statText is None:
            return None
        elif statText.strip() == 'N/A':
            return None
        else:
            return float(statText.replace('$','').strip())

    def _format_id(self, playerLink):
        '''
        get playerid from link
        '''

        if playerLink is None:
            return None
        else:
            return playerLink.strip().split('/')[-1].strip()
