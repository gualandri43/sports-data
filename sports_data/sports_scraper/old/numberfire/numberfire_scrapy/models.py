
from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey
from sqlalchemy.orm import Session, relationship
from sqlalchemy.ext.declarative import declarative_base

BASE = declarative_base()

class NumberfireProjectionsNFL(BASE):
    '''Class to hold projection information.'''

    __tablename__ = 'nfl_weekly_projections'
    __table_args__ = {'schema':'scraper_numberfire'}

    player_id = Column(String, primary_key=True)
    scrape_time = Column(DateTime, primary_key=True)
    week = Column(Integer)
    name = Column(String) 
    link = Column(String)   
    p_yd = Column(Float)
    p_td = Column(Float)
    p_int = Column(Float)
    rsh_att = Column(Float)
    rsh_yd = Column(Float)
    rsh_td = Column(Float)
    rec_rec = Column(Float)
    rec_yd = Column(Float)
    rec_td = Column(Float)
    fanduel_cost = Column(Float)
    draftkings_cost = Column(Float)
    yahoo_cost = Column(Float)
