# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class NumberfireWeeklyProjectionItem(scrapy.Item):
    # define the fields for your item here like:
    
    id = scrapy.Field()
    scrape_time_utc = scrapy.Field()
    scrape_time_local = scrapy.Field()
    scrape_time_local_str = scrapy.Field()
    idx = scrapy.Field()
    link = scrapy.Field()
    week = scrapy.Field()
    name = scrapy.Field()
    p_yd = scrapy.Field()
    p_td = scrapy.Field()
    p_int = scrapy.Field()
    rsh_att = scrapy.Field()
    rsh_yd = scrapy.Field()
    rsh_td = scrapy.Field()
    rec_rec = scrapy.Field()
    rec_yd = scrapy.Field()
    rec_td = scrapy.Field()
    fanduel_cost = scrapy.Field()
    draftkings_cost = scrapy.Field()
    yahoo_cost = scrapy.Field()
