import json
import datetime as dt
import glob
from sports_data.settings import DatabaseDefinition

def save_nf_json_to_mongo(filePath, dbDef):

    with open(filePath, 'r') as f:
        data = json.load(f)

    for record in data:
        new_rec = record

        if "req" in new_rec:
            del new_rec["req"]

        old_scrape_time = new_rec.pop("scrape_time")
        if old_scrape_time is None:
            raise ValueError("scrape_time field not found.")

        local_time = dt.datetime.strptime(old_scrape_time, "%Y-%m-%d %H:%M:%S")

        new_rec["scrape_time_local_str"] = old_scrape_time
        new_rec["scrape_time_local"] = local_time
        new_rec["scrape_time_utc"] = dt.datetime.utcfromtimestamp(local_time.timestamp())

        db = dbDef.get_mongodb_database("numberfire")

        collection = db["weekly_nfl_projections"]
        collection.insert_one(new_rec)


#save_nf_json_to_mongo("D:/scrapy_test/numberfire/numberfire/nflscrape_2018-10-15.16.59.55.json", DatabaseDefinition("192.168.1.101", 27017))
for f in glob.glob('D:/scrapy_test/numberfire/numberfire/*.json'):
    print('processing: {0}'.format(f))
    save_nf_json_to_mongo(f, DatabaseDefinition("192.168.1.101", 27017))