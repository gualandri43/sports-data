'''
This module and script is designed to be the overall runner of all the various scrapers.
It ingests command line arguments and starts the required spiders/schedules them for later.
'''

import datetime as dt
import argparse
import enum
import logging

from twisted.internet import reactor, task
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.settings import Settings, get_settings_priority

from sports_modules import SETTINGS
from sports_data.support.database import DatabaseDefinition
import sports_data.support.logging as logging_support
import sports_data.sports_scraper.numberfire.numberfire_scrapy.spiders as numberfire_spiders
import sports_data.sports_scraper.fangraphs.fangraphs_scrapy.spiders as fangraphs_spiders


LOGGER_NAME = 'scraper_runner'

class AVAILABLE_SCRAPERS(enum.Enum):
    '''
    all available scrapers
    '''
    #pylint: disable=invalid-name

    FANGRAPHS_PROJECTIONS_SEASON = enum.auto()
    FANGRAPHS_PROJECTIONS_DAILY = enum.auto()
    FANGRAPHS_PROJECTIONS_ROS = enum.auto()
    FANGRAPHS_PROJECTIONS_UPDATE = enum.auto()
    NUMBERFIRE_PROJECTIONS_NFL_WEEKLY = enum.auto()
    NUMBERFIRE_PROJECTIONS_MLB_YEARLY = enum.auto()
    NUMBERFIRE_PROJECTIONS_MLB_ROS = enum.auto()
    NUMBERFIRE_PROJECTIONS_MLB_WEEKLY = enum.auto()
    NUMBERFIRE_PLAYERS_MLB = enum.auto()

class ScraperDefinition:
    '''
    data container to hold info needed to build a
    CrawlerRunner.

    spiders:    list of AVAILABLE_SCRAPERS to run with given settings
    settings:   A scrapy.Settings object defining any settings overrides
    '''
    def __init__(self, spiders, settings):
        self.spiders = spiders
        self.settings = settings

    def get_spider_classes(self):
        '''
        builds a list of scraper classes corresponding to the enum values in
        self.spiders
        '''
        spiderClasses = []

        if AVAILABLE_SCRAPERS.NUMBERFIRE_PLAYERS_MLB in self.spiders:
            spiderClasses.append(numberfire_spiders.MLBplayers)

        if AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_NFL_WEEKLY in self.spiders:
            spiderClasses.append(numberfire_spiders.NflProjections)

        if AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_YEARLY in self.spiders:
            spiderClasses.append(numberfire_spiders.MLBYearlyProjections)

        if AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_WEEKLY in self.spiders:
            spiderClasses.append(numberfire_spiders.MLBWeeklyProjections)

        if AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_ROS in self.spiders:
            spiderClasses.append(numberfire_spiders.MLBrosProjections)

        if AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_SEASON in self.spiders:
            spiderClasses.append(fangraphs_spiders.MLBProjectionsSeason)

        if AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_DAILY in self.spiders:
            spiderClasses.append(fangraphs_spiders.MLBProjectionsDaily)

        if AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_ROS in self.spiders:
            spiderClasses.append(fangraphs_spiders.MLBProjectionsROS)

        if AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_UPDATE in self.spiders:
            spiderClasses.append(fangraphs_spiders.MLBProjectionsUpdate)

        return spiderClasses

class ScraperRunner:
    '''
    controls the running of scrapy spiders
    '''

    def __init__(self, dbDefinition):
        self.dbDef = dbDefinition

        self._logger = logging.getLogger(LOGGER_NAME)

    def _run_crawlers(self, spiders, settings=None):
        '''
        adapted from scrapy docs regarding running via script

        send a set of spiders to an externally managed reactor.
        creates deffereds/tasks using the scrapy CrawlerRunner class that
            runs in the reactor when started
        the twisted reactor is global by default so the scrapy internals take care of sending it

        returns a twisted.deffered object that fires once all scrapers are finished via the
        scrapy join(). callbacks can be added to this deffered object.

        spiders:    list of scrapy.Spider classes to crawl using this runner
        settings:   a scrapy.Settings() class instance holding any settings to override defaults with
        '''

        # configure_logging() sets up default logging settings for scrapy
        configure_logging()

        # create the runner class. pass any specific settings in as a Settings() class
        runner = CrawlerRunner(settings)

        # add all passed spiders to the crawler
        for spider in spiders:
            runner.crawl(spider)

        # block until all the spiders in the runner have finished.
        # returns a twisted deffered object
        return runner.join()

    def _schedule_callback(self, result, scraperDef, lastStartTime, interval, stopTime=None):
        '''
        made to be a callback for a twisted deffered object. first argument "result"
        is the result of the deffered. the rest are custom.

        this callback function allows for the scheduling of a new crawler run after another
        finishes. when set as a callback function to the Deffered object returned, it fires
        after those finish, effectively rescheduling itself.

        this function continually adds itself as the callback to all new tasks it sends
        to the reactor, incrementing the scheduled time using the given interval.

        scraperDef:     an instance of ScraperDefinition that holds a set of scrapers
                        to be run with a common set of settings.
        lastStartTime:  a datetime object that holds the last scheduled scrape start time
        interval:       a timedelta object that holds the scraping interval
        stopTime:       an optional datetime object that identifies a cutoff
                        when this is reached, no more jobs are scheduled and a stop signal is sent to the reactor.
        '''

        # no supplied stop time is equivalent to indefinate running.
        if stopTime is None:
            stopTime = dt.datetime.max

        # calculate time between next scheduled time and current time
        # prevents drift due to long running process calls
        # if the process took too long, calling the callback after the next scheduled
        #   time, run it immediately
        newStartTime = lastStartTime + interval
        delayTime = newStartTime - dt.datetime.now()
        if delayTime < dt.timedelta(seconds=0):
            newStartTime = dt.datetime.now()
            delayTime = dt.timedelta(seconds=0)

        spiders = scraperDef.get_spider_classes()

        #schedule the crawling task with the calculated delay
        d = task.deferLater(reactor, delayTime.total_seconds(), self._run_crawlers, spiders, settings=scraperDef.settings)

        # if the stopTime comes before the next scheduled start time after the future task that was
        #       just scheduled above (so lastStartTime + interval + interval)
        #       a stop command is set as the callback, and no further jobs are scheduled.
        # if the stopTime isn't reached, it adds itself as a callback function to be fired once the above
        #   crawling deffered task (d) has finished.
        if newStartTime + interval > stopTime:
            d.addBoth(lambda _: reactor.stop())
        else:
            d.addCallback(self._schedule_callback, scraperDef, newStartTime, interval, stopTime=stopTime)
            d.addErrback(self._schedule_errback)

    def _schedule_errback(self, failure):
        '''
        called in case of an error for a scheduled deferred task. stops the reactor.
        '''

        self._logger.error('Error occurred during scheduled task.')
        self._logger.error(failure.getBriefTraceback())
        reactor.stop()

    def _run_crawlers_once(self, scraperDef):
        '''
        internal entry point for running a set of spiders once, then exiting.

        function to control the one-time running of crawlers. It get the
        twisted deferred object that fires when the all the crawlers sent to
        _run_crawlers has finished, and then sends a stop signal to the reactor as
        a callback.

        This function stops the reactor when finished.

        scraperDef:     an instance of ScraperDefinition that holds a set of scrapers
                        to be run with a common set of settings.
        '''

        # get deffered from _run_crawlers
        d = self._run_crawlers(scraperDef.get_spider_classes(), settings=scraperDef.settings)

        # add a reactor stop callback to the deffered to fire once the crawler jobs
        # have finished
        d.addBoth(lambda _: reactor.stop())

        # start the reactor event loop
        # will block here until the callback from the runner.join() method is fired, stopping the reactor
        reactor.run()

    def _run_crawlers_schedule(self, scraperDef, startTime, interval, stopTime=None):
        '''
        internal entry point for running scheduled, repeating crawler tasks

        function to control continued, scheduled running of crawlers.

        runs the crawlers immediately if the startTime is prior to current time, and sets the startTime to
        current time

        Creates a deferred task to run the scrapers at the given time, then adds the _schedule_callback() method
        as a callback. This will automatically reschedule the same crawler process at the given interval.
        The reactor is started here and will continue indefinitely until stopped or the stopTime has been reached.

        scraperDef:     an instance of ScraperDefinition that holds a set of scrapers
                        to be run with a common set of settings.
        startTime:      datetime object
        interval:       timedelta, interval between runs
        stopTime:       datetime object
        '''

        spiders = scraperDef.get_spider_classes()

        if dt.datetime.now() >= startTime:
            startTime = dt.datetime.now()
            d = self._run_crawlers(spiders, settings=scraperDef.settings)
        else:
            delayTime = startTime - dt.datetime.now()
            d = task.deferLater(reactor, delayTime.total_seconds(), self._run_crawlers, spiders, settings=scraperDef.settings)

        # set helper _schedule_callback() as the callback, passing required parameters to
        #   allow further scheduling
        # the helper sets itself as future callbacks, so that when a task is finished it reschedules itself
        d.addCallback(self._schedule_callback, scraperDef, startTime, interval, stopTime=stopTime)
        d.addErrback(self._schedule_errback)

        # start the reactor event loop
        # will block here continuously
        reactor.run()

    def run_scrapers(self, scrapersToRun, dbName, logLevel='DEBUG'):
        '''
        create a ScraperDefinition object and runs it once. can be a set of
        scrapers to run, but they must use a common set of settings.

        scrapersToRun:  list of AVAILABLE_SCRAPERS that are to be run
        dbName:         name of database to save result items to
        logLevel:       level of debugging to use
        '''

        #catches both empty list and None
        if not scrapersToRun:
            raise ValueError('no scrapers selected.')

        if not dbName:
            raise ValueError('no database selected.')

        #instantiate database settings with highest scrapy priority
        settings = Settings(values={
                                    'DB_DEF': self.dbDef,
                                    'DB_NAME': dbName,
                                    'LOG_LEVEL': logLevel
                                    }, priority=get_settings_priority('cmdline'))

        self._run_crawlers_once( ScraperDefinition(scrapersToRun, settings) )

    def run_scrapers_scheduled(self, scrapersToRun, dbName,
                               startTime=dt.datetime.now(), interval=dt.timedelta(days=1), stopTime=None, logLevel='DEBUG'):
        '''
        create a ScraperDefinition object and runs it on a schedule. can be a set of
        scrapers to run, but they must use a common set of settings.

        scrapersToRun:  list of AVAILABLE_SCRAPERS that are to be run
        dbName:         name of database to save result items to
        logLevel:       level of debugging to use

        startTime:      datetime object with job start time
        interval:       timedelta object with repeat interval
        stopTime:       datetime object with job stop time
        '''

        #catches both empty list and None
        if not scrapersToRun:
            raise ValueError('no scrapers selected.')

        if not dbName:
            raise ValueError('no database selected.')

        #instantiate database settings with highest scrapy priority
        settings = Settings(values={
                                    'DB_DEF': self.dbDef,
                                    'DB_NAME': dbName,
                                    'LOG_LEVEL': logLevel
                                    }, priority=get_settings_priority('cmdline'))

        self._run_crawlers_schedule(ScraperDefinition(scrapersToRun, settings),
                                     startTime, interval, stopTime=stopTime)

def get_script_args():

    parser = argparse.ArgumentParser(description='Run the various web scrapers.')
    parser.add_argument('--start-schedule', dest='runScheduled', action='store_true',
                        help='Start a process that runs scraping jobs at their scheduled times.')

    #common
    parser.add_argument('--all', dest='all', action='store_true',
                        help='select all available scrapers.')
    parser.add_argument('--startTime', dest='startTime', action='store',
                        help='The date and time to start a scraping schedule, at string in YYYY-MM-DD HH:MM:SS format.')
    parser.add_argument('--stopTime', dest='stopTime', action='store',
                        help='The date and time to stop a scraping schedule, at string in YYYY-MM-DD HH:MM:SS format.')
    parser.add_argument('--intervalDays', dest='intervalDays', action='store', type=int,
                        help='The integer interval between scheduled scrapings, in days.')
    parser.add_argument('--intervalMinutes', dest='intervalMinutes', action='store', type=int,
                        help='The integer interval between scheduled scrapings, in minutes.')
    parser.add_argument('--intervalSeconds', dest='intervalSeconds', action='store', type=int,
                        help='The integer interval between scheduled scrapings, in seconds.')

    #numberfire
    parser.add_argument('--run-numberfire-projections', dest='runNumberfireProjs', action='store_true',
                        help='Run the numberfire NFL projections scraper.'
                             'Must be used with scraper scope selections, or --all.')
    parser.add_argument('--run-numberfire-projections-scheduled', dest='runNumberfireProjsSched', action='store_true',
                        help='Run the numberfire NFL projections scraper according to a schedule.'
                             'Must be accompanied by appropriate scraper scope selections, or --all.'
                             'A startTime and inteval must also be defined.')
    parser.add_argument('--run-numberfire-players', dest='runNumberfirePlayers', action='store_true',
                        help='Run the numberfire NFL players scraper.'
                             'Must be used with a sport specifier')
    parser.add_argument('--numberfire-scope', dest='numberfireScope', action='store',
                        choices=['weekly', 'yearly', 'ros'],
                        help='The timeframe scope to download for.')
    parser.add_argument('--numberfire-sport', dest='numberfireSport', action='store',
                        choices=['nfl', 'mlb'],
                        help='The sport to download for.')

    #fangraphs
    parser.add_argument('--run-fangraphs-projections', dest='runFangraphsProjs', action='store_true',
                        help='Run the fangraphs scraper. Must be accompanied by appropriate scraper scope selections, or --all.')
    parser.add_argument('--run-fangraphs-projections-scheduled', dest='runFangraphsProjsSched', action='store_true',
                        help='Run the fangraphs scraper according to a schedule. '
                             'Must be accompanied by appropriate scraper scope selections, or --all. '
                             'A startTime and inteval must also be defined.')
    parser.add_argument('--fangraphs-season', dest='fangraphsSeason', action='store_true',
                        help='Scrape fangraphs season scope.')
    parser.add_argument('--fangraphs-daily', dest='fangraphsDaily', action='store_true',
                        help='Scrape fangraphs daily scope.')
    parser.add_argument('--fangraphs-ros', dest='fangraphsRos', action='store_true',
                        help='Scrape fangraphs rest of season scope.')
    parser.add_argument('--fangraphs-update', dest='fangraphsUpdate', action='store_true',
                        help='Scrape fangraphs update scope.')

    #common settings parser args
    parser = SETTINGS.add_args_to_argparse(parser)

    return parser.parse_args()

def parse_date_args(args):
    '''
    handles the date and interval arguments

    returns tuple of startTime, interval, stopTime
                    (datetime, timedelta, datetime)
    '''

    schedStart = None
    schedInterval = None
    schedStop = None

    if args.startTime is not None:
        schedStart = dt.datetime.strptime(args.startTime, '%Y-%m-%d %H:%M:%S')

    if args.stopTime is not None:
        schedStop = dt.datetime.strptime(args.stopTime, '%Y-%m-%d %H:%M:%S')

    if args.intervalDays is not None or args.intervalMinutes is not None or args.intervalSeconds is not None:
        schedInterval = dt.timedelta(seconds=0)

    if args.intervalDays is not None:
        schedInterval += dt.timedelta(days=args.intervalDays)

    if args.intervalMinutes is not None:
        schedInterval += dt.timedelta(minutes=args.intervalMinutes)

    if args.intervalSeconds is not None:
        schedInterval += dt.timedelta(seconds=args.intervalSeconds)

    return schedStart, schedInterval, schedStop

def handle_fangraphs_args(runner, args):
    '''
    function to handle all fangraphs args and job running.

    runner: a ScraperRunner instance ready to run jobs
    args:   parsed commandline args
    '''

    logger = logging.getLogger(LOGGER_NAME)

    dbName = 'fangraphs'

    #parse any given dates
    schedStart, schedInterval, schedStop = parse_date_args(args)

    if args.runFangraphsProjs and args.runFangraphsProjsSched:
        raise RuntimeError('Only single or scheduled run may be selected, not both.')

    #define requested scraping scopes
    scrapeScopes = []

    if args.fangraphsSeason:
        scrapeScopes.append(AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_SEASON)

    if args.fangraphsDaily:
        scrapeScopes.append(AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_DAILY)

    if args.fangraphsRos:
        scrapeScopes.append(AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_ROS)

    if args.fangraphsUpdate:
        scrapeScopes.append(AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_UPDATE)

    if args.all:
        scrapeScopes = [AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_SEASON,
                        AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_DAILY,
                        AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_ROS,
                        AVAILABLE_SCRAPERS.FANGRAPHS_PROJECTIONS_UPDATE]

    #run scrapers

    if args.runFangraphsProjs:
        logger.info('running fangraphs scraper.')
        runner.run_scrapers(scrapeScopes, dbName, logLevel=SETTINGS.LOG_LEVEL)

    if args.runFangraphsProjsSched:
        logger.info('running scheduled fangraphs scraper.')

        if schedStart is None:
            raise RuntimeError('no start time has been specified.')

        if schedInterval is None:
            raise RuntimeError('no interval has been specified.')

        #schedStop can be None, indicates indefinite run
        runner.run_scrapers_scheduled(scrapeScopes, dbName, startTime=schedStart,
                                      interval=schedInterval, stopTime=schedStop,
                                      logLevel=SETTINGS.LOG_LEVEL)

def handle_numberfire_args(runner, args):
    '''
    function to handle all numberfire args and job running.

    runner: a ScraperRunner instance ready to run jobs
    args:   parsed commandline args
    '''

    logger = logging.getLogger(LOGGER_NAME)

    dbName = 'numberfire'

    #parse any given dates
    schedStart, schedInterval, schedStop = parse_date_args(args)

    #define requested scraping scopes
    scrapeScopes = []

    #validate
    if not args.runNumberfireProjs and not args.runNumberfireProjsSched and not args.runNumberfirePlayers:
        # no numberfire scrapers set to run. skip the rest of the function
        return None

    if args.runNumberfireProjs and args.runNumberfireProjsSched:
        raise RuntimeError("Must select either a single run or scheduled scrape, not both.")

    if not args.numberfireSport:
        raise RuntimeError("Must select a league scope.")

    if not args.numberfireScope and not args.runNumberfirePlayers:
        raise RuntimeError("Must select a time scope.")

    #select appropriate scrapers
    if args.numberfireScope == 'weekly':

        if args.numberfireSport == 'nfl':
            scrapeScopes.append(AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_NFL_WEEKLY)

        if args.numberfireSport == 'mlb':
            scrapeScopes.append(AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_WEEKLY)

    if args.numberfireScope == 'yearly':

        if args.numberfireSport == 'mlb':
            scrapeScopes.append(AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_YEARLY)

    if args.numberfireScope == 'ros':

        if args.numberfireSport == 'mlb':
            scrapeScopes.append(AVAILABLE_SCRAPERS.NUMBERFIRE_PROJECTIONS_MLB_ROS)

    if args.runNumberfirePlayers:
        print('hi')
        if args.numberfireSport == 'mlb':
            print('boo')
            scrapeScopes.append(AVAILABLE_SCRAPERS.NUMBERFIRE_PLAYERS_MLB)

    if args.runNumberfireProjs or args.runNumberfirePlayers:
        logger.info('running numberfire scraper.')
        runner.run_scrapers(scrapeScopes, dbName, logLevel=SETTINGS.LOG_LEVEL)

    if args.runNumberfireProjsSched:
        logger.info('running scheduled numberfire scraper.')
        runner.run_scrapers_scheduled(scrapeScopes, dbName, startTime=schedStart, interval=schedInterval, stopTime=schedStop, logLevel=SETTINGS.LOG_LEVEL)

def main():
    '''
    script entry point and argument parser.
    '''

    #parse arguments
    args = get_script_args()
    SETTINGS.reload(cmdArgs=args)
    logging_support.set_up_console_logger(LOGGER_NAME)

    # configuration
    #
    if SETTINGS.MONGODB_IP is None or SETTINGS.MONGODB_PORT is None:
        raise RuntimeError('Storage database not fully specified.')

    dbDef = DatabaseDefinition()
    runner = ScraperRunner(dbDef)

    # scrapers
    #
    handle_fangraphs_args(runner, args)
    handle_numberfire_args(runner, args)

if __name__ == "__main__":
    main()
