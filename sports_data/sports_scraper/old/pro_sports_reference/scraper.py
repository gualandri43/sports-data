'''Classes and functions for scraping data from the Pro Sports References websites. (pro-football-reference,pro-baseball-reference)'''

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import json
import pandas as pd
import numpy as np
import datetime as dt
import time
import pytz
import random
from selenium import webdriver
import sportsdbInterface as db
from sports_data.common.common_funcs import isNumber
from sports_data.sportsScraper.common import GetURLtext, CleanUnicodeText, parseStandardHTMLTable

class ProReferenceBase:
    #class serves as a common base for the sport-reference sites such as baseball-reference and pro-football-reference

    def TableParse(self,body_html,includeTH=True):
        #parses a typical data table
        #the body_html input is a beautiful soup object of the tbody element
        #uses the 'data-stat' attribute as the returned header label
        #the data stat is included inside the th or td tag in the table body
        #each entry is a dictionary with val and link keys
        #returns a dataframe of the results

        rowList = []

        rows = body_html.findAll('tr')
        for r in rows:
            rowDict = {}

            #check for in-row th tags
            if includeTH:
                for h in r.findAll('th'):

                    if h.has_attr('data-stat'):
                        rowDict[h['data-stat']] = {}
                        rowDict[h['data-stat']]['val'] = h.text

                    for a in h.findAll('a'):
                        if a.has_attr('href'):
                            rowDict[h['data-stat']]['link'] = a['href']
                            rowDict[h['data-stat']]['val'] = a.text

            #check for in row td tags
            for d in r.findAll('td'):

                if d.has_attr('data-stat'):
                    rowDict[d['data-stat']] = {}
                    rowDict[d['data-stat']]['val'] = d.text


                    for a in d.findAll('a'):
                        if a.has_attr('href'):
                            rowDict[d['data-stat']]['link'] = a['href']
                            rowDict[d['data-stat']]['val'] = a.text

            if len(rowDict) > 0:
                rowList.append(rowDict)

        #create data frame
        df = pd.DataFrame(rowList)

        return df

class proFootballReference_scraper(ProReferenceBase):
    #scraper to get nfl data from http://www.pro-football-reference.com
    #inherits from ProReferenceBase
    #Pro Football Reference is source ID: 1

    def __init__(self):

        #get a database connection
        self.db = db.dbInterface()

    def RefreshTeamList(self):
        #routine fetches all the NFL teams from source and adds or updates them in the database
        #tables affected: nfl.teams, nfl.id_conversion_team

        #get table of
        url = 'http://www.pro-football-reference.com/teams'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        teamTable = pg.find("table",{"id":"teams_active"})
        body = teamTable.find("tbody")

        #get data frame of table data
        tms = self.TableParse(body)

        #get team names and PFR team ids
        #ids are held in the link
        #data variable team_name is the PFR id variable
        #only teams with link in the entry are active teams
        teamList = []
        for i,t in tms['team_name'].iteritems():
            if 'link' in t:
                tmID = t['link'].split('/')[2]
                teamList.append((tmID,t['val']))  #(team id, team name) pair


        #check if there is a team id conversion present and update team name
        #if not, add new entry to teams list and record conversion
        #for t in teamList:
        #	if db.session.query(db.nfl.id_conversion_team).filter(db.nfl.id_conversion_team.val==t[0]) is None:

        return

class baseballReference_scraper(ProReferenceBase):
    """Scraper to get mlb data from http://www.baseball-reference.com/. Inherits from ProReferenceBase.

    Baseball Reference is source ID: 2

    Attributes:
        db (dbInterface): dbInterface class object representing the connection to the sportsdb.
        source_id (int): ID number for the source in the database.
        players: Holds scraped player information. Typically a dictionary.
        games: Holds scraped game information.
    """

    def __init__(self):

        #get a database connection
        self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')
        #self.db = db.dbInterface()
        self.source_id = 2

        self.players = None
        self.games = None

    def _ConvertProblematicTeams(self,tm):
        """Some teams have multiple IDs. This function converts the baseball reference team ids to the chosen ID.

        Args:
            tm (str): Team abbreviation to convert.

        Returns:
            (str): Abbreviation used for that team in the local database.

        """

        convTeams = {}
        convTeams['MLN'] = 'ATL'
        convTeams['BSN'] = 'ATL'
        convTeams['SLB'] = 'BAL'
        convTeams['MLA'] = 'BAL'
        convTeams['ANA'] = 'LAA'
        convTeams['CAL'] = 'LAA'
        convTeams['BRO'] = 'LAD'
        convTeams['FLA'] = 'MIA'
        convTeams['SEP'] = 'MIL'
        convTeams['WSH'] = 'MIN'
        convTeams['KCA'] = 'OAK'
        convTeams['PHA'] = 'OAK'
        convTeams['NYG'] = 'SFG'
        convTeams['TBD'] = 'TBR'
        convTeams['WSA'] = 'TEX'
        convTeams['MON'] = 'WSN'

        if tm in convTeams:
            return convTeams[tm]
        else:
            return tm

    def _ConvertPositions(self,pos):
        """Converts the positions to the database format. Acceptable conversions:

            - Catcher: C
            - First Baseman: 1B
            - Second Baseman: 2B
            - Third Baseman: 3B
            - Shortstop: SS
            - Rightfielder: RF, OF
            - Leftfielder: LF, OF
            - Centerfielder: CF, OF
            - Pitcher: P

        Args:
            pos (str): Baseball Reference position string.

        Returns:
            Database positions abbreviation. Returns None if the position string is not defined here.

        """

        posConv = {}
        posConv['Catcher'] = 'C'
        posConv['First Baseman'] = '1B'
        posConv['Second Baseman'] = '2B'
        posConv['Third Baseman'] = '3B'
        posConv['Shortstop'] = 'SS'
        posConv['Rightfielder'] = 'RF'
        posConv['Leftfielder'] = 'LF'
        posConv['Centerfielder'] = 'CF'
        posConv['Outfielder'] = 'OF'
        posConv['Pitcher'] = 'P'
        posConv['Designated Hitter'] = 'DH'

        if pos in posConv:
            return posConv[pos]
        else:
            return None

    def _ParseTeamID(self,lnk):
        """Parses the team ID out of the link url

        Args:
            lnk (str): Link that contains the team ID.

        Example link: /teams/ARI/2014.shtml
        """

        tmpStr = str(lnk)
        return tmpStr.split('/')[2]

    def _ParseGameID(self,lnk):
        """Parses the game ID out of the link url (for the boxscore)

        Args:
            lnk (str): Link that contains the game ID.

        Example link: /boxes/ARI/ARI201403230.shtml
        """

        tmpStr = str(lnk)
        return tmpStr.split('/')[3].replace('.shtml','')

    def RefreshTeamList(self):
        #routine fetches all the MLB teams from source and adds or updates them in the database
        #tables affected: mlb.teams, mlb.id_conversion_team

        #get table of
        url = 'http://www.baseball-reference.com/teams/'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        teamTable = pg.find("table",{"id":"teams_active"})
        body = teamTable.find("tbody")

        #get data frame of table data
        tms = self.TableParse(body)

        #get team names and BR team ids
        #ids are held in the link
        #data variable franchise_name is the BR id variable
        #only teams with link in the entry are active teams, other rows are extraneous
        teamList = []
        for i,t in tms['franchise_name'].iteritems():
            if 'link' in t:
                tmID = self._ParseTeamID(t['link'])
                teamList.append((tmID,t['val']))  #(team id, team name) pair

        #call database routine to add or update the team entry.
        for t in teamList:
            self.db.AddOrUpdateTeam('mlb',t[0],self.source_id,self.db.mlb.Teams(teamname=t[1],abbrev=t[0].lower()))

    def _GetOnlinePlayerList(self):
        """Populates players dictionary with the baseball reference player IDs from the web. Retrieves all players on the pages. Key is the BR source ID."""

        #start with fresh player list
        self.players = {}

        #alphabet letters
        alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

        #iterate through all lists of players
        for ltr in alpha:

            #get table of players (players starting with b have a link like http://www.baseball-reference.com/players/b)
            url = 'http://www.baseball-reference.com/players/' + ltr
            pg = BeautifulSoup(GetURLtext(url))

            #get list of players
            playerTable = pg.find("div",{"id":"div_players_"})

            #loop through entries
            #a bolded player is an active player
            for p in playerTable.findAll('p'):
                isActive = False

                boldSearch = p.findAll('b')
                if len(boldSearch) > 0:
                    #active player. example: <p><b><a href="/players/b/baezja01.shtml">Javier Baez</a>  (2014-2016)</b></p>
                    for ap in boldSearch:
                        for info in ap.findAll('a'):
                            #player id
                            srcID = info['href'].split('/')[3].replace('.shtml','').strip()

                            #player name
                            pName = info.text

                            #player is active
                            isActive = True

                else:
                    #inactive player. example <p><a href="/players/b/backmle01.shtml">Les Backman</a>  (1909-1910)</p>
                    for info in p.findAll('a'):
                        #player id
                        srcID = info['href'].split('/')[3].replace('.shtml','').strip()

                        #player name
                        pName = info.text

                        #player is active
                        isActive = False


                #store in dictionary
                if srcID not in self.players:
                    self.players[srcID] = {}

                self.players[srcID]['fullname'] = pName
                self.players[srcID]['firstname'] = pName.split(' ')[0].strip()
                self.players[srcID]['lastname'] = ''.join(pName.split(' ')[1:]).strip()
                self.players[srcID]['is_active'] = isActive

    def _GetPlayerDetails(self,pID):
        """Gets a single player's information details from the web. Adds the info to that players dictionary entry in 'players'

        Args:
            pID (str): Baseball reference player ID to download data from.
        """

        print('Downloading id: ' + str(pID))

        #retrieve player web page
        url = 'http://www.baseball-reference.com/players/' + pID[0] + '/' + pID + '.shtml'
        pg = BeautifulSoup(GetURLtext(url))

        playerDetail = pg.find("div",{"class":"players"})

        if 'positions' not in self.players[pID]:
            self.players[pID]['positions'] = []

        if 'birthdate' not in self.players[pID]:
            self.players[pID]['birthdate'] = None

        if 'team' not in self.players[pID]:
            self.players[pID]['team'] = None

        if 'bats' not in self.players[pID]:
            self.players[pID]['bats'] = None

        if 'throws' not in self.players[pID]:
            self.players[pID]['throws'] = None

        for p in playerDetail.findAll('p'):

            for t in p.findAll('strong'):

                #positions	<p><strong>Positions:</strong>First Baseman, Third Baseman and Leftfielder</p>
                if t.text.strip() == 'Position:' or t.text.strip() == 'Positions:':
                    tmpTxt = p.text.replace('and',',')
                    tmpTxt = tmpTxt.replace('Positions:','').strip()
                    tmpTxt = tmpTxt.replace('Position:','').strip()

                    for iterPos in tmpTxt.split(','):
                        self.players[pID]['positions'].append(self._ConvertPositions(iterPos.strip()))


                #team   <p><strong>Team:</strong><a href="/teams/DET/2016.shtml">Detroit Tigers</a>  (40-man)</p>
                if t.text.strip() == 'Team:':
                    for a in p.findAll('a'):
                        tmID = a['href'].split('/')[2].strip()

                    self.players[pID]['team'] = self._ConvertProblematicTeams(CleanUnicodeText(tmID))  #source teamID

                #bats   <p><strong>Bats: </strong>Right&nbsp;&bull;&nbsp;<strong>Throws: </strong>Right</p>
                if t.text.strip() == 'Bats:':

                    self.players[pID]['bats'] = CleanUnicodeText(p.contents[2])  #had to encode and decode to get rid of extra characters.
                    self.players[pID]['throws'] = CleanUnicodeText(p.contents[4])


            #birthdate <p><strong><a href="/bio/">Born:</a></strong><span itemprop="birthDate" id="necro-birth" data-birth="1983-04-18"><a href="/friv/birthdays.cgi?month=4&day=18">April 18</a>, <a href="/leagues/MLB/1983-births.shtml">1983</a></span><span itemprop="birthPlace">in&nbsp;Maracay,&nbsp;Venezuela</span><span class="f-i f-ve">ve</span></p>
            for bd in p.findAll('span'):
                if bd.has_attr('data-birth'):
                    self.players[pID]['birthdate'] = dt.datetime.strptime(CleanUnicodeText(bd['data-birth']),'%Y-%m-%d')

    def _AddPlayerInfoToDatabase(self,pID):
            """Adds player info to the sports database from the 'players' dictionary entry for the given pID.

            Args:
                pID (str): ID in the 'players' dictionary to add data for. Usually the baseball reference player ID.
            """

            #get player info sub dictionary for the given pID
            pInfo = self.players[pID]

            #create player
            self.db.AddOrUpdatePlayer('mlb',pID,self.source_id,self.db.mlb.Players(fullname=pInfo['fullname'],
                                                                                        firstname=pInfo['firstname'],
                                                                                        lastname=pInfo['lastname'],
                                                                                        is_active=pInfo['is_active'],
                                                                                        bats=pInfo['bats'],
                                                                                        throws=pInfo['throws'],
                                                                                        birthdate=pInfo['birthdate']))

            #player position updates
            if not self.db.AddOrUpdatePlayerPos('mlb', pID, self.source_id, pInfo['positions']):
                print('**PlayerPos Error**: ' + str(sID) + ' positions did not update.')

            #player team updates
            if not self.db.AddOrUpdatePlayerTeam('mlb', pID, self.source_id, pInfo['team']):
                print('**PlayerTeam Error**: ' + str(sID) + ' team did not update.')

    def RefreshPlayers(self,onlyActive=False):
        """Routine fetches the MLB players from source and adds or updates them in the database.

        If the onlyActive flag is true, the basic info (name, active status) is still updated in the database. This keeps the active players current but prevents
        unnecessary web requests.

        Tables affected: mlb.players, mlb.id_conversion

        Args:
            onlyActive (bool): Flag. Indicates whether to download information about all players, or just the ones marked active on baseball reference's page.
            Defaults to False (update all players).

        """

        #get list of online players. exit function if exception is raised.
        try:
            self._GetOnlinePlayerList()
        except Exception as e:
            print('Error ocurred while getting baseball reference players: ' + str(e))
            return None

        #get individual detailed information about the players
        isProcPlayer = False
        for sID in self.players:

            #determine whether or not the player detail should be updated
            if onlyActive:
                if self.players[sID]['is_active']:
                    isProcPlayer = True
                else:
                    isProcPlayer = False
            else:
                isProcPlayer = True

            #process player detail if needed
            if isProcPlayer:

                #add delay so website doesn't get hammered with requests
                time.sleep(0.8)

                try:
                    #get player details
                    self._GetPlayerDetails(sID)

                except Exception as e:
                    print('Error ocurred while downloading details for baseball reference player ID ' + sID + ': ' + str(e))


            #put database adding outside the conditional so that players that are no longer active get updated as such in the database.
            try:

                #add player information to the database
                self._AddPlayerInfoToDatabase(sID)

            except Exception as e:
                print('Error ocurred while adding baseball reference player ID ' + sID + ' to database: ' + str(e))

    def _GetOnlineGameList(self,year):
        """Gets list of games for the given year from online and adds them to the games dictionary.

        Args:
            year (int): Year to download the games for.
        """

        #get list of game IDs
        url = 'http://www.baseball-reference.com/leagues/MLB/' + int(year) + '-schedule.shtml'
        pg = BeautifulSoup(GetURLtext(url))

        #loop through tags of a that are game class to get the list of game ids
        for d in pg.findAll('div'):
            game_date = None
            date_text = None

            #get date text from group heading (<h3>Sunday, May 7, 2017</h3>)
            for h in d.findAll('h3'):
                date_text = h.text.strip()

            #dig into each game info
            for p in pg.findAll("p",attrs={"class":"game"}):
                teamList = []
                game_id = None
                home_team = None
                away_team = None
                hasBoxscore = False
                time_zone = None
                game_time = None

                #unplayed games have a time and timezone in a span tag. (<span tz="E"><strong>1:10 pm</strong></span>)
                for s in p.findAll('span'):
                     time_zone = s['tz'].strip()
                     game_time = s.text

                #other info in <a> tags
                for a in p.findAll('a'):
                    if a.has_attr('href'):

                        #link is for a team (href="/teams/LAD/2014.shtml")
                        if str(a['href']).find('teams') != -1:
                            teamList.append(self._ParseTeamID(a['href']))

                        #link is for a the game id (href="/boxes/ARI/ARI201403230.shtml")
                        if str(a['href']).find('boxes') != -1 and str(a['href']).find('.shtml') != -1:
                            game_id = self._ParseGameID(a['href'])
                            hasBoxscore = True

                        #link is for a the date (href="/boxes/?date=2014-03-23")
                        if str(a['href']).find('boxes') != -1 and str(a['href']).find('date') != -1:
                            game_date = a['href'].split('?')[1].replace('date=','')
                            hasBoxscore = True


                #assign home and away teams (home team is the first 3 letters of the game id if box score is available)
                #otherwise default to order being listed as away team @ home team
                for t in teamList:

                    if hasBoxscore:
                        #use game ID
                        if t.upper() == game_id[:3].upper():
                            home_team = t
                        else:
                            away_team = t
                    else:
                        #assign best guess at the game ID and home/away
                        #format: <home team id><date in yyyymmdd><0 for single game, 1 for first game of dh, 2 for second game of dh>

                        away_Team = teamList[0]
                        home_team = teamList[1]
                        tmpDate = dt.datetime.strptime(date_text, '%A, %b %d, %Y')
                        game_id = home_team.upper() + tmpDate.strftime('%Y%m%d')

                        #check for a double header situation. if none, add a 0 on the end
                        chkID = game_id + '0'
                        if chkID in self.games:
                            #make this game game 2
                            game_id = game_id + '2'

                            #change the existing dict entry to a 1
                            self.games[game_id + '1'] = self.games.pop(chkID)
                            self.games[game_id + '1'][chkID] = game_id + '1'

                        else:
                            #add the zero to game_id
                            game_id = game_id + '0'

                #add to games storage
                if game_id not in self.games:
                    self.games[game_id] = {}

                self.games[game_id]['game_id'] = game_id
                self.games[game_id]['home_team'] = home_team
                self.games[game_id]['away_team'] = away_team
                self.games[game_id]['hasBoxScore'] = hasBoxscore

                #set date based on what is available
                if date_text is None:
                    #a game date was found (already played games). date is stored and time info is in the boxscore.
                    self.games[game_id]['game_date'] = game_date

                else:
                    #only the heading text and separate time are available. create the game date from these.

                    #format a temporary string with the time included to parse all in one shot
                    tmpDate = date_text + ', ' + game_time
                    game_date = dt.datetime.strptime(tmpDate, '%A, %b %d, %Y, %H:%M %p')

                    if time_zone == 'E':
                        #options: 'US/Eastern', 'US/Central', 'US/Mountain', 'US/Pacific'
                        game_date = pytz.timezone('US/Eastern').localize(game_date)

                    self.games[game_id]['game_date'] = game_date

    def _GetGameDetails(self,gmID):
        """Gets the details of a given game from online by parsing the box score page. Updates info in the games dictionary.

        Games in the future do not have a box score page. This routine just adds None to the dictionary entries if this is the case.

        Args:
            gmID (str): Baseball reference ID of the game to get details for.
        """

        box_date = None
        box_time = None
        box_attendance = None
        box_venue = None
        box_duration = None
        box_surface = None

        if games[gID]['hasBoxScore']:
            #parse box info
            urlBox = 'http://www.baseball-reference.com/boxes/'+ home_team + '/'+ gmID + '.shtml'
            pgBox = BeautifulSoup(GetURLtext(urlBox))



            #parse meta data
            for scm in pgBox.findAll('div',attrs={"class":"scorebox_meta"}):
                for dv in scm.findAll('div'):

                    if dv.text.find('Start Time') != -1:
                        box_time = dv.text.replace('Start Time:','').replace('Local','').strip()

                    elif dv.text.find('Attendance:') != -1:
                        box_attendance = dv.text.replace('Attendance:','').strip()

                    elif dv.text.find('Venue:') != -1:
                        box_venue = dv.text.replace('Venue:','').strip()

                    elif dv.text.find('Game Duration:') != -1:
                        box_duration = dv.text.replace('Game Duration:','').strip()

                    elif dv.text.find('Game, on') != -1:
                        box_surface = dv.text.split(',')[1].replace('on','').strip()

                    else:
                        #date in Saturday, March 22, 2014 form
                        box_date = dv.text.strip()

            #sort out info
            tmpDate = games[gmID]['game_date'].strftime('%Y-%m-%d') + ', ' + box_time
            games[gmID]['game_date'] = dt.datetime.strptime(tmpDate,'%Y-%m-%d, %H:%M %p')  #leave timezone blank if unknown, will clean up later

        #add info to dictionary
        games[gmID]['attendance'] = box_attendance
        games[gmID]['venue'] = box_venue
        games[gmID]['duration'] = box_duration
        games[gmID]['surface'] = box_surface

    def _AddGameInfoToDatabase(self,gmID):
        """Adds the game info for the given game to the sportsdb.

        Args:
            gmID (str): Baseball reference ID of the game to get details for.
        """

        #Note: there should be a field to mark whether time is local, a specific time zone, or utc. functions will clean up local times later

        #convert home team to internal ID
        hmConv = self.db.GetTeamConversion('mlb',games[gmID]['home_team'],self.source_id)
        if hmConv is None:
            newTeam = self.db.mlb.Teams(teamname=games[gmID]['home_team'])
            hmTeam = self.db.AddOrUpdateTeam('mlb',games[gmID]['home_team'],self.source_id,newTeam)
            homeID = hmTeam.team_id
        else:
            homeID = hmConv.team_id

        #convert away team to internal ID
        awayConv = self.db.GetTeamConversion('mlb',games[gmID]['away_team'],self.source_id)
        if awayConv is None:
            newTeam = self.db.mlb.Teams(teamname=games[gmID]['away_team'])
            awayTeam = self.db.AddOrUpdateTeam('mlb',games[gmID]['away_team'],self.source_id,newTeam)
            awayID = awayTeam.team_id
        else:
            awayID = awayConv.team_id

        #deal with game time
        if games[gmID]['game_date'].tzname() is None:
            #timezone not defined, means that it is unknown and labeled as local time
            strTimeZone = 'local'
            storeDate = games[gmID]['game_date']

        if games[gmID]['game_date'].tzname() == 'UTC':
            #timezone is UTC, label as such and add time unaltered
            strTimeZone = 'utc'
            storeDate = games[gmID]['game_date']

        if games[gmID]['game_date'].tzname() != 'UTC' and games[gmID]['game_date'].tzname() is not None:
            #timezone is something other than utc, convert to utc
            strTimeZone = 'utc'
            storeDate = games[gmID]['game_date'].astimezone(pytz.utc)


        #add stadium if not found
        if games[gmID]['venue'] is None:
            stadID = None
        else:
            stadConv = self.db.GetStadiumConversion('mlb',games[gmID]['venue'],self.source_id)
            if stadConv is None:
                newStad = self.db.mlb.Stadiums(stadium=games[gmID]['venue'])
                newStad = self.db.AddOrUpdateStadium('mlb',games[gmID]['venue'],self.source_id,newStad)
                stadID = newStad.stadium_id
            else:
                stadID = stadConv.stadium_id

        #add game, remove tz info from date
        newGame = self.db.mlb.Schedule(home_team= homeID,
                                        away_team= awayID,
                                        stadium_id= stadID,
                                        time= storeDate.replace(tzinfo=None),
                                        is_season= False,
                                        tz=strTimeZone)

        newGame = self.db.AddOrUpdateGame('mlb',games[gmID]['game_id'],self.source_id,newGame)

    def GetSchedule(self,year):
        """Downloads and updates game schedule for the given year.

        Args:
            year (int): Year to download the schedule for.


            - List of games at http://www.baseball-reference.com/leagues/MLB/2014-schedule.shtml
            - Specific game box score http://www.baseball-reference.com/boxes/ARI/ARI201403220.shtml
                - Game IDs are typically the home team and date and an additional number for double headers
            - Games that are not yet played do not have a box score and have a slightly different schedule page structure
        """

        #get a fresh games dictionary.
        self.games = {}

        #get games for the year
        self._GetOnlineGameList(year)

        #get more details about each game (box score)
        for gID in games:
            self._GetGameDetails(gID)

        #add info to database
        for gID in games:
            self._AddGameInfoToDatabase(gID)

    def _GetHitterGameLog(self,pID):
        """Download a batter's game log stats

        Args:
            pID (str): Baseball reference player ID to download the game log from.
        """

        #get table
        url = 'http://www.baseball-reference.com/players/gl.fcgi?id=heywaja01&t=b&year=2016'
        pg = BeautifulSoup(GetURLtext(url))

        #get body of the table of teams
        logTable = pg.find("table",{"id":"batting_gamelogs"})
        body = logTable.find("tbody")

        #get data frame of table data
        log = self.TableParse(body)


        return

    def _GetPitcherGameLog(self,pID):
        """Download a pitchers's game log stats

        Args:
            pID (str): Baseball reference player ID to download the game log from.
        """

        return

if __name__ == "__main__":
    print('import module to access classes for the ProSportsReferences scrapers (pro-football-reference,pro-baseball-reference).')