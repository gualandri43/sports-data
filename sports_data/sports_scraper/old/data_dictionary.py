'''
fangraphs data column mappings
'''


PREFIXES = [
    'stats.batting',
    'stats.pitching'
]

DATA_HEADERS = [
    'scrape.timeLocal',
    'scrape.timeUtc',
    #'scrape.timeLocalString',
    'meta.seasonYear',
    'meta.projectionDate', #stored as unix epoch time in db
    'meta.projectionSource',
    'meta.projectionScope',
    'player.id',
    'player.name',
    'player.team',
    'player.position',
    'stats.batting.game',
    'stats.batting.plateAppearance',
    'stats.batting.atBat',
    'stats.batting.hit',
    'stats.batting.double',
    'stats.batting.triple',
    'stats.batting.homerun',
    'stats.batting.run',
    'stats.batting.rbi',
    'stats.batting.walk',
    'stats.batting.hitByPitch',
    'stats.batting.strikeout',
    'stats.batting.stolenBase',
    'stats.batting.caughtStealing',
    'stats.batting.avg',
    'stats.batting.obp',
    'stats.batting.slg',
    'stats.batting.ops',
    'stats.batting.woba',
    'stats.batting.war',
    'stats.batting.wrcPlus',
    'stats.pitching.game',
    'stats.pitching.hit',
    'stats.pitching.homerun',
    'stats.pitching.strikeout',
    'stats.pitching.win',
    'stats.pitching.loss',
    'stats.pitching.save',
    'stats.pitching.era',
    'stats.pitching.er',
    'stats.pitching.gameStarted',
    'stats.pitching.ip',
    'stats.pitching.whip',
    'stats.pitching.strikeoutPerNine',
    'stats.pitching.walkPerNine',
    'stats.pitching.fip',
    'stats.pitching.war',
    'stats.pitching.totalBattersFaced'
]

DATA_HEADERS_2 = {
    'scrape': [
        'timeLocal',
        'timeUtc'
    ],
    'meta': [
        'seasonYear',
        'projectionDate', #stored as unix epoch time in db
        'projectionSource',
        'projectionScope'
    ],
    'player': [
        'id',
        'name',
        'team',
        'position'
    ],
    'stats': {
        'batting': [
            'game',
            'plateAppearance',
            'atBat',
            'hit',
            'double',
            'triple',
            'homerun',
            'run',
            'rbi',
            'walk',
            'hitByPitch',
            'strikeout',
            'stolenBase',
            'caughtStealing',
            'avg',
            'obp',
            'slg',
            'ops',
            'woba',
            'war',
            'wrcPlus'
        ],
        'pitching': [
            'game',
            'hit',
            'homerun',
            'strikeout',
            'win',
            'loss',
            'save',
            'era',
            'er',
            'gameStarted',
            'ip',
            'whip',
            'strikeoutPerNine',
            'walkPerNine',
            'fip',
            'war',
            'totalBattersFaced'
        ]
    }
}


WEBSITE_HEADER_MAP = {
    'player_id': 'player_id',
    'playerid': 'player_id',
    'Name': 'player_name',
    'Team': 'player_team',
    'Pos': 'player_position',
    'G': 'game',
    'PA': 'plate_appearance',
    'AB': 'at_bat',
    'H': 'hit',
    '1B': 'single',
    '2B': 'double',
    '3B': 'triple',
    'HR': 'homerun',
    'R': 'run',
    'RBI': 'rbi',
    'BB': 'walk',
    'SO': 'strikeout',
    'SB': 'stolen_base',
    'CS': 'caught_stealing',
    'AVG': 'average',
    'OBP': 'obp',
    'SLG': 'slg',
    'OPS': 'ops',
    'wOBA': 'woba',
    'W': 'win',
    'L': 'loss',
    'SV': 'save',
    'ERA': 'era',
    'ER': 'er',
    'GS': 'game_started',
    'IP': 'ip',
    'WHIP': 'whip',
    'K/9': 'strikeout_per_nine',
    'BB/9': 'walk_per_nine',
    'FIP': 'fip',
    'wRC+': 'wrc_plus',
    'BsR': 'bsr',
    'Fld': 'fld',
    'HBP': 'hit_by_pitch',
    'TBF': 'total_batters_faced',
    'WAR': 'war'
}




def get_database_header(dataHeader):
    '''
    get database header from DATA_HEADER entry.
    '''

    if dataHeader not in DATA_HEADERS:
        raise ValueError("dataHeader must be in DATA_HEADERS.")

    hdr = dataHeader

    #replace periods with underscore
    hdr = hdr.replace('.', '_')

    #replace capital letter with _<lowercase>
    newHdr = ''

    for letter in hdr:
        if letter.isupper():
            newHdr += '_' + letter.lower()
        else:
            newHdr += letter

    return newHdr

def get_data_header_from_database_header(databaseHeader):
    '''
    get database header from DATA_HEADER entry.
    '''

    hdr = databaseHeader

    #replace periods with underscore
    if hdr.startswith('batting_'):
        hdr = hdr.replace('batting_', 'batting.')

    if hdr.startswith('pitching_'):
        hdr = hdr.replace('pitching_', 'pitching.')

    #replace _<lowercase> with capital letter
    #
    newHdr = ''
    capitalizeNext = False

    for letter in hdr:

        if letter == '_':
            #skip adding _ and capitalize next letter
            capitalizeNext = True

        else:
            if capitalizeNext:
                newHdr += letter.upper()
            else:
                newHdr += letter

            capitalizeNext = False

    return newHdr

def get_data_header_from_website_header(prefix, websiteHeader):
    '''
    get the data header that corresponds to the fangraphs website header
    '''

    if prefix not in PREFIXES:
        raise ValueError("prefix not valid.")

    noPrefixHdrMap = {
        'player_id': 'player.id',
        'playerid': 'player.id',
        'Name': 'player.name',
        'Team': 'player.team',
        'Pos': 'player.position',
    }

    hdrMap = {
        'G': 'game',
        'PA': 'plateAppearance',
        'AB': 'atBat',
        'H': 'hit',
        '2B': 'double',
        '3B': 'triple',
        'HR': 'homerun',
        'R': 'run',
        'RBI': 'rbi',
        'BB': 'walk',
        'SO': 'strikeout',
        'SB': 'stolenBase',
        'CS': 'caughtStealing',
        'AVG': 'avg',
        'OBP': 'obp',
        'SLG': 'slg',
        'OPS': 'ops',
        'wOBA': 'woba',
        'W': 'win',
        'L': 'loss',
        'SV': 'save',
        'ERA': 'era',
        'ER': 'er',
        'GS': 'gameStarted',
        'IP': 'ip',
        'WHIP': 'whip',
        'K/9': 'strikeoutPerNine',
        'BB/9': 'walkPerNine',
        'FIP': 'fip',
        'wRC+': 'wrcPlus',
        'BsR': 'bsr',
        'Fld': 'fld',
        'HBP': 'hitByPitch',
        'TBF': 'totalBattersFaced',
        'WAR': 'war'
    }

    if websiteHeader in hdrMap:
        return '{0}.{1}'.format(prefix, hdrMap[websiteHeader])

    elif websiteHeader in noPrefixHdrMap:
        return noPrefixHdrMap[websiteHeader]

    else:
        return None
