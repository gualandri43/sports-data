'''Classes and functions to scrape data from the DFS Gold website.'''

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import json
import pandas as pd
import numpy as np
import datetime as dt
import time
import pytz
import random
from selenium import webdriver
import sportsdbInterface as db
from sports_data.common.common_funcs import isNumber
from sports_data.sportsScraper.common import GetURLtext, CleanUnicodeText, parseStandardHTMLTable

class dfsgold_scraper:

    def __init__(self,type,recap_date,dbInstance):
        #recap_date shoule be a datetime object
        #dbInstance should be ready to accept new stuff

        self.type = type
        self.recap_date = recap_date
        self.dbInstance = dbInstance
        self.idLookup = None
        self.url = None
        self.recapPg = None
        self.tbls = None		#table 0 is best lineup, table 1 is constent recap, table 2 is salary/player results
        self.sal_list = []
        self.contest_list = None
        sal_list = None

        if self.type == 'nfl':
            self.url = 'http://www.dfsgold.com/nfl/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        if self.type == 'nba':
            self.url = 'http://www.dfsgold.com/nba/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        if self.type == 'mlb':
            self.url = 'http://www.dfsgold.com/mlb/draftkings-daily-fantasy-recap-' + recap_date.strftime('%b-%d-%Y')

        #build lookup
        self.GenerateIDLookup()

    def GenerateIDLookup(self):

        if self.type == 'nfl':
            self.idLookup = dbInstance.GetLookup('idConversion_nfl','dfsgold_id','master_id')

        if self.type == 'nba':
            self.idLookup = dbInstance.GetLookup('idConversion_nba','dfsgold_id','master_id')

        if self.type == 'mlb':
            self.idLookup = dbInstance.GetLookup('idConversion_mlb','dfsgold_id','master_id')

    def GetRecapPageTables(self):

        if not self.url is None:
            pg = urlopen(self.url)
            self.recapPg = BeautifulSoup(pg)

            #check if recap date is correct
            chkStr = self.recap_date.strftime('%b %d, %Y')

            title = self.recapPg.find('title').get_text()

            if title.find(chkStr) != -1:
                return False

            self.tbls = self.recapPg.find_all('table')

            return True

    def ParsePageRecapPage(self):

        #contest info
        self.contest_list = parseStandardHTMLTable(self.tbls[1])

        #salary info
        bd = tbls[2].find('tbody')
        for row in bd.find_all('tr'):
            col = row.find_all('td')
            pos = col[0].strip()
            tm = col[2].strip()
            pid = int(col[1].a['href'].split('/')[-1])  #player id is the last part of the href link
            name = col[1].get_text()
            sal = int(col[4].get_text().replace('$','').replace(',','').strip())
            d = self.recap_date.strftime('%Y-%m-%d')

            self.sal_list.append([pid,name,sal,d,pos,tm])

    def AddSalaryToDatabase(self):

        #add to database
        for p in self.sal_list:

            mID = self.idLookup.lookup(p[0])

            if self.type == 'nfl':
                if mID is None:
                    #need to add player to database master list
                    np = db.playersNFL( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionNFL.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionNFL.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryNFL(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionNFL.merge(psal)

            if self.type == 'nba':
                if mID is None:
                    np = db.playersNBA( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionNBA.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionNBA.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryNBA(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionNBA.merge(psal)

            if self.type == 'mlb':
                if mID is None:
                    np = db.playersMLB( fullname = p[1], firstname=p[1].split(' ')[0],lastname= ''.join(p[1].split(' ')[1:]), pos = p[4], team = p[5] )
                    self.dbInstance.sessionMLB.merge(np)

                    nconv = db.idConversionNFL(master_id = np.master_id, dfsgold_id = p[0] )
                    self.dbInstance.sessionMLB.merge(np)

                    #rebuild lookup
                    self.GenerateIDLookup()

                    #set new master id for salary insert
                    mID = np.master_id

                #insert into salary list
                psal = db.DFSsalaryMLB(player_id = mID, sal = p[2], sal_date = p[3])
                self.dbInstance.sessionMLB.merge(psal)

if __name__ == "__main__":
    print('import module to access classes for the dfsgold.com scraper.')