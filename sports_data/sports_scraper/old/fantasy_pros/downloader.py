'''
functions for downloading data from fantasypros.com

can get basic stuff without logging in, but logging in
provides access to high/low values for projections.
'''

import requests
import lxml.html
import lxml.etree


LOGIN_URL = 'https://secure.fantasypros.com/accounts/login/'

def _parse_hdrs(hdrRows):
    '''
    parses the header rows of the table and returns a formatted list
    with columns in order from left to right

    hdrRows     list of header row elements (lxml)
    '''
    hdrs1 = []
    hdrRowCount = 0

    #top header row
    for hdrCol in hdrRows[hdrRowCount].xpath('.//td'):
        colSpan = hdrCol.get('colspan')

        colText = hdrCol.xpath('.//text()')[0].getparent().text.strip()

        if colSpan is None:
            hdrs1.append(colText)
        else:
            for i in range(int(colSpan)):
                hdrs1.append(colText)

    if len(hdrs1) > 0:
        hdrRowCount += 1
        hdrs1 = [x.strip() for x in hdrs1]

    #second header row
    hdrs2 = []
    for hdrCol in hdrRows[hdrRowCount].xpath('.//th'):

        colText = hdrCol.xpath('.//text()')[0].getparent().text.strip()
        hdrs2.append(colText)

    #combine
    if len(hdrs1) < len(hdrs2):
        hdrs1.extend([''] * (len(hdrs2) - len(hdrs1)))

    hdrs = []
    for hdr in zip(hdrs1, hdrs2):
        if hdr[0] == '':
            hdrs.append(hdr[1].strip().lower())
        else:
            hdrs.append('{0}.{1}'.format(hdr[0].strip().lower(), hdr[1].strip().lower()))

    hdrs = [x.replace(' ', '_') for x in hdrs]

    return hdrs

def _clean_stat(stat):

    if stat is None:
        return None

    if isinstance(stat, str):
        return float(stat.replace(',','').strip())

    if isinstance(stat, int):
        return float(stat)

    return float(stat)

def parse_csrf_token(respHtml):
    '''
    parse out the CSRF token from the login page response.
    '''

    return lxml.html.fromstring(respHtml).xpath('//*[@name="csrfmiddlewaretoken"]')[0].get('value')

def build_initial_login_request():
    '''
    This request GETs the login page to retrieve the CSRF token.

    returns a requests.Request object.
    '''

    return requests.Request('GET', LOGIN_URL)

def build_secondary_login_request(username, password, csrfToken):
    '''
    Build a POST requests to login to website.

    returns a requests.Request object.
    '''

    return requests.Request('POST',
        LOGIN_URL,
        data= {
            'username': username,
            'password': password,
            'csrfmiddlewaretoken': csrfToken
        }
    )

def build_nfl_projections_requests(minMax=False):
    '''
    returns a list of requests.Request objects for each nfl position.

    If minMax = True, then this request must be sent within a loggd-in session.

    example url
    https://www.fantasypros.com/nfl/projections/rb.php?max-yes=true&min-yes=true&week=draft&scoring=PPR&week=draft
    '''

    positions = ['qb','rb','wr','te','k','dst']

    params = {
        'week': 'draft',
        'scoring': 'PPR'
    }

    if minMax:
        params.update(
            {
                'max-yes': 'true',
                'min-yes': 'true'
            }
        )

    reqs = list()
    for pos in positions:
        url = 'https://www.fantasypros.com/nfl/projections/{0}.php'.format(pos)
        reqs.append(requests.Request('GET', url, params=params))

    return reqs

def login(username, password):
    '''
    return a requests.Session object that has been logged in to fantasypros.com
    using the given username and password.

    Fantasypros.com uses a csrf token during login authentication. The token is stored in
    an <input> element with name=csrfmiddlewaretoken and value=token_value. To login,
    follow these steps.
        1. Start a session to store cookies, headers, etc.
        2. GET request the login page within the session.
        3. parse and extract the CSRF token.
        4. POST request using username, password, and token
        5. session now has proper authentication cookies.

    '''

    _session = requests.Session()

    _initialLoginPage = _session.send(
        _session.prepare_request(
            build_initial_login_request()
        )
    )

    _csrfToken = parse_csrf_token(_initialLoginPage.text)

    _resp = _session.send(
        _session.prepare_request(
            build_secondary_login_request(username, password, _csrfToken)
        )
    )

    if _resp.status_code != 200:
        raise Exception("Non-200 status code resulted from login.")

    return _session

def parse_nfl_projections(respText):
    '''
    respText        the text of the response.

    returns list of dictionary records
    '''

    respPage = lxml.html.fromstring(respText)

    projTable = respPage.xpath('//div[@class="main-content"]//table[@id="data"]')[0]

    #headers
    #two header rows, top with spans and bottom with stat headings
    hdrRows = projTable.xpath('.//thead/tr')
    hdrs = _parse_hdrs(hdrRows)

    # position
    #
    pos = respPage.xpath('//*[@data-mobile-label="Positions"]/li[@class="active"]/a')[0].text

    #data
    items = []
    for playerRow in projTable.xpath('.//tbody/tr'):
        item = {}
        item['position'] = pos.lower()

        colCount = 0
        for playerCol in playerRow.xpath('.//td'):

            if playerCol.get('class') == 'player-label':
                # column is a player name

                # fp-id-16413
                # the id class is in an <a> element with multiple classes
                #   split class string returned from xpath by spaces to get individual class names
                idNumber = None
                for aTag in playerCol.xpath('.//a'):
                    classNames = aTag.get('class')

                    for className in classNames.split(' '):
                        if 'fp-id' in className:
                            idNumber = int(className.split('-')[-1])

                item['player.name'] = playerCol.xpath('.//a[@class="player-name"]')[0].text.strip()
                item['player.link'] = playerCol.xpath('.//a[@class="player-name"]')[0].get('href')
                #item['player.team'] = playerCol.text.strip()
                item['player.id'] = idNumber

            else:
                #data column
                # columns are in same order as hdr row names

                if item['position'] == 'dst':
                    prefix = 'dst.'

                elif item['position'] == 'k':
                    prefix = 'k.'

                else:
                    prefix = ''

                minStatElem = playerCol.xpath('.//div[@class="min-cell"]')
                maxStatElem = playerCol.xpath('.//div[@class="max-cell"]')

                if minStatElem:
                    item[prefix + hdrs[colCount] + '.min'] = _clean_stat(minStatElem[0].text)

                if maxStatElem:
                    item[prefix + hdrs[colCount] + '.max'] = _clean_stat(maxStatElem[0].text)

                item[prefix + hdrs[colCount] + '.consensus'] = _clean_stat(playerCol.text)

            colCount += 1

        items.append(item)

    return items


def main():
    '''
    test function.
    '''

    _pw = input('input fantasypros.com password: ')
    _session = login('gualandri43@gmail.com', _pw)

    projs = list()
    for req in build_nfl_projections_requests(minMax=True):

        resp = _session.send(
            _session.prepare_request(req)
        )

        projs.extend(parse_nfl_projections(resp.text))
        print(projs[-1])

    print(projs[0])
    print(projs[-1])
    print(len(projs))

if __name__ == "__main__":
    main()
