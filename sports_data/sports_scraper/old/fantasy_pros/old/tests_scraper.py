import scraper
import pandas as pd
import numpy as np

def fantasy_pros_login():
    
    scrp = scraper.fantasyProsBase('nfl',isTesting=True)
    scrp._LogIn()
    df = scrp._DownloadProjections('qb',['Player','Patt','Pcmp','Pyd','Ptd','Pint','Ratt','Ryd','Rtd','FL','fpts'],'draft')
    scrp._Close()
    print(df)

def fantasy_pros_consensus():
    
    #return should be close to [.499289,.933649,.287726,.444617]

    scrp = scraper.fantasyProsBase('nfl',isTesting=True)
    
    srcs = np.array(['1:5','1:2','1:4','1:2:4:5'])
    combs = np.array([0.471953,0.716469,0.39358,0.54123])

    projData = pd.Series(combs,index=srcs)

    projs = scrp._RecoverSourceProjections(projData)
    print(projs)

def fantasy_pros_consensus_NFL():
    
    scrp = scraper.fantasyProsNFL_scraper('nfl',isTesting=True)
    scrp.DownloadLatestProjections()

    print(scrp.scraped)

def fantasy_pros_indiv_NFL():
    
    scrp = scraper.fantasyProsNFL_scraper('nfl',isTesting=True)
    scrp.DownloadLatestProjections(breakout=True)

    #print(scrp.scraped)
    print(scrp.projs)

#fantasy_pros_login()
#fantasy_pros_consensus()
fantasy_pros_indiv_NFL()