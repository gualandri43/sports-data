'''Classes and functions to scrape data from the FantasyPros.com website.'''

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import json
import pandas as pd
import numpy as np
import datetime as dt
import time
import pytz
import random
from selenium import webdriver
import sportsdbInterface as db
from sports_data.common.common_funcs import isNumber
from sports_data.sportsScraper.common import GetURLtext, CleanUnicodeText, parseStandardHTMLTable

class fantasyProsBase:
    '''Base class for the fantasypros.com scrapers.

    Attributes:
        source_ids (dict): Dictionary defining the source IDs that are for each source scraped from this site.
        sport (str): Sport the scraper is for.
        players: Holds scraped projection data.
        db: Instance of dbInterface class defining the connection to the database.

    '''

    def __init__(self,sport,week='draft',isTesting=False):
        '''Constructor.

        Args:
            sport (str): Sport the scraper is for. Options are ['mlb','nfl'].
            week: Optional. Define the week to download stats for. Defaults to 'draft' which means pre-draft full season.
            isTesting (bool): Optional flag. Set to true if it is desired to use the testing database.
        '''

        self.source_ids = {}
        self.sport = sport
        self.week = week
        self.players = None
        self.browser = None

        #if isTesting:
        #	self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')
        #else:
        #	self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb')

    def _CleanStat(self,stat):
        """Cleans the stats if not a number (-).

        Args:
            stat (value): Stat to clean.

        Returns:
            num: Returns a number (float or int) or None.
        """

        if isNumber(stat):
            return stat
        else:
            return None

    def _LogIn(self):
        '''Log in to the fantasy pros website using selenium. Returns True if login is successful.'''

        self.browser = webdriver.Firefox()

        #log in
        url = 'https://secure.fantasypros.com/accounts/login/'
        self.browser.get(url)

        userbox = self.browser.find_element_by_id('id_username')
        passbox = self.browser.find_element_by_id('id_password')

        if userbox is None or passbox is None:
            return False

        userbox.send_keys('gualandri43@gmail.com')
        passbox.send_keys('dtg1989')

        time.sleep(0.5)

        loginBtn = self.browser.find_element_by_xpath("//*[contains(text(),'LOG IN')]")
        loginBtn.click()

    def _Close(self):
        '''Cleans up browser session.'''

        if self.browser is not None:
            self.browser.quit()

    def _DownloadProjections(self,pos,cols,week,sources=None):
        '''Method to download consensus projections. Must use selenium to download

        Download url is formulated as https://www.fantasypros.com/<sport>/projections/<pos>.php?week=draft

        Args:
            pos (str): Position to download.
            cols: List of strings defining what the scraped column names on the web page should be. First column must be 'Player'
            week: Defines the week to download projections for. "draft" is for the pre-season projections.
            sources: Optional. List of integers representing the FantasyPros source ID. Defaults to downloading the full consensus projections.

        Return:
            Pandas dataframe with projections.
        '''

        #build url
        if sources is None:
            url = 'https://www.fantasypros.com/{0}/projections/{1}.php?week=draft'.format(self.sport,pos)
        else:
            url = 'https://www.fantasypros.com/{0}/projections/{1}.php?filters={2}&week=draft'.format(self.sport,pos,':'.join([str(x) for x in sources]))

        #get page html
        attempts = 0
        #while attempts < 3:
            #try:
        if self.browser is None:
            resp = urlopen(url)
            pg_html = resp.read()
        else:
            self.browser.get(url)
            pg_html = self.browser.page_source
            #except:
                #attempts +=1


        #parse html
        pg = BeautifulSoup(pg_html,"lxml")
        tbl = pg.find('table',id='data')	#projection data is in 'data' table
        tbl_string = str(tbl)

        #extract player ID and name info from Player column. Extracts to format <player id>:<player name>
        tbl_string = re.sub('<a.*?class="fp-player-link fp-id-(.*?)" fp-player-name="(.*?)".*?<\/a>', '\\1:\\2', tbl_string)

        #table will be the only one. skip the first header row, which is the spanned categories. Use the 2nd row as header (now the 1st after skip)
        #table headers don't seem to be consistent, so implemented a way to have it test until it finds correct header row.
        test_col = ''
        skipStart = 3
        while test_col.lower() != 'player':

            df = pd.read_html(tbl_string,skiprows=skipStart,header=0)[0]
            test_col = df.columns[0]
            skipStart -= 1

        df.columns = cols

        #expand player column
        expanded = df['Player'].str.split(":",expand=True)
        expanded.columns = ['pID','pName']

        #combine back into projection table
        proj = pd.merge(df,expanded,left_index=True,right_index=True)

        #add the source combination as a column
        if sources is None:
            proj = proj.set_index('pID')
        else:
            #proj['src'] = ':'.join([str(x) for x in sources])
            proj['src'] = ':'.join([str(x) for x in list(set(sources))])
            proj = proj.set_index(['pID','src'])

        proj['pos'] = str(pos)

        return proj

    def _RecoverSourceProjections(self,projData):
        '''Recovers individual source projections by solving a system of equations. Assumes that the consensus value is a result
        of the straight average of individual source components. Must have more that 2 different sources to provide a meaningful answer.

        This function evaluates one statistic at a time.

        Solves the system of equations. The first source is used in every line to prevent a singular coefficient matrix.

            0.5*src1 + 0.5*src2 = b1
            0.5*src1 + 0.5*src3 = b2
            0.5*src1 + 0.5*src4 = b3
            0.25*src1 + 0.25*src2 + 0.25*src3 + 0.25*src4 = b4

        The final version of this function uses all available source pairings, resulting in num equations > num unknowns. This is an overdetermined system,
        so np.linalg.lstsq() is used. This uses a least squares algorithmn to approximate the solution. The A matrix in this instance will result in a deterministic
        solution, since the extra equations are linear combinations of each other.

        Args:
            projData: input projection data for each the combination of sources for a given stat. Is a pandas series indexed by the
            source combination of form <src1>:<src2>.

        Returns:
            A pandas series with the recovered stats, indexed by source number.

        '''

        #get list of sources from the entries in the input data
        #only use pairs of sources for this routine
        src_list = []
        for comb in projData.index.tolist():
            splitSrc = comb.split(':')

            #if len(splitSrc) == 2:
            for src in splitSrc:
                src_list.append(int(src))

        srcs = np.array(src_list)
        srcs = np.unique(srcs)			#want unique values
        srcs.sort()						#sort in ascending order

        #return zeros if all inputs are zero
        if (projData < 0.0001).all():
            return pd.Series(np.zeros(srcs.size),index=srcs)

        #build A and B matrix
        A = np.zeros((projData.index.size,srcs.size))
        B = np.zeros((projData.index.size,1))

        rowCount = 0

        for idx in projData.index:

            idxSources = idx.split(':')
            for s in idxSources:

                A[rowCount,np.where(srcs == int(s))] = 1/len(idxSources)

            B[rowCount,0] = projData[idx]

            rowCount += 1

        #solve system and organize answers
        #will be an array of len(B)
        #use lstsq instead of solve, since the system will be over-determined
        sol = np.linalg.lstsq(A,B)

        retSeries = pd.Series(sol[0].flatten(),index=srcs)

        return retSeries

class fantasyProsNFL_scraper(fantasyProsBase):
    '''Class for nfl scraping from Fantasypros.com.

    Site source numbers: 11,71,73,120,152
                       : CBS,ESPN,numberFire,STATS,FFToday
    '''

    def __init__(self,week='draft',isTesting=False):
        '''Constructor.'''

        self.srcs = [11,71,73,120,152]
        self.positions = ['qb','rb','wr','te','k','dst']
        self.columns = {
                        'qb':['Player','pAtt','pCmp','pYd','pTd','pInt','rshAtt','rshYd','rshTd','fum','fPts'],
                        'rb':['Player','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','fPts'],
                        'wr':['Player','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','fPts'],
                        'te':['Player','recAtt','recYd','recTd','fum','fPts'],
                        'k':['Player','kFg','kFga','kXpt','fPts'],
                        'dst':['Player','defSack','defInt','defFr','defFf','defTd','defAssist','defSaf','defPa','defYdAgst','fPts']
                        }
        self.statColumns = ['pAtt','pCmp','pYd','pTd','pInt','rshAtt','rshYd','rshTd','recAtt','recYd','recTd','fum','kFg','kFga','kXpt',
                            'defSack','defInt','defFr','defFf','defTd','defAssist','defSaf','defPa','defYdAgst']
        self.scraped = None
        self.projs = None

        super(fantasyProsNFL_scraper, self).__init__('nfl',week=week,isTesting=isTesting)

    def _GetIndividualProjections(self,srcs):
        '''Download and parse out individual projections for the given sources.

        Args:
            srcs: List of integer site ids of the sources to download.
        '''

        #ensure there are more than two listed sources
        if len(srcs) < 2:
            return 'Unable to download projections for a single source.'

        self._LogIn()
        time.sleep(1)


        #create list of sources to iterate through
        #fills out all combinations of the source values that are greater than 1 item long
        #definitely not the most efficient way to do this
        #srcLists = []
        #for k in range(len(srcs)):
        #    for i in range(k+1,len(srcs)):
        #        for j in range(i,len(srcs)):
         #           l = srcs[k:i],[srcs[j]]
         #           srcLists.append([item for sublist in l for item in sublist])

        #download for all positions
        for pos in self.positions:

            #put in a time delay to prevent hammering the website
            time.sleep(random.randrange(1,10))

            #loop through sources so that all pairs are downloaded only once
            #for comb in srcLists:
            for src in srcs:

                    #print('Downloading projections for {0}, sources={1}'.format(pos,':'.join([str(x) for x in comb])))
                    print('Downloading projections for {0}, source={1}'.format(pos,src))

                    time.sleep(random.randrange(1,3))
                    projs = self._DownloadProjections(pos,self.columns[pos],self.week,sources=[src,src])
                    #projs['position'] = pos

                    if self.scraped is None:
                        self.scraped = projs.copy()
                    else:
                        self.scraped = pd.concat([self.scraped,projs])

        self._Close()



        #self.scraped = pd.DataFrame.from_csv('C:/Users/Dan/Desktop/scraped.csv',header=0,index_col=['pID','src'])

        #deduplicate (~ notation is for the logical not)
        #players may appear on multiple positions.
        self.scraped = self.scraped[~self.scraped.index.duplicated(keep='first')]

        ##recover individual stats
        #combinedSeries = pd.Series()

        ##idx will be a tuple of (pID,src) of the index
        #processCount = 0
        #totalToProcess = len(self.scraped)
        #for idx in self.scraped.index:
        #    print('Processing stats for {0}. {1}% complete.'.format(idx[0],processCount*100/totalToProcess))

        #    for stat in self.scraped.columns:
        #        if stat in self.statColumns:
        #            pID = idx[0]
        #            statSeries = self.scraped[stat][pID]

        #            numMask = np.logical_not(np.isnan(statSeries))

        #            #must have at least three sources to allow for deterministic recovery of a stat
        #            if np.sum(numMask) >= 3 and np.any(statSeries != 0):
        #                statSeries = self._RecoverSourceProjections(statSeries[numMask])

        #                newIdx = pd.MultiIndex.from_product([[pID],[stat],statSeries.index.tolist()],names=['pID','stat','src'])

        #                combinedSeries = combinedSeries.append(pd.Series(statSeries.values,index=newIdx))

        #    processCount += 1

        #self.projs = pd.DataFrame(combinedSeries,index=pd.MultiIndex.from_tuples(combinedSeries.index))
        #self.projs = self.projs.drop_duplicates(keep='first')
        #self.projs = self.projs.unstack()

        #temporary output
        #self.projs.to_csv('C:/Users/Dan/Desktop/projs.csv')

    def _GetConsensusProjections(self):
        '''Only download the consensus projections.'''

        for pos in self.positions:

            print('Downloading projections for {0}'.format(pos))

            #put in a time delay to prevent hammering the website
            time.sleep(random.randrange(1,5))

            projs = self._DownloadProjections(pos,self.columns[pos],self.week)

            if self.scraped is None:
                self.scraped = projs.copy()
            else:
                self.scraped = pd.concat([self.scraped,projs])

    def DownloadLatestProjections(self,breakout=False,saveScraped=None):
        '''Method to download the latest sets of projections from the site.

        Args:
            breakout (bool): Optional. If true, the projections will be broken out into their components. False causes only the consensus projections to be retrieved.
        '''

        if breakout:
            self._GetIndividualProjections(self.srcs)
        else:
            self._GetConsensusProjections()

        if saveScraped is not None:
            self.scraped.to_csv(saveScraped)

class fantasyPros_scraper:
    """Scrapes info from Fantasypros.com.

    Using pandas from_html works pretty well, but loses the links which have the player ID numbers in them.
    z = pd.read_html('https://www.fantasypros.com/mlb/projections/pitchers.php',attrs = {'id': 'data'})

    could use regex to extract links into text part? (http://stackoverflow.com/questions/31771619/html-table-to-pandas-table-info-inside-html-tags)
    tbl = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', tbl)

    THIS NO LONGER APPLIES:: clicking on name brings to a page with all the source projections, so all the different ones can be scraped for each individual player

    """

    def __init__(self,sport):
        self.source_ids = {}
        self.sport = sport
        self.players = None
        self.db = db.dbInterface(conString='postgresql://python_read_write:py89@localhost/sportsdb_test')

        self.source_ids['nfl'] = {}

        self.source_ids['mlb'] = {}
        self.source_ids['mlb']['FantasyPros'] = 3
        self.source_ids['mlb']['Steamer Blog'] = 4
        self.source_ids['mlb']['Clay Davenport'] = 5
        self.source_ids['mlb']['RotoChamp'] = 6
        self.source_ids['mlb']['CBS Sports'] = 7
        self.source_ids['mlb']['ESPN'] = 8
        self.source_ids['mlb']['Razzball'] = 9
        self.source_ids['mlb']['Baseball Think Factory'] = 11

    def _CleanStat(self,stat):
        """Cleans the stats if not a number (-).

        Args:
            stat (value): Stat to clean.

        Returns:
            num: Returns a number (float or int) or None.
        """

        if isNumber(stat):
            return stat
        else:
            return None

    def GetSeasonProjections(self,season):
        """Downloads season projections for the appropriate sport.

        Args:
            season (int): Season projections are being downloaded for.
        """

        if self.sport == 'mlb':
            self._mlbSeasonProjections(season)

    def _mlbSeasonProjections(self,season):
        """Downloads the season projections for MLB players from all sources on each players projection page."""

        #create a game for the season in questions
        newGame = self.db.CreateSeasonGame('mlb',season)

        #get overall projections and cleaned player info dataframe
        self._mlbGetPlayers()

        #get projections and add to database
        for posType in self.players:
            for pIter in self.players[posType].iterrows():
                tmpID = None
                p = pIter[1]
                pProj = self._mlbGetSinglePlayerProjections('https://www.fantasypros.com' + p['PlayerLink'])

                print('Processing projections for ' + p['PlayerName'])

                for src in pProj:
                    try:
                        #ensure player is in database and get their ID number
                        plyr = self.db.mlb.Players(fullname=p['PlayerName'],is_active=True)
                        teamConv = self.db.GetTeamConversion('mlb',p['PlayerTeam'],src)

                        if tmpID is None:
                            if teamConv is None:
                                plyr = self.db.AddOrUpdatePlayer('mlb',int(p['PlayerID']),int(src),plyr,updateExisting=False)
                            else:
                                plyr = self.db.AddOrUpdatePlayer('mlb',int(p['PlayerID']),int(src),plyr,updateExisting=False,team_id=teamConv.team_id)

                            tmpID = plyr.player_id

                        #ensure a game is in the table for the season stats (will create a conversion for it as well)
                        statGame = self.db.AddOrUpdateGame('mlb','season-2017',src,newGame)

                        #add projections
                        if posType == 'hitters':

                            #make projection object with common stats, then add dependent on source
                            tempProj = self.db.mlb.PlayerStatsProj(player_id= tmpID,
                                                                    source_id= int(src),
                                                                    game_id= statGame.game_id,
                                                                    h_ab= self._CleanStat(pProj[src]['AB']),
                                                                    h_r= self._CleanStat(pProj[src]['R']),
                                                                    h_hr= self._CleanStat(pProj[src]['HR']),
                                                                    h_rbi= self._CleanStat(pProj[src]['RBI']),
                                                                    h_sb= self._CleanStat(pProj[src]['SB']),
                                                                    h_avg= self._CleanStat(pProj[src]['AVG']),
                                                                    h_obp= self._CleanStat(pProj[src]['OBP']),
                                                                    h_h= self._CleanStat(pProj[src]['H']),
                                                                    h_bb= self._CleanStat(pProj[src]['BB']),
                                                                    h_k= self._CleanStat(pProj[src]['SO']),
                                                                    h_ops= self._CleanStat(pProj[src]['OPS']))

                            #add in site specific projections to avoid zeros
                            if src not in [6,8]:
                                #no double or triple projections for espn or rotochamp

                                if '2B' in pProj[src]:
                                    tempProj.h_2b= self._CleanStat(pProj[src]['2B'])

                                if '3B' in pProj[src]:
                                    tempProj.h_3b= self._CleanStat(pProj[src]['3B'])


                        if posType == 'pitchers':
                            #no QS data for any source
                            #no CG data for steamer
                            #no CG data for daveport
                            #no HR, G, GS, CG for rotochamp
                            #no HR, G, GS for cbs
                            #no L, HR, CG for espn
                            #no HR, CG for razzball
                            #no CG for think factory

                            #make projection object with common stats, then add dependent on source
                            tempProj = self.db.mlb.PlayerStatsProj(player_id= tmpID,
                                                                    source_id= int(src),
                                                                    game_id= statGame.game_id,
                                                                    p_ip= self._CleanStat(pProj[src]['IP']),
                                                                    p_k= self._CleanStat(pProj[src]['K']),
                                                                    p_w= self._CleanStat(pProj[src]['W']),
                                                                    p_era= self._CleanStat(pProj[src]['ERA']),
                                                                    p_whip= self._CleanStat(pProj[src]['WHIP']),
                                                                    p_er= self._CleanStat(pProj[src]['ER']),
                                                                    p_h= self._CleanStat(pProj[src]['H']),
                                                                    p_bb= self._CleanStat(pProj[src]['BB']),
                                                                    p_sv= self._CleanStat(pProj[src]['SV']),
                                                                    p_bs= self._CleanStat(pProj[src]['BS']),
                                                                    p_hld= self._CleanStat(pProj[src]['HD']))

                            #add site specific projections so that zeros don't get added to the database
                            if src != 8:
                                if 'L' in pProj[src]:
                                    tempProj.p_l = self._CleanStat(pProj[src]['L'])

                            if src not in [6,7,8,9]:
                                if 'HR' in pProj[src]:
                                    tempProj.p_hr = self._CleanStat(pProj[src]['HR'])

                            if src not in [6,7]:
                                if 'G' in pProj[src]:
                                    tempProj.p_g = self._CleanStat(pProj[src]['G'])

                            if src not in [6,7]:
                                if 'GS' in pProj[src]:
                                    tempProj.p_gs = self._CleanStat(pProj[src]['GS'])

                            if src not in [4,5,6,8,9,11]:
                                if 'CG' in pProj[src]:
                                    tempProj.p_cg = self._CleanStat(pProj[src]['CG'])


                        #merge into database, overwriting any pre-existing projection
                        if posType in ['hitters','pitchers']:
                            self.db.session.merge(tempProj)

                    except Exception as e:
                        print(str(e))

                self.db.session.commit()

        #commit changes
        self.db.session.commit()

    def _mlbGetSinglePlayerProjections(self,url):
        """Gets player projections from all sources on their player page that are defined in the sourc_ids dict.

        Args:
            url (str): URL to download player stats from

        Returns:
            dict: Dictionary with the projections from each source. Structured as key=srcID, val=dict of key=stat, val=projection value.

        """

        #result dict
        res = {}

        #get web page data
        pg_resp = urlopen(url)
        html = pg_resp.read()
        pg = BeautifulSoup(html,"lxml")

        #projection table is in here
        tbl = pg.find('div',attrs={"class":"mobile-table"})

        #parse projection table
        #headings
        headings = []
        for hdTag in tbl.find_all('thead'):
            for hd in hdTag.find_all('tr'):
                for h in hd.find_all('th'):
                    headings.append(h.text)

        #parse body
        body = []
        for bd in tbl.find_all('tbody'):
            for tr in bd.find_all('tr'):
                tempList = []
                for td in tr.find_all('td'):

                    #parse out expert name that's buried in a span element.
                    if len(td.find_all('span')) > 0:
                        for s in td.find_all('span'):
                            if s['class'] != ['expert-name']:
                                #ignore the expert name
                                #record all others using expert-affiliation as source identifier

                                tempList.append(s.text)
                    else:
                        tempList.append(td.text)

                body.append(tempList)

        #put body into dictionary entries for the player
        for r in body:
            for i in range(len(r)):

                #first entry is the source
                if headings[i] == 'Source':
                    srcID = self.source_ids['mlb'][r[i]]
                    res[srcID] = {}
                else:
                    res[srcID][headings[i]] = r[i]


        #return dictionary of source projections
        return res

    def _mlbCleanPlayerInfo(self,df):
        """Cleans the player info column from the consensus page and returns a dataframe with added columns with clean info.

        Args:
            df (dataframe): The dataframe of consensus player data to be cleaned.

        Returns:
            dataframe: Original dataframe with addes columns of cleaned information.

        """

        #"/mlb/projections/mookie-betts.php             : player link regex --> \/mlb\/projections.*?.php
        #class="fp-player-link fp-id-3215"              : id regex --> fp-id-.*?\"
        #fp-player-name="Clayton Kershaw                : name regex --> fp-player-name=.*
        #(/mlb/teams/los-angeles-dodgers.php LAD - SP)  : team/position regex --> \(\/mlb\/teams.*?\)
        #t = re.search('fp-id-.*?\"',df_p['Player'][0]).group(0)
        df['PlayerID'] = df['Player'].str.extract('(fp-id-.*?\")')  #parentheses define a capture group
        df['PlayerID'] = df['PlayerID'].str.replace('fp-id-','')
        df['PlayerID'] = df['PlayerID'].str.replace('"','')

        df['PlayerName'] = df['Player'].str.extract('(fp-player-name=.*)')  #parentheses define a capture group
        df['PlayerName'] = df['PlayerName'].str.replace('fp-player-name="','')

        df['PlayerTeam'] = df['Player'].str.extract('(\(\/mlb\/teams.*?\))')  #parentheses define a capture group
        df['PlayerTeam'] = df['PlayerTeam'].str.replace('\/mlb\/teams.*?.php','')
        df['PlayerTeam'] = df['PlayerTeam'].str.split('-')
        df['PlayerPos'] = df['PlayerTeam'].apply(lambda x: x[1].replace(')','').strip() if not isNumber(x) else None)
        df['PlayerTeam'] = df['PlayerTeam'].apply(lambda x: x[0].replace('(','').strip() if not isNumber(x) else None)

        df['PlayerLink'] = df['Player'].str.extract('(\/mlb\/projections.*?.php)')  #parentheses define a capture group

        return df

    def _mlbGetPlayers(self):
        """Gets list of players and their IDs from the concensus page"""

        #get data from web
        pg_resp_pitchers = urlopen('https://www.fantasypros.com/mlb/projections/pitchers.php')
        html_pitchers = pg_resp_pitchers.read()
        pg_resp_hitters = urlopen('https://www.fantasypros.com/mlb/projections/hitters.php')
        html_hitters = pg_resp_hitters.read()

        #pull links out from href tags
        cleaned_pitchers = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', str(html_pitchers))
        cleaned_hitters = re.sub('<a.*?href="(.*?)">(.*?)</a>', '\\1 \\2', str(html_hitters))

        #parse into pandas dataframes
        #Players column will be like '/mlb/projections/clayton-kershaw.php Clayton Kershaw (/mlb/teams/los-angeles-dodgers.php LAD - SP) #" class="fp-player-link fp-id-3215" fp-player-name="Clayton Kershaw'
        df_pitchers_parse = pd.read_html(cleaned_pitchers)
        df_pitchers = df_pitchers_parse[0]
        df_hitters_parse = pd.read_html(cleaned_hitters)
        df_hitters = df_hitters_parse[0]

        #clean dataframe player information
        df_pitchers = self._mlbCleanPlayerInfo(df_pitchers)
        df_hitters = self._mlbCleanPlayerInfo(df_hitters)

        #store
        self.players = {}
        self.players['hitters'] = df_hitters
        self.players['pitchers'] = df_pitchers

if __name__ == "__main__":
    print('import module to access classes for the FantasyPros.com scrapers.')