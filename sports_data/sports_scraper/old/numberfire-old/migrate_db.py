import data_layer as dl
import data_layer_old as dlold

dbConString_sqlite = 'sqlite:///scraped.db'
db_sqlite = dlold.DatabaseConnection(dbConString_sqlite)

dbConString_postgres = 'postgresql://python_rw:pylogin89@192.168.1.119:5432/sportsdata'
db_postgres = dl.DatabaseConnection(dbConString_postgres)

print('migrating players')

# migrate player table
for plyr in db_sqlite.session.query(dlold.NumberfirePlayers).all():
    new_p = dl.NumberfirePlayers(player_id=plyr.player_id,
                                 nf_id=plyr.nf_id,
                                 first_name=plyr.first_name,
                                 middle_name=plyr.middle_name,
                                 last_name=plyr.last_name,
                                 pos=plyr.pos,
                                 team=plyr.team)

    db_postgres.session.add(new_p)

db_postgres.session.commit()

print('migrating projections')

# migrate projection table
for stat in db_sqlite.session.query(dlold.NumberfireProjections).all():
    new_s = dl.NumberfireProjections(   player_id = stat.player_id,
                                        timestamp = stat.timestamp,
                                        week_start = stat.week_start,
                                        week_end = stat.week_end,
                                        p_g = stat.p_g,
                                        p_gs = stat.p_gs,
                                        p_ip = stat.p_ip,
                                        p_w = stat.p_w,
                                        p_l = stat.p_l,
                                        p_sv = stat.p_sv,
                                        p_k = stat.p_k,
                                        p_bb = stat.p_bb,
                                        p_er = stat.p_er,
                                        p_era = stat.p_era,
                                        p_whip = stat.p_whip,
                                        h_pa = stat.h_pa,
                                        h_r = stat.h_r,
                                        h_bb = stat.h_bb,
                                        h_k = stat.h_k,
                                        h_1b = stat.h_1b,
                                        h_2b = stat.h_2b,
                                        h_3b = stat.h_3b,
                                        h_hr = stat.h_hr,
                                        h_rbi = stat.h_rbi,
                                        h_sb = stat.h_sb,
                                        h_cs = stat.h_cs,
                                        h_avg = stat.h_avg,
                                        h_obp = stat.h_obp,
                                        h_slg = stat.h_slg)
    db_postgres.session.add(new_s)

db_postgres.session.commit()

db_sqlite.close()
db_postgres.close()
