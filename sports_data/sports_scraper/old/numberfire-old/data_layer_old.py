'''
Data model for numberfire scraping.
'''

from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey
from sqlalchemy.orm import Session, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import create_engine, MetaData

BASE = declarative_base()

class NumberfireProjections(BASE):
    '''Class to hold projection information.'''

    __tablename__ = 'numberfire_projections'

    player_id = Column(Integer, ForeignKey('numberfire_players.player_id'), primary_key=True)
    timestamp = Column(DateTime, primary_key=True)
    week_start = Column(DateTime)
    week_end = Column(DateTime)
    player = relationship('NumberfirePlayers', back_populates='projections')
    p_g = Column(Float)
    p_gs = Column(Float)
    p_ip = Column(Float)
    p_w = Column(Float)
    p_l = Column(Float)
    p_sv = Column(Float)
    p_k = Column(Float)
    p_bb = Column(Float)
    p_er = Column(Float)
    p_era = Column(Float)
    p_whip = Column(Float)
    h_pa = Column(Float)
    h_r = Column(Float)
    h_bb = Column(Float)
    h_k = Column(Float)
    h_1b = Column(Float)
    h_2b = Column(Float)
    h_3b = Column(Float)
    h_hr = Column(Float)
    h_rbi = Column(Float)
    h_sb = Column(Float)
    h_cs = Column(Float)
    h_avg = Column(Float)
    h_obp = Column(Float)
    h_slg = Column(Float)

class NumberfirePlayers(BASE):
    '''Class to hold player information.'''

    __tablename__ = 'numberfire_players'

    player_id = Column(Integer, primary_key=True, autoincrement=True)
    nf_id = Column(String(255), unique=True)
    first_name = Column(String(255))
    middle_name = Column(String(255))
    last_name = Column(String(255))
    pos = Column(String(255))
    team = Column(String(255))

    projections = relationship('NumberfireProjections', back_populates='player')

    @hybrid_property
    def fullname(self):
        """Assembles the player's full name from their first and last names."""

        return '{0} {1}'.format(self.first_name, self.last_name)

    # @hybrid_property
    # def positions(self):
    #     '''Creates a list of all positions the player plays.'''

    #     return [str(x.pos) for x in self.pos]

class DatabaseConnection(object):
    '''
    Class serves as the main interface to connect and interact with the storage database.
    '''

    def __init__(self, conString):
        '''Constructor.'''

        self._engine = create_engine(conString)
        self.meta = BASE.metadata

        self.meta.create_all(self._engine)
        self.session = Session(bind=self._engine)

    def close(self):
        '''Clean up database connection.'''

        self.session.close()

def main():
    '''Main script.'''
    print('import module to access classes for the Numberfire data layer.')
    #testdb = DatabaseConnection('sqlite:///testdb.db')

if __name__ == "__main__":
    main()
