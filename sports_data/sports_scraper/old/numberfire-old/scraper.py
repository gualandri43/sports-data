'''
Numberfire scraper logic.

example table code snippets. data is split into a data index with player data, and
a projection table that references the data index for player names.

player data index
------------------
'class' = 'projection-table--fixed'

     <tr data-row-index="1">
        <td class="player">
            <a href="/mlb/players/carson-fulmer">
                <span class="full">Carson Fulmer</span>
                <span class="abbrev">C. Fulmer</span>
            </a> (RP, CHW)
        </td>
    </tr>

projection data
---------------
'class' = 'no-fix'

    <tr data-row-index="1">
        <td class="g">
            3                        </td>
        <td class="gs">
            3.00                        </td>
        <td class="ip active">
            16.00                        </td>
        <td class="w">
            0.77                        </td>
        <td class="l">
            1.29                        </td>
        <td class="sv">
            0.00                        </td>
        <td class="k">
            13.33                        </td>
        <td class="bb">
            7.05                        </td>
        <td class="er">
            9.92                        </td>
        <td class="era">
            5.58                        </td>
        <td class="whip">
            1.48                        </td>
    </tr>

'''

import argparse
import requests
from bs4 import BeautifulSoup
import datetime as dt
import re
import sports_data.sports_scraper.numberfire.data_layer as dl

class ScrapedPlayerData:
    def __init__(self, id_url, display_name):
        self.nf_id = str(id_url).split('/')[-1]
        self.first_name = display_name.split(' ')[0]
        self.last_name = ' '.join(display_name.split(' ')[1:])

def _get_projection_stat(data_row, stat):

    val = data_row.find('td', {'class': stat})

    if val is None:
        return None
    else:
        return float(val.text.strip())

def _parse_nf_weekly_projections(pageText, positionType, dbConString):
    '''Parses a set of weekly numberfire projections.

    Args:
        pageText            html of the page to scrape. can be a string or file handle.
        positionType        "pitcher", "hitter"
        dbConString         connection string to database for storage
    '''

    #record download time
    dl_time = dt.datetime.now()

    isPitcher = False
    isHitter = False

    if positionType == 'pitcher':
        isPitcher = True

    if positionType == 'hitter':
        isHitter = True

    db = dl.DatabaseConnection(dbConString)
    soup = BeautifulSoup(pageText, 'lxml')

    #parse and store player information and their indices
    player_data_table = soup.find('table', {'class':'projection-table--fixed'})
    player_data_table_body = player_data_table.find('tbody')

    dataRowIdx = {}
    for row in player_data_table_body.find_all('tr'):
        for col in row.find_all('td'):
            dataRowIdx[row['data-row-index']] = ScrapedPlayerData(col.find('a', href=True)['href'],
                                                                  col.find('span', {'class':'full'}).text)


    #find week start and end dates
    #parse into python datetime objects
    week_elem = soup.find('span', {'class':'projection-rankings--date'})
    wk_start = None
    wk_end = None
    if not week_elem is None:
        dates = re.search('From (.*\d).* to (.*\d).*', week_elem.text)
        wk_start = dt.datetime.strptime(dates.group(1), '%A, %B %d')
        wk_end = dt.datetime.strptime(dates.group(2), '%A, %B %d')

        wk_start = wk_start.replace(year=dt.datetime.now().year)
        wk_end = wk_end.replace(year=dt.datetime.now().year)


    print('Getting projections for {0} through {1}.'.format(wk_start.strftime('%Y-%m-%d'), wk_end.strftime('%Y-%m-%d')))

    #parse projection data and add to database
    projection_table = soup.find('table', {'class':'no-fix'})
    projection_table_body = projection_table.find('tbody')

    for row in projection_table_body.find_all('tr'):
        proj = dl.NumberfireProjections()

        plyr_data = dataRowIdx[row['data-row-index']]

        #look for player id info existing in database. Create if not found.
        plyr = db.session.query(dl.NumberfirePlayers).filter_by(nf_id= plyr_data.nf_id).first()

        if plyr is None:
            plyr = dl.NumberfirePlayers()
            plyr.nf_id = plyr_data.nf_id
            plyr.first_name = plyr_data.first_name
            plyr.last_name = plyr_data.last_name
            db.session.add(plyr)
            db.session.flush()      #flush to get assigned id number

        #build projection data
        proj.player_id = plyr.player_id
        proj.timestamp = dl_time
        proj.week_start = wk_start
        proj.week_end = wk_end

        if isPitcher:
            proj.p_g = _get_projection_stat(row, 'g')
            proj.p_gs = _get_projection_stat(row, 'gs')
            proj.p_ip = _get_projection_stat(row, 'ip active')
            proj.p_w = _get_projection_stat(row, 'w')
            proj.p_l = _get_projection_stat(row, 'l')
            proj.p_sv = _get_projection_stat(row, 'sv')
            proj.p_k = _get_projection_stat(row, 'k')
            proj.p_bb = _get_projection_stat(row, 'bb')
            proj.p_er = _get_projection_stat(row, 'er')
            proj.p_era = _get_projection_stat(row, 'era')
            proj.p_whip = _get_projection_stat(row, 'whip')

        if isHitter:
            proj.h_pa = _get_projection_stat(row, 'pa active')
            proj.h_r = _get_projection_stat(row, 'r')
            proj.h_bb = _get_projection_stat(row, 'bb')
            proj.h_k = _get_projection_stat(row, 'k')
            proj.h_2b = _get_projection_stat(row, 'h2b')
            proj.h_3b = _get_projection_stat(row, 'h3b')
            proj.h_hr = _get_projection_stat(row, 'hr')
            proj.h_1b = _get_projection_stat(row, 'h') - (proj.h_2b + proj.h_3b + proj.h_hr)
            proj.h_rbi = _get_projection_stat(row, 'rbi')
            proj.h_sb = _get_projection_stat(row, 'sb')
            proj.h_cs = _get_projection_stat(row, 'cs')
            proj.h_avg = _get_projection_stat(row, 'avg')
            proj.h_ops = _get_projection_stat(row, 'ops')
            proj.h_obp = _get_projection_stat(row, 'obp')
            proj.h_slg = _get_projection_stat(row, 'slg')

        db.session.add(proj)

    db.session.commit()
    db.close()

def scrape_weekly(dbConString = 'sqlite:///nf_projs.db'):
    '''scrape weekly baseball projections from numberfire'''

    print('Downloading pitcher projections.')
    urlPitcher = 'https://www.numberfire.com/mlb/fantasy/weekly-projections/pitchers'
    reqPitcher = requests.get(urlPitcher)
    _parse_nf_weekly_projections(reqPitcher.text, 'pitcher', dbConString)

    print('Downloading hitter projections.')
    urlHitter = 'https://www.numberfire.com/mlb/fantasy/weekly-projections/batters'
    reqHitter = requests.get(urlHitter)
    _parse_nf_weekly_projections(reqHitter.text, 'hitter', dbConString)


    #with open('testdata/www.numberfire.com_mlb_fantasy_weekly-projections_batters.html','r') as f:



def main():
    '''Main script.'''
    #print('import module to access classes and functions for Numberfire data scraping.')

    parser = argparse.ArgumentParser(description='Scrape data from numberfire.com')

    parser.add_argument('--weekly-mlb', action='store_true',
                                        help='Download weekly mlb projections.')

    parser.add_argument('--db-con-string', action='store',
                                           help='Define the connection string to the database to save data to.')

    args = parser.parse_args()

    if args.weekly_mlb:
        if args.db_con_string is None:
            scrape_weekly()
        else:
            scrape_weekly(dbConString=args.db_con_string)



if __name__ == "__main__":
    main()
