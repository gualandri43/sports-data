'''
Scraping data and routines for fangraphs.com
'''

#
# TODO: can use pybaseball library to download fangraphs stats for players (not projections)
#

import datetime as dt
import re
import io
import json
import csv
from typing import Dict, Iterable, List
import requests
import bs4

from sports_data.data_model.schema import MlbBatting, MlbPitching, MlbProjection, MlbPlayerGamelog, MlbPlayerSeason, MlbFangraphsGuts
from sports_data.utils import TimeRecorder


class FangraphsScraperBase:
    '''
    Class containing methods for downloading
    '''

    def _create_batting_stat_from_dict(self, data: dict) -> MlbBatting:
        '''
            Create a MlbBatting stat instance from the data dictionary.
        '''

        s = dict(data)

        stats = MlbBatting(
            custom_data = dict(),
            game = s.pop('G', None),
            plate_appearance = s.pop('PA', None),
            at_bat = s.pop('AB', None),
            hit = s.pop('H', None),
            single = s.pop('1B', None),
            double = s.pop('2B', None),
            triple = s.pop('3B', None),
            homerun = s.pop('HR', None),
            hit_by_pitch = s.pop('HBP', None),
            intentional_walk = s.pop('IBB', None),
            run = s.pop('R', None),
            rbi = s.pop('RBI', None),
            walk = s.pop('BB', None),
            strikeout = s.pop('SO', None),
            stolen_base = s.pop('SB', None),
            caught_stealing = s.pop('CS', None),
            average = s.pop('AVG', None),
            obp = s.pop('OBP', None),
            slg = s.pop('SLG', None),
            ops = s.pop('OPS', None),
            woba = s.pop('wOBA', None),
            wrc_plus = s.pop('wRC+', None)
        )

        stats.custom_data.update(s)

        return stats

    def _create_pitching_stat_from_dict(self, data: dict) -> MlbPitching:
        '''
            Create a MlbPitching stat instance from the data dictionary.
        '''
        s = dict(data)

        stats = MlbPitching(
            custom_data = dict(),
            complete_game = s.pop('CG', None),
            blown_sv = s.pop('BS', None),
            er = s.pop('ER', None),
            era = s.pop('ERA', None),
            fip = s.pop('FIP', None),
            game = s.pop('G', None),
            game_started = s.pop('GS', None),
            hit = s.pop('H', None),
            hit_by_pitch = s.pop('HBP', None),
            hold = s.pop('HLD', None),
            homerun = s.pop('HR', None),
            intentional_walk = s.pop('IBB', None),
            ip = s.pop('IP', None),
            loss = s.pop('L', None),
            quality_start = s.pop('QS', None),
            shoutout = s.pop('SHO', None),
            single = s.pop('1B', None),
            strikeout = s.pop('SO', None),
            strikeout_per_nine = s.pop('K/9', None),
            sv = s.pop('SV', None),
            total_batters_faced = s.pop('TBF', None),
            walk = s.pop('BB', None),
            walk_per_nine = s.pop('BB/9', None),
            war = s.pop('WAR', None),
            whip = s.pop('WHIP', None),
            win = s.pop('W', None)
        )

        stats.custom_data.update(s)

        return stats

    def _parse_player_stat_html_table(self, tableElement: bs4.element.PageElement) -> Iterable[dict]:
        '''Parse out a player stat page html table element into a list of
            dictionary records.

            Headers in return dictionary are named after the <td> attribute "data-stat".

            Parameters
            ----------
            tableElement
                The HTML table loaded into a BeautifulSoup element.
        '''

        recs = list()

        for dataRow in tableElement.find('tbody').find_all('tr'):
            #
            # data rows are tr with entries of td.
            #

            rec = dict()

            for dataCol in dataRow.find_all('td'):
                if 'data-stat' in dataCol.attrs:
                    hdr = dataCol['data-stat'].strip()
                    if hdr.lower() != 'divider':
                        rec[hdr] = dataCol.get_text()

            recs.append(rec)

        return recs

    def _parse_csv(self, csvText: str, colsToDelete: List[str] = None) -> Iterable[dict]:
        '''Parse the csv text into dictionary records.

            Parameters
            ----------
            csvText
                The CSV table as text.

            colsToDelete
                List of column names to remove

            Yields
            ------
            A dictionary for each row of the CSV text.
        '''

        reader = csv.DictReader(io.StringIO(csvText))

        for row in reader:
            data = dict(row)

            if colsToDelete:
                for _col in colsToDelete:
                    data.pop(_col, None)

            yield data

    def build_csv_download_request(self, pageGetResponse: requests.Response) -> requests.Request:
        '''Builds a CSV download POST request from the response of a GET request to the download page.

            Parameters
            ----------
            pageGetResponse
                The response from a GET request to the projection page.


            The projections page uses postback methods to perform actions on the
            page. can either do pagination and html scrape, or post the event method
            that returns a CSV response
                - using CSV response is trickier to set up but way more robust

            parse method extracts all necessary info from a normal GET request and then
            schedules a POST request that will retrieve the CSV file

            method
            ----------
            first GET the url for the projections
            parse GET response for the state variables
            __EVENTTARGET: get from the postback event link for the "export data" link ( javascript:__doPostBack('ProjectionBoard1$cmdCSV','') )
            __EVENTARGUMENT: "" (blank)
            __VIEWSTATE:  get from response
            __VIEWSTATEGENERATOR: get from response
            __SCROLLPOSITIONX: "0" (just set to zero)
            __SCROLLPOSITIONY: "0" (just set to zero)
            __EVENTVALIDATION: get from response

            then POST request to same url using the above state values and the __EVENTTARGET
            this gets CSV string.

        '''

        getRespXML = bs4.BeautifulSoup(pageGetResponse.text, 'lxml')

        # xpath method returns a list, we just want the first result for all of these
        #   get() method gets the value of an element attribute.
        #
        targetLink = getRespXML.find('a', string="Export Data")['href']
        eventTarget = re.search(r"\('(.*)',.*\)", targetLink).group(1)
        viewState = getRespXML.find(id="__VIEWSTATE")['value']
        viewStateGenerator =  getRespXML.find(id="__VIEWSTATEGENERATOR")['value']
        eventValidation = getRespXML.find(id="__EVENTVALIDATION")['value']

        #build form payload data from extracted info
        payload = {
            "__EVENTTARGET": eventTarget,
            "__EVENTARGUMENT": "",
            "__VIEWSTATE": viewState,
            "__VIEWSTATEGENERATOR": viewStateGenerator,
            "__SCROLLPOSITIONX": "0",
            "__SCROLLPOSITIONY": "0",
            "__EVENTVALIDATION": eventValidation,
        }

        # enable compression
        headers = {
            "Accept-Encoding": "gzip, deflate, br"
        }

        # The CSV data can be downloaded by a POST request to the
        # same page url, using the payload data constructed above.
        # The response will be CSV text.
        #
        return requests.Request('POST', pageGetResponse.url, headers=headers, data=payload)

class FangraphsScraperHistorical(FangraphsScraperBase):
    '''
    Class containing methods for downloading and parsing historical stats.
    '''

    def __init__(self):
        super().__init__()

    def parse_mlb_player_season_stats(self, pg: bs4.BeautifulSoup, playerId: str) -> Iterable[MlbPlayerSeason]:
        '''Parse a player's seasonal stat page

            To retrieve the full stats, the request needs to be
            executed in an environment that can render javascript. Running a
            selenium instance in Docker is the simplest way, or a splash server.

            Parameters
            ----------
            page
                The MLB player page, loaded into a BeautifulSoup object.

            playerId
                The ID of the player.

                https://www.fangraphs.com/players/mike-trout/10155/stats
                https://www.fangraphs.com/statss.aspx?playerid=13510

            Yields
            ------
            This method is a generator, yielding a MlbPlayerSeason object for each season stat row.

        '''

        position = pg.find('div', class_="player-info-box-pos").text
        playerName = pg.find('div', class_='player-info-box-name').text

        allStats = dict()
        battingStats = dict()
        pitchingStats = dict()
        for _tblDiv in pg.find_all('div', class_='player-page-table'):

            # loop through every stat table
            for _nestedDiv in _tblDiv.find_all('div', class_='table-fixed'):

                _tbl = _nestedDiv.find('table')

                # each row of the table is a set of stats for a season
                for _seasonStats in self._parse_player_stat_html_table(_tbl):
                    level = _seasonStats.pop('Level',None)
                    season = _seasonStats.pop('Season',None)

                    # only process MLB regular season records
                    if level == 'MLB' and season.lower() not in ['total', 'postseason']:

                        if (level,season) not in allStats:

                            allStats[(level,season)] = MlbPlayerSeason(
                                custom_data = {
                                    'player_id': playerId,
                                    'player_name': playerName,
                                    'player_position': position,
                                },
                                season_year = season,
                                source_site = 'fangraphs'
                            )

                            battingStats[(level,season)] = dict()
                            pitchingStats[(level,season)] = dict()

                        if position.upper() in ['SP', 'RP', 'P']:
                            pitchingStats[(level,season)].update(_seasonStats)
                        else:
                            battingStats[(level,season)].update(_seasonStats)

        # make into set of unified objects
        for (_level,_season) in allStats:
            if position.upper() in ['SP', 'RP', 'P']:
                allStats[(_level,_season)].pitching_stats = self._create_pitching_stat_from_dict(pitchingStats[(_level,_season)])
            else:
                allStats[(_level,_season)].batting_stats = self._create_batting_stat_from_dict(battingStats[(_level,_season)])

            yield allStats[(_level,_season)]

    def parse_mlb_player_gamelog_stats(self, apiResponse: str, positionType: str) -> Iterable[MlbPlayerGamelog]:
        '''Parses the JSON api response for a player's gamelog.

            Parameters
            ----------
            apiResponse
                The text response from the API call.

            positionType : {batter,pitcher}
                The position category.

            Yields
            ------
            This is a generator that yields a MlbPlayerGamelog per record.
        '''

        data = json.loads(apiResponse)

        for _game in data['mlb']:

            _gameData = dict(_game)

            gamelog = MlbPlayerGamelog(
                custom_data = {
                    'player_id': _gameData.pop('playerid',None),
                    'player_name': _gameData.pop('PlayerName',None),
                    'dh': _gameData.pop('dh', None),
                    'team': _gameData.pop('Team', None),
                    'opponent': _gameData.pop('Opp', None)
                },
                season_year = _gameData.pop('season',None),
                source_site = 'fangraphs',
                game_date = dt.datetime.strptime(_gameData.pop('gamedate', None), '%Y-%m-%d').date()
            )

            if positionType == 'pitcher':
                gamelog.pitching_stats = self._create_pitching_stat_from_dict(_gameData)

            if positionType == 'batter':
                gamelog.batting_stats = self._create_batting_stat_from_dict(_gameData)

            yield gamelog

    def build_mlb_player_season_stat_request(self, fangraphsId: str) -> requests.Request:
        '''Builds a request to download the fangraphs player stat page.

            To retrieve the full stats, the request needs to be
            executed in an environment that can render javascript. This can
            be done using playwright or by running a selenium instance in Docker.

            Parameters
            ----------
            fangraphsId
                The ID of the player whose stats are to be retrieved.
        '''

        return requests.Request(
            method='GET',
            url='https://www.fangraphs.com/statss.aspx',
            params={
                'playerid': fangraphsId
            }
        )

    def build_mlb_player_gamelog_stat_request(self, fangraphsId: str, season: int, playerPageResponse: requests.Response) -> requests.Request:
        '''Builds a request to download the fangraphs player gamelog stat page.

            This method can call the backend API directly for the data. Don't need the javascript renderer
            for this, or the player page request.

            Parameters
            ----------
            fangraphsId
                The player ID

            season
                The season year to download.

            playerPageResponse
                Use the data in the player stat page to find info needed for
                the gamelog API query (such as the position).
                can use build_mlb_player_season_stat_request() to build the request.

        '''

        # example URL for Alex Colome
        #  https://cdn.fangraphs.com/api/players/game-log?playerid=6661&position=P&type=1&season=2019
        #

        # first get the players position from a prior player page query for the API query
        pg = bs4.BeautifulSoup(playerPageResponse.text, 'lxml')
        position = pg.find('div', class_="player-info-box-pos").text

        # build the gamelog request
        return requests.Request(
            method='GET',
            url='https://cdn.fangraphs.com/api/players/game-log',
            params={
                'playerid': fangraphsId,
                'type': 1,
                'season': season,
                'position': position
            }
        )

class FangraphsScraperProjections(FangraphsScraperBase):
    '''
        Class containing methods for downloading and parsing projections

        Parameters
        ----------
        source : {'zips','steamer','steamer600','atc','theBat','theBatX','depthCharts'}
            The projection source to download.

        scope : {'season','restOfSeason','update','daily'}
            The scope of the projections to download.
    '''

    projectionSources = [
        'zips',
        'steamer',
        'steamer600',
        'atc',
        'theBat',
        'theBatX',
        'depthCharts'
    ]

    projectionScopes = [
        'season',
        'restOfSeason',
        'update',
        'daily'
    ]

    def __init__(self, source, scope):

        super().__init__()

        if source not in self.projectionSources:
            raise ValueError('Invalid projection source.')

        if scope not in self.projectionScopes:
            raise ValueError('Invalid projection scope.')

        self.source = source
        self.scope = scope

    def _parse_projection_date(self, projectionPage: bs4.BeautifulSoup) -> Dict[str,dt.datetime]:
        '''Parse the projection date from a fangraphs page title.
            the season long will just be a year, daily will be a date.

            Parameters
            ----------
            projectionPage
                The projection page loaded into a BeautifulSoup object.

            Returns
            -------
            dict
                A dictionary with keys: seasonYear,projectionDate
        '''

        # projections date and season from page title
        #
        titleText = projectionPage.find(id='Head1').find('title').text
        dateInfo = re.search(r'(.*) Projections', titleText.strip())

        if re.match(r"\d*/\d*/\d\d\d\d", dateInfo.group(1)):
            # matches date in m/d/yyyy format

            titleDate = dt.datetime.strptime(dateInfo.group(1), '%m/%d/%Y')

            return {
                'seasonYear': titleDate.year,
                'projectionDate': titleDate
            }

        elif re.match(r"\d\d\d\d", dateInfo.group(1)):
            # just match year

            titleDate = dt.datetime.strptime(dateInfo.group(1), '%Y')

            return {
                'seasonYear': titleDate.year,
                'projectionDate': None
            }

        else:
            return {
                'seasonYear': None,
                'projectionDate': None
            }

    def _parse_html_table(self, table: bs4.element.PageElement, colsToDelete: List[str] = None) -> Iterable[dict]:
        '''Parse a HTML table

            Parameters
            ----------
            table
                A BeautifulSoup element that represents the table element to parse.

            Yields
            ------
            This method is a generator. Yields a dict for each row of the table. The keys are
            the headers.
        '''

        #parse header
        headers = []
        for headerRow in table.find('thead').find_all('tr'):

            # there is a header row (tr) that has the
            #   pager. class is rgPager.
            #
            # normal header row has no class
            #

            if headerRow.get('class') != 'rgPager':
                for headerCol in headerRow.find_all('th'):
                    headers.append(headerCol.find('a').text)


        for dataRow in table.find('tbody').find_all('tr'):
            #
            # data rows are tr with entries of td. same order as header row
            #   players have player ID numbers imbedded in a href link. text is in
            #   a <a> tag. if no link, text is in <td> tag.
            #

            colCount = 0
            record = dict()
            for dataCol in dataRow.find_all('td'):

                hdr = headers[colCount]
                if hdr is not None and len(hdr) > 0:

                    aTag = dataCol.find('a')

                    if aTag is not None:
                        record[hdr] = aTag.text
                    else:
                        record[hdr] = dataCol.text

                if hdr == 'Name':
                    # extract player id from link as well as name
                    #   https://www.fangraphs.com/statss.aspx?playerid=11477&position=OF
                    #

                    aTag = dataCol.find('a')

                    if aTag is not None:
                        m = re.search(r'playerid=(.*)\&', aTag['href'])
                        record['player_id'] = m[1]

                colCount += 1

            # remove any columns specified for deletion
            if colsToDelete:
                for _col in colsToDelete:
                    record.pop(_col, None)

            # add to result list
            yield record

    def parse_mlb_projections_csv(self, pgResponse: requests.Response, csvResponse: requests.Response, position: str) -> Iterable[MlbProjection]:
        '''Parse mlb batting projections from downloaded fangraphs csv text.

            Parameters
            ----------
            pgResponse
                The response from the page GET request

            csvResponse
                The response from the POST request for the CSV data.

            position : {batter,pitcher}
                The position type.

            Yields
            ------
            MlbProjection
                This function is a generator yielding an object for each projection record.

        '''

        if position not in ['batter', 'pitcher']:
            raise ValueError('Not a valid position choice.')

        scrapeTime = TimeRecorder()
        projPage = bs4.BeautifulSoup(pgResponse.text, 'lxml')
        projDates = self._parse_projection_date(projPage)

        for _record in self._parse_csv(csvResponse.text, colsToDelete= ['-1', '-1.1', '-1.2', '-1.3', 'ADP']):

            proj = MlbProjection(
                custom_data = dict(),
                source_site = self.source,
                scope = self.scope,
                scrape_time_utc = scrapeTime.timeUtc,
                scrape_time_local = scrapeTime.timeLocal,
                projection_date = projDates['projectionDate'],
                season_year = projDates['seasonYear']
            )

            proj.custom_data['playerId'] = _record.pop('playerid', None)

            if position == 'batter':
                proj.batting_stats = self._create_batting_stat_from_dict(_record)

            if position == 'pitcher':
                proj.pitching_stats = self._create_batting_stat_from_dict(_record)

            yield proj

    def build_mlb_projections_download_request(self, position: str) -> requests.Request:
        '''Build requests.Request objects for the given projections source and scope.
            The source and scope are defined in the contructor

            Parameters
            ----------
            position : {batter,pitcher}
                The position type.

        returns dict of 2 requests, with keys {batters: req, pitchers: req}
        '''

        srcMap = {
            ('zips', 'season'): 'zips',
            ('zips', 'restOfSeason'): 'rzips',
            ('zips', 'update'): 'uzips',
            ('steamer', 'season'): 'steamer',
            ('steamer', 'restOfSeason'): 'steamerr',
            ('steamer', 'update'): 'steameru',
            ('steamer600', 'season'): 'steamer600',
            ('steamer600', 'update'): 'steamer600u',
            ('depthCharts', 'season'): 'fangraphsdc',
            ('depthCharts', 'restOfSeason'): 'rfangraphsdc',
            ('atc', 'season'): 'atc',
            ('theBat', 'season'): 'thebat',
            ('theBat', 'restOfSeason'): 'rthebat',
            ('theBatX', 'season'): 'thebatx',
            ('theBatX', 'restOfSeason'): 'rthebatx',
            #('saberSim', 'daily'): 'sabersim'
        }

        if position not in ['batter','pitcher']:
            raise ValueError('invalid position')

        if self.scope == 'daily':
            baseURL = 'https://www.fangraphs.com/dailyprojections.aspx'
        else:
            baseURL = 'https://www.fangraphs.com/projections.aspx'

        if (self.source, self.scope) not in srcMap:
            raise ValueError("Invalid source,scope combo: {0},{1}".format(self.source, self.scope))

        if position == 'batter':
            return requests.Request(
                'GET',
                baseURL,
                params={
                    'type': srcMap.get((self.source, self.scope)),
                    'pos': 'all',
                    'stats': 'bat',
                    'team': 0,
                    'lg': 'all',
                    'players': 0
                }
            )

        if position == 'pitcher':
            return requests.Request(
                'GET',
                baseURL,
                params={
                    'type': srcMap.get((self.source, self.scope)),
                    'pos': 'all',
                    'stats': 'pit',
                    'team': 0,
                    'lg': 'all',
                    'players': 0
                }
            )

class FangraphsScraperGuts(FangraphsScraperBase):
    '''
        Fetch constants, park factors, etc. from Fangraphs.com
    '''

    def build_mlb_woba_fip_constants_request(self) -> requests.Request:
        '''Build a request to get the GUTS fangraphs page with the wOBA and FIP constants for each season.
            This request gets the page, then build_csv_download_request() must be used to download data.

            Returns
            -------
            The request object that will retrieve the desired web page.
        '''

        return requests.Request(
            method='GET',
            url='https://www.fangraphs.com/guts.aspx',
            params={'type': 'cn'}
        )

    def parse_mlb_woba_fip_constants(self, csvResponse: requests.Response) -> MlbFangraphsGuts:
        '''Parse the csv response from the wOBA and FIP constants.

            Parameters
            ----------
            csvResponse
                The response from the CSV POST request that retrieves the constants.

            Yields
            ------
            A MlbFangraphsGuts instance for each parsed record.
        '''

        # the text string comes in with a BOM \ufeff, decode it to a
        #   string to remove
        #   https://stackoverflow.com/questions/17912307/u-ufeff-in-python-string
        #
        csvText = csvResponse.content.decode('utf-8-sig')
        constants = self._parse_csv(csvText)

        for _rec in constants:
            yield MlbFangraphsGuts(
                season_year=_rec.get('Season', None),
                woba=_rec.get('wOBA', None),
                woba_scale=_rec.get('wOBAScale', None),
                w_bb=_rec.get('wBB', None),
                w_hbp=_rec.get('wHBP', None),
                w_1b=_rec.get('w1B', None),
                w_2b=_rec.get('w2B', None),
                w_3b=_rec.get('w3B', None),
                w_hr=_rec.get('wHR', None),
                run_sb=_rec.get('runSB', None),
                run_cs=_rec.get('runCS', None),
                r_per_pa=_rec.get('R/PA', None),
                r_per_w=_rec.get('R/W', None),
                c_fip=_rec.get('cFIP', None)
            )

