'''
Module for scraping https://www.fantasycruncher.com
'''

from typing import Iterable, List
import requests

from sports_data.data_model.schema import NflDfsContest
import sports_data.sports_scraper.fanduel as fanduel
import sports_data.sports_scraper.draftkings as draftkings

class FantasyCruncherScraper:
    '''Scrape data from https://www.fantasycruncher.com'''

    def build_dfs_contests_requests(self, league: str, period: str, sites: List[str]) -> Iterable[requests.Request]:
        '''Builds the requests needed to query the list of DFS contest links for a given period.
            The query targets the internal API endpoint that the website used to get data to display.
            The response is a JSON string.

            https://www.fantasycruncher.com/contest-links/NFL/2021-week-12

            Parameters
            ----------
            league : {nfl,mlb}
                The league to query

            period
                The period to query contests for. Of the format <year>-week-<week number> for nfl contests.
                Examples: 2021-week-10, 2020-week-2

            sites : {draftkings,draftkings_pickem,draftkings_showdown,fanduel,fanduel_single,fanduel_super,fantasydraft,yahoo,superdraft}
                A list of the DFS sites to query contests from

            Yields
            ------
            requests.Request
                A series of requests to execute to retrieve the data. They must be executed sequentially in the same session.
        '''

        if not isinstance(sites, list):
            raise ValueError('sites must be a list of strings with valid site options.')

        yield requests.Request(
            method='GET',
            url='https://www.fantasycruncher.com/contest-links/{0}/{1}'.format(league.upper(), period)
        )

        yield requests.Request(
            method='POST',
            url='https://www.fantasycruncher.com/funcs/tournament-analyzer/get-contests.php',
            data={
                'sites[]': sites,
                'leagues[]': league.upper(),
                'periods[]': period
            }
        )

    def parse_dfs_contests_response_nfl(self, resp: requests.Response) -> Iterable[NflDfsContest]:
        '''Parse the JSON response of the dfs contests request for NFL contests.

            Parameters
            ----------
            resp
                The response of the web request.

            Yields
            ------
            NflDfsContest
                A populated NflDfsContest object for each contest record.
        '''

        # example response
        #   {'id': 863219979, 'site': 'draftkings', 'league': 'NFL', 'slate': 59524, 'site_id': 118954760, 'name': 'NFL $1.11M Play-Action [$100K to 1st, 20 Entry Max]', 'period': '2021-week-12', 'max_entries': 20, 'max_entrants': 440392, 'cost': 3, 'prizepool': 1111111, 'places_paid': 108622, 'total_entrants': 440392, 'winning_score': 232.28, 'mincash_score': 127.98, 'startdate': 1638122400, 'winning_payout': 100000, 'mincash_payout': 5, 'DateTime': '2021-11-28 13:00:00', 'Title': '', 'game_cnt': 10, 'winner_cnt': 1, 'winner': 'blackbmore', 'has_lineups': 1}

        errors = list()

        for _record in resp.json():

            # clean dfs site name
            #
            if 'draftkings' in _record['site']:
                _record['site'] = 'draftkings'

            elif 'fanduel' in _record['site']:
                _record['site'] = 'fanduel'

            elif 'yahoo' in _record['site']:
                _record['site'] = 'yahoo'

            else:
                raise ValueError('Site not handled in parser.')

            # build contest link and clean
            #
            if _record['site'] == 'draftkings':
                contestLink = draftkings.build_contest_link(_record['site_id'])

            elif _record['site'] == 'fanduel':
                # IDs for fanduel contests are <slate>-<contest> format
                #
                _record['site_id'] = _record['site_id'].split('-')[-1]
                contestLink = fanduel.build_contest_link(_record['slate'], _record['site_id'])

            elif _record['site'] == 'yahoo':
                #https://sports.yahoo.com/dailyfantasy/contest/9816178
                contestLink = 'https://sports.yahoo.com/dailyfantasy/contest/{0}'.format(_record['site_id'])

            elif _record['site'] == 'superdraft':
                #https://superdraft.io/live/contest?contestId=368599
                contestLink = 'https://superdraft.io/live/contest?contestId={0}'.format(_record['site_id'])

            # populate object
            #
            try:
                contest = NflDfsContest(
                    source_site='fantasy_cruncher',
                    season_year=_record['period'].split('-')[0],
                    week=_record['period'].split('-')[-1],
                    dfs_site=_record['site'],
                    contest_id=_record['site_id'],
                    contest_name=_record['name'],
                    contest_link=contestLink,
                    slate_id=_record['slate'],
                    slate_games=_record['game_cnt'],
                    entry_cost=_record['cost'],
                    prize_pool=_record['prizepool'],
                    max_lineups=_record['max_entries'],
                    max_entries=_record['max_entrants'],
                    actual_entries=_record['total_entrants'],
                    score_to_cash=_record['mincash_score'],
                    min_prize=_record['mincash_payout'],
                    winning_prize=_record['winning_payout'],
                    score_to_win=_record['winning_score'],
                    places_paid=_record['places_paid']
                )

                yield contest

            except Exception as ex:
                errors.append(
                    (ex, dict(_record))
                )

        if len(errors) > 0:
            allErrors = '\n'.join([f'{x}: {y}' for x,y in errors])
            raise RuntimeError(f'Scraper encountered {len(errors)} errors:\n {allErrors}')

