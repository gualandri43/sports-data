'''
Utility classes or functions for general use.
'''

import datetime as dt

class TimeRecorder:
    '''
        Provides a standardized object for recording of current time.
    '''

    def __init__(self):
        '''
        Records current time in the constructor.
        '''

        self.timeUtc = None
        self.timeLocal = None

        self.record()

    def record(self):
        '''
        re-record the current time
        '''

        self.timeUtc = dt.datetime.utcnow()
        self.timeLocal = dt.datetime.now()

    def seconds_since_last_record(self) -> int:
        '''
        Calculate the time elapsed (in seconds) since the last record().
        '''

        elapsed = self.time_since_last_record()
        return elapsed.total_seconds()

    def time_since_last_record(self) -> dt.timedelta:
        '''
        Calculate the time elapsed (as a timedelta object) since the last record().
        '''

        elapsed = dt.datetime.utcnow() - self.timeUtc
        return elapsed

    def to_dict(self) -> dict:
        '''
        create a dict of the recorded attributes
        '''

        newDict = dict()

        newDict['timeUtc'] = self.timeUtc
        newDict['timeLocal'] = self.timeLocal

        return newDict

    def add_to_dict(self, target: dict):
        '''
        add time entries to an existing dict

        target      An existing dictionary.
        '''

        if not isinstance(target, dict):
            raise ValueError("target is not a dictionary.")

        target.update(self.to_dict())
