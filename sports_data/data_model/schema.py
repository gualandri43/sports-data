
# pylint: disable=invalid-name

import uuid
import datetime
import sqlalchemy
from sqlalchemy.orm import registry, relationship, Session
from sqlalchemy import Column, Integer, Integer, Float, JSON, Date, DateTime, Text, String
from sqlalchemy.sql.schema import ForeignKey, UniqueConstraint


MAPPER_REGISTRY = registry()
BASE = MAPPER_REGISTRY.generate_base()
TYPES_CACHE = dict()
FIELDS_CACHE = dict()

def create_tables(engine):
    '''Create all tables in a database'''
    MAPPER_REGISTRY.metadata.create_all(bind=engine)

def add_available_sites(engine):
    '''Add the source sites to the database.'''

    sites = [
        Sites(site='draftkings', url='https://www.draftkings.com'),
        Sites(site='fanduel', url='https://www.fanduel.com'),
        Sites(site='baseball_savant', url='https://baseballsavant.mlb.com/'),
        Sites(site='fantasy_cruncher', url='https://www.fantasycruncher.com'),
        Sites(site='fantasypros', url='https://www.fantasypros.com'),
        Sites(site='numberfire', url='https://www.numberfire.com'),
        Sites(site='rotoguru', url='http://rotoguru2.com/'),
        Sites(site='baseball_reference', url='https://www.baseball-reference.com'),
        Sites(site='pro_football_reference', url='https://www.pro-football-reference.com'),
        Sites(site='fangraphs', url='https://www.fangraphs.com'),
        Sites(site='yahoo', url='https://www.yahoo.com'),
        Sites(site='zips', url='https://www.fangraphs.com'),
        Sites(site='steamer', url='https://www.fangraphs.com'),
        Sites(site='steamer600', url='https://www.fangraphs.com'),
        Sites(site='atc', url='https://www.fangraphs.com'),
        Sites(site='theBat', url='https://www.fangraphs.com'),
        Sites(site='theBatX', url='https://www.fangraphs.com'),
        Sites(site='depthCharts', url='https://www.fangraphs.com')
    ]

    with Session(engine) as session:

        for _site in sites:
            try:
                session.add(_site)
                session.commit()
            except:
                session.rollback()

class SaUuid(sqlalchemy.types.TypeDecorator):
    """
        Platform-independent UUID type.
        Uses PostgreSQL's UUID type, otherwise uses
        CHAR(32), storing as stringified hex values.

        adapted from https://gist.github.com/gmolveau/7caeeefe637679005a7bb9ae1b5e421e
    """
    impl = sqlalchemy.types.CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(sqlalchemy.dialects.postgresql.UUID())
        else:
            return dialect.type_descriptor(sqlalchemy.types.CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value

    @property
    def python_type(self):
        return uuid.UUID

class DataModelBase:
    '''Base class for data model classes.'''

    __tablename__ = ''

    def __repr__(self) -> str:

        if hasattr(self, "_id"):
            localId = getattr(self, "_id")
            return '{0}(_id={1})'.format(type(self).__name__, localId)

    def __eq__(self, o: object) -> bool:

        if type(self) is not type(o):
            return False

        # look for fields info in the cache and build if not found
        #
        fieldsCache = FIELDS_CACHE.get(self.__tablename__, None)

        if not fieldsCache:
            tbl = MAPPER_REGISTRY.metadata.tables[self.__tablename__]
            FIELDS_CACHE[self.__tablename__] = set([c.name for c in tbl.columns])
            fieldsCache = FIELDS_CACHE[self.__tablename__]

        for _field in fieldsCache:
            val1 = getattr(self, _field)
            val2 = getattr(o, _field)
            if val1 != val2:
                return False

        return True

    def __ne__(self, o: object) -> bool:
        return not (self == o)

    def __setattr__(self, key, value):

        # look for type info in the cache and build if not found
        #
        tableTypeCache = TYPES_CACHE.get(self.__tablename__, None)

        if not tableTypeCache:
            tbl = MAPPER_REGISTRY.metadata.tables[self.__tablename__]
            TYPES_CACHE[self.__tablename__] = {c.name:c.type.python_type for c in tbl.columns}
            tableTypeCache = TYPES_CACHE[self.__tablename__]

        targetType = tableTypeCache.get(key, None)

        # perform validation and conversion
        #
        if targetType is None:
            # if not a column field, delegate to normal setattr
            #   this can handle property decorators
            super().__setattr__(key, value)

        else:
            # cast to the correct type or throw error
            #   if not able to be set
            #

            if value is None:
                return None

            if isinstance(value, str):
                if value.strip() == '':
                    return None

            val = None

            if targetType == float:

                if isinstance(value, str):
                    value = value.replace('%','').strip()

                val = float(value)

            elif targetType == int:
                val = int(value)

            elif targetType == str:
                val = str(value)

            elif targetType == dict:
                if isinstance(value, dict):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a dict.')

            elif targetType == list:
                if isinstance(value, list):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a list.')

            elif targetType == datetime.date:
                # dates can be parsed automatically if in ISO format

                if isinstance(value, datetime.date):
                    val = value
                else:
                    # try converting from iso format
                    try:
                        val = datetime.date.fromisoformat(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.date or parseable ISO string.') from ex

            elif targetType == datetime.datetime:
                # datetimes can be parsed automatically if in ISO format

                if isinstance(value, datetime.datetime):
                    val = value
                else:
                    # try converting from iso format
                    try:
                        val = datetime.datetime.fromisoformat(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.datetime or parseable ISO string.') from ex

            elif targetType == datetime.timedelta:
                # timedeltas can be parsed automatically if they can be read from int

                if isinstance(value, datetime.timedelta):
                    val = value
                else:
                    # try converting from an integer
                    try:
                        val = datetime.timedelta(seconds=int(value))
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.timedelta or integer.') from ex

            elif targetType == uuid.UUID:
                # UUID objects or strings

                if isinstance(value, uuid.UUID):
                    val = value
                else:
                    # try converting from a string
                    try:
                        val = uuid.UUID(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a UUID or parseable string.') from ex

            else:
                raise TypeError(str(key) + ' is not a known data type.')

            # set the value
            super().__setattr__(key, val)

class Sites(DataModelBase, BASE):
    '''List of allowable sites to use as data sources. This
        table will be used as a validation list.

        Parameters
        ----------
        site : str
            The identifier of a website used as a data source.

        url : str
            The url of the site

    '''

    __tablename__ = 'sites'

    site = Column(String(255), primary_key=True)
    url = Column(Text)

class NflOffense(DataModelBase, BASE):
    '''Stat data container for NFL offensive statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        pass_attempt : float
            Player pass attempts.

        pass_completion : float
            Player pass completion.

        pass_yard : float
            Player passing yards.

        pass_touchdown : float
            Player passing touchdowns.

        pass_interception: float
            Player passing interceptions.

        rush_attempt: float
            Player rushing attempts.

        rush_yard: float
            Player rushing yards.

        rush_touchdown: float
            Player rushing touchdowns.

        rec_reception: float
            Player receptions.

        rec_yard: float
            Player receiving yards.

        rec_touchdowns: float
            Player receiving touchdowns.

        fumble_lost: float
            Player fumbles lost.
    '''

    __tablename__ = 'nfl_offense'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    pass_attempt = Column(Float)
    pass_completion = Column(Float)
    pass_yard = Column(Float)
    pass_touchdown = Column(Float)
    pass_interception = Column(Float)
    rush_attempt = Column(Float)
    rush_yard = Column(Float)
    rush_touchdown = Column(Float)
    rec_target = Column(Float)
    rec_reception = Column(Float)
    rec_yard = Column(Float)
    rec_touchdown = Column(Float)
    fumble = Column(Float)
    fumble_lost = Column(Float)
    snaps_offense = Column(Float)
    snaps_offense_pct = Column(Float)
    sacked = Column(Float)
    sacked_yards_lost = Column(Float)

class NflDefense(DataModelBase, BASE):
    '''Stat data container for NFL team defensive statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        sack : float
            Sacks of the opposing quarterback.

        interception : float
            Interceptions.

        fumble_recovery : float
            Fumble recoveries.

        fumble_forced : float
            Forced fumbles.

        touchdown: float
            Defensive or special teams touchdowns.

        safety: float
            Safeties earned.

        points_allowed: float
            Points allowed to the opposing offense.

        yards_against: float
            Yards allowed to the opposing offense.
    '''

    __tablename__ = 'nfl_defense'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    sack = Column(Float)
    interception = Column(Float)
    fumble_recovery = Column(Float)
    fumble_forced = Column(Float)
    touchdown = Column(Float)
    safety = Column(Float)
    points_allowed = Column(Float)
    yards_against = Column(Float)

class NflKicking(DataModelBase, BASE):
    '''Stat data container for NFL kicking statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        field_goal : float
            Made field goals.

        field_goal_30_under : float
            Made field goals under 30 yards long.

        field_goal_31_40 : float
            Made field goals 31-40 yards long.

        field_goal_41_50 : float
            Made field goals 41-50 yards long.

        field_goal_attempt : float
            Total attempted field goals.

        extra_point : float
            Made extra points.

        extra_point_attempt : float
            Total attempted extra points.
    '''

    __tablename__ = 'nfl_kicking'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    field_goal = Column(Float)
    field_goal_30_under = Column(Float)
    field_goal_31_40 = Column(Float)
    field_goal_41_50 = Column(Float)
    field_goal_50_over = Column(Float)
    field_goal_attempt = Column(Float)
    extra_point = Column(Float)
    extra_point_attempt = Column(Float)

class NflTeamDvoa(DataModelBase, BASE):
    '''Stat data container for NFL team DVOA statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        total_dvoa:
            Total DVOA (off, def, special teams)

        total_dave:
            Total DAVE (preseason/regular season weighted combination)

        weighted_dvoa:
            Total DVOA weighted for recent games

        offense_dvoa:
            Offensive DVOA

        defense_dvoa:
            Defensive DVOA

        special_teams_dvoa:
            Special Teams DVOA
    '''

    __tablename__ = 'nfl_team_dvoa'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    total_dvoa = Column(Float)
    total_dave = Column(Float)
    weighted_dvoa = Column(Float)
    offense_dvoa = Column(Float)
    defense_dvoa = Column(Float)
    special_teams_dvoa = Column(Float)

class NflProjection(DataModelBase, BASE):
    '''Data container for an NFL stat projection.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        scope : str
            The scope of the projection, such as season, or daily, etc.

        scrape_time_local : datetime.datetime
            The date and time the projection was downloaded from the source in the local timezone.

        scrape_time_utc : datetime.datetime
            The date and time the projection was downloaded from the source in UTC time.

        season_year : int
            The season the projection is for.

        projection_date : datetime.date
            The date of the game the projection is for.

        projection_week : int
            The week that the projected stats cover.

        player_id : str
            The player identifier that the projection is for. The player's pro football reference
            identifier is used.

        offense_stats_id : int
            The ID of the stat record for the projected offensive stats.

        defense_stats_id : int
            The ID of the stat record for the projected defensive stats.

        kicking_stats_id : int
            The ID of the stat record for the projected kicking stats.
    '''

    __tablename__ = 'nfl_projection'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'), index=True)
    scope = Column(String, index=True)
    scrape_time_local = Column(DateTime, index=True)
    scrape_time_utc = Column(DateTime, index=True)
    season_year = Column(Integer, index=True)
    projection_date = Column(Date, index=True)
    projection_week = Column(Integer, index=True)
    player_id = Column(String(255), index=True)
    offense_stats_id = Column(Integer, ForeignKey('nfl_offense._id'))
    defense_stats_id = Column(Integer, ForeignKey('nfl_defense._id'))
    kicking_stats_id = Column(Integer, ForeignKey('nfl_kicking._id'))

    offense_stats = relationship('NflOffense', uselist=False)
    defense_stats = relationship('NflDefense', uselist=False)
    kicking_stats = relationship('NflKicking', uselist=False)

class NflGame(DataModelBase, BASE):
    '''Data container for an NFL game. Sourced from pro-football-reference.com. Each record is a team's game.
        This means there will be multiple records for the same game key.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        season_year : int
            The season the projection is for.

        game_date : datetime.date
            The date of the game.

        pro_football_reference_game_key : str
            The game key identifier for the game from pro football reference.

        team : str
            The team code for this game record.

        opponent : str
            The team code for the team's opponent for this game record.

        location : str
            home or away

        result : str
            win, lose, tie

        points_team : int
            Points scored by the team in this game.

        points_opponent : int
            Points scored by the team's opponent in this game.

        yards_team : int
            Yards gained by the team in this game.

        yards_opponent : int
            Yards gained by the team's opponent in this game.

        turnovers_team : int
            Turnovers lost by the team in this game.

        turnovers_opponent : int
            Turnovers lost by the team's opponent in this game.
    '''

    __tablename__ = 'nfl_game'

    _id = Column(Integer, primary_key=True)
    source_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    game_date = Column(Date)
    pro_football_reference_game_key = Column(String(255), index=True)
    team = Column(String(255), index=True)
    opponent = Column(String(255), index=True)
    location = Column(String(255), index=True)
    result = Column(String(255), index=True)
    points_team = Column(Integer)
    points_opponent = Column(Integer)
    yards_team = Column(Integer)
    yards_opponent = Column(Integer)
    turnovers_team = Column(Integer)
    turnovers_opponent = Column(Integer)

class NflPlayerGamelog(DataModelBase, BASE):
    '''Data container for an NFL player stat gamelog entry (one game).

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        season_year : int
            The season the projection is for.

        game_date : datetime.date
            The date of the game.

        pro_football_reference_game_key : str
            The game key identifier for the game from pro football reference.

        player_id : str
            The player identifier that the projection is for. The player's pro football reference
            identifier is used.

        offense_stats_id : int
            The ID of the stat record for the offensive stats.

        defense_stats_id : int
            The ID of the stat record for the defensive stats.

        kicking_stats_id : int
            The ID of the stat record for the kicking stats.
    '''

    __tablename__ = 'nfl_player_gamelog'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    game_date = Column(Date)
    pro_football_reference_game_key = Column(String(255))
    player_id = Column(String(255), index=True)
    offense_stats_id = Column(Integer, ForeignKey('nfl_offense._id'))
    defense_stats_id = Column(Integer, ForeignKey('nfl_defense._id'))
    kicking_stats_id = Column(Integer, ForeignKey('nfl_kicking._id'))

    offense_stats = relationship('NflOffense', uselist=False)
    defense_stats = relationship('NflDefense', uselist=False)
    kicking_stats = relationship('NflKicking', uselist=False)

class NflRoster(DataModelBase, BASE):
    '''Data container for an NFL team roster.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        season_year : int
            The season the projection is for.

        pro_football_reference_team_key : str
            The team key identifier for the team from pro football reference.

        player_id : str
            The player identifier that the projection is for. The player's pro football reference
            identifier is used.

        jersey_number : int
            The player's jersey number.

        position : str
            The player's position

    '''

    __tablename__ = 'nfl_roster'

    __table_args__ = (
        UniqueConstraint('season_year','pro_football_reference_team_key','player_id'),
    )

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    pro_football_reference_team_key = Column(String(255))
    player_id = Column(String(255), index=True)
    jersey_number = Column(Integer)
    position = Column(String(255))

class NflDfsContest(DataModelBase, BASE):
    '''Data container for a NFL DFS contest.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        dfs_site : str
            The site the DFS contest is on. Must be a valid site.

        season_year : int
            The season the contest is in.

        week : int
            The week the contest is in.

        slate_id : int
            The id number of the contest slate.

        slate_games : int
            The number of games in the contest slate.

        contest_name : str
            The name of the contest.

        contest_id : int
            The ID number of the contest.

        contest_link : str
            The link to the contest page.

        entry_cost : float
            The cost to enter the contest.

        prize_pool : float
            The total prize pool of the contest.

        max_lineups : int
            The maximum number of lineups that may be entered.

        max_entries : int
            The maximum number of entries for the contest.

        actual_entries : int
            The actual number of entries at the close of the contest.

        score_to_cash : float
            The minimum score required to win a prize.

        min_prize : float
            The prize amount for the minimum cash slot.

        winning_prize : float
            The prize amount for the winning lineup.

        score_to_win : float
            The score of the lineup that won the contest.

        places_paid : int
            The number of finishing places that are paid.

    '''

    __tablename__ = 'nfl_dfs_contest'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    dfs_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    week = Column(Integer)
    slate_id = Column(Integer)
    slate_games = Column(Integer)
    contest_name = Column(Text)
    contest_id = Column(Integer)
    contest_link = Column(Text)
    entry_cost = Column(Float)
    prize_pool = Column(Float)
    max_lineups = Column(Integer)
    max_entries = Column(Integer)
    actual_entries = Column(Integer)
    score_to_cash = Column(Float)
    min_prize = Column(Float)
    winning_prize = Column(Float)
    score_to_win = Column(Float)
    places_paid = Column(Integer)

class NflPlayerDfs(DataModelBase, BASE):
    '''Data container for DFS data for a player.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        dfs_site : str
            The site the DFS contest is on. Must be a valid site.

        season_year : int
            The season the salary is for.

        week : int
            The week the salary is for.

        player_id : str
            The player identifier that the projection is for. The player's pro football reference
            identifier is used.

        salary : float
            The player's salary.

    '''

    __tablename__ = 'nfl_player_dfs'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    dfs_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    week = Column(Integer)
    player_id = Column(String(255))
    salary = Column(Float)

class MlbBatting(DataModelBase, BASE):
    '''Stat data container for MLB batting statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        game : float
            Games played.
    '''

    __tablename__ = 'mlb_batting'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    game = Column(Float)
    plate_appearance = Column(Float)
    at_bat = Column(Float)
    hit = Column(Float)
    single = Column(Float)
    double = Column(Float)
    triple = Column(Float)
    homerun = Column(Float)
    total_bases = Column(Float)
    grand_slam = Column(Float)
    run = Column(Float)
    rbi = Column(Float)
    walk = Column(Float)
    intentional_walk = Column(Float)
    hit_by_pitch = Column(Float)
    strikeout = Column(Float)
    stolen_base = Column(Float)
    caught_stealing = Column(Float)
    sacrifice_bunt = Column(Float)
    sacrifice_fly = Column(Float)
    gidp = Column(Float)
    average = Column(Float)
    obp = Column(Float)
    slg = Column(Float)
    ops = Column(Float)
    woba = Column(Float)
    war = Column(Float)
    wrc_plus = Column(Float)
    cycle = Column(Float)

class MlbPitching(DataModelBase, BASE):
    '''Stat data container for MLB pitching statistics.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        game : float
            Games played.
    '''

    __tablename__ = 'mlb_pitching'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    game = Column(Float)
    hit = Column(Float)
    single = Column(Float)
    double = Column(Float)
    triple = Column(Float)
    homerun = Column(Float)
    grand_slam = Column(Float)
    strikeout = Column(Float)
    walk = Column(Float)
    intentional_walk = Column(Float)
    win = Column(Float)
    loss = Column(Float)
    sv = Column(Float)
    blown_sv = Column(Float)
    hold = Column(Float)
    era = Column(Float)
    er = Column(Float)
    runs_allowed = Column(Float)
    wild_pitch = Column(Float)
    balk = Column(Float)
    game_started = Column(Float)
    ip = Column(Float)
    whip = Column(Float)
    strikeout_per_nine = Column(Float)
    walk_per_nine = Column(Float)
    fip = Column(Float)
    war = Column(Float)
    total_batters_faced = Column(Float)
    complete_game = Column(Float)
    shoutout = Column(Float)
    quality_start = Column(Float)
    no_hitter = Column(Float)
    perfect_game = Column(Float)
    hit_by_pitch = Column(Float)

class MlbStatcast(DataModelBase, BASE):
    '''Stat data container for MLB statcast pitch record data.

        Can uniquely identify a record using ('game_mlb_key','game_plate_appearance','plate_appearance_pitch_number'),
        but a record ID integer is generated for each record for easier reference. Can check the combo of those three
        to ensure no duplicate records exist.

        In the parameter descriptions below, the statcast data field name is in (parentheses).

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        game_mlb_key:
            Unique Id for Game. (game_pk)

        game_date:
            Date of the Game. (game_date)

        game_season:
            Year game took place. (game_year)

        game_type:
            Type of Game. (game_type) [E = Exhibition, S = Spring Training, R = Regular Season, F = Wild Card, D = Divisional Series, L = League Championship Series, W = World Series]

        game_event_id:
            Non-unique Id of play event per game. (sv_id)

        game_plate_appearance
            Plate appearance number of the game. (at_bat_number)

        game_home_team:
            Abbreviation of home team. (home_team)

        game_away_team:
            Abbreviation of away team. (away_team)

        plate_appearance_pitch_number:
            Total pitch number of the plate appearance. (pitch_number)

        plate_appearance_batter_id:
            MLB Player Id tied to the play event. (batter)

        plate_appearance_pitcher_id:
            MLB Player Id tied to the play event. (pitcher)

        plate_appearance_inning:
            Pre-pitch inning number. (inning)

        plate_appearance_inning_topbot:
            Pre-pitch top or bottom of inning. (inning_topbot)

        event_fielder_2:
            Pre-pitch MLB Player Id of Catcher. (fielder_2)

        event_fielder_3:
            MLB Player Id for 1B. (fielder_3)

        event_fielder_4:
            MLB Player Id for 2B. (fielder_4)

        event_fielder_5:
            MLB Player Id for 3B. (fielder_5)

        event_fielder_6:
            MLB Player Id for SS. (fielder_6)

        event_fielder_7:
            MLB Player Id for LF. (fielder_7)

        event_fielder_8:
            MLB Player Id for CF. (fielder_8)

        event_fielder_9:
            MLB Player Id for RF. (fielder_9)

        event_if_fielding_alignment:
            Infield fielding alignment at the time of the pitch. (if_fielding_alignment)

        event_of_fielding_alignment:
            Outfield fielding alignment at the time of the pitch. (of_fielding_alignment)

        event_runner_on_3b:
            Pre-pitch MLB Player Id of Runner on 3B. (on_3b)

        event_runner_on_2b:
            Pre-pitch MLB Player Id of Runner on 2B. (on_2b)

        event_runner_on_1b:
            Pre-pitch MLB Player Id of Runner on 1B. (on_1b)

        event_balls_pre:
            Pre-pitch number of balls in count. (balls)

        event_strikes_pre:
            Pre-pitch number of strikes in count. (strikes)

        event_outs_pre:
            Pre-pitch number of outs. (outs_when_up)

        event_home_score_pre:
            Pre-pitch home score (home_score)

        event_away_score_pre:
            Pre-pitch away score (away_score)

        event_bat_score_pre:
            Pre-pitch bat team score (bat_score)

        event_fld_score_pre:
            Pre-pitch field team score (fld_score)

        event_home_score_post:
            Post-pitch home score (post_home_score)

        event_away_score_post:
            Post-pitch away score (post_away_score)

        event_bat_score_post:
            Post-pitch bat team score (post_bat_score)

        event_fld_score_post:
            Post-pitch field team score (post_fld_score)

        event_pitcher_throws:
            Hand pitcher throws with. (p_throws)

        pitch_type:
            The type of pitch derived from Statcast. (pitch_type)

        pitch_name:
            The name of the pitch derived from the Statcast Data. (pitch_name)

        pitch_release_speed:
            Pitch velocities from 2008-16 are via Pitch F/X, and adjusted to roughly out-of-hand release point. All velocities from 2017 and beyond are Statcast, which are reported out-of-hand.  (release_speed)

        pitch_release_pos_x:
            Horizontal Release Position of the ball measured in feet from the catcher's perspective (release_pos_x)

        pitch_release_pos_y:
            Release position of pitch measured in feet from the catcher's perspective. (release_pos_y)

        pitch_release_pos_z:
            Vertical Release Position of the ball measured in feet from the catcher's perspective. (release_pos_z)

        pitch_release_spin_rate:
            Spin rate of pitch tracked by Statcast. (release_spin_rate)

        pitch_release_extension:
            Release extension of pitch in feet as tracked by Statcast. (release_extension)

        pitch_spin_axis:
            The Spin Axis in the 2D X-Z plane in degrees from 0 to 360, such that 180 represents a pure backspin fastball and 0 degrees represents a pure topspin (12-6) curveball.  (spin_axis)

        pitch_pfx_x:
            Horizontal movement in feet from the catcher's perspective. (pfx_x)

        pitch_pfx_z:
            Vertical movement in feet from the catcher's perpsective. (pfx_z)

        pitch_plate_x:
            Horizontal position of the ball when it crosses home plate from the catcher's perspective. (plate_x)

        pitch_plate_z:
            Vertical position of the ball when it crosses home plate from the catcher's perspective. (plate_z)

        pitch_sz_top:
            Top of the batter's strike zone set by the operator when the ball is halfway to the plate. (sz_top)

        pitch_sz_bot:
            Bottom of the batter's strike zone set by the operator when the ball is halfway to the plate. (sz_bot)

        pitch_zone_location:
            Zone location of the ball when it crosses the plate from the catcher's perspective. (zone)

        pitch_vx0:
            The velocity of the pitch, in feet per second, in x-dimension, determined at y=50 feet. (vx0)

        pitch_vy0:
            The velocity of the pitch, in feet per second, in y-dimension, determined at y=50 feet. (vy0)

        pitch_vz0:
            The velocity of the pitch, in feet per second, in z-dimension, determined at y=50 feet. (vz0)

        pitch_ax:
            The acceleration of the pitch, in feet per second per second, in x-dimension, determined at y=50 feet. (ax)

        pitch_ay:
            The acceleration of the pitch, in feet per second per second, in y-dimension, determined at y=50 feet. (ay)

        pitch_az:
            The acceleration of the pitch, in feet per second per second, in z-dimension, determined at y=50 feet. (az)

        event_batter_side:
            Side of the plate batter is standing. (stand)

        result_pitch_description:
            Description of the resulting pitch. (description)

        result_plate_appearance_description:
            Plate appearance description from game day. (des)

        result_event:
            Event of the resulting Plate Appearance. (events)

        result_pitch_type:
            Short hand of pitch result. [B = ball,S = strike,X = in play] (type)

        result_hit_location:
            Position of first fielder to touch the ball. (hit_location)

        result_batted_ball_type:
            Batted ball type [ground_ball,line_drive,fly_ball,popup]  (bb_type)

        result_batted_ball_hit_coord_x:
            Hit coordinate X of batted ball. (hc_x)

        result_batted_ball_hit_coord_y:
            Hit coordinate Y of batted ball. (hc_y)

        result_projected_hit_distance:
            Projected hit distance of the batted ball. (hit_distance_sc)

        result_launch_speed:
            Exit velocity of the batted ball as tracked by Statcast. For the limited subset of batted balls not tracked directly, estimates are included. (launch_speed)

        result_launch_angle:
            Launch angle of the batted ball as tracked by Statcast. For the limited subset of batted balls not tracked directly, estimates are included. (launch_angle)

        result_effective_speed:
            Derived speed based on the the extension of the pitcher's release. (effective_speed)

        result_estimated_batting_avg:
            Estimated Batting Avg based on launch angle and exit velocity. (estimated_ba_using_speedangle)

        result_estimated_woba:
            Estimated wOBA based on launch angle and exit velocity. (estimated_woba_using_speedangle)

        result_woba:
            wOBA value based on result of play. (woba_value)

        result_woba_denom:
            wOBA denominator based on result of play. (woba_denom)

        result_babip:
            BABIP value based on result of play. (babip_value)

        result_iso:
            ISO value based on result of play. (iso_value)

        result_batted_ball_category:
            Launch speed/angle zone based on launch angle and exit velocity. [1: Weak,2: Topped,3: Under,4: Flare/Burner,5: Solid Contact,6: Barrel] (launch_speed_angle)

        result_delta_home_win_exp:
            The change in Win Expectancy before the Plate Appearance and after the Plate Appearance (delta_home_win_exp)

        result_delta_run_exp:
            The change in Run Expectancy before the Pitch and after the Pitch (delta_run_exp)

        Deprecated
            spin_dir
            spin_rate_deprecated
            break_angle_deprecated
            break_length_deprecated
            tfs_deprecated
            tfs_zulu_deprecated
            umpire

        Not Used
            player_name -- Player's name tied to the event of the search. (don't need this)
        '''

    __tablename__ = 'mlb_statcast'

    __table_args__ = (
        UniqueConstraint('game_mlb_key','game_plate_appearance','plate_appearance_pitch_number'),
    )

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)

    #Descriptive
    game_mlb_key = Column(Integer, index=True)
    game_date = Column(DateTime, index=True)
    game_season = Column(Integer, index=True)
    game_type = Column(String(255), index=True)
    game_event_id = Column(Integer)
    game_plate_appearance = Column(Integer)
    game_home_team = Column(String(255))
    game_away_team = Column(String(255))
    plate_appearance_pitch_number = Column(Integer)
    plate_appearance_batter_id = Column(Integer)
    plate_appearance_pitcher_id = Column(Integer)
    plate_appearance_inning = Column(Integer)
    plate_appearance_inning_topbot = Column(String(255))
    event_fielder_2 = Column(Integer)
    event_fielder_3 = Column(Integer)
    event_fielder_4 = Column(Integer)
    event_fielder_5 = Column(Integer)
    event_fielder_6 = Column(Integer)
    event_fielder_7 = Column(Integer)
    event_fielder_8 = Column(Integer)
    event_fielder_9 = Column(Integer)
    event_if_fielding_alignment = Column(String(255))
    event_of_fielding_alignment = Column(String(255))
    event_runner_on_3b = Column(Integer)
    event_runner_on_2b = Column(Integer)
    event_runner_on_1b = Column(Integer)
    event_balls_pre = Column(Integer)
    event_strikes_pre = Column(Integer)
    event_outs_pre = Column(Integer)
    event_home_score_pre = Column(Integer)
    event_away_score_pre = Column(Integer)
    event_bat_score_pre = Column(Integer)
    event_fld_score_pre = Column(Integer)
    event_home_score_post = Column(Integer)
    event_away_score_post = Column(Integer)
    event_bat_score_post = Column(Integer)
    event_fld_score_post = Column(Integer)

    #Pitch Info
    event_pitcher_throws = Column(String(255))
    pitch_type = Column(String(255))
    pitch_name = Column(String(255))
    pitch_release_speed = Column(Float)
    pitch_release_pos_x = Column(Float)
    pitch_release_pos_y = Column(Float)
    pitch_release_pos_z = Column(Float)
    pitch_release_spin_rate = Column(Float)
    pitch_release_extension = Column(Float)
    pitch_spin_axis = Column(Float)
    pitch_pfx_x = Column(Float)
    pitch_pfx_z = Column(Float)
    pitch_plate_x = Column(Float)
    pitch_plate_z = Column(Float)
    pitch_sz_top = Column(Float)
    pitch_sz_bot = Column(Float)
    pitch_zone_location = Column(Integer)
    pitch_vx0 = Column(Float)
    pitch_vy0 = Column(Float)
    pitch_vz0 = Column(Float)
    pitch_ax = Column(Float)
    pitch_ay = Column(Float)
    pitch_az = Column(Float)

    #Batter Info
    event_batter_side = Column(String(50))

    ###result
    result_pitch_description = Column(Text)
    result_plate_appearance_description = Column(Text)
    result_event = Column(Text)
    result_pitch_type = Column(String(255))
    result_hit_location = Column(Integer)
    result_batted_ball_type = Column(String(255))
    result_batted_ball_hit_coord_x = Column(Float)
    result_batted_ball_hit_coord_y = Column(Float)
    result_projected_hit_distance = Column(Float)
    result_launch_speed = Column(Float)
    result_launch_angle = Column(Float)
    result_effective_speed = Column(Float)
    result_estimated_batting_avg = Column(Float)
    result_estimated_woba = Column(Float)
    result_woba = Column(Float)
    result_woba_denom = Column(Float)
    result_babip = Column(Float)
    result_iso = Column(Float)
    result_batted_ball_category = Column(Integer)
    result_delta_home_win_exp = Column(Float)
    result_delta_run_exp = Column(Float)

class MlbProjection(DataModelBase, BASE):
    '''Data container for an MLB stat projection.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        scope : str
            The scope of the projection, such as season, or daily, etc.

        scrape_time_local : datetime.datetime
            The date and time the projection was downloaded from the source in the local timezone.

        scrape_time_utc : datetime.datetime
            The date and time the projection was downloaded from the source in UTC time.

        season_year : int
            The season the projection is for.

        projection_date : datetime.date
            The date of the game the projection is for.

        projection_week : int
            The week that the projected stats cover.

        player_id : str
            The player identifier that the projection is for. The player's baseball reference
            identifier is used.

        batting_stats_id : int
            The ID of the stat record for the projected batting stats.

        pitching_stats_id : int
            The ID of the stat record for the projected pitching stats.
    '''

    __tablename__ = 'mlb_projection'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'), index=True)
    scope = Column(String, index=True)
    scrape_time_local = Column(DateTime, index=True)
    scrape_time_utc = Column(DateTime, index=True)
    season_year = Column(Integer, index=True)
    projection_date = Column(Date, index=True)
    projection_week = Column(Integer, index=True)
    player_id = Column(String(255), index=True)
    batting_stats_id = Column(Integer, ForeignKey('mlb_batting._id'))
    pitching_stats_id = Column(Integer, ForeignKey('mlb_pitching._id'))

    batting_stats = relationship('MlbBatting', uselist=False)
    pitching_stats = relationship('MlbPitching', uselist=False)

class MlbPlayerSeason(DataModelBase, BASE):
    '''Data container for an MLB historical season stats.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        season_year : int
            The season the projection is for.

        player_id : str
            The player identifier that the projection is for. The player's baseball reference
            identifier is used.

        batting_stats_id : int
            The ID of the stat record for the projected batting stats.

        pitching_stats_id : int
            The ID of the stat record for the projected pitching stats.
    '''

    __tablename__ = 'mlb_player_season'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    player_id = Column(String(255), index=True)
    batting_stats_id = Column(Integer, ForeignKey('mlb_batting._id'))
    pitching_stats_id = Column(Integer, ForeignKey('mlb_pitching._id'))

    batting_stats = relationship('MlbBatting', uselist=False)
    pitching_stats = relationship('MlbPitching', uselist=False)

class MlbPlayerGamelog(DataModelBase, BASE):
    '''Data container for an MLB player stat gamelog entry (one game).

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        season_year : int
            The season the projection is for.

        game_date : datetime.date
            The date of the game.

        baseball_reference_game_key : str
            The game key identifier for the game from pro football reference.

        player_id : str
            The player identifier that the projection is for. The player's baseball reference
            identifier is used.

        batting_stats_id : int
            The ID of the stat record for the projected batting stats.

        pitching_stats_id : int
            The ID of the stat record for the projected pitching stats.
    '''

    __tablename__ = 'mlb_player_gamelog'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    game_date = Column(Date)
    baseball_reference_game_key = Column(String(255))
    player_id = Column(String(255), index=True)
    batting_stats_id = Column(Integer, ForeignKey('mlb_batting._id'))
    pitching_stats_id = Column(Integer, ForeignKey('mlb_pitching._id'))

    batting_stats = relationship('MlbBatting', uselist=False)
    pitching_stats = relationship('MlbPitching', uselist=False)

class MlbFangraphsGuts(DataModelBase, BASE):
    '''Data container for Fangraphs Guts adjustment factors.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

    '''

    __tablename__ = 'mlb_fangraphs_guts'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    season_year = Column(Integer, index=True, unique=True)
    woba = Column(Float)
    woba_scale = Column(Float)
    w_bb = Column(Float)
    w_hbp = Column(Float)
    w_1b = Column(Float)
    w_2b = Column(Float)
    w_3b = Column(Float)
    w_hr = Column(Float)
    run_sb = Column(Float)
    run_cs = Column(Float)
    r_per_pa = Column(Float)
    r_per_w = Column(Float)
    c_fip = Column(Float)

class MlbPlayerDfs(DataModelBase, BASE):
    '''Data container for DFS data for a player.

        Parameters
        ----------
        _id : int
            The unique ID number of the record.

        custom_data : dict
            Any miscellaneous data desired to be stored that doesn't fit in to established fields.

        source_site : str
            The source of the data. Must be a valid site.

        dfs_site : str
            The site the DFS contest is on. Must be a valid site.

        season_year : int
            The season the salary is for.

        game_date : datetime.date
            The week the salary is for.

        player_id : str
            The player identifier that the projection is for. The player's baseball reference
            identifier is used.

        salary : float
            The player's salary.

    '''

    __tablename__ = 'mlb_player_dfs'

    _id = Column(Integer, primary_key=True)
    custom_data = Column(JSON)
    source_site = Column(String(255), ForeignKey('sites.site'))
    dfs_site = Column(String(255), ForeignKey('sites.site'))
    season_year = Column(Integer)
    game_date = Column(Date)
    player_id = Column(String(255))
    salary = Column(Float)
