'''
Basic structures for data_model.
'''
import abc
import json
import datetime
import uuid
import collections
import dataclasses


@dataclasses.dataclass
class IDataModel(abc.ABC):
    '''
        Interface to implement for a data model class. The
        derived class must be a dataclass and implement the
        required methods defined in this interface.
    '''

    @classmethod
    def get_fieldnames(cls, prefix=None, exclude=None):
        '''build list of field names for the class, with optional prefix.'''

        names = set([x.name for x in dataclasses.fields(cls)])

        if exclude:
            for _excluded in exclude:
                names.remove(_excluded)

        if prefix:
            return ['{0}{1}'.format(prefix,x) for x in names]
        else:
            return list(names)

    @abc.abstractmethod
    def is_empty(self):
        '''Returns True if all fields are None or empty, False otherwise.'''
        pass

@dataclasses.dataclass
class DataModelBase(IDataModel):
    '''
        A base class for data model objects.

        Contains default methods and a custom __setattr__ implementation to
        check for correct data types upon attribute assignment, as well as implicitly
        convert data types if possible.
            Dates: datetimes, dates can convert from ISO formatted strings.
            Timedeltas: timedelta classes can convert from integer number of seconds.
            UUIDs: can parse the string representation of a UUID to a uuid.UUID class.
            Strings: empty strings are converted to None.
            int,float: will attempt to cast using int() or float().
    '''

    def __post_init__(self):

        # build a lookup for the various field types after class has been initialized.
        self._fieldTypes = {x.name:x.type for x in dataclasses.fields(self)}

    def __setattr__(self, key, value):

        if not hasattr(self, '_fieldTypes'):
            # allow for normal setting if _fieldTypes hasn't been defined.
            #   need this to allow for initialization of class
            #
            super().__setattr__(key, value)

        elif key not in self._fieldTypes:
            # if not a dataclass field, delegate to normal setattr
            #   this can handle property decorators
            super().__setattr__(key, value)

        else:
            # cast to the correct type or throw error
            #   if not able to be set
            #

            targetType = self._fieldTypes[key]
            val = None

            if value is None:
                val = None

            elif isinstance(value, str) and value.strip() == '':
                val = None

            elif targetType == float:
                val = float(value)

            elif targetType == int:
                val = int(value)

            elif targetType == str:
                val = str(value)

            elif targetType == dict:
                if isinstance(value, dict):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a dict.')

            elif targetType == list:
                if isinstance(value, list):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a list.')

            elif targetType == datetime.date:
                # dates can be parsed automatically if in ISO format

                if isinstance(value, datetime.date):
                    val = value
                else:
                    # try converting from iso format
                    try:
                        val = datetime.date.fromisoformat(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.date or parseable ISO string.') from ex

            elif targetType == datetime.datetime:
                # datetimes can be parsed automatically if in ISO format

                if isinstance(value, datetime.datetime):
                    val = value
                else:
                    # try converting from iso format
                    try:
                        val = datetime.datetime.fromisoformat(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.datetime or parseable ISO string.') from ex

            elif targetType == datetime.timedelta:
                # timedeltas can be parsed automatically if they can be read from int

                if isinstance(value, datetime.timedelta):
                    val = value
                else:
                    # try converting from an integer
                    try:
                        val = datetime.timedelta(seconds=int(value))
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a datetime.timedelta or integer.') from ex

            elif targetType == uuid.UUID:
                # UUID objects or strings

                if isinstance(value, uuid.UUID):
                    val = value
                else:
                    # try converting from a string
                    try:
                        val = uuid.UUID(value)
                    except Exception as ex:
                        raise TypeError(str(key) + ' must be a UUID or parseable string.') from ex

            elif issubclass(targetType, IDataModel):

                if isinstance(value, IDataModel):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a IDataModel.')

            elif issubclass(targetType, DataModelCollection):

                if isinstance(value, DataModelCollection):
                    val = value
                else:
                    raise TypeError(str(key) + ' must be a DataModelCollection.')

            else:
                raise TypeError(str(key) + ' is not a known data type.')

            # set the value
            super().__setattr__(key, val)

    def is_empty(self):

        for _fieldname in self.get_fieldnames():

            val = getattr(self, _fieldname)

            if isinstance(val, DataModelBase):
                if not val.is_empty():
                    return False

            elif val:
                return False

        return True

    @classmethod
    def get_fieldnames(cls, prefix=None, exclude=None):
        '''build list of field names for the class, with optional prefix.'''

        names = set([x.name for x in dataclasses.fields(cls)])

        if exclude:
            for _excluded in exclude:
                names.remove(_excluded)

        if prefix:
            return ['{0}{1}'.format(prefix,x) for x in names]
        else:
            return list(names)

class DataModelCollection(collections.UserList):
    '''
    List-like collection to hold a set of IDataModel objects.
    '''

    def __init__(self, initlist=None):
        super().__init__(initlist=initlist)

        self.lookupMap = dict()

    def _remove_from_map(self, i):
        '''remove a lookup map entry'''

        recordKey = self.data[i].uniqueId
        self.lookupMap.pop(recordKey, None)

    def _add_to_map(self, i, item):
        '''add an item to the lookup map.'''

        if not isinstance(item, IDataModel):
            raise ValueError('Collection items must be derived from IDataModel.')

        # add a key --> index mapping
        if item.uniqueId not in self.lookupMap:
            self.lookupMap[item.uniqueId] = i

    def __add__(self, other):

        if not isinstance(other, IDataModel):
            raise ValueError('Collection items must be derived from IDataModel.')

        super().__add__(other)

    def __delitem__(self, i):

        self._remove_from_map(i)
        super().__delitem__(i)

    def __setitem__(self, i, item):

        if not isinstance(item, IDataModel):
            raise ValueError('Collection items must be derived from IDataModel.')

        self._add_to_map(i, item)
        super().__setitem__(i, item)

    def __radd__(self, other):

        if not isinstance(other, IDataModel):
            raise ValueError('Collection items must be derived from IDataModel.')

        super().__radd__(other)

    def __iadd__(self, other):

        if not isinstance(other, IDataModel):
            raise ValueError('Collection items must be derived from IDataModel.')

        super().__iadd__(other)

    def append(self, item):

        super().append(item)
        self._add_to_map(len(self.data)-1, item)

    def get_by_key(self, lookupKey):
        '''
        Get a value from the collection using a unique identifier.
        If identifier doesn't exist, return None.
        '''

        if lookupKey in self.lookupMap:
            return self.data[self.lookupMap[lookupKey]]
        else:
            return None

class ExtendedJsonEncoder(json.JSONEncoder):
    '''
    Add serialization formatting for nonstandard classes and types,
    such as UUIDs, datetimes, etc.
    '''

    def default(self, o):

        if o is None:
            return super().default(o)

        if isinstance(o, uuid.UUID):
            # UUID serialization as hex string
            return str(o)

        elif isinstance(o, datetime.date) or isinstance(o, datetime.datetime) or isinstance(o, datetime.time):
            # date, datetime, time serialization in ISO 8601 format
            return o.isoformat()

        elif isinstance(o, datetime.timedelta):
            # timedelta serialization to seconds
            return o.total_seconds()

        else:
            # use default encoder for all others
            return super().default(o)

def extended_json_decoding(obj, targetType):
    '''
    decode a string representation into an object. use for nonstandard classes/types

    targetType      the type of the object to convert string to (use type() or similar)
    obj             obj is assumed to be a serialized string if it is one of the handled types
    '''

    # do not try to convert a Null object
    if obj is None:
        return obj

    if targetType is None:
        return obj

    if targetType is uuid.UUID:
        return uuid.UUID(obj)

    elif targetType is datetime.date:
        return datetime.date.fromisoformat(obj)

    elif targetType is datetime.datetime:
        return datetime.datetime.fromisoformat(obj)

    elif targetType is datetime.time:
        return datetime.time.fromisoformat(obj)

    elif targetType is datetime.timedelta:
        return datetime.timedelta(seconds=int(obj))

    else:
        return obj
