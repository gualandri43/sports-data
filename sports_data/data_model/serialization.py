'''
Data model serialization.
'''

import abc
import os
import json
import uuid
import datetime
import sqlalchemy
import pandas as pd

from sports_data.data_model.schema import MAPPER_REGISTRY


class ISerializer(abc.ABC):
    '''Interface for a serializer class.'''

    @abc.abstractmethod
    def save(self, obj):
        '''
        Save the given data model object or collection of objects
        '''
        pass

    @abc.abstractmethod
    def load(self, data, targetClass):
        '''
        Load a data model object from serialized data.

        returns either a single object of type targetClass, or
        a collection with all loaded objects
        '''
        pass

class SerializerBase(ISerializer):
    '''Base class for common serializer functionality.'''

    def __init__(self):

        self.fieldTypes = dict()
        self.fieldNames = dict()

        # gather data from the mapped metadata
        for _table in MAPPER_REGISTRY.metadata.tables.values():
            self.fieldTypes[_table.name] = {c.name:c.type.python_type for c in _table.columns}
            self.fieldNames[_table.name] = [c.name for c in _table.columns]

        super().__init__()

    def _check_if_mapped_class(self, obj):

        if not hasattr(obj, '__tablename__'):
            return False

        if obj.__tablename__ in MAPPER_REGISTRY.metadata.tables:
            return True
        else:
            return False

    def _is_empty(self, obj):

        for _fieldname in self.fieldNames:

            val = getattr(obj, _fieldname)

            if self._check_if_mapped_class(obj):
                if self._is_empty(val):
                    return False

            elif val:
                return False

        return True

class DictSerializer(SerializerBase):
    '''Load/save data model objects from dictionary.'''

    def _handle_dict(self, obj):

        outData = dict()
        for _key,_val in obj.items():

            if isinstance(_val, list):
                outData[_key] = self._handle_list(_val)

            elif isinstance(_val, dict):
                outData[_key] = self._handle_dict(_val)

            else:
                outData[_key] = _val

        return outData

    def _handle_list(self, obj):

        outData = list()
        for _entry in obj:

            if isinstance(_entry, list):
                outData.append(self._handle_list(_entry))

            elif isinstance(_entry, dict):
                outData.append(self._handle_dict(_entry))

            else:
                outData.append(_entry)

        return outData

    def _obj_to_dict(self, obj):

        outDict = dict()

        # serialize fields
        for _field in self.fieldNames[obj.__tablename__]:
            val = getattr(obj, _field, None)

            if isinstance(val, list):
                outDict[_field] = self._handle_list(val)

            elif isinstance(val, dict):
                # process each dict entry
                outDict[_field] = self._handle_dict(val)

            else:
                outDict[_field] = val

        # serialize nested objects (relationships)
        for _rel in sqlalchemy.inspect(obj).mapper.relationships:
            relVal = getattr(obj, _rel.key)
            if relVal:
                outDict[_rel.key] = self._obj_to_dict(relVal)
            else:
                outDict[_rel.key] = relVal

        return outDict

    def save(self, obj):

        if isinstance(obj, list):
            if len(obj) == 0:
                return list()
            else:
                return [self._obj_to_dict(x) for x in obj]

        elif self._check_if_mapped_class(obj):
            return self._obj_to_dict(obj)

        else:
            raise ValueError("obj must be either a mapped data model object.")

    def load(self, data, targetClass):

        if not isinstance(data, dict) and not isinstance(data, list):
            raise ValueError("data must be a dict or a list.")

        if not self._check_if_mapped_class(targetClass):
            raise ValueError("targetClass must be an instance of a mapped data model class.")

        if isinstance(data, dict):
            toProcess = [data]
        else:
            toProcess = data

        # process all data entries
        #
        loaded = list()
        for _entry in toProcess:

            # handle nested data
            importedObj = dict(_entry)

            for _rel in sqlalchemy.inspect(targetClass).mapper.relationships:
                if importedObj[_rel.key]:
                    importedObj[_rel.key] = self.load(importedObj[_rel.key], _rel.entity.class_)
                    importedObj[_rel.key + '_id'] = importedObj[_rel.key]._id

            # initialize all fields into a dict with null values, and
            # update new values from data
            newObj = targetClass(**importedObj)

            loaded.append(newObj)

        if len(loaded) == 1:
            return loaded[0]
        else:
            return loaded

class JsonSerializer(SerializerBase):
    '''Load/save objects from JSON string.'''

    def _encoder(self, obj):
        '''Function to encode non-default'''

        if isinstance(obj, uuid.UUID):
            # UUID serialization as hex string
            return str(obj)

        elif isinstance(obj, datetime.date) or isinstance(obj, datetime.datetime) or isinstance(obj, datetime.time):
            # date, datetime, time serialization in ISO 8601 format
            return obj.isoformat()

        elif isinstance(obj, datetime.timedelta):
            # timedelta serialization to seconds
            return obj.total_seconds()

        else:
            defaultEncoder = json.JSONEncoder()
            return defaultEncoder.default(obj)

    def _decoder(self, dct):
        '''Function to decode non-default json data.'''

        for _field,_type in self.fieldTypes[self.tablename].items():

            val = dct.get(_field, None)

            # don't try to convert a null field
            if val is not None:

                if _type is uuid.UUID:
                    dct[_field] = uuid.UUID(val)

                elif _type is datetime.date:
                    dct[_field] = datetime.date.fromisoformat(val)

                elif _type is datetime.datetime:
                    dct[_field] = datetime.datetime.fromisoformat(val)

                elif _type is datetime.time:
                    dct[_field] = datetime.time.fromisoformat(val)

                elif _type is datetime.timedelta:
                    dct[_field] = datetime.timedelta(seconds=int(val))

                elif _type is dict:
                    # if field is nested object, recurse
                    dct[_field] = self._decoder(val)

        return dct

    def save(self, obj):

        dictSerial = DictSerializer()
        return json.dumps(dictSerial.save(obj), default=self._encoder)

    def load(self, data, targetClass):

        if not isinstance(data, str):
            raise ValueError("data must be a string.")

        #if not issubclass(targetClass, IDataModel):
        #    raise ValueError("targetClass must be an instance of IDataModel.")

        self.tablename = targetClass.__tablename__
        dictSerial = DictSerializer()
        loaded = json.loads(data, object_hook=self._decoder)
        self.tablename = None

        return dictSerial.load(loaded, targetClass)

class JsonLinesSerializer(JsonSerializer):
    '''
        Load/save objects from JSON lines string. A JSON lines string
        is appendable and is delimited by line endings between records instead of
        the top level being {} or [] container.

        save() outputs either a single object line, or a string of objects separated
        by the a line ending character.

        the input data for load() is a single line of a JSON lines file, or multiple lines
        delimited by the line separater and returns a single object or a list of objects.
    '''

    def save(self, obj):

        if isinstance(obj, list):
            toProcess = obj
        else:
            toProcess = list()
            toProcess.append(obj)

        return os.linesep.join([super().save(x) for x in toProcess])

    def load(self, data, targetClass):
        lines = data.splitlines()
        loaded = [super().load(x, targetClass) for x in lines]

        if len(loaded) == 0:
            return None
        elif len(loaded) == 1:
            return loaded[0]
        else:
            return loaded

class PandasSerializer(SerializerBase):
    '''
    Save objects to pandas dataframe. This is not intended to be able to
    load objects back, and that function is not implemented.

    Assumes dataframe columns with a . are flattened from nested objects.

    If a field is a dictionary or a list, those are not included in the save() output.
    '''

    def _dtype_conversion(self, pythonType):
        '''Get the pandas string representation for pandas dtype.'''

        if pythonType is int:
            return 'Int64'

        elif pythonType is float:
            return 'float64'

        elif pythonType is bool:
            return 'boolean'

        elif pythonType is str:
            return 'string'

        elif pythonType is datetime.date:
            return 'datetime64[ns]'

        elif pythonType is datetime.datetime:
            return 'datetime64[ns]'

        elif pythonType is datetime.timedelta:
            return 'timedelta64[ns]'

        elif pythonType is uuid.UUID:
            return 'string'

        else:
            return 'object'

    def _build_dtype_map(self, obj, prefix=''):
        '''Build a column dtype mapping dict.'''

        dtypeMap = dict()

        for _field,_type in self.fieldTypes[obj.__tablename__].items():

            if self._check_if_mapped_class(_type):
                dtypeMap.update(
                    self._build_dtype_map(getattr(obj, _field), prefix=_field)
                )

            elif _type is dict:
                # do not include
                pass
            elif _type is list:
                # do not include
                pass
            else:
                if prefix != '':
                    fieldname = prefix + '.' + _field
                else:
                    fieldname = _field

                dtypeMap[fieldname] = self._dtype_conversion(_type)

        return dtypeMap

    def save(self, obj):

        dictSerial = DictSerializer()
        data = pd.json_normalize(dictSerial.save(obj))

        dtypeMap = None
        if isinstance(obj, list):
            dtypeMap = self._build_dtype_map(obj[0])

        if self._check_if_mapped_class(obj):
            dtypeMap = self._build_dtype_map(obj)

        # only include columns that are convertible
        commonCols = data.columns.intersection(dtypeMap.keys())

        # cast columns to the correct data type
        return data.loc[:, commonCols].astype(dtypeMap)

    def load(self, data, targetClass):

        raise NotImplementedError("Loading from Pandas is not implemented.")

class DbSerializer(SerializerBase):
    '''Base class for connecting to database using SQLAlchemy.'''

    def __init__(self, engine):
        super().__init__()

        self.engine = engine
        self.session = sqlalchemy.orm.Session(self.engine)

    def save(self, obj):

        if isinstance(obj, list):
            for _entry in obj:
                self.session.add(_entry)
        else:
            self.session.add(obj)

        self.session.commit()

    def load(self, data, targetClass):

        # data is an id or list of IDs for database backends

        if not isinstance(data, list):
            data = [data]

        stmnt = sqlalchemy.select(targetClass).where(targetClass._id.in_(data))
        result = self.session.execute(stmnt)
        allResults = result.scalars().all()
        if len(allResults) == 1:
            return allResults[0]
        else:
            return allResults

    def close(self):
        '''Close and clean up session and connection.'''
        self.session.close()
        self.engine.dispose()

class PostgresSerializer(DbSerializer):
    '''
        Postgres database serializer. Connection parameters can also be
        specified via environment variables.
        Constructor parameters take precendence over enivronment variables.

        When loading objects, the data input is an id or list of IDs for record.
        If not given, all records in the table will be retrieved.

        +-----------------------------------+
        | Environment Variable              |
        +===================================+
        | SPORTS_DATA_POSTGRES_HOST         |
        +-----------------------------------+
        | SPORTS_DATA_POSTGRES_PORT         |
        +-----------------------------------+
        | SPORTS_DATA_POSTGRES_USERNAME     |
        +-----------------------------------+
        | SPORTS_DATA_POSTGRES_PASSWORD     |
        +-----------------------------------+
        | SPORTS_DATA_POSTGRES_DBNAME       |
        +-----------------------------------+

    '''

    def __init__(self, host=None, port=None, username=None, password=None, dbName=None):

        # don't store password in memory
        self.host = host
        self.port = port
        self.username = username
        self.dbName = dbName

        if not self.host:
            self.host = os.environ.get('SPORTS_DATA_POSTGRES_HOST')

        if not self.port:
            self.port = os.environ.get('SPORTS_DATA_POSTGRES_PORT')

        if not self.username:
            self.username = os.environ.get('SPORTS_DATA_POSTGRES_USERNAME')

        if not password:
            password = os.environ.get('SPORTS_DATA_POSTGRES_PASSWORD')

        if not self.dbName:
            self.dbName = os.environ.get('SPORTS_DATA_POSTGRES_DBNAME')


        connectionString = 'postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(
            self.username,
            password,
            self.host,
            self.port,
            self.dbName
        )

        super().__init__(sqlalchemy.create_engine(connectionString))

class SqliteSerializer(DbSerializer):
    '''
        SQLite database serializer. Connection parameters can also be
        specified via environment variables.
        Constructor parameters take precendence over enivronment variables.

        When loading objects, the data input is an id or list of IDs for record.
        If not given, all records in the table will be retrieved.

        +-----------------------------------+
        | Environment Variable              |
        +===================================+
        | SPORTS_DATA_SQLITE_PATH           |
        +-----------------------------------+

    '''

    def __init__(self, dbPath=None, inMemory=False):

        self.dbPath = dbPath
        self.inMemory = inMemory


        if not self.dbPath:
            self.dbPath = os.environ.get('SPORTS_DATA_SQLITE_PATH')

        if self.inMemory:
            connectionString = 'sqlite://'
        else:
            connectionString = 'sqlite:///{0}'.format(
                self.dbPath
            )

        super().__init__(sqlalchemy.create_engine(connectionString))

