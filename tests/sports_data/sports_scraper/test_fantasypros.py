'''
    Define the following environment variables to allow login to the fantasypros scraper.
        SPORTS_DATA_FANTASYPROS_USERNAME
        SPORTS_DATA_FANTASYPROS_PASSWORD
'''

import pytest
from sports_data.data_model.schema import MlbProjection, NflProjection
from sports_data.sports_scraper.fantasypros import FantasyprosScraper


class TestFantasyprosScraper:
    '''Tests for FantasyprosScraper class.'''

    @pytest.mark.scraper
    def test_parse_mlb_projections(self):
        '''test parse_mlb_projections() method.'''

        scraper = FantasyprosScraper('season')
        _session = scraper.login()
        resp = _session.send(scraper.build_mlb_projections_request('hitters', minMax=True).prepare())

        projs = list(scraper.parse_mlb_projections(resp))

        assert len(projs) > 0
        assert type(projs[0]) is MlbProjection
        assert projs[0].custom_data['position'] == 'hitters'

    @pytest.mark.scraper
    def test_parse_nfl_projections(self):
        '''test parse_nfl_projections() method.'''

        scraper = FantasyprosScraper('season')
        _session = scraper.login()
        resp = _session.send(scraper.build_nfl_projections_requests('qb', minMax=True).prepare())

        projs = list(scraper.parse_nfl_projections(resp))

        assert len(projs) > 0
        assert type(projs[0]) is NflProjection
        assert projs[0].custom_data['position'] == 'qb'

