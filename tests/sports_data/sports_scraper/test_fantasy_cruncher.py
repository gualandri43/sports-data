
import pytest
import requests

from sports_data.sports_scraper.fantasy_cruncher import FantasyCruncherScraper


@pytest.mark.scraper
def test_parse_dfs_contests():

    scraper = FantasyCruncherScraper()
    session = requests.Session()

    for _req in scraper.build_dfs_contests_requests('nfl', '2021-week-10', ['draftkings','fanduel']):
        resp = session.send(_req.prepare())

    contests = list(scraper.parse_dfs_contests_response_nfl(resp))

    assert len(contests) > 0
    assert contests[0].source_site == 'fantasy_cruncher'
    assert contests[1].contest_id is not None

