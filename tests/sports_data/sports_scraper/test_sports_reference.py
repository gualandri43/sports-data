'''Test sports_reference module.'''

import pytest
import requests
import datetime

from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright

from sports_data.data_model.schema import MlbPlayerGamelog, NflPlayerGamelog, NflRoster
from sports_data.sports_scraper.sports_reference import BaseballReferenceScraper, ProFootballReferenceScraper


class TestBaseballReferenceScraper:
    '''Tests for BaseballReferenceScraper class.'''

    @pytest.mark.scraper
    def test_parse_player_game_logs_batting(self):
        '''test parse_player_game_logs() method.'''

        scraper = BaseballReferenceScraper()
        s = requests.Session()

        req = scraper.build_player_gamelog_request('abreujo02', 2019 , 'batting')
        resp = s.send(req.prepare())
        gl = list(scraper.parse_player_game_logs(resp))

        assert isinstance(gl[0], MlbPlayerGamelog)
        assert len(gl) == 159
        assert gl[18].game_date == datetime.date(2019, 4, 19)
        assert gl[3].season_year == 2019
        assert gl[82].baseball_reference_game_key == 'CHA201907032'
        assert gl[1].pitching_stats is None
        assert gl[2].batting_stats is not None

    @pytest.mark.scraper
    def test_parse_player_game_logs_pitching(self):
        '''test parse_player_game_logs() method.'''

        scraper = BaseballReferenceScraper()
        s = requests.Session()

        req = scraper.build_player_gamelog_request('adamja01', 2019 , 'pitching')
        resp = s.send(req.prepare())
        gl = list(scraper.parse_player_game_logs(resp))

        assert isinstance(gl[0], MlbPlayerGamelog)
        assert len(gl) == 23
        assert gl[16].game_date == datetime.date(2019, 9, 14)
        assert gl[2].season_year == 2019
        assert gl[4].baseball_reference_game_key == 'TOR201908100'
        assert gl[1].batting_stats is None
        assert gl[2].pitching_stats is not None

    @pytest.mark.scraper
    def test_parse_season_stats_page_batting(self):
        '''test the parse_season_stats_page() method.'''

        with sync_playwright() as p:

            scraper = BaseballReferenceScraper()
            req = scraper.build_season_stat_request(2019, 'batting')

            browser = p.chromium.launch()
            page = browser.new_page()
            page.goto(req.prepare().url, timeout=300000.0)

            stats = list(scraper.parse_season_stats_page(page.content()))

            assert len(stats) == 1554
            assert stats[0].season_year == 2019
            assert stats[1].batting_stats.rbi == pytest.approx(123)

            browser.close()

    @pytest.mark.scraper
    def test_parse_season_stats_page_pitching(self):
        '''test the parse_season_stats_page() method.'''

        with sync_playwright() as p:

            scraper = BaseballReferenceScraper()
            req = scraper.build_season_stat_request(2019, 'pitching')

            browser = p.chromium.launch()
            page = browser.new_page()
            page.goto(req.prepare().url, timeout=300000.0)

            stats = list(scraper.parse_season_stats_page(page.content()))

            assert len(stats) == 1035
            assert stats[0].season_year == 2019
            assert stats[0].pitching_stats.era == pytest.approx(4.15)

            browser.close()

    @pytest.mark.scraper
    def test_parse_team_stats_page(self):

        with sync_playwright() as p:

            scraper = BaseballReferenceScraper()
            req = scraper.build_team_page_request('CHW', 2018)

            browser = p.chromium.launch()
            page = browser.new_page()
            page.goto(req.prepare().url, timeout=300000.0)

            stats = scraper.parse_team_stats_page(page.content())

            assert isinstance(stats, dict)
            assert len(stats) == 7

            for _tbl in stats:
                assert len(stats[_tbl]) > 0

            browser.close()


class TestProFootballReferenceScraper:
    '''Tests for ProFootballReferenceScraper class.'''

    @pytest.mark.scraper
    def test_parse_parse_team_roster_response(self):
        '''test parse_team_roster_response() method.'''

        scraper = ProFootballReferenceScraper()

        with sync_playwright() as p:

            req = scraper.build_team_roster_request('CHI', 2020)

            browser = p.chromium.launch()
            page = browser.new_page()
            page.goto(req.prepare().url, timeout=300000.0)

            pg = BeautifulSoup(page.content())
            players = list(scraper.parse_team_roster_response(pg))

            assert isinstance(players[0], NflRoster)
            assert len(players) == 64
            assert players[-1].pro_football_reference_team_key == 'CHI'
            assert players[1].season_year == 2020
            assert players[2].source_site == 'pro_football_reference'

            browser.close()

    @pytest.mark.scraper
    def test_parse_player_game_logs(self):
        '''test parse_player_game_logs() method.'''

        scraper = ProFootballReferenceScraper()
        s = requests.Session()

        req = scraper.build_player_gamelog_request('CousKi00', 2020)
        resp = s.send(req.prepare())
        gl = list(scraper.parse_player_game_logs(resp))

        assert isinstance(gl[0], NflPlayerGamelog)
        assert len(gl) == 16
        assert gl[0].offense_stats is not None

