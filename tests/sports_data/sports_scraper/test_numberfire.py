'''Tests for the numberfire scraper.'''

from datetime import datetime
import pytest
import requests
from sports_data.sports_scraper.numberfire import NumberfireScraper

class TestNumberfireScraper:

    @pytest.mark.scraper
    @pytest.mark.parametrize("position", ['batters','pitchers'])
    def test_build_mlb_projections_requests(self,position):

        scraper = NumberfireScraper('season')

        req = scraper.build_mlb_projections_requests(position)
        assert isinstance(req, requests.Request)

    @pytest.mark.scraper
    @pytest.mark.parametrize("position", ['qb','rb','wr','te','k','d'])
    def test_build_nfl_projections_requests(self, position):

        rosScraper = NumberfireScraper('restOfSeason')

        req = rosScraper.build_nfl_projections_requests(position)
        assert isinstance(req, requests.Request)

        weeklyScraper = NumberfireScraper('weekly')

        req = weeklyScraper.build_nfl_projections_requests(position)
        assert isinstance(req, requests.Request)

    @pytest.mark.scraper
    @pytest.mark.parametrize("position", ['batters','pitchers'])
    def test_parse_mlb_projections(self, position):

        scraper = NumberfireScraper('season')

        s = requests.Session()
        req = scraper.build_mlb_projections_requests(position)
        resp = s.send(req.prepare())
        projs = list(scraper.parse_mlb_projections(resp, position))

        assert len(projs) > 0
        assert (projs[0].batting_stats is not None or projs[0].pitching_stats is not None)
        assert projs[0].scope == 'season'
        assert projs[0].scrape_time_local.day == datetime.now().day
        assert projs[0].source_site == 'numberfire'

    @pytest.mark.scraper
    @pytest.mark.parametrize("position", ['qb','rb','wr','te','k','d'])
    def test_parse_nfl_projections(self, position):

        scraper = NumberfireScraper('restOfSeason')

        s = requests.Session()
        req = scraper.build_nfl_projections_requests(position)
        resp = s.send(req.prepare())

        if position in ['k','d']:
            with pytest.raises(NotImplementedError):
                projs = list(scraper.parse_nfl_projections(resp, position))

        else:
            projs = list(scraper.parse_nfl_projections(resp, position))

            assert len(projs) > 0
            assert (projs[0].offense_stats is not None)
            assert projs[0].scope == 'restOfSeason'
            assert projs[0].scrape_time_local.day == datetime.now().day
            assert projs[0].source_site == 'numberfire'