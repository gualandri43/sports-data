'''Tests for the Rotoguru scraper.'''

import pytest
import requests
from bs4 import BeautifulSoup
from sports_data.sports_scraper.rotoguru import RotoGuruScraper

class TestRotoGuruScraper:

    @pytest.mark.scraper
    @pytest.mark.parametrize("site", ['draftkings', 'fanduel', 'yahoo'])
    def test_parse_dfs_salary_response_nfl(self, site):

        scraper = RotoGuruScraper()

        s = requests.Session()
        req = scraper.build_dfs_salary_request_nfl(site, 2020, 8)
        resp = s.send(req.prepare())
        data = list(scraper.parse_dfs_salary_response_nfl(BeautifulSoup(resp.text, 'lxml')))

        assert len(data) > 0
        assert data[0].source_site == 'rotoguru'

    @pytest.mark.scraper
    def test_parse_player_ids_from_lookup_page_mlb(self):

        scraper = RotoGuruScraper()
        req = scraper.build_player_lookup_page_request_mlb(2021)
        resp = requests.Session().send(req.prepare())

        data = list(scraper.parse_player_ids_from_lookup_page(BeautifulSoup(resp.text, 'lxml'), 'mlb'))

        assert len(data) > 0

    @pytest.mark.scraper
    def test_parse_player_ids_from_lookup_page_nfl(self):

        scraper = RotoGuruScraper()
        req = scraper.build_player_lookup_page_request_nfl(2021)
        resp = requests.Session().send(req.prepare())

        data = list(scraper.parse_player_ids_from_lookup_page(BeautifulSoup(resp.text, 'lxml'), 'nfl'))

        assert len(data) > 0

    @pytest.mark.parametrize("player", ['112nx', '3352'])
    def test_parse_dfs_player_salary_history_mlb(self, player):

        scraper = RotoGuruScraper()

        s = requests.Session()
        req = scraper.build_dfs_player_salary_history_mlb(2020, player)
        resp = s.send(req.prepare())
        data = list(scraper.parse_dfs_player_salary_history_mlb(BeautifulSoup(resp.text, 'lxml')))

        assert len(data) > 0
        assert data[0].source_site == 'rotoguru'

    @pytest.mark.scraper
    def test_parse_dfs_player_salary_history_nfl(self):

        scraper = RotoGuruScraper()

        s = requests.Session()
        req = scraper.build_dfs_player_salary_history_nfl(2020, '5830')
        resp = s.send(req.prepare())
        data = list(scraper.parse_dfs_player_salary_history_nfl(BeautifulSoup(resp.text, 'lxml')))

        assert len(data) > 0
        assert data[0].source_site == 'rotoguru'
