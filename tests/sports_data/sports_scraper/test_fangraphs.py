'''Test fangraphs module.'''


import pytest
import requests
from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright

from sports_data.data_model.schema import MlbPlayerGamelog, MlbPlayerSeason, MlbProjection, MlbFangraphsGuts
from sports_data.sports_scraper.fangraphs import FangraphsScraperGuts, FangraphsScraperProjections, FangraphsScraperHistorical

@pytest.mark.scraper
@pytest.mark.parametrize("position", ['batter','pitcher'])
def test_parse_mlb_projections_csv(position):

        scraper = FangraphsScraperProjections('steamer','season')
        session = requests.Session()

        projReq = scraper.build_mlb_projections_download_request(position)
        resp = session.send(projReq.prepare())
        csvReq = scraper.build_csv_download_request(resp)
        respCsv = session.send(csvReq.prepare())

        projs = list(scraper.parse_mlb_projections_csv(resp, respCsv, position))

        assert len(projs) > 0
        assert isinstance(projs[0], MlbProjection)

        if position == 'batter':
            assert projs[0].batting_stats is not None

        if position == 'pitcher':
            assert projs[0].pitching_stats is not None

@pytest.mark.scraper
def test_parse_mlb_player_season_stats():
    '''test parse_mlb_player_season_stats() method.'''

    with sync_playwright() as p:

        scraper = FangraphsScraperHistorical()
        req = scraper.build_mlb_player_season_stat_request(10155)

        browser = p.chromium.launch()
        page = browser.new_page()
        page.goto(req.prepare().url, timeout=300000.0)

        soup = BeautifulSoup(page.content())
        stats = list(scraper.parse_mlb_player_season_stats(soup, 10155))

        assert isinstance(stats[0], MlbPlayerSeason)
        assert len(stats) > 0

        browser.close()

@pytest.mark.scraper
def test_mlb_player_gamelog_stat_request():
    '''test mlb_player_gamelog_stat_request() method.'''

    scraper = FangraphsScraperHistorical()
    session = requests.Session()
    playerPgReq = scraper.build_mlb_player_season_stat_request(10155)
    playerPgResp = session.send(playerPgReq.prepare())
    req = scraper.build_mlb_player_gamelog_stat_request(10155, 2020, playerPgResp)
    resp = session.send(req.prepare())

    stats = list(scraper.parse_mlb_player_gamelog_stats(resp.text, 'batter'))

    assert len(stats) > 0
    assert isinstance(stats[0], MlbPlayerGamelog)
    assert stats[0].batting_stats is not None

@pytest.mark.scraper
def test_guts_woba_fip():

    scraper = FangraphsScraperGuts()
    session = requests.Session()
    req = scraper.build_mlb_woba_fip_constants_request()
    resp = session.send(req.prepare())
    csvReq = scraper.build_csv_download_request(resp)
    csvResp = session.send(csvReq.prepare())

    guts = list(scraper.parse_mlb_woba_fip_constants(csvResp))

    assert len(guts) > 0
    assert isinstance(guts[0], MlbFangraphsGuts)
