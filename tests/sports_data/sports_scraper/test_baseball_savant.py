
import pytest
import requests
from sports_data.data_model.schema import MlbStatcast
from sports_data.sports_scraper.baseball_savant import BaseballSavantScraper,StatcastSearchRequest

class TestBaseballSavantScraper:
    '''Tests for BaseballSavantScraper class.'''

    @pytest.mark.scraper
    def test_parse_statcast_request(self):
        '''test parse_statcast_request() method.'''

        req = StatcastSearchRequest(hfSea=[2019], player_type='pitcher', pitchers_lookup=608337)
        resp = requests.get(url=req.build_url())
        scraper = BaseballSavantScraper()
        recs = list(scraper.parse_statcast_response(resp))

        assert len(recs) > 0
        assert type(recs[0]) is MlbStatcast

