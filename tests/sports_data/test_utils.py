'''Test basic utility functions or classes.'''


import datetime
import time
import copy

from sports_data.utils import TimeRecorder


class TestTimeRecorder:
    '''Tests for TimeRecorder class.'''

    def test_record(self):
        '''test record method.'''

        rec1 = TimeRecorder()
        rec2 = copy.deepcopy(rec1)
        time.sleep(0.5)
        rec1.record()

        assert rec1.timeLocal > rec2.timeLocal
        assert rec1.timeUtc > rec2.timeUtc

    def test_seconds_since_last_record(self):
        '''test seconds_since_last_record method'''

        rec = TimeRecorder()

        time.sleep(1.1)
        assert rec.seconds_since_last_record() > 1
        assert rec.seconds_since_last_record() < 2

        time.sleep(1.1)
        assert rec.seconds_since_last_record() > 2
        assert rec.seconds_since_last_record() < 3

    def test_time_since_last_record(self):
        '''test the time_since_last_record method'''

        rec = TimeRecorder()

        time.sleep(2.1)
        assert rec.time_since_last_record().seconds > 1
        assert rec.time_since_last_record().seconds < 3

        time.sleep(2.1)
        assert rec.time_since_last_record().seconds > 3
        assert rec.time_since_last_record().seconds < 5

        assert isinstance(rec.time_since_last_record(), datetime.timedelta)

    def test_to_dict(self):
        '''test the to_dict method'''

        rec = TimeRecorder()
        recDict = rec.to_dict()

        assert 'timeUtc' in recDict
        assert 'timeLocal' in recDict

    def test_add_to_dict(self):
        '''test the add_to_dict method'''

        rec = TimeRecorder()
        recDict = {'v1': 'k1'}
        rec.add_to_dict(recDict)

        assert 'timeUtc' in recDict
        assert 'timeLocal' in recDict
        assert 'v1' in recDict
