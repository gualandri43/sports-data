
import pytest
import pandas as pd

from tests.sports_data.data_model.prepare import create_package_data_model_objects, create_with_random_data
from sports_data.data_model.schema import NflOffense, MlbBatting, MlbPitching
import sports_data.data_model.serialization as serialization


@pytest.mark.parametrize("model", create_package_data_model_objects())
def test_dict(model):
    '''test serializing and deserializing to dictionary.'''

    serializer = serialization.DictSerializer()

    serialized = serializer.save(model)
    deserialized = serializer.load(serialized, type(model))

    assert deserialized == model

def test_dict_collection():
    '''test multiple object serializing and deserializing to dictionary.'''

    serializer = serialization.DictSerializer()

    models = [
        create_with_random_data(NflOffense),
        create_with_random_data(NflOffense),
        create_with_random_data(NflOffense)
    ]

    serialized = serializer.save(models)
    deserialized = serializer.load(serialized, NflOffense)
    assert deserialized == models

@pytest.mark.parametrize("model", create_package_data_model_objects())
def test_json(model):
    '''test serializing and deserializing to json.'''

    serializer = serialization.JsonSerializer()

    serialized = serializer.save(model)
    deserialized = serializer.load(serialized, type(model))

    assert deserialized == model

def test_json_collection():
    '''test multiple object serializing and deserializing to json.'''

    serializer = serialization.JsonSerializer()

    models = [
        create_with_random_data(MlbBatting),
        create_with_random_data(MlbBatting),
        create_with_random_data(MlbBatting)
    ]

    serialized = serializer.save(models)
    deserialized = serializer.load(serialized, MlbBatting)
    assert deserialized == models

def test_pandas():
    '''test serializing to pandas.'''

    serializer = serialization.PandasSerializer()

    models = [
        create_with_random_data(MlbPitching),
        create_with_random_data(MlbPitching),
        create_with_random_data(MlbPitching)
    ]

    serialized = serializer.save(models)

    assert isinstance(serialized, pd.DataFrame)
    assert len(serialized) == 3

