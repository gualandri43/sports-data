'''
    Routines to prepare or execute common testing actions
'''


import random
import string
import datetime
import uuid
import sports_data.data_model.schema as schema


def create_with_random_data(targetClass, exclude=[]):
    '''Fill a schema object with random data.'''

    obj = targetClass()

    tbl = schema.MAPPER_REGISTRY.metadata.tables[obj.__tablename__]
    fieldTypes = {c.name:c.type.python_type for c in tbl.columns}

    for _field,_type in fieldTypes.items():

        if _field not in exclude:
            if _type is int:
                setattr(obj, _field, random.randint(1, 1000000))

            if _type is float:
                setattr(obj, _field, round(random.random(), 3))

            if _type is datetime.date:
                setattr(obj, _field, datetime.date.today())

            if _type is datetime.datetime:
                setattr(obj, _field, datetime.datetime.now())

            if _type is str:
                setattr(obj, _field, ''.join(random.choice(string.ascii_letters) for i in range(10)))

            if _type is uuid.UUID:
                setattr(obj, _field, uuid.uuid4())

    return obj

def create_package_data_model_objects():
    '''Create a list of data_model objects to test defined schemas.'''

    # mlb

    mlbBatting = create_with_random_data(schema.MlbBatting)
    mlbBatting.custom_data = {'c1':'d1'}

    mlbPitching = create_with_random_data(schema.MlbPitching)

    mlbProjection = create_with_random_data(schema.MlbProjection)
    mlbProjection.source_site = 'site1'
    mlbProjection.pitching_stats = create_with_random_data(schema.MlbPitching)
    mlbProjection.batting_stats = create_with_random_data(schema.MlbBatting)
    mlbProjection.pitching_stats_id = mlbProjection.pitching_stats._id
    mlbProjection.batting_stats_id = mlbProjection.batting_stats._id

    mlbGamelog = create_with_random_data(schema.MlbPlayerGamelog)
    mlbGamelog.source_site = 'site1'
    mlbGamelog.pitching_stats = create_with_random_data(schema.MlbPitching)
    mlbGamelog.batting_stats = create_with_random_data(schema.MlbBatting)
    mlbGamelog.pitching_stats_id = mlbGamelog.pitching_stats._id
    mlbGamelog.batting_stats_id = mlbGamelog.batting_stats._id

    mlbSeason = schema.MlbPlayerSeason(
        _id = 12,
        season_year = 2019,
        pitching_stats = create_with_random_data(schema.MlbPitching),
        source_site = 'fangraphs'
    )
    mlbSeason.source_site = 'site1'
    mlbSeason.pitching_stats_id = mlbSeason.pitching_stats._id

    mlbStatcast = create_with_random_data(schema.MlbStatcast)

    #nfl

    nflOffense = create_with_random_data(schema.NflOffense)
    nflOffense.custom_data = {'k1':'v1'}

    nflDefense = create_with_random_data(schema.NflDefense)
    nflDefense.custom_data = {'k2':'v2'}

    nflKicking = create_with_random_data(schema.NflKicking)
    nflKicking.custom_data = {'k3':'v3'}

    nflTeamDvoa = create_with_random_data(schema.NflTeamDvoa)
    nflTeamDvoa.custom_data = {'k4':'v4'}

    nflProjection = create_with_random_data(schema.NflProjection)
    nflProjection.custom_data = {'k5':'v5'}
    nflProjection.source_site = 'site1'
    nflProjection.offense_stats = create_with_random_data(schema.NflOffense)
    nflProjection.offense_stats_id = nflProjection.offense_stats._id
    nflProjection.defense_stats = create_with_random_data(schema.NflDefense)
    nflProjection.defense_stats_id = nflProjection.defense_stats._id
    nflProjection.kicking_stats = create_with_random_data(schema.NflKicking)
    nflProjection.kicking_stats_id = nflProjection.kicking_stats._id

    nflGame = create_with_random_data(schema.NflGame)
    nflGame.source_site = 'site1'

    return [
        mlbBatting,
        mlbPitching,
        mlbProjection,
        mlbGamelog,
        mlbSeason,
        mlbStatcast,
        nflOffense,
        nflDefense,
        nflKicking,
        nflTeamDvoa,
        nflProjection,
        nflGame
    ]

def create_sites():
    '''make some source sites'''

    return [
        schema.Sites(site='site1',url='www.example.com')
    ]
