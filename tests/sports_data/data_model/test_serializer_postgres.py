'''
    Connect to a testing Postgres Database and run serialization testing.

    Define database for testing using env variables
        SPORTS_DATA_POSTGRES_HOST
        SPORTS_DATA_POSTGRES_PORT
        SPORTS_DATA_POSTGRES_USERNAME
        SPORTS_DATA_POSTGRES_PASSWORD
        SPORTS_DATA_POSTGRES_DBNAME
'''

import os

import pytest
import psycopg2

import sports_data.data_model.schema as schema
from sports_data.data_model.serialization import PostgresSerializer
from tests.sports_data.data_model.prepare import create_package_data_model_objects, create_with_random_data, create_sites


@pytest.fixture
def pg_serializer():
    '''
        Set up a fresh testing database for each test and connect a PostgresSerializer to it.
        Use the SPORTS_DATA_POSTGRES_DBNAME.

        Per documentation, setup code is done before a yield statement, and cleanup code is
        after. (https://docs.pytest.org/en/6.2.x/fixture.html)
    '''

    dbName = os.getenv('SPORTS_DATA_POSTGRES_DBNAME')

    con = psycopg2.connect(
        host=os.getenv('SPORTS_DATA_POSTGRES_HOST'),
        port=os.getenv('SPORTS_DATA_POSTGRES_PORT'),
        user=os.getenv('SPORTS_DATA_POSTGRES_USERNAME'),
        password=os.getenv('SPORTS_DATA_POSTGRES_PASSWORD')
    )

    con.autocommit = True
    with con.cursor() as curs:
        curs.execute('DROP DATABASE IF EXISTS ' + dbName + ';')
        curs.execute('CREATE DATABASE ' + dbName + ';')
    con.commit()
    con.close()


    # create serializer and database
    serializer = PostgresSerializer()

    # create tables in new database
    schema.create_tables(serializer.engine)

    # add the source sites
    for _site in create_sites():
        serializer.session.add(_site)

    serializer.session.commit()

    yield serializer

    # close connection database
    serializer.close()

@pytest.mark.parametrize("model", create_package_data_model_objects())
def test_roundtrip(pg_serializer, model):
    '''test serializing and deserializing to db.'''

    pg_serializer.save(model)
    deserialized = pg_serializer.load(model._id, type(model))
    assert deserialized == model

def test_assign_id(pg_serializer):
    '''test serializing and deserializing to db an empty Identifier.'''

    obj = schema.MlbPitching(
        strikeout=5.0,
        custom_data={'k1':'v1'}
    )

    pg_serializer.save(obj)
    deserialized = pg_serializer.load(obj._id, schema.MlbPitching)

    assert obj._id is not None
    assert obj == deserialized

def test_collection(pg_serializer):
    '''test multiple object serializing to db.'''

    dataCollection = [
        create_with_random_data(schema.NflOffense),
        create_with_random_data(schema.NflOffense),
        create_with_random_data(schema.NflOffense)
    ]

    dataCollection.sort(key=lambda x: x._id)

    pg_serializer.save(dataCollection)
    deserialized = pg_serializer.load([x._id for x in dataCollection], schema.NflOffense)
    deserialized.sort(key=lambda x: x._id)

    assert len(deserialized) == len(dataCollection)
    assert deserialized == dataCollection


