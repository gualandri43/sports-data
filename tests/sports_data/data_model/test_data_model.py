'''Test basic data_model stuff.'''


import uuid
import datetime
import pytest

import sports_data.data_model.schema as schema


class TestDataModelBase:
    '''Tests for DataModelBase class.'''

    def test_value_setting_normal(self):
        '''test setting attributes.'''


        obj = schema.MlbPlayerGamelog()
        obj2 = schema.MlbProjection()

        try:
            obj._id = 2
            obj.custom_data = {'c1':'v1'}
            obj.season_year = 2020
            obj.source_site = 'fangraphs'
            obj.game_date = datetime.date.today()
            obj2.scrape_time_local = datetime.datetime.now()
            assert True

        except:
            assert False

    def test_value_setting_casted(self):
        '''test implicit casting of attributes.'''

        currentTime = datetime.datetime.now()
        currentDate = datetime.date.today()

        obj = schema.MlbPlayerGamelog()
        obj2 = schema.MlbProjection()

        obj._id = '3'
        obj.season_year = '1999'
        obj.game_date = currentDate.isoformat()
        obj2.scrape_time_local = currentTime.isoformat()

        assert obj._id == 3
        assert obj.season_year == 1999
        assert obj2.scrape_time_local == currentTime
        assert obj.game_date == currentDate

    def test_value_setting_wrong_type(self):
        '''test failure of implicit casting of attributes.'''

        obj = schema.MlbPlayerGamelog()
        obj2 = schema.MlbProjection()

        with pytest.raises(Exception):
            obj2.scrape_time_local = 'asoinasb'

        with pytest.raises(Exception):
            obj.game_date = 6

    def test_value_setting_wrong_type_constructor(self):
        '''test failure of implicit casting of attributes during construction.'''

        currentTime = datetime.datetime.now()
        currentDate = datetime.date.today()

        with pytest.raises(Exception):
            obj1 = schema.MlbPlayerGamelog(
                game_date = 6
            )

        with pytest.raises(Exception):
            obj3 = schema.MlbPlayerGamelog(
                season_year = currentDate
            )

        with pytest.raises(Exception):
            obj4 = schema.MlbProjection(
                scrape_time_local = 'asoinasb'
            )

        with pytest.raises(Exception):
            obj5 = schema.MlbBatting(
                single = 'pop'
            )

    def test_equality(self):
        '''test equality operations.'''

        assert schema.MlbBatting(_id=3, homerun=2.0) == schema.MlbBatting(_id=3, homerun=2.0)

        assert schema.MlbBatting(_id=3, homerun=2.0) != schema.MlbBatting(_id=3, homerun=1.0)

        assert schema.MlbBatting(_id=3, homerun=2.0) != schema.MlbPitching(_id=3, homerun=2.0)

    def test_nulls(self):
        '''test setting attributes as nulls.'''

        obj = schema.MlbStatcast()

        obj.event_batter_side = ''
        obj.game_date = None
        obj.game_season = ''
        obj.pitch_az = '5.2'
        obj.event_balls_pre = '3'

        assert obj.event_batter_side is None
        assert obj.game_date is None
        assert obj.game_season is None
        assert obj.event_balls_pre == 3
        assert obj.pitch_az == 5.2
