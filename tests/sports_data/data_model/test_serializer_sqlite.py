'''
    Connect to a testing SQLite Database and run serialization testing.

'''

import pytest

import sports_data.data_model.schema as schema
from sports_data.data_model.serialization import SqliteSerializer
from tests.sports_data.data_model.prepare import create_package_data_model_objects, create_with_random_data, create_sites


@pytest.fixture
def sqlite_serializer():
    '''set up a fresh testing sqlite database for each test and connect a SqliteSerializer to it.'''

    # set up a fresh testing database for each test in memory
    # create tables in new database
    serializer = SqliteSerializer(inMemory=True)
    schema.create_tables(serializer.engine)

    # add the source sites
    for _site in create_sites():
        serializer.session.add(_site)

    return serializer

@pytest.mark.parametrize("model", create_package_data_model_objects())
def test_roundtrip(sqlite_serializer, model):
    '''test serializing and deserializing to db.'''

    sqlite_serializer.save(model)
    deserialized = sqlite_serializer.load(model._id, type(model))
    assert deserialized == model

def test_assign_id(sqlite_serializer):
    '''test serializing and deserializing to db an empty Identifier.'''

    obj = schema.MlbPitching(
        strikeout=5.0,
        custom_data={'k1':'v1'}
    )

    sqlite_serializer.save(obj)
    deserialized = sqlite_serializer.load(obj._id, schema.MlbPitching)

    assert obj._id is not None
    assert obj == deserialized

def test_collection(sqlite_serializer ):
    '''test multiple object serializing to db.'''

    dataCollection = [
        create_with_random_data(schema.NflOffense),
        create_with_random_data(schema.NflOffense),
        create_with_random_data(schema.NflOffense)
    ]

    dataCollection.sort(key=lambda x: x._id)

    sqlite_serializer.save(dataCollection)
    deserialized = sqlite_serializer.load([x._id for x in dataCollection], schema.NflOffense)
    deserialized.sort(key=lambda x: x._id)

    assert len(deserialized) == len(dataCollection)
    assert deserialized == dataCollection


