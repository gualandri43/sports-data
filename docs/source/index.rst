.. Sports Data documentation master file, created by
   sphinx-quickstart on Tue Nov  2 09:18:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Sports Data documentation!
=========================================

This package provides tooling for querying sports data from around internet, loading it in
to a unified data model, and storing it for later analysis.

The general usage is to use a scraper to retrieve data, which loads everything into the data schema
defined by the data model module. Those objects can then be stored to disk with one of the many
provided serializer options, and retrieved at a later time for analysis.

.. note::
   Some sites have licensing or restrictions on the use of their data, and users of this
   package should respect that where applicable. Please use the scrapers and download tools responsibly,
   and avoid hitting these sites with unnecessary traffic.

.. note::
   This package is currently under early development, and more data sources are in the works.

.. toctree::
   :maxdepth: 2
   :caption: Modules:

   data_model
   serialization
   scrapers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
