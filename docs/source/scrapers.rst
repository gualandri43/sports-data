Scrapers (Data Retrieval)
=========================

The major purpose of this package is to provide ways to download sports data from various internet
sources and load them into a common data schema for storage and/or later analysis. The data
retrieval is accomplished using one of the scraper objects in the sports_scraper module.
Each data source scraper has methods for building the right HTTP request, and then parsing the
response from the website when that request is sent. It is split up this way to allow for
more flexibility, since each data source has its own quirks and workflows needed. Some
HTTP requests can be executed using a simple GET request. Some require a logged in session.
Others require certain pages to be rendered using a javascript engine to populate the data on the
page. Special requirements like this are noted in each scraper as needed.

HTTPS requests can typically by handled by the excellent `requests` library, unless javascript is
needed. Then, an automation tool such as `playwright`, `selenium`, or `splash` must be used
to render the content. The request/response workflow can also be adapted to be used in a
`scrapy` spider.

Data scrapers are available for the following data sources:
    - :doc:`/scrapers/baseball_savant`
    - :doc:`/scrapers/fangraphs`
    - :doc:`/scrapers/fantasypros`
    - :doc:`/scrapers/numberfire`
    - :doc:`/scrapers/baseball_reference`
    - :doc:`/scrapers/pro_football_reference`
