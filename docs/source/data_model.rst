Data Model
==========

The classes that define the data model can be imported from `sports_data.data_model.schema`.
The various serializers and scraper methods return them directly.

All of the classes in the data model are `SQLAlchemy` classes with some added functionality.
The database serializers use that functionality under the hood, but the objects can be queried,
filtered, and worked with directly as any `SQLAlchemy` object can be outside of those serializers.

.. autoclass:: sports_data.data_model.schema.Sites
    :members:

.. autoclass:: sports_data.data_model.schema.NflOffense
    :members:

.. autoclass:: sports_data.data_model.schema.NflDefense
    :members:

.. autoclass:: sports_data.data_model.schema.NflKicking
    :members:

.. autoclass:: sports_data.data_model.schema.NflTeamDvoa
    :members:

.. autoclass:: sports_data.data_model.schema.NflProjection
    :members:

.. autoclass:: sports_data.data_model.schema.NflGame
    :members:

.. autoclass:: sports_data.data_model.schema.NflPlayerGamelog
    :members:

.. autoclass:: sports_data.data_model.schema.MlbBatting
    :members:

.. autoclass:: sports_data.data_model.schema.MlbPitching
    :members:

.. autoclass:: sports_data.data_model.schema.MlbStatcast
    :members:

.. autoclass:: sports_data.data_model.schema.MlbProjection
    :members:

.. autoclass:: sports_data.data_model.schema.MlbPlayerSeason
    :members:

.. autoclass:: sports_data.data_model.schema.MlbPlayerGamelog
    :members:

.. autoclass:: sports_data.data_model.schema.MlbFangraphsGuts
    :members:

