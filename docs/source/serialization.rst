Serialization
=============

A variety of serializers are provided for saving and loading data model object to different
storage types, formats, and backends. All serializers in this package implement the
`ISerializer` interface. To use, instantiate a serializer class, and use the save() and load()
methods to dump objects to disk, or to load them back in for later use.

.. code-block:: python

    from sports_data.data_model import JsonSerializer, MlbBatting

    data = MlbBatting(homerun=5.0)
    serializer = JsonSerializer()

    # save the MlbBatting object to a JSON string
    savedString = serializer.save(data)

    # load the data in that string back into a MlbBatting object
    loadedData = serializer.load(savedString, MlbBatting)


Serializers API
---------------

.. autoclass:: sports_data.data_model.serialization.ISerializer
    :members:

.. autoclass:: sports_data.data_model.serialization.DictSerializer

.. autoclass:: sports_data.data_model.serialization.JsonSerializer

.. autoclass:: sports_data.data_model.serialization.JsonLinesSerializer

.. autoclass:: sports_data.data_model.serialization.PandasSerializer

.. autoclass:: sports_data.data_model.serialization.PostgresSerializer

.. autoclass:: sports_data.data_model.serialization.SqliteSerializer
