Installation
============
Install this package with pip from the GitLab package registry.

.. code-block::

    pip install sports-data --extra-index-url https://gitlab.com/api/v4/projects/30526708/packages/pypi/simple

