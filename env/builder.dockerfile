#
# build an docker image to use for building/packaging/deploying
#

FROM python:3.8

#
# install poetry
#
RUN apt-get update && apt-get install -y curl && curl --insecure -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -

#
# prevent creation of virtual env (no need with docker container)
#
ENV PATH="/root/.local/bin:$PATH"
RUN poetry config virtualenvs.create false

#
# install sphinx documentation tools and theme
#
RUN pip install --upgrade sphinx && pip install pydata-sphinx-theme
