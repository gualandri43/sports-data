#
# build an docker image to use for building/packaging/deploying
#

# start from the Playwright base image to allow for scraper testing.
#
FROM mcr.microsoft.com/playwright:focal

#
# Install pytest for test running
#
RUN pip install pytest

#
# Install coverage for test coverage generation
#
RUN pip install coverage

#
# Install coverage for test coverage generation
#
RUN pip install pylint

#
# Playwright Python library
#
RUN pip install playwright && playwright install
