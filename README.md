# Background

This project was split out of a previous larger project to focus on the data schema and data modeling of sports data. Commit history was retained for the relevant files.

# Documentation

Read the package [documentation](https://gualandri43.gitlab.io/sports-data/).

# Development

## Documentation Style

This project uses the [Numpy docstring style](https://numpydoc.readthedocs.io/en/latest/format.html) to document the classes and functions in the library.
The documentation may be built using the `sphinx-build -b html source build` command inside the `docs/` folder.


## Environment

Poetry is recommended to use to setup an isolated development environment. This project has a
`pyproject.toml` file specifying required and development dependencies.

For non-development usage, the package can be installed from the public GitLab registry using pip, poetry, or
other package installers that can install PyPi packages.

## Tests

To spin up containers for testing in a dev environment, use docker compose. The environment variables are defined in the .env file
at the root of the repository.

```
# start test stack
docker-compose -f env/test_stack.yml --env-file .env up

# stop test stack
docker compose --file env/test_stack.yml down
```